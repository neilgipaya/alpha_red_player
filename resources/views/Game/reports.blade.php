@extends('layouts/contentLayoutMaster')
@section('page-styles')
@endsection
@section('breadcrumb')
    <div class="clearfix"></div>
@endsection
@section('content')
    <game-reports :game_session_id="{{$game_session_id}}"></game-reports>
@endsection
@section('vendor-scripts')
@endsection
