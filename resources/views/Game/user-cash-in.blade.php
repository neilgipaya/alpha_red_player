@extends('layouts/contentLayoutMaster')


@section('page-styles')

@endsection
@section('breadcrumb')
    <div class="clearfix"></div>
@endsection
@section('content')
    @if(auth()->user()->type == 'Player' || auth()->user()->type == 'Gold Agent' || auth()->user()->type == 'Silver Agent')
    <topup-player-history></topup-player-history>
    @else
        <div class="card-body">
            <ul class="widget-timeline ps ps--active-y">
                @foreach($topups as $topup)

                <li class="timeline-items timeline-icon-success active">
                    <p class="timeline-text" style="color: black"><strong>Transaction Code:</strong> {{ $topup->transaction_code }}</p>

                    <div class="timeline-time" style="color: black">{{ \Carbon\Carbon::parse($topup->created_at)->diffForHumans() }} <br /> ({{ \Carbon\Carbon::parse($topup->created_at)->format('F g, Y: h:i: a') }})</div>
                    <h6 class="timeline-title" style="color: black">Amount: {{ $topup->amount }}</h6>
                    <p class="timeline-text" style="color: black">Status: {{ $topup->status }}</p>
                    @if($topup->status === 'Declined')
                        <p class="timeline-text" style="color: black">Status: {{ $topup->decline_template->description ?? "" }}</p>
                    @endif
                </li>
                    <hr />
                @endforeach


                <div class="ps__rail-x" style="left: 0px; bottom: -375px;"><div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps__rail-y" style="top: 375px; height: 415px; right: 0px;"><div class="ps__thumb-y" tabindex="0" style="top: 197px; height: 218px;"></div></div></ul>
        </div>
    @endif
@endsection

@section('vendor-scripts')

@endsection
