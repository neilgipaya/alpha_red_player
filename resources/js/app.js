import Vue from 'vue'
import axios from "./axios.js"
import moment from 'moment'
import numeral from 'numeral'

import themeConfig from "./themeConfig";
import VueCoreVideoPlayer from 'vue-core-video-player'
import VueFriendlyIframe from 'vue-friendly-iframe';
import VueGoogleCharts from 'vue-google-charts'
import JsonCSV from 'vue-json-csv'
Vue.use(VueGoogleCharts)
Vue.prototype.$http = axios;

Vue.component('downloadCsv', JsonCSV);
Vue.component('my-cashout', require('./Component/Withdrawal/my-cashout').default);
Vue.component('banker', require('./Component/Configuration/banker').default);
Vue.component('my-cashout-agent', require('./Component/Withdrawal/my-cashout-agent').default);
Vue.component('deactivate-players', require('./Component/Users/deactivate-players').default);
Vue.component('dashboard', require('./Component/dashboard').default);
Vue.component('agent-dashboard',require('./Component/agent-dashboard').default);
Vue.component('view-agent-players', require('./Component/Users/view-agent-players').default);
Vue.component('view-agent-downline', require('./Component/Users/view-agent-downline').default);
Vue.component('view-agent-players-bet', require('./Component/Users/view-agent-players-bet').default);
Vue.component('users', require('./Component/Users/users').default);
Vue.component('play-stats', require('./Component/play-stats').default);
Vue.component('players', require('./Component/Users/players').default);
Vue.component('agents', require('./Component/Users/agents').default);
Vue.component('agent-players', require('./Component/Users/agent-players').default);
Vue.component('my-players', require('./Component/Users/my-players').default);
Vue.component('my-inactiveplayers', require('./Component/Users/my-inactiveplayers').default);
Vue.component('myd-agents', require('./Component/Users/myd-agents').default);
Vue.component('top-up', require('./Component/Topup/top-up').default);
Vue.component('banker-topup-approval', require('./Component/Topup/banker-topup-approval').default);
Vue.component('top-up-other', require('./Component/Topup/top-up-other').default);
Vue.component('top-up-recent', require('./Component/Topup/top-up-recent').default);
Vue.component('top-up-recent-agent', require('./Component/Topup/top-up-recent-agent').default);
Vue.component('top-up-list', require('./Component/Topup/top-up-list').default);
Vue.component('top-up-list-agent', require('./Component/Topup/top-up-list-agent').default);
Vue.component('top-up-personal', require('./Component/Topup/top-up-personal').default);
Vue.component('payment-method', require('./Component/Configuration/PaymentMethods/payment-methods').default);
Vue.component('bank-accounts', require('./Component/Configuration/Banks/bank-accounts').default);
Vue.component('bank-transactions', require('./Component/Configuration/Banks/bank-details').default);
Vue.component('decline-template', require('./Component/Configuration/Templates/decline-template').default);
Vue.component('roles', require('./Component/Configuration/Roles/roles').default);
Vue.component('funds-request', require('./Component/Funds/funds-request').default);
Vue.component('funds-pending', require('./Component/Funds/funds-pending').default);
Vue.component('funds-list', require('./Component/Funds/funds-list').default);
Vue.component('play-game', require('./Component/Game/play-game').default);
Vue.component('play-game-desktop', require('./Component/Game/play-game-desktop').default);
Vue.component('lobby-game', require('./Component/Game/lobby-game').default);
Vue.component('create-game', require('./Component/Game/create-game').default);
Vue.component('current-game', require('./Component/Game/current-game').default);
Vue.component('game-history', require('./Component/Game/game-history').default);
Vue.component('withdrawal-request', require('./Component/Withdrawal/withdrawal-request').default);
Vue.component('withdrawal-template', require('./Component/Withdrawal/manageWithTemplate').default);
Vue.component('withdrawal-request-agent', require('./Component/Withdrawal/withdrawal-agent').default);
Vue.component('withdrawal-recent', require('./Component/Withdrawal/withdrawal-recent').default);
Vue.component('withdrawal-recent-agents', require('./Component/Withdrawal/withdrawal-recent-agents').default);
Vue.component('withdrawal-logs', require('./Component/Withdrawal/withdrawal-logs').default);
Vue.component('withdrawal-logs-agents', require('./Component/Withdrawal/withdrawal-logs-agents').default);
Vue.component('partner-setting', require('./Component/Configuration/Partners/partner-settings').default);
Vue.component('game-interface', require('./Component/Game/game-interface').default);
Vue.component('game-reports', require('./Component/Game/game-reports').default);
Vue.component('view-betting', require('./Component/Game/view-betting').default);
Vue.component('player-balance', require('./Component/Game/player-bet').default);
Vue.component('notification-content', require('./Component/Helpers/notification-content').default);
Vue.component('notification-user', require('./Component/Helpers/notification-user').default);
Vue.component('player-logs', require('./Component/Users/player-logs').default);
Vue.component('total-rake', require('./Component/Reporting/total-rake').default);
Vue.component('cash-in', require('./Component/Reporting/cash-in').default);
Vue.component('topup-player', require('./Component/Topup/topup-player').default);
Vue.component('topup-player-history', require('./Component/Topup/topup-player-history').default);
Vue.component('cash-out', require('./Component/Reporting/cashout').default);
Vue.component('fund-transfer', require('./Component/Reporting/fund-transfer').default);
Vue.component('fund-account', require('./Component/Reporting/fund-account').default);
Vue.component('betting', require('./Component/Reporting/betting').default);
Vue.component('user-reports', require('./Component/Reporting/user-reports').default);
Vue.component('current-bets', require('./Component/Game/current-bets').default);
Vue.component('online-players', require('./Component/Users/online-players').default);
Vue.component('player-game-bet', require('./Component/Users/player-game-bets').default);
Vue.component('wallet-history', require('./Component/Users/wallet-history').default);
Vue.component('wallet-transfer', require('./Component/Users/wallet-transfer').default);
Vue.component('agent-commission', require('./Component/Reporting/agent-commission').default);
Vue.component('advance-reports', require('./Component/Reporting/advance-report').default);
Vue.component('edit-report', require('./Component/Reporting/edit-report').default);
Vue.component('convert-commission', require('./Component/Users/convert-commission').default);

Vue.filter('formatDate', function(value) {
    if (value) {
        return moment(String(value)).format('MM/DD/YYYY hh:mm a')
    }
});

Vue.filter('dateonly', function(value) {
    if (value) {
        return moment(String(value)).format('MM/DD/YYYY')
    }
});

Vue.filter('timeonly', function(value) {
    if (value) {
        return moment(String(value)).format('hh:mm a')
    }
});

Vue.filter('calendar', function(value) {
    if (value) {
        return moment(String(value)).fromNow();
    }
});

Vue.filter("numeric", function (value) {
    return numeral(value).format("0,0"); // displaying other groupings/separators is possible, look at the docs
});

Vue.filter("decimal", function (value) {
    return numeral(value).format("0,0.00"); // displaying other groupings/separators is possible, look at the docs
});

Vue.filter('capitalize', function (value) {
    if (!value) return ''
    value = value.toString();
    return value.toUpperCase()
});

Vue.filter('percentage', function(value, decimals) {
    if(!value) {
        value = 0;
    }

    if(!decimals) {
        decimals = 0;
    }

    value = value * 100;
    value = Math.round(value * Math.pow(10, decimals)) / Math.pow(10, decimals);
    value = value + '%';
    return value;
});

Vue.use(VueCoreVideoPlayer);
Vue.use(VueFriendlyIframe);

const app = new Vue({
    el: '#app',
    model: 'history'
});

