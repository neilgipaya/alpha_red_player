<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TopupPlayer extends Model
{
    public $timestamps = true;
    protected $table = "topups_player";
    protected $fillable = [
        'user_id','agent_id','amount','details','status','remarks'
    ];
    public function users()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
