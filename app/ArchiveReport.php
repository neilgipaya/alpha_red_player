<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArchiveReport extends Model
{
    //
    protected $fillable = ['archive_name','archive_by','archive_file'];
    public $timestamps = true;
    protected $table = "archive_report";
    protected $guarded = [];
    public function users(){
        return $this->belongsTo('\App\User','archive_by');
    }
}
