<?php

namespace App\Events;

use App\Notifications;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class NotificationEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;


    public $message;

    private $user_id;

    private $module_url;

    private $user_to;

    /**
     * NotificationEvent constructor.
     * @param $message
     * @param $user_id
     * @param $module_url
     * @param $user_to
     */
    public function __construct($message, $user_id, $module_url, $user_to)
    {
        $this->message = $message;
        $this->user_id = $user_id;
        $this->module_url = $module_url;

        Notifications::create([
           'notification_content' => $message,
            'triggered_user_from' => $user_id,
            'triggered_user_to' => $user_to,
            'module_url' => $module_url
        ]);
        $this->user_to = $user_to;
    }

    public function broadcastOn()
    {
        return ['my-alpha'];
    }

    public function broadcastAs()
    {
        return 'notification-event';
    }
}
