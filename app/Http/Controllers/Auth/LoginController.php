<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Jenssegers\Agent\Agent;
use App\UserLogs;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function username()
    {
        return 'username';
    }

    // Login
    public function showLoginForm(){

        $agent = new Agent();

//        if ($agent->isMobile()){
//            return view('login-mobile');
//        } else {
            $pageConfigs = [
                'bodyClass' => "bg-full-screen-image",
                'blankPage' => true
            ];

            return view('login-frontend', [
                'pageConfigs' => $pageConfigs
            ]);
//        }

    }

    public function authenticated()
    {
        if (\auth()->user()->type !== 'Official'){
            return redirect('/game/play-game');
        }

        return redirect('dashboard');
    }
    public function login(Request $request)
    {
        $this->validate($request, [
            'username' => 'required',
            'password' => 'required',
        ]);

        $user = \DB::table('users')->where('username', $request->input('username'))->first();
        if(!$user){
            \Session::flash('message', 'Username and Password is Incorrect !');
            \Session::flash('type', 'error');
        }
        else{
            if($user->status == 'Active' || $user->type == 'Official'){
                if (auth()->guard('web')->attempt(['username' => $request->input('username'), 'password' => $request->input('password')])) {
                    \DB::table('sessions')->where('user_id',$user->id)->delete();
                    $user = auth()->guard('web')->user();
                    UserLogs::create([
                        'user_id' => $user->id,
                        'type' => 'auth',
                        'content' => 'User Successfully Logged in'
                    ]);
                    return redirect($this->redirectTo);
                }
            }else{
                \Session::flash('message', 'Your Account has not been activated yet');
                \Session::flash('type', 'error');
            }
        }
        return back();

    }
    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        return $this->loggedOut($request) ?: redirect('/auth-login');
    }
}
