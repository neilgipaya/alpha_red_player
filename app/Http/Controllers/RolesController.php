<<<<<<< HEAD
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RolesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if(!auth()->user()->can('partner-settings')){
            abort(403, 'Unauthorized');
        }
        return view('Configuration.Roles.index');
    }


    public function getList()
    {
        $roles = Role::latest()->get();

        return response()->json($roles);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

        $data['permissions'] = Permission::all();

        return view('Configuration.Roles.create-role', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        DB::beginTransaction();

        try {
            //

            $name = $request->post('role_name');


            $role = Role::create(['name'=>$name]);
            $role->syncPermissions($request->input('permission'));


            DB::commit();
            $notification = array(
                'message' => 'Role Created successfully',
                'type' => 'success',
            );

            return redirect()->back()->with($notification);
        } catch (\Exception $e) {

            DB::rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger',
            );

            return redirect()->back()->with($notification);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data['role'] = Role::findOrFail($id);
        $data['rolePermissions'] = Role::findOrFail($id)->permissions->pluck('id')->toArray();
        $data['permissions'] = Permission::all();

        return view('Configuration.Roles.update-role', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        DB::beginTransaction();

        try {
            //

            $role = Role::find($id);

            $role->syncPermissions($request->except('_token', '_method', 'role_name'));

            DB::commit();
            $notification = array(
                'message' => 'Role Updated Successfully',
                'alert-type' => 'info'
            );
            return redirect()->back()->with($notification);
        } catch (\Exception $e) {

            DB::rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger',
            );

            return redirect()->back()->with($notification);

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        DB::beginTransaction();

        try {
            //

            Role::destroy($id);

            DB::commit();
            $notification = array(
                'message' => 'Role Deleted successfully',
                'type' => 'warning',
            );

            return response()->json($notification);
        } catch (\Exception $e) {

            DB::rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger',
            );

            return response()->json($notification);
        }
    }
}
=======
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RolesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if(!auth()->user()->can('roles')){
            abort(403, 'Unauthorized');
        }
        return view('Configuration.Roles.index');
    }


    public function getList()
    {
        $roles = Role::latest()->get();

        return response()->json($roles);
    }
    public function getRolesUser(){
        $roles = Role::where('name','!=','Player')->latest()->get();

        return response()->json($roles);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

        $data['permissions'] = Permission::all();

        return view('Configuration.Roles.create-role', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        DB::beginTransaction();

        try {
            //

            $name = $request->post('role_name');


            $role = Role::create(['name'=>$name]);
            $role->syncPermissions($request->input('permission'));


            DB::commit();
            $notification = array(
                'message' => 'Role Created successfully',
                'type' => 'success',
            );

            return redirect()->back()->with($notification);
        } catch (\Exception $e) {

            DB::rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger',
            );

            return redirect()->back()->with($notification);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data['role'] = Role::findOrFail($id);
        $data['rolePermissions'] = Role::findOrFail($id)->permissions->pluck('id')->toArray();
        $data['permissions'] = Permission::all();

        return view('Configuration.Roles.update-role', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        DB::beginTransaction();

        try {
            //

            $role = Role::find($id);

            $role->syncPermissions($request->except('_token', '_method', 'role_name'));

            DB::commit();
            $notification = array(
                'message' => 'Role Updated Successfully',
                'alert-type' => 'info'
            );
            return redirect()->back()->with($notification);
        } catch (\Exception $e) {

            DB::rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger',
            );

            return redirect()->back()->with($notification);

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        DB::beginTransaction();

        try {
            //
            $get = Role::find($id);
            $check = \App\User::Role($get->name)->first();
            if($check){
                $notification = array(
                    'message' => 'Remove user that has this role before to delete it.',
                    'type' => 'danger',
                );
                return response()->json($notification);
            }
            Role::destroy($id);

            DB::commit();
            $notification = array(
                'message' => 'Role Deleted successfully',
                'type' => 'success',
            );

            return response()->json($notification);
        } catch (\Exception $e) {

            DB::rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger',
            );

            return response()->json($notification);
        }
    }
}
>>>>>>> ae5b3d3ce322a6f5ff74d79fe1c0fb883afacf4c
