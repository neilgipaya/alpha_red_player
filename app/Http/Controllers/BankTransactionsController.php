<?php

namespace App\Http\Controllers;

use App\BankTransactions;
use Illuminate\Http\Request;

class BankTransactionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BankTransactions  $bankTransactions
     * @return \Illuminate\Http\Response
     */
    public function show(BankTransactions $bankTransactions)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BankTransactions  $bankTransactions
     * @return \Illuminate\Http\Response
     */
    public function edit(BankTransactions $bankTransactions)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BankTransactions  $bankTransactions
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BankTransactions $bankTransactions)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BankTransactions  $bankTransactions
     * @return \Illuminate\Http\Response
     */
    public function destroy(BankTransactions $bankTransactions)
    {
        //
    }
}
