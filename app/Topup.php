<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Topup extends Model
{
    //

    public $timestamps = true;

    protected $guarded = [];

    public function users()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function payments()
    {
        return $this->belongsTo('App\BankAccounts', 'payment_id');
    }

    public function decline_template()
    {
        return $this->belongsTo('App\DeclineTemplate', 'decline_template_id');
    }
    public function approved_by()
    {
        return $this->belongsTo('App\User','approved_by');
    }
}
