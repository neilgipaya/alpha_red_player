// axios
import axios from 'axios'

const domain = ""
const service = axios.create({
    baseURL: process.env.BASE_API, // API url
    timeout: 30 * 1000 // API request timeout set to 30s
})
// abort duplicate request
const pending = {}
const CancelToken = axios.CancelToken
const removePending = (config, f) => {
    // make sure the url is same for both request and response
    const url = config.url.replace(config.baseURL, '/')
    // stringify whole RESTful request with URL params
    const flagUrl = url + '&'
        + config.method + '&'
        + JSON.stringify(config.params)
    if (flagUrl in pending) {
        if (f) {
            f() // abort the request
        } else {
            delete pending[flagUrl]
        }
    } else {
        if (f) {
            pending[flagUrl] = f // store the cancel function
        }
    }
}
// axios interceptors
service.interceptors.request.use(config => {
    // you can apply cancel token to all or specific requests
    // e.g. except config.method == 'options'
    config.cancelToken = new CancelToken((c) => {
        removePending(config, c)
    })
    return config
}, error => {
    Promise.reject(error)
})
service.interceptors.response.use(
    response => {
        removePending(response.config)
        return response.data
    },
    error => {
        removePending(error.config)

        if (!axios.isCancel(error)) {
            return Promise.reject(error)
        } else {
            // return empty object for aborted request
            return Promise.resolve({})
        }
    }
)
export default axios.create({
    domain
    // You can add your headers here
})
