<?php

namespace App\Http\Controllers;

use App\BankAccounts;
use App\BankTransactions;
use App\Events\NotificationEvent;
use App\FundTransfer;
use App\UserLogs;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FundTransferController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if(!auth()->user()->can('funds-recent')){
            abort(403, 'Unauthorized');
        }
        return view('Funds.pending-funds');
    }

    public function logs()
    {
        if(!auth()->user()->can('funds-history')){
            abort(403, 'Unauthorized');
        }
        return view('Funds.fund-list');
    }

    public function request()
    {
        if(!auth()->user()->can('funds-request')){
            abort(403, 'Unauthorized');
        }
        return view('Funds.request');
    }

    public function getList()
    {
        $funds = FundTransfer::with('transaction_from')->with('transaction_to')->with('users')->latest()->get();

        return response()->json($funds);
    }
    public function getReportFiltered(Request $request){
        $from = $request->from;
        $to = $request->to;
        $topup = \DB::table('bank_accounts as a')
        ->select('a.account_type','c.status','c.amount','d.name','d.contact_number','c.created_at')
        ->leftjoin('fund_transfers as c','c.to_id','=','a.id')
        ->leftjoin('users as d','d.id','=','c.to_id')
        ->whereBetween('c.created_at',[$from, $to])
        ->get();
        $data = [];
        $collection = collect($topup);
        $count = 0;
        foreach($topup as $top_val){
            $check = false;
            foreach($data as $data_val){
                if($data_val['account_type'] == $top_val->account_type){
                    $check = true;
                }
            }
            if($check == false){
                $data[] = [
                    'account_type' => $top_val->account_type,
                    'total' => 0,
                    'total_no' => 0,
                    'total_declined' => 0,
                    'total_approved' => 0,
                    'ave_approval' => 0
                ];
                $par = $top_val->account_type;
                $par1 = 'Approved';
                $filtered = $collection->filter(function ($value) use($par,$par1) {
                    return $value->account_type == $par && $value->status == $par1;
                });
                $data[$count]['users'] = $filtered;
                $count++;
            }
            if($top_val->status == 'Approved'){
                for($i = 0 ; $i < count($data); $i++){
                    if($data[$i]['account_type'] == $top_val->account_type){
                        $data[$i]['total_approved'] +=1;
                        $data[$i]['total'] +=intval($top_val->amount);
                        $data[$i]['total_no'] += 1;
                        $data[$i]['ave_approval'] = $data[$i]['total_approved'] / ($data[$i]['total_approved'] + $data[$i]['total_declined']);
                    }
                }
            }else if($top_val->status == 'Declined'){
                for($i = 0 ; $i < count($data); $i++){
                    if($data[$i]['account_type'] == $top_val->account_type){
                        $data[$i]['total_declined'] +=1;
                        $data[$i]['total_no'] += 1;
                        if($data[$i]['total_approved'] <= 0){
                            $data[$i]['ave_approval'] = 0;
                        }else{
                            $data[$i]['ave_approval'] = $data[$i]['total_approved'] / ($data[$i]['total_approved'] + $data[$i]['total_declined']);
                        }
                    }
                }

            }
        }
        return response()->json($data);
    }
    public function getReport(){
        $topup = \DB::table('bank_accounts as a')
        ->select('a.account_type','c.status','c.amount','d.name','d.contact_number','c.created_at')
        ->leftjoin('fund_transfers as c','c.to_id','=','a.id')
        ->leftjoin('users as d','d.id','=','c.to_id')
        ->whereMonth('c.created_at',Carbon::today())
        ->get();
        $data = [];
        $collection = collect($topup);
        $count = 0;
        foreach($topup as $top_val){
            $check = false;
            foreach($data as $data_val){
                if($data_val['account_type'] == $top_val->account_type){
                    $check = true;
                }
            }
            if($check == false){
                $data[] = [
                    'account_type' => $top_val->account_type,
                    'total' => 0,
                    'total_no' => 0,
                    'total_declined' => 0,
                    'total_approved' => 0,
                    'ave_approval' => 0
                ];
                $par = $top_val->account_type;
                $par1 = 'Approved';
                $filtered = $collection->filter(function ($value) use($par,$par1) {
                    return $value->account_type == $par && $value->status == $par1;
                });
                $data[$count]['users'] = $filtered;
                $count++;
            }
            if($top_val->status == 'Approved'){
                for($i = 0 ; $i < count($data); $i++){
                    if($data[$i]['account_type'] == $top_val->account_type){
                        $data[$i]['total_approved'] +=1;
                        $data[$i]['total'] +=intval($top_val->amount);
                        $data[$i]['total_no'] += 1;
                        $data[$i]['ave_approval'] = $data[$i]['total_approved'] / ($data[$i]['total_approved'] + $data[$i]['total_declined']);
                    }
                }
            }else if($top_val->status == 'Declined'){
                for($i = 0 ; $i < count($data); $i++){
                    if($data[$i]['account_type'] == $top_val->account_type){
                        $data[$i]['total_declined'] +=1;
                        $data[$i]['total_no'] += 1;
                        if($data[$i]['total_approved'] <= 0){
                            $data[$i]['ave_approval'] = 0;
                        }else{
                            $data[$i]['ave_approval'] = $data[$i]['total_approved'] / ($data[$i]['total_approved'] + $data[$i]['total_declined']);
                        }
                    }
                }

            }
        }
        return response()->json($data);

    }
    public function getFundTransferChartReports(Request $request){
        $get = BankAccounts::with('funds')
        ->whereHas('funds',function($q){
            $q->whereMonth('created_at',Carbon::today());
        })
        ->where('account_type', $request->type)->get();
        return response()->json($get);
    }
    public function getFundTransferChartReportsPOST(Request $request){
        $from = $request->from;
        $to = $request->to;
        $get = DB::table('bank_accounts')
        ->select('bank_accounts.*','fund_transfers.amount','fund_transfers.created_at','fund_transfers.transaction_code')
        ->join('fund_transfers','fund_transfers.to_id','=','bank_accounts.id')
        ->where('fund_transfers.status','Approved')
        ->where('fund_transfers.transaction_code','!=','')
        ->whereBetween('fund_transfers.created_at',[$from, $to])
        ->where('bank_accounts.account_type',$request->type)->get();
        return response()->json($get);
    }

    public function getPending()
    {
        $funds = FundTransfer::with('transaction_from')->with('transaction_to')->with('users')->where('status', 'Pending')->latest()->get();

        return response()->json($funds);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        DB::beginTransaction();

        try {
            //

            $request->merge([
                'transaction_code' => "FT-".$this->generateRandomString(20),
                'requested_by' => auth()->user()->id
            ]);


            $message = "You have a New Fund Transfer Request";

            event(new NotificationEvent($message, $request->post('user_id'), '/funds', ''));

            FundTransfer::create($request->all());

            $ft = $request->post('transaction_code');

            UserLogs::create([
                'user_id' => \auth()->user()->id,
                'type' => 'Funds_Transfer',
                'content' => "Created Fund Transfer $ft"
            ]);
            DB::commit();
            $notification = array(
                'message' => 'Fund Transfer Created Successfully',
                'type' => 'success',
            );

            return response()->json($notification);
        } catch (\Exception $e) {

            DB::rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger',
            );

            return response()->json($notification);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\FundTransfer  $fundTransfer
     * @return \Illuminate\Http\Response
     */
    public function show(FundTransfer $fundTransfer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\FundTransfer  $fundTransfer
     * @return \Illuminate\Http\Response
     */
    public function edit(FundTransfer $fundTransfer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\FundTransfer  $fundTransfer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {


        DB::beginTransaction();

        try {
            //

            $request->merge([
                'status' => 'Approved'
            ]);




            $funds = FundTransfer::with('transaction_from')->with('transaction_to')->find($id);

            UserLogs::create([
                'user_id' => \auth()->user()->id,
                'type' => 'Funds_Transfer',
                'content' => "Fund Transferred $funds->transaction_code has been approved"
            ]);

            $amount = BankAccounts::find($funds->from_id);

            if ($amount->balance >= $funds->amount + $request->post('processing_fee')) {


                BankTransactions::create(
                    [
                        'bank_id' => $funds->from_id,
                        'debit' => $funds->amount + $request->post('processing_fee'),
                        'credit' => 0,
                        'transaction_code' => $funds->transaction_code,
                        'transaction_details' => "Funds has been transferred to ". $funds->transaction_to->account_name
                    ]
                );

                BankTransactions::create(
                    [
                        'bank_id' => $funds->to_id,
                        'credit' => $funds->amount,
                        'debit' => 0,
                        'transaction_code' => $funds->transaction_code,
                        'transaction_details' => "Funds has been transferred from ". $funds->transaction_from->account_name
                    ]
                );

                BankAccounts::find($funds->from_id)->decrement('balance', $funds->amount + $request->post('processing_fee'));
                BankAccounts::find($funds->to_id)->increment('balance', $funds->amount);

                $funds->update($request->only('status'));


                DB::commit();
                $notification = array(
                    'message' => 'Fund Transfer Approved successfully',
                    'type' => 'success',
                );

                return response()->json($notification);
            } else {
                $notification = array(
                    'message' => 'Invalid Fund Transfer',
                    'type' => 'danger',
                );

                return response()->json($notification);
            }
        } catch (\Exception $e) {

            DB::rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger',
            );

            return response()->json($notification);
        }
    }

    public function decline(Request $request, $id)
    {


        DB::beginTransaction();

        try {
            //

            $request->merge([
                'status' => 'Declined'
            ]);

            $funds = FundTransfer::find($id);

            UserLogs::create([
                'user_id' => \auth()->user()->id,
                'type' => 'Funds_Transfer',
                'content' => "Fund Transferred $funds->transaction_code has been declined"
            ]);
            $funds->update($request->only('status', 'decline_reason', 'decline_template_id'));


            DB::commit();
            $notification = array(
                'message' => 'Fund Transfer Declined successfully',
                'type' => 'error',
            );

            return response()->json($notification);
        } catch (\Exception $e) {

            DB::rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger',
            );

            return response()->json($notification);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\FundTransfer  $fundTransfer
     * @return \Illuminate\Http\Response
     */
    public function destroy(FundTransfer $fundTransfer)
    {
        //
    }

    private function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}
