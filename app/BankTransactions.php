<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class BankTransactions extends Model
{
    //

    protected $guarded = [];

    protected $dates = [
        'created_at', 'updated_at'
    ];

}
