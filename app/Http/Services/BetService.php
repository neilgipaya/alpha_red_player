<?php


namespace App\Http\Services;


use App\Bets;
use App\Matches;
use App\Partner;
use App\User;
use App\BankAccounts;
use phpDocumentor\Reflection\Types\Mixed_;
use PHPUnit\Framework\MockObject\Builder\Match;

/**
 * Class BetService
 * @package App\Http\Services
 */
class BetService
{

    protected $left_bet;

    protected $right_bet;

    /**
     * @param $request
     * @return bool
     */
    public function drawBet($request){
        $get_banker = BankAccounts::where('category','Banker')->where('banker',1)->first();
        $banker_id = ($get_banker)?$get_banker->id:0;
        $partner_rake = $this->getRake();
        $checkBet = $this->checkBet($request->post('game_id'));
        $position = "middle_bet";
        if($checkBet){
            Bets::where('game_id', $request->post('game_id'))
                ->where('user_id', auth()->user()->id)
                ->increment('bet_middle', $request->post('bet_amount'));
            Bets::where('game_id', $request->post('game_id'))
                ->where('user_id', auth()->user()->id)
                ->increment('bet_amount', $request->post('bet_amount'));
            $get_banker->increment('balance',$request->post('bet_amount'));
        }else{
            $agent_commission = $this->agentCommission();
            $bets = Bets::create([
                'game_id' => $request->post('game_id'),
                'matches_id' => $request->post('matches_id'),
                'user_id' => auth()->user()->id,
                'banker_id' => $banker_id,
                'bet_amount' => $request->post('bet_amount'),
                'bet_middle' => $request->post('bet_amount'),
                'agent_commission' => $agent_commission
            ]);
            $get_banker->increment('balance',$request->post('bet_amount'));
        }
        $this->updateBets($request, $position);
        Matches::where('game_id',$request->post('game_id'))
        ->increment('total_draw_bet',$request->post('bet_amount'));
        $this->deductBet($request);
    }
    public function placeBet($request)
    {
        $partner_rake = $this->getRake();
        $checkBet = $this->checkBet($request->post('game_id'));
        if ($checkBet){
            if ($request->post('bet_position') === 'left'){
                $position = "left_bet";
                Bets::where('game_id', $request->post('game_id'))
                    ->where('user_id', auth()->user()->id)
                    ->increment('bet_left', $request->post('bet_amount'));
                Bets::where('game_id', $request->post('game_id'))
                    ->where('user_id', auth()->user()->id)
                    ->increment('bet_amount', $request->post('bet_amount'));
            }
            else {
                $position = "right_bet";
                Bets::where('game_id', $request->post('game_id'))
                    ->where('user_id', auth()->user()->id)
                    ->increment('bet_right', $request->post('bet_amount'));
                Bets::where('game_id', $request->post('game_id'))
                    ->where('user_id', auth()->user()->id)
                    ->increment('bet_amount', $request->post('bet_amount'));
            }
        }
        else {

            if ($request->post('bet_position') === 'left'){
                $agent_commission = $this->agentCommission();
                $bets = Bets::create([
                    'game_id' => $request->post('game_id'),
                    'matches_id' => $request->post('matches_id'),
                    'user_id' => auth()->user()->id,
                    'bet_amount' => $request->post('bet_amount'),
                    'bet_left' => $request->post('bet_amount'),
                    'agent_commission' => $agent_commission

                ]);
                $position = "left_bet";
            }
            else {
                $agent_commission = $this->agentCommission();
                $bets = Bets::create([
                    'game_id' => $request->post('game_id'),
                    'matches_id' => $request->post('matches_id'),
                    'user_id' => auth()->user()->id,
                    'bet_amount' => $request->post('bet_amount'),
                    'bet_right' => $request->post('bet_amount'),
                    'agent_commission' => $agent_commission
                ]);
                $position = "right_bet";
            }
        }
        //update Matches
        $this->updateBets($request, $position);

        $currentMatch = $this->checkMatch($request->post('game_id'));

        $this->setLeftBet(floatval($currentMatch->left_bet));
        $this->setRightBet(floatval($currentMatch->right_bet));

        /*
         * Calculate Bet Percentage
         */


        $bet_percentage = $this->calculateBetPercentage();

        /*
         * Calculate Percent Difference
         */

        $percent_difference = $this->calculatePercentDifference($bet_percentage);

        /*
         * Calculate Bet Against
         */

        $percent_against = $this->calculatePercentAgainst();

        /*
         * Calculate Odds
         */
        $odds = $this->calculateOdds($percent_difference, $percent_against);

        /*
         * Odds less rake
         */

        $odds_less_rake = $this->calculateOddsLessRake($odds, $partner_rake);

        $total_bet = $this->getTotalBet();

        $total_rake = $this->calculateTotalRake($total_bet, $partner_rake);

        $net_payout = $this->calculateNetPayout($total_rake);


        $request->merge([
            'left_odds' => $odds[0],
            'right_odds' => $odds[1],
            'left_bet_percentage' => $bet_percentage[0],
            'right_bet_percentage' => $bet_percentage[1],
            'left_percent_difference' => $percent_difference[0],
            'right_percent_difference' => $percent_difference[1],
            'left_percent_less_rake' => $odds_less_rake[0],
            'right_percent_less_rake' => $odds_less_rake[1],
            'left_percent_against' => $percent_against[0],
            'right_percent_against' => $percent_against[1],
            'total_bet' => $total_bet,
            'total_rake'=> $total_rake,
            'net_payout' => $net_payout,
        ]);

        $this->updateMatches($request);
        $this->deductBet($request);


    }

    private function deductBet($request){
        User::find(auth()->user()->id)->decrement('wallet_balance', $request->post('bet_amount'));
    }

    /**
     * @return int|mixed
     */
    private function agentCommission(){
        $user = User::where('id', auth()->user()->agent_id)->latest()->first();

        if ($user){
            return $user->agent_rate;
        }

        return 0;
    }

    /**
     * @param $game_id
     * @return bool
     */
    public function checkMatch($game_id)
    {
        $match = Matches::where('game_id', $game_id)->latest()->first();

        if ($match){
            return $match;
        }

        return false;
    }

    private function calculateNetPayout($total_rake) : float
    {
        return $this->getTotalBet() - $total_rake;
    }

    /**
     * @param $total_bet
     * @param $partner_rake
     * @return float
     */
    private function calculateTotalRake($total_bet, $partner_rake) : float
    {
        return $total_bet * $partner_rake;
    }

    /**
     * @return float
     */
    private function getTotalBet() : float {

        return $this->getLeftBet() + $this->getRightBet();
    }

    private function calculateOddsLessRake($odds, $rake) : array
    {
        $left_odds_less_rake = floatval($odds[0]) * (1 - floatval($rake));
        $right_odds_less_rake = floatval($odds[1]) * (1 - floatval($rake));

        return [
            $left_odds_less_rake,
            $right_odds_less_rake
        ];
    }

    /**
     * @param $percent_difference
     * @param $percent_against
     * @return array
     */
    private function calculateOdds($percent_difference, $percent_against) : array {

        $left_odds = 1 + $percent_difference[0] + $percent_against[0];
        $right_odds = 1 + $percent_difference[1] + $percent_against[1];

        return [
            $left_odds,
            $right_odds
        ];
    }

    /**
     * @param $request
     * @param $position
     * @return mixed
     */
    private function updateBets($request, $position) {

        $match = Matches::where('id', $request->post('matches_id'))
            ->where('game_id', $request->post('game_id'))
            ->increment($position, $request->post('bet_amount'));

        return $match;
    }

    private function updateMatches($request){

        $match = Matches::find($request->post('matches_id'))->update($request->only('left_bet_percentage', 'right_bet_percentage',
            'left_percent_difference', 'right_percent_difference', 'left_percent_less_rake', 'right_percent_less_rake',
            'left_percent_against', 'right_percent_against', 'left_odds', 'right_odds', 'total_bet', 'net_payout', 'total_rake'));

        return $match;

    }

    /**
     * @return array
     */
    private function calculateBetPercentage() : array
    {

        $bet_percentage_left = 0;
        $bet_percentage_right = 0;

        $left_bet = $this->getLeftBet();
        $right_bet = $this->getRightBet();

        if ($left_bet > $right_bet){
            $bet_percentage_right = $left_bet -  $right_bet;
        } else {
            $bet_percentage_left = $right_bet -  $left_bet;
        }

        return [
            floatval($bet_percentage_left),
            floatval($bet_percentage_right)
        ];
    }

    /**
     * @param $bet_percentage
     * @return array
     */
    private function calculatePercentDifference($bet_percentage) : array
    {


        $left_percent_difference = $bet_percentage[0] > 0 ? $bet_percentage[0] / $this->getLeftBet() : 0;

        $right_percent_difference = $bet_percentage[1] > 0 ? $bet_percentage[1] / $this->getRightBet() : 0;

        return [
            (float) number_format($left_percent_difference, 11),
            (float) number_format($right_percent_difference, 11)
        ];

    }

    /**
     * @return array
     */
    private function calculatePercentAgainst() : array
    {

        $left_bet = $this->getLeftBet();
        $right_bet = $this->getRightBet();

        $bet_percent_against_right = min($left_bet /  $right_bet ?? 0, 1.0);
        $bet_percent_against_left =  min($right_bet /  $left_bet ?? 0, 1.0);

//        dd($bet_percent_against_left, $bet_percent_against_right);

        return [
            (float) number_format($bet_percent_against_left, 11),
            (float) number_format($bet_percent_against_right, 11)
        ];
    }

    /**
     * @param $game_id
     * @return bool|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|null
     */
    private function checkBet($game_id)
    {
        $checkBet = Bets::with('matches')->where('game_id', $game_id)->where('user_id', auth()->user()->id)->latest()->first();

        if ($checkBet){
            return $checkBet;
        }

        return false;

    }

    /**
     * @return float|int
     */
    public function getRake()
    {

        $rake = Partner::all()->sum('rake_percent');

        $rake_percentage = $rake;

        return $rake_percentage;

    }

    /**
     * @param mixed $left_bet
     * @return BetService
     */
    private function setLeftBet($left_bet)
    {
        $this->left_bet = $left_bet;
        return $this;
    }

    /**
     * @return mixed
     */
    private function getLeftBet()
    {
        return max($this->left_bet, 1);
    }

    /**
     * @return mixed
     */
    private function getRightBet()
    {
        return max($this->right_bet, 1);
    }

    /**
     * @param mixed $right_bet
     */
    private function setRightBet($right_bet)
    {
        $this->right_bet = $right_bet;
    }


}
