<?php

namespace App\Http\Controllers;

use App\TopupPlayer;
use App\User;
use App\UserLogs;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TopupPlayerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return 'create';
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try{
            TopupPlayer::create($request->all());
            DB::commit();
            $notification = array(
                'message' => 'New Topup Created successfully',
                'type' => 'success',
            );
            return response()->json($notification);
        }catch(\Exception $e){
            DB::rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger',
            );
            return response()->json($notification);
        }
    }
    /**
     * Display topup request of agents player.
     *
     * @param  \App\TopupPlayer  $topupPlayer
     * @return \Illuminate\Http\Response
     */
    public function myplayer(TopupPlayer $topupPlayer){
        return $topupPlayer->with('users')->where('status','Pending')->where('agent_id',auth()->user()->id)->orderBy('id','DESC')->get();
    }
    /**
     * Approve Player Request Cash -in.
     */
    public function approvedRequest(Request $request){
        DB::beginTransaction();
        try{
            $topup = TopupPlayer::find($request->id);
            $topup->update(['status' => 'Approved']);
            $agent = User::find(auth()->user()->id);
            if($agent->wallet_balance < $topup->amount){
                $notification = array(
                    'message' => 'You do not have enough points to process this request',
                    'type' => 'danger',
                );
                return response()->json($notification);
            }
            $agent->decrement('wallet_balance',$topup->amount);
            $user = User::find($request->user_id);
            $user->increment('wallet_balance',$topup->amount);

            UserLogs::create([
                'user_id' => auth()->user()->id,
                'type' => 'Topup',
                'content' => 'Agent: '.auth()->user()->name.'('.auth()->user()->id.') Approved Players Topup: '.$user->name.'('.$user->id.')
                with amount of '.$topup->amount
            ]);
            DB::commit();
            $notification = array(
                'message' => 'Cash in has been approved',
                'type' => 'success',
            );
            return response()->json($notification);
        }catch(\Exception $e){
            DB::rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger',
            );
            return response()->json($notification);
        }
    }
    /**
     * Agents downline history
     */
    public function topuphistory(TopupPlayer $topupPlayer){
        return $topupPlayer->with('users')->where('status','!=','Pending')->where('agent_id',auth()->user()->id)->orderBy('id','DESC')->get();
    }
    /**
     * Decline Users topup
     */
    public function declineTopup(Request $request){
        DB::beginTransaction();
        try{
            $topup = TopupPlayer::find($request->id);
            $topup->update(['status' => 'Declined','remarks' => $request->decline_template_description]);
            $user = User::find($topup->user_id);

            UserLogs::create([
                'user_id' => auth()->user()->id,
                'type' => 'Topup',
                'content' => 'Agent: '.auth()->user()->name.'('.auth()->user()->id.') Declined Players Topup: '.$user->name.'('.$user->id.')
                with amount of '.$topup->amount
            ]);
            DB::commit();
            $notification = array(
                'message' => 'Cash in has been declined',
                'type' => 'success',
            );
            return response()->json($notification);
        }catch(\Exception $e){
            DB::rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger',
            );
            return response()->json($notification);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TopupPlayer  $topupPlayer
     * @return \Illuminate\Http\Response
     */
    public function show(TopupPlayer $topupPlayer,$id)
    {
        return $topupPlayer->where('user_id',$id)->orderBy('id','DESC')->get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TopupPlayer  $topupPlayer
     * @return \Illuminate\Http\Response
     */
    public function edit(TopupPlayer $topupPlayer)
    {
        return 'edit';
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TopupPlayer  $topupPlayer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TopupPlayer $topupPlayer)
    {
        return 'update';
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TopupPlayer  $topupPlayer
     * @return \Illuminate\Http\Response
     */
    public function destroy(TopupPlayer $topupPlayer)
    {
        return 'destroy';
    }
}
