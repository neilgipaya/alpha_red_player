<<<<<<< HEAD
<?php


namespace App\Http\Services;


use App\Bets;
use App\Game;
use App\Matches;
use App\User;
use App\WinningStreak;
use Carbon\Carbon;
use function Sodium\increment;

class RedeclarePayoutService
{
    /**
     * @var BetService
     */
    private $betService;
    private $bulkService;

    /**
     * PayoutService constructor.
     * @param BetService $betService
     */
    public function __construct(BetService $betService,BulkUpdateService $bulkService)
    {
        $this->betService = $betService;
        $this->bulkService = $bulkService;
    }

    /**
     * @param $request
     * @param $id
     */
    public function processWinner($request, $game_id)
    {
        $match = $this->betService->checkMatch($game_id);
        $game = Bets::select('bets.*','users.wallet_balance')->where('bets.game_id',$game_id)->where('bets.bet_result','Win')->join('users','users.id','=','bets.user_id')->get();
        if($match->left_win){
            $this->revertwin($game_id,'left',$game);
        }else if($match->right_win){
            $this->revertwin($game_id,'right',$game);
        }else if($match->middle_win){
            $this->revertwin($game_id,'middle',$game);
        }else{
            $this->getback($game_id);
        }
        $data = $this->executeWinner($game_id,$request->winner,$match);
        $update_match = Matches::where('game_id',$game_id)->first();
        if($request->winner == 'left'){
            $update_match->update([
                'left_win' => 1,
                'right_win' => 0,
                'middle_win' => 0,
                'match_status' => 'Completed',
                'total_rake' => $data['total_rake'],
                'net_payout' => $data['net_payout'],
            ]);
        }else if($request->winner == 'right'){
            $update_match->update([
                'left_win' => 0,
                'right_win' => 1,
                'middle_win' => 0,
                'match_status' => 'Completed',
                'total_rake' => $data['total_rake'],
            ]);
        }else if($request->winner == 'middle'){
            $update_match->update([
                'left_win' => 0,
                'right_win' => 0,
                'middle_win' => 1,
                'match_status' => 'Completed',
                'net_payout' => 0,
                'total_rake' => $data['total_rake'],
                'draw_net_payout' => $data['draw_net_payout']
            ]);
        }else if($request->winner == 'cancel'){
            $update_match->update([
                'left_win' => 0,
                'right_win' => 0,
                'middle_win' => 0,
                'match_status' => 'Pending',
                'total_rake' => $data['total_rake'],
                'net_payout' => $data['net_payout'],
                'draw_net_payout' => $data['draw_net_payout']
            ]);
        }
    }
    public function revertwin($game_id,$win_side,$game){
        $success_revert = [];
        $updateUser=[];
        $bank_deduct = 0;
        $banker_id = 0;
        foreach($game as $value){
            $banker_id = $value->banker_id;
            if($win_side == 'left' || $win_side == 'right'){
                if($value->bet_left){
                    $must_deduct = $value->bet_payout;
                    $final_deduct = $value->wallet_balance - $must_deduct;
                    $updateUser[]=[
                        'id' => $value->user_id,
                        'wallet_balance' => $final_deduct
                    ];
                }
            }else if($win_side == 'middle'){
                if($value->bet_middle){
                    $must_deduct = $value->bet_payout + $value->bet_left + $value->bet_right;
                    $final_deduct = $value->wallet_balance - $must_deduct ;
                    $bank_deduct += $value->bet_payout - $value->bet_middle;
                    $updateUser[]=[
                        'id' => $value->user_id,
                        'wallet_balance' => $final_deduct
                    ];
                }
            }
        }
        if($banker_id){
            $backBet = \App\BankAccounts::find($banker_id);
            $backBet->decrement('balance',$bank_deduct);
        }
        $this->bulkService->process('users',$updateUser,'id');
        $update_bet = Bets::where('game_id',$game_id)
        ->where('bet_result','Win')
        ->update(['bet_result' => 'Queue']);
    }
    public function executeWinner($game_id,$win_side,$match){
        $game = Bets::select('bets.*','users.wallet_balance','users.agent_id')->where('bets.game_id',$game_id)->join('users','users.id','=','bets.user_id')->get();
        $partner_rake = $this->betService->getRake();
        $data['net_payout'] = 0;
        $data['total_rake'] = 0;
        $data['draw_net_payout'] = 0;
        $bank_deduct = 0;
        $updateUsers=[];
        $updateBets=[];
        foreach($game as $value){
            if($win_side == 'right'){
                if($value->bet_right){
                    $right_payout = $value->bet_right * $match->right_odds;
                    $rake_payout = $right_payout * $partner_rake;
                    $right_takehome = $right_payout - $rake_payout;
                    $bef_balance = $value->wallet_balance;
                    $aft_balance = $value->wallet_balance + $right_takehome;
                    $data['net_payout'] += $right_takehome;
                    $data['total_rake'] += $rake_payout;
                    $updateBets[]=[
                        'id' => $value->id,
                        'bet_odds' => round($match->right_odds, 2),
                        'bet_result' => 'Win',
                        'bet_rake' => round($rake_payout, 2),
                        'bet_winnings' => round($right_payout, 2),
                        'bet_payout' => round($right_takehome, 2),
                        'before_balance' => $bef_balance,
                        'after_balance' => $aft_balance 
                    ];
                    $updateUsers[]=[
                        'id' => $value->user_id,
                        'wallet_balance' => $aft_balance
                    ];
                }
            }else if($win_side == 'left'){
                if($value->bet_left){
                    $left_payout = $value->bet_left * $match->left_odds;
                    $rake_payout = $left_payout * $partner_rake;
                    $left_takehome = $left_payout - $rake_payout;
                    $bef_balance = $value->wallet_balance;
                    $aft_balance = $value->wallet_balance + $left_takehome;
                    $data['net_payout'] += $left_takehome;
                    $data['total_rake'] += $rake_payout;
                    $updateBets[]=[
                        'id' => $value->id,
                        'bet_odds' => round($match->left_odds, 2),
                        'bet_result' => 'Win',
                        'bet_rake' => round($rake_payout, 2),
                        'bet_winnings' => round($left_payout, 2),
                        'bet_payout' => round($left_takehome, 2),
                        'before_balance' => $bef_balance,
                        'after_balance' => $aft_balance 
                    ];
                    $updateUsers[]=[
                        'id' => $value->user_id,
                        'wallet_balance' => $aft_balance
                    ];
                }
            }else if($win_side == 'middle'){
                if($value->bet_middle){
                    $middle_payout = $value->bet_middle * 8;
                    $bef_balance = $value->wallet_balance;
                    $aft_balance = $value->wallet_balance + $middle_payout + $value->bet_left + $value->bet_right;
                    $data['draw_net_payout'] += $middle_payout - $value->bet_middle;
                    $updateBets[]=[
                        'id' => $value->id,
                        'bet_result' => 'Win',
                        'bet_winnings' => round($middle_payout, 2),
                        'bet_payout' => round($middle_payout, 2),
                        'before_balance' => $bef_balance,
                        'after_balance' => $aft_balance 
                    ];
                    $updateUsers[]=[
                        'id' => $value->user_id,
                        'wallet_balance' => $aft_balance
                    ];
                }else{
                    $bef_balance = $value->wallet_balance;
                    $aft_balance = $value->wallet_balance + $value->bet_amount;
                    $updateBets[]=[
                        'id' => $value->id,
                        'bet_result' => 'Draw',
                        'bet_winnings' => 0,
                        'bet_payout' => 0,
                        'before_balance' => $bef_balance,
                        'after_balance' => $aft_balance 
                    ];
                    $updateUsers[]=[
                        'id' => $value->user_id,
                        'wallet_balance' => $aft_balance
                    ];
                }
            }else if($win_side == 'cancel'){
                $bef_balance = $value->wallet_balance;
                $aft_balance = $value->wallet_balance + $value->bet_amount;
                if($value->agent_id){
                    $agent = User::find($value->agent_id);
                    if($agent){
                        $this->revertAgentCommission($agent,$value->bet_rake);
                    }
                }
                $updateBets[]=[
                    'id' => $value->id,
                    'bet_result' => 'Cancelled',
                    'bet_winnings' => 0,
                    'bet_payout' => 0,
                    'bet_rake' => 0,
                    'before_balance' => $bef_balance,
                    'after_balance' => $aft_balance 
                ];
                $updateUsers[]=[
                    'id' => $value->user_id,
                    'wallet_balance' => $aft_balance
                ];
            }
        }
        $this->bulkService->process('users',$updateUsers,'id');
        $this->bulkService->process('bets',$updateBets,'id');
        $status = '';
        if($win_side == 'left' || $win_side == 'right'){
            $status = 'Completed';
        }else if($win_side == 'middle'){
            $status = 'Draw';
        }else{
            $status = 'Cancelled';
        }
        Game::where('id',$game_id)->update(['status' => $status,'redeclared' => 1]);
        return $data;
    }
    // public function revertwin($game_id,$win_side){
    //     $game = Bets::where('game_id',$game_id)->where('bet_result','Win')->get();
    //     $success_revert = [];
    //     foreach($game as $game_value){
    //         if($win_side == 'left'){
    //             if($game_value->bet_left){
    //                 $user = User::find($game_value->user_id);
    //                 if($user->wallet_balance >= $game_value->bet_payout){
    //                     $user->decrement('wallet_balance',$game_value->bet_payout + $game_value->bet_rake);
    //                     array_push($success_revert,$game_value->user_id);
    //                 } 
    //             }
    //         }else if($win_side == 'right'){
    //             if($game_value->bet_right){
    //                 $user = User::find($game_value->user_id);
    //                 if($user->wallet_balance >= $game_value->bet_payout){
    //                     $user->decrement('wallet_balance',$game_value->bet_payout + $game_value->bet_rake);
    //                     array_push($success_revert,$game_value->user_id);
    //                 } 
    //             }
    //         }else if($win_side == 'middle'){
    //             if($game_value->bet_middle){
    //                 $user = User::find($game_value->user_id);
    //                 if($user->wallet_balance >= $game_value->bet_payout){
    //                     if($game_value->banker_id){
    //                         $backBet = \App\BankAccounts::find($game_value->banker_id);
    //                         $backBet->increment('balance',($game_value->bet_payout - $game_value->bet_middle) + $game_value->bet_rake);
    //                     }
    //                     $user->decrement('wallet_balance',$game_value->bet_payout + $game_value->bet_left + $game_value->bet_right + $game_value->bet_rake);
    //                     array_push($success_revert,$game_value->user_id);
    //                 }
    //             }
    //         }
    //     }
    //     $update_bet = Bets::whereIn('user_id',$success_revert)
    //     ->where('game_id',$game_id)
    //     ->update(['bet_result' => 'Loss']);
    // }
    // public function executeWinner($game_id,$win_side,$match){
    //     $partner_rake = $this->betService->getRake();
    //     $data['net_payout'] = 0;
    //     $data['total_rake'] = 0;
    //     $data['draw_net_payout'] =0;
    //     $game = Bets::where('game_id',$game_id)->get();
    //     foreach($game as $game_value){
    //         if($win_side == 'right'){
    //             if($game_value->bet_right){
    //                 $right_payout = $game_value->bet_right * $match->right_odds;
    //                 $rake_payout = $right_payout * $partner_rake;
    //                 $right_takehome = $right_payout - $rake_payout;
    //                 $user = User::find($game_value->user_id);
    //                 $prev = $user->wallet_balance;
    //                 $user->increment(
    //                     'wallet_balance', round($right_takehome, 2)
    //                 );
    //                 $data['net_payout'] += $right_takehome;
    //                 $data['total_rake'] += $rake_payout;
    //                 $aft = $user->wallet_balance;
    //                 Bets::find($game_value->id)->update([
    //                     'bet_odds' => round($match->right_odds, 2),
    //                     'bet_result' => 'Win',
    //                     'bet_rake' => round($rake_payout, 2),
    //                     'bet_winnings' => round($right_payout, 2),
    //                     'bet_payout' => round($right_takehome, 2),
    //                     'before_balance' => $prev,
    //                     'after_balance' => $aft
    //                 ]);
    //             }
    //         }else if($win_side == 'left'){
    //             if($game_value->bet_left){
    //                 $right_payout = $game_value->bet_left * $match->right_odds;
    //                 $rake_payout = $right_payout * $partner_rake;
    //                 $right_takehome = $right_payout - $rake_payout;
    //                 $user = User::find($game_value->user_id);
    //                 $prev = $user->wallet_balance;
    //                 $user->increment(
    //                     'wallet_balance', round($right_takehome, 2)
    //                 );
    //                 $data['net_payout'] += $right_takehome;
    //                 $aft = $user->wallet_balance;
    //                 Bets::find($game_value->id)->update([
    //                     'bet_odds' => round($match->right_odds, 2),
    //                     'bet_result' => 'Win',
    //                     'bet_rake' => round($rake_payout, 2),
    //                     'bet_winnings' => round($right_payout, 2),
    //                     'bet_payout' => round($right_takehome, 2),
    //                     'before_balance' => $prev,
    //                     'after_balance' => $aft
    //                 ]);
    //             }
    //         }else if($win_side == 'middle'){
    //             if($game_value->bet_middle){
    //                 $partner_rake = 0;
    //                 $net = 0;
    //                 $agent_commission_payout=0;
    //                 $check = \App\BankAccounts::where('category','Banker')->where('banker',1)->first();
    //                 if($check->rake){
    //                     $partner_rake = $this->betService->getRake();
    //                 }
    //                 $user = User::find($game_value->user_id);
    //                 $prev = $user->wallet_balance;
    //                 $drawprize = $game_value->bet_middle * 8;
    //                 if($partner_rake > 0){
    //                     $agent_commission_payout = $game_value->bet_amount * $game_value->agent_commission;
    //                     $bet_rake = $drawprize * $partner_rake;
    //                     $bet_payout = $drawprize - $bet_rake;
    //                     $total = $bet_payout + $game_value->bet_left + $game_value->bet_right;
    //                 }else{
    //                     $bet_payout = $drawprize;
    //                     $bet_rake = 0;
    //                     $total = $game_value->bet_left + $game_value->bet_right + $drawprize;
    //                 }
    //                 $user->increment(
    //                     'wallet_balance', round($total, 2)
    //                 );
    //                 $data['draw_net_payout']+=$bet_payout;
    //                 $aft = $user->wallet_balance;
    //                 $check->decrement('balance',$drawprize);
    //                 Bets::find($game_value->id)->update([
    //                     'bet_result' => 'Draw',
    //                     'bet_rake' => $bet_rake,
    //                     'bet_winnings' => $drawprize,
    //                     'bet_payout' => $bet_payout,
    //                     'before_balance' => $prev,
    //                     'agent_commission_payout' => $agent_commission_payout,
    //                     'after_balance' => $aft
    //                 ]);       
    //             }
    //         }else if($win_side == 'cancel'){
    //             $user = User::find($game_value->user_id);
    //             $prev = $user->wallet_balance;
    //             if($game_value->bet_result == 'Win'){
    //                 $total_payout = $game_value->bet_rake + $game_value->bet_payout;
    //                 $total = $total_payout - $game_value->bet_amount;
    //                 $user->decrement('wallet_balance',$total);
    //             }else{
    //                 $user->increment('wallet_balance',$game_value->bet_amount);
    //             }
    //             if($user->agent_id){
    //                 $agent = User::find($user->agent_id);
    //                 if($agent){
    //                     $this->revertAgentCommission($agent,$game_value->bet_rake);
    //                 }
    //             }
    //             $aft = $user->wallet_balance;
    //             Bets::find($game_value->id)->update([
    //                 'bet_result' => 'Cancelled',
    //                 'bet_payout' => 0,
    //                 'before_balance' => $prev,
    //                 'after_balance' => $aft,
    //                 'bet_winnings' => 0
    //             ]);  
    //         }
    //     }
    //     Game::where('id',$game_id)->update(['status' => 'Cancelled','redeclared' => 1]);
    //     return $data;
    // }
    public function getback($game_id){
        $game = Bets::where('game_id',$game_id)->get();
        foreach($game as $game_value){
            $user = User::find($game_value->user_id);
            $user->decrement('wallet_balance',$game_value->bet_amount);
        }
        Bets::where('game_id',$game_id)
        ->update([
            'bet_result' => 'Cancelled'
        ]);
    }
    private function revertAgentCommission($agent, $rake){
        $bet_rake = $rake;
        $partner_rake = $this->betService->getRake();
        $userAgent = $this->getUserByType('Silver Agent', $agent->agent_id ?? '');
        $masterAgent = $this->getUserByType('Gold Agent', $userAgent->agent_id ?? $agent->agent_id);
        $incorporator = $this->getUserByType('Master Agent', $masterAgent->agent_id ?? $agent->agent_id);

        //get percentage
        $ar = $userAgent->agent_rate ?? 0;
        $mr = $masterAgent->agent_rate ?? 0;
        $ir = $incorporator->agent_rate ?? 0;
        $dw = $partner_rake - $ir ?? 0;

        //calculate agent commission
        //calculate for incorporator
        $ic = ($ir - ($mr - $ar) - $ar) / $partner_rake;
        $mc = ($mr - $ar) / $partner_rake;
        $ac = $ar / $partner_rake;
        $dwr = ($partner_rake - $ir) / $partner_rake;

        if (!empty($userAgent)) {
            User::find($userAgent->id)->decrement('agent_commission_balance', $ac * $bet_rake);
        }

        if (!empty($masterAgent->id)) {
            User::find($masterAgent->id)->decrement('agent_commission_balance', $mc * $bet_rake);
        }

        if (!empty($incorporator->id)){
            User::find($incorporator->id)->decrement('agent_commission_balance', $ic * $bet_rake);
        }
    }
    private function getUserByType($type, $agent_id){
        if (!empty($agent_id)){
            $user = User::where('id', $agent_id)->where('type', $type)->first();
            if (isset($user)){
                return $user;
            }
            return null;
        }
        return null;
    }
}
=======
<?php


namespace App\Http\Services;


use App\Bets;
use App\Game;
use App\Matches;
use App\User;
use App\WinningStreak;
use Carbon\Carbon;
use function Sodium\increment;

class RedeclarePayoutService
{
    /**
     * @var BetService
     */
    private $betService;
    private $bulkService;

    /**
     * PayoutService constructor.
     * @param BetService $betService
     */
    public function __construct(BetService $betService,BulkUpdateService $bulkService)
    {
        $this->betService = $betService;
        $this->bulkService = $bulkService;
    }

    /**
     * @param $request
     * @param $id
     */
    public function processWinner($request, $game_id)
    {
        $match = $this->betService->checkMatch($game_id);
        $game = Bets::select('bets.*','users.wallet_balance')->where('bets.game_id',$game_id)->where('bets.bet_result','Win')->join('users','users.id','=','bets.user_id')->get();
        if($match->left_win){
            $this->revertwin($game_id,'left',$game);
        }else if($match->right_win){
            $this->revertwin($game_id,'right',$game);
        }else if($match->middle_win){
            $this->revertwin($game_id,'middle',$game);
        }else{
            $this->getback($game_id);
        }
        $data = $this->executeWinner($game_id,$request->winner,$match);
        $update_match = Matches::where('game_id',$game_id)->first();
        if($request->winner == 'left'){
            $update_match->update([
                'left_win' => 1,
                'right_win' => 0,
                'middle_win' => 0,
                'match_status' => 'Completed',
                'total_rake' => $data['total_rake'],
                'net_payout' => $data['net_payout'],
            ]);
        }else if($request->winner == 'right'){
            $update_match->update([
                'left_win' => 0,
                'right_win' => 1,
                'middle_win' => 0,
                'match_status' => 'Completed',
                'total_rake' => $data['total_rake'],
            ]);
        }else if($request->winner == 'middle'){
            $update_match->update([
                'left_win' => 0,
                'right_win' => 0,
                'middle_win' => 1,
                'match_status' => 'Completed',
                'net_payout' => 0,
                'total_rake' => $data['total_rake'],
                'draw_net_payout' => $data['draw_net_payout']
            ]);
        }else if($request->winner == 'cancel'){
            $update_match->update([
                'left_win' => 0,
                'right_win' => 0,
                'middle_win' => 0,
                'match_status' => 'Pending',
                'total_rake' => $data['total_rake'],
                'net_payout' => $data['net_payout'],
                'draw_net_payout' => $data['draw_net_payout']
            ]);
        }
    }
    public function revertwin($game_id,$win_side,$game){
        $success_revert = [];
        $updateUser=[];
        $bank_deduct = 0;
        $banker_id = 0;
        foreach($game as $value){
            $banker_id = $value->banker_id;
            if($win_side == 'left'){
                if($value->bet_left){
                    $must_deduct = $value->bet_payout;
                    $final_deduct = $value->wallet_balance - $must_deduct;
                    $updateUser[]=[
                        'id' => $value->user_id,
                        'wallet_balance' => $final_deduct
                    ];
                }
            }else if($win_side == 'right'){
                if($value->bet_right){
                    $must_deduct = $value->bet_payout;
                    $final_deduct = $value->wallet_balance - $must_deduct;
                    $updateUser[]=[
                        'id' => $value->user_id,
                        'wallet_balance' => $final_deduct
                    ];
                }
            }
            else if($win_side == 'middle'){
                if($value->bet_middle){
                    $must_deduct = $value->bet_payout + $value->bet_left + $value->bet_right;
                    $final_deduct = $value->wallet_balance - $must_deduct ;
                    $bank_deduct += $value->bet_payout - $value->bet_middle;
                    $updateUser[]=[
                        'id' => $value->user_id,
                        'wallet_balance' => $final_deduct
                    ];
                }
            }
        }
        if($banker_id){
            $backBet = \App\BankAccounts::find($banker_id);
            $backBet->decrement('balance',$bank_deduct);
        }
        $this->bulkService->process('users',$updateUser,'id');
        $update_bet = Bets::where('game_id',$game_id)
        ->where('bet_result','Win')
        ->update(['bet_result' => 'Loss']);
    }
    public function executeWinner($game_id,$win_side,$match){
        $game = Bets::select('bets.*','users.wallet_balance','users.agent_id')->where('bets.game_id',$game_id)->join('users','users.id','=','bets.user_id')->get();
        $partner_rake = $this->betService->getRake();
        $data['net_payout'] = 0;
        $data['total_rake'] = 0;
        $data['draw_net_payout'] = 0;
        $bank_deduct = 0;
        $updateUsers=[];
        $updateBets=[];
        foreach($game as $value){
            if($win_side == 'right'){
                if($match->middle_win){
                    if($value->bet_left && !$value->bet_middle){
                        $bef_balance = $value->wallet_balance;
                        $aft_balance = $value->wallet_balance - $value->bet_left;
                        $updateBets[]=[
                            'id' => $value->id,
                            'bet_odds' => 0,
                            'bet_result' => 'Loss',
                            'bet_rake' => 0,
                            'bet_winnings' => 0,
                            'bet_payout' => 0,
                            'before_balance' => $bef_balance,
                            'after_balance' => $aft_balance 
                        ];
                        $updateUsers[]=[
                            'id' => $value->user_id,
                            'wallet_balance' => $aft_balance
                        ];
                    }
                }
                if($value->bet_right){
                    $right_payout = $value->bet_right * $match->right_odds;
                    $rake_payout = $right_payout * $partner_rake;
                    $right_takehome = $right_payout - $rake_payout;
                    if($match->middle_win){
                        $bef_balance = $value->wallet_balance - ($value->bet_left + $value->bet_right);
                    }else{
                        $bef_balance = $value->wallet_balance;
                    }
                    $aft_balance = $bef_balance + $right_takehome;
                    $data['net_payout'] += $right_takehome;
                    $data['total_rake'] += $rake_payout;
                    $updateBets[]=[
                        'id' => $value->id,
                        'bet_odds' => round($match->right_odds, 2),
                        'bet_result' => 'Win',
                        'bet_rake' => round($rake_payout, 2),
                        'bet_winnings' => round($right_payout, 2),
                        'bet_payout' => round($right_takehome, 2),
                        'before_balance' => $bef_balance,
                        'after_balance' => $aft_balance 
                    ];
                    $updateUsers[]=[
                        'id' => $value->user_id,
                        'wallet_balance' => $aft_balance
                    ];
                }
            }else if($win_side == 'left'){
                if($match->middle_win){
                    if($value->bet_right && !$value->bet_middle){
                        $bef_balance = $value->wallet_balance;
                        $aft_balance = $value->wallet_balance - $value->bet_right;
                        $updateBets[]=[
                            'id' => $value->id,
                            'bet_odds' => 0,
                            'bet_result' => 'Loss',
                            'bet_rake' => 0,
                            'bet_winnings' => 0,
                            'bet_payout' => 0,
                            'before_balance' => $bef_balance,
                            'after_balance' => $aft_balance 
                        ];
                        $updateUsers[]=[
                            'id' => $value->user_id,
                            'wallet_balance' => $aft_balance
                        ];
                    }
                }
                if($value->bet_left){
                    $left_payout = $value->bet_left * $match->left_odds;
                    $rake_payout = $left_payout * $partner_rake;
                    $left_takehome = $left_payout - $rake_payout;
                    if($match->middle_win){
                        $bef_balance = $value->wallet_balance - ($value->bet_left + $value->bet_right);
                    }else{
                        $bef_balance = $value->wallet_balance;
                    }
                    $aft_balance = $bef_balance + $left_takehome;
                    $data['net_payout'] += $left_takehome;
                    $data['total_rake'] += $rake_payout;
                    $updateBets[]=[
                        'id' => $value->id,
                        'bet_odds' => round($match->left_odds, 2),
                        'bet_result' => 'Win',
                        'bet_rake' => round($rake_payout, 2),
                        'bet_winnings' => round($left_payout, 2),
                        'bet_payout' => round($left_takehome, 2),
                        'before_balance' => $bef_balance,
                        'after_balance' => $aft_balance 
                    ];
                    $updateUsers[]=[
                        'id' => $value->user_id,
                        'wallet_balance' => $aft_balance
                    ];
                }
            }else if($win_side == 'middle'){
                if($value->bet_middle){
                    $middle_payout = $value->bet_middle * 8;
                    $bef_balance = $value->wallet_balance;
                    $aft_balance = $value->wallet_balance + $middle_payout + $value->bet_left + $value->bet_right;
                    $data['draw_net_payout'] += $middle_payout - $value->bet_middle;
                    $updateBets[]=[
                        'id' => $value->id,
                        'bet_result' => 'Win',
                        'bet_winnings' => round($middle_payout, 2),
                        'bet_payout' => round($middle_payout, 2),
                        'before_balance' => $bef_balance,
                        'after_balance' => $aft_balance 
                    ];
                    $updateUsers[]=[
                        'id' => $value->user_id,
                        'wallet_balance' => $aft_balance
                    ];
                }else{
                    $bef_balance = $value->wallet_balance;
                    $aft_balance = $value->wallet_balance + $value->bet_amount;
                    $updateBets[]=[
                        'id' => $value->id,
                        'bet_result' => 'Draw',
                        'bet_winnings' => 0,
                        'bet_payout' => 0,
                        'before_balance' => $bef_balance,
                        'after_balance' => $aft_balance 
                    ];
                    $updateUsers[]=[
                        'id' => $value->user_id,
                        'wallet_balance' => $aft_balance
                    ];
                }
            }else if($win_side == 'cancel'){
                if($match->middle_win){
                    if($value->bet_middle){
                        $aft_balance = $value->wallet_balance + $value->bet_amount;
                    }else{
                        $aft_balance = $value->wallet_balance;
                    }
                }else{
                    $aft_balance = $value->wallet_balance + $value->bet_amount;
                }
                $bef_balance = $value->wallet_balance;
                if($value->agent_id){
                    $agent = User::find($value->agent_id);
                    if($agent){
                        $this->revertAgentCommission($agent,$value->bet_rake);
                    }
                }
                $updateBets[]=[
                    'id' => $value->id,
                    'bet_result' => 'Cancelled',
                    'bet_winnings' => 0,
                    'bet_payout' => 0,
                    'bet_rake' => 0,
                    'before_balance' => $bef_balance,
                    'after_balance' => $aft_balance 
                ];
                $updateUsers[]=[
                    'id' => $value->user_id,
                    'wallet_balance' => $aft_balance
                ];
            }
        }
        $this->bulkService->process('users',$updateUsers,'id');
        $this->bulkService->process('bets',$updateBets,'id');
        $status = '';
        if($win_side == 'left' || $win_side == 'right'){
            $status = 'Completed';
        }else if($win_side == 'middle'){
            $status = 'Draw';
        }else{
            $status = 'Cancelled';
        }
        Game::where('id',$game_id)->update(['status' => $status,'redeclared' => 1]);
        return $data;
    }
    // public function revertwin($game_id,$win_side){
    //     $game = Bets::where('game_id',$game_id)->where('bet_result','Win')->get();
    //     $success_revert = [];
    //     foreach($game as $game_value){
    //         if($win_side == 'left'){
    //             if($game_value->bet_left){
    //                 $user = User::find($game_value->user_id);
    //                 if($user->wallet_balance >= $game_value->bet_payout){
    //                     $user->decrement('wallet_balance',$game_value->bet_payout + $game_value->bet_rake);
    //                     array_push($success_revert,$game_value->user_id);
    //                 } 
    //             }
    //         }else if($win_side == 'right'){
    //             if($game_value->bet_right){
    //                 $user = User::find($game_value->user_id);
    //                 if($user->wallet_balance >= $game_value->bet_payout){
    //                     $user->decrement('wallet_balance',$game_value->bet_payout + $game_value->bet_rake);
    //                     array_push($success_revert,$game_value->user_id);
    //                 } 
    //             }
    //         }else if($win_side == 'middle'){
    //             if($game_value->bet_middle){
    //                 $user = User::find($game_value->user_id);
    //                 if($user->wallet_balance >= $game_value->bet_payout){
    //                     if($game_value->banker_id){
    //                         $backBet = \App\BankAccounts::find($game_value->banker_id);
    //                         $backBet->increment('balance',($game_value->bet_payout - $game_value->bet_middle) + $game_value->bet_rake);
    //                     }
    //                     $user->decrement('wallet_balance',$game_value->bet_payout + $game_value->bet_left + $game_value->bet_right + $game_value->bet_rake);
    //                     array_push($success_revert,$game_value->user_id);
    //                 }
    //             }
    //         }
    //     }
    //     $update_bet = Bets::whereIn('user_id',$success_revert)
    //     ->where('game_id',$game_id)
    //     ->update(['bet_result' => 'Loss']);
    // }
    // public function executeWinner($game_id,$win_side,$match){
    //     $partner_rake = $this->betService->getRake();
    //     $data['net_payout'] = 0;
    //     $data['total_rake'] = 0;
    //     $data['draw_net_payout'] =0;
    //     $game = Bets::where('game_id',$game_id)->get();
    //     foreach($game as $game_value){
    //         if($win_side == 'right'){
    //             if($game_value->bet_right){
    //                 $right_payout = $game_value->bet_right * $match->right_odds;
    //                 $rake_payout = $right_payout * $partner_rake;
    //                 $right_takehome = $right_payout - $rake_payout;
    //                 $user = User::find($game_value->user_id);
    //                 $prev = $user->wallet_balance;
    //                 $user->increment(
    //                     'wallet_balance', round($right_takehome, 2)
    //                 );
    //                 $data['net_payout'] += $right_takehome;
    //                 $data['total_rake'] += $rake_payout;
    //                 $aft = $user->wallet_balance;
    //                 Bets::find($game_value->id)->update([
    //                     'bet_odds' => round($match->right_odds, 2),
    //                     'bet_result' => 'Win',
    //                     'bet_rake' => round($rake_payout, 2),
    //                     'bet_winnings' => round($right_payout, 2),
    //                     'bet_payout' => round($right_takehome, 2),
    //                     'before_balance' => $prev,
    //                     'after_balance' => $aft
    //                 ]);
    //             }
    //         }else if($win_side == 'left'){
    //             if($game_value->bet_left){
    //                 $right_payout = $game_value->bet_left * $match->right_odds;
    //                 $rake_payout = $right_payout * $partner_rake;
    //                 $right_takehome = $right_payout - $rake_payout;
    //                 $user = User::find($game_value->user_id);
    //                 $prev = $user->wallet_balance;
    //                 $user->increment(
    //                     'wallet_balance', round($right_takehome, 2)
    //                 );
    //                 $data['net_payout'] += $right_takehome;
    //                 $aft = $user->wallet_balance;
    //                 Bets::find($game_value->id)->update([
    //                     'bet_odds' => round($match->right_odds, 2),
    //                     'bet_result' => 'Win',
    //                     'bet_rake' => round($rake_payout, 2),
    //                     'bet_winnings' => round($right_payout, 2),
    //                     'bet_payout' => round($right_takehome, 2),
    //                     'before_balance' => $prev,
    //                     'after_balance' => $aft
    //                 ]);
    //             }
    //         }else if($win_side == 'middle'){
    //             if($game_value->bet_middle){
    //                 $partner_rake = 0;
    //                 $net = 0;
    //                 $agent_commission_payout=0;
    //                 $check = \App\BankAccounts::where('category','Banker')->where('banker',1)->first();
    //                 if($check->rake){
    //                     $partner_rake = $this->betService->getRake();
    //                 }
    //                 $user = User::find($game_value->user_id);
    //                 $prev = $user->wallet_balance;
    //                 $drawprize = $game_value->bet_middle * 8;
    //                 if($partner_rake > 0){
    //                     $agent_commission_payout = $game_value->bet_amount * $game_value->agent_commission;
    //                     $bet_rake = $drawprize * $partner_rake;
    //                     $bet_payout = $drawprize - $bet_rake;
    //                     $total = $bet_payout + $game_value->bet_left + $game_value->bet_right;
    //                 }else{
    //                     $bet_payout = $drawprize;
    //                     $bet_rake = 0;
    //                     $total = $game_value->bet_left + $game_value->bet_right + $drawprize;
    //                 }
    //                 $user->increment(
    //                     'wallet_balance', round($total, 2)
    //                 );
    //                 $data['draw_net_payout']+=$bet_payout;
    //                 $aft = $user->wallet_balance;
    //                 $check->decrement('balance',$drawprize);
    //                 Bets::find($game_value->id)->update([
    //                     'bet_result' => 'Draw',
    //                     'bet_rake' => $bet_rake,
    //                     'bet_winnings' => $drawprize,
    //                     'bet_payout' => $bet_payout,
    //                     'before_balance' => $prev,
    //                     'agent_commission_payout' => $agent_commission_payout,
    //                     'after_balance' => $aft
    //                 ]);       
    //             }
    //         }else if($win_side == 'cancel'){
    //             $user = User::find($game_value->user_id);
    //             $prev = $user->wallet_balance;
    //             if($game_value->bet_result == 'Win'){
    //                 $total_payout = $game_value->bet_rake + $game_value->bet_payout;
    //                 $total = $total_payout - $game_value->bet_amount;
    //                 $user->decrement('wallet_balance',$total);
    //             }else{
    //                 $user->increment('wallet_balance',$game_value->bet_amount);
    //             }
    //             if($user->agent_id){
    //                 $agent = User::find($user->agent_id);
    //                 if($agent){
    //                     $this->revertAgentCommission($agent,$game_value->bet_rake);
    //                 }
    //             }
    //             $aft = $user->wallet_balance;
    //             Bets::find($game_value->id)->update([
    //                 'bet_result' => 'Cancelled',
    //                 'bet_payout' => 0,
    //                 'before_balance' => $prev,
    //                 'after_balance' => $aft,
    //                 'bet_winnings' => 0
    //             ]);  
    //         }
    //     }
    //     Game::where('id',$game_id)->update(['status' => 'Cancelled','redeclared' => 1]);
    //     return $data;
    // }
    public function getback($game_id){
        $game = Bets::where('game_id',$game_id)->get();
        foreach($game as $game_value){
            $user = User::find($game_value->user_id);
            $user->decrement('wallet_balance',$game_value->bet_amount);
        }
        Bets::where('game_id',$game_id)
        ->update([
            'bet_result' => 'Cancelled'
        ]);
    }
    private function revertAgentCommission($agent, $rake){
        $bet_rake = $rake;
        $partner_rake = $this->betService->getRake();
        $userAgent = $this->getUserByType('Silver Agent', $agent->agent_id ?? '');
        $masterAgent = $this->getUserByType('Gold Agent', $userAgent->agent_id ?? $agent->agent_id);
        $incorporator = $this->getUserByType('Master Agent', $masterAgent->agent_id ?? $agent->agent_id);

        //get percentage
        $ar = $userAgent->agent_rate ?? 0;
        $mr = $masterAgent->agent_rate ?? 0;
        $ir = $incorporator->agent_rate ?? 0;
        $dw = $partner_rake - $ir ?? 0;

        //calculate agent commission
        //calculate for incorporator
        $ic = ($ir - ($mr - $ar) - $ar) / $partner_rake;
        $mc = ($mr - $ar) / $partner_rake;
        $ac = $ar / $partner_rake;
        $dwr = ($partner_rake - $ir) / $partner_rake;

        if (!empty($userAgent)) {
            User::find($userAgent->id)->decrement('agent_commission_balance', $ac * $bet_rake);
        }

        if (!empty($masterAgent->id)) {
            User::find($masterAgent->id)->decrement('agent_commission_balance', $mc * $bet_rake);
        }

        if (!empty($incorporator->id)){
            User::find($incorporator->id)->decrement('agent_commission_balance', $ic * $bet_rake);
        }
    }
    private function getUserByType($type, $agent_id){
        if (!empty($agent_id)){
            $user = User::where('id', $agent_id)->where('type', $type)->first();
            if (isset($user)){
                return $user;
            }
            return null;
        }
        return null;
    }
}
>>>>>>> ae5b3d3ce322a6f5ff74d79fe1c0fb883afacf4c
