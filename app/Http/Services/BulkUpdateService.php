<?php


namespace App\Http\Services;


use App\Bets;
use App\Game;
use App\Matches;
use App\User;
use App\WinningStreak;
use Carbon\Carbon;
use function Sodium\increment;

class BulkUpdateService
{
    public function process($table,$values,$index){
        if (!count($values)) {
            return false;
        }
        $raw = false;
        $final = [];
        $ids = [];
        foreach ($values as $key => $val) {
            $ids[] = $val[$index];
            foreach (array_keys($val) as $field) {
                if ($field !== $index) {
                    $finalField = $val[$field];
                    $value = (is_null($val[$field]) ? 'NULL' : $finalField);
                    $final[$field][] = 'WHEN `' . $index . '` = \'' . $val[$index] . '\' THEN "' . $value . '" ';
                }
            }
        }
        $cases = '';
                    foreach ($final as $k => $v) {
                        $cases .= '`' . $k . '` = (CASE ' . implode("\n", $v) . "\n"
                            . 'ELSE `' . $k . '` END), ';
                    }
        $query = "UPDATE `" . $table . "` SET " . substr($cases, 0, -2) . " WHERE `$index` IN(" . '"' . implode('","', $ids) . '"' . ");";
        \DB::update($query);
        return response()->json($query);
    }
}
