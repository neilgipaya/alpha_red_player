<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WithdrawalPlayer extends Model
{
    public $timestamps = true;
    protected $guarded = [];
    protected $table = "withdrawals_players";
    public function users()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function withdrawal_template()
    {
        return $this->belongsTo('App\WithdrawalTemplate', 'withdrawal_template_id');
    }

    public function decline_template()
    {
        return $this->belongsTo('App\DeclineTemplate', 'decline_reason_id');
    }
    public function processed_by()
    {
        return $this->belongsTo('App\User','processed_by');
    }
}
