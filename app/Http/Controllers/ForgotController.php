<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
class ForgotController extends Controller
{
    public function send(Request $request){
        $getuser = \App\User::where('email',$request->email)->first();
        try{
            if(!$getuser){
                \Session::flash('message', 'Email Doesn`t Exist');
                \Session::flash('type', 'error');
                return back();
            }
            $otp = random_int(10000 , 99999);
            $reset_token = $this->generateToken();
            $array = ['otp' => $otp,'reset_token' => $reset_token,'url'=> asset('/activate-account?reset_token='.$reset_token)];
            $getuser->update(['otp' => $otp,'reset_token' => $reset_token]);
            $to_name = 'RECEIVER_NAME';
            $to_email = $request->email;
            \Mail::send('emails.mail',['data' => $array], function($message) use ($to_name, $to_email) {
                $message->to($to_email, $to_name)
                ->subject('Reset Password Vertification');
                $message->from($to_email,'Alpha PH');
            });
            \Session::flash('message', 'We have send a link to your email please check');
            \Session::flash('type1', 'success');
            return back();
        }catch(\Exception $e){
            return $e;
        }
        
    }
    public function generateToken()
    {
        return md5(rand(1, 10) . microtime());
    }
    public function resetpassword(Request $request){
        try{
            $getuser = \App\User::where('id',$request->id)->first();
            if($getuser->reset_token != $request->reset_token){
                $m = array(
                    'message' => 'Invalid Token cannot start the process',
                    'type' => 'error'
                );
            }else if($getuser->otp != $request->otp){
                $m = array(
                    'message' => 'Invalid Activation Code',
                    'type' => 'error'
                );
            }else if($request->password != $request->confirm_password){
                $m = array(
                    'message' => 'Password and Confirm password doesn`t Match',
                    'type' => 'error'
                );
            }else{
                $getuser->update(['password' => $request->password,'reset_token' => null]);
                \Session::flash('message', 'Successfully reset the password ');
                \Session::flash('type1', 'success');
                return redirect('login');
            }
            return back()->withErrors($m);
        }catch(\Exception $e){
            $m = array(
                'message' => $e->getMessage(),
                'type' => 'error'
            );
            return back()->withErrors($m);
        }
    }
}
