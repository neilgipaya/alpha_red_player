<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from iamubaidah.com/html/miscoo/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 15 Aug 2020 01:25:35 GMT -->
@include('frontend.head')

<body data-spy="scroll" data-target="#navbar" data-offset="0">

<!-- preloader begin -->
<div class="preloader">
	<img src="{{ asset('frontend') }}/img/preloader.gif" alt="">
</div>
<!-- preloader end -->

<!-- header section begin -->
<div class="header" id="header">
	@include('frontend.header')
</div>
<!-- header section end -->

@yield('content')

<!-- footer begin -->
<div class="footer">
	<div class="footer-top">
		<div class="container">
			<div class="row">
				<div class="col-xl-4 col-lg-4 col-sm-10">
					<div class="about-widget">
						<a href="index.html" class="logo">
							<img src="{{ asset('frontend') }}/img/dw.png" alt="">
						</a>
						<p>For any technical concern please contact our administrator</p>
						<div class="social-links">
							<ul>
								<li>
									<a href="#0" class="single-link">
										<i class="fab fa-facebook-f"></i>
									</a>
								</li>

							</ul>
						</div>
					</div>
				</div>
				<div class="col-xl-2 col-lg-2 col-sm-10">
					<div class="link-widget">
						<h4 class="title">
							Navigation
						</h4>
						<ul>
							<li>
								<a href="#0" class="single-link">
									Login
								</a>
							</li>
							<li>
								<a href="#0" class="single-link">
									Register
								</a>
							</li>
							<li>
								<a href="#0" class="single-link">
									Terms & Conditions
								</a>
							</li>
							<li>
								<a href="#0" class="single-link">
									Contact Us
								</a>
							</li>

						</ul>
					</div>
				</div>
				<div class="col-xl-2 col-lg-2 col-sm-10">

				</div>
			</div>
		</div>
	</div>
	<div class="copyright-area">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-xl-8 col-lg-8">
					<p>Copyright © <a href="/">Alpha</a> - {{ date('Y') }}. All Rights Reserved</p>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- footer end -->

<!-- jquery -->
<script src="{{ asset('frontend') }}/js/jquery.js"></script>
<!-- bootstrap -->
<script src="{{ asset('frontend') }}/js/bootstrap.min.js"></script>
<!-- owl carousel -->
<script src="{{ asset('frontend') }}/js/owl.carousel.min.js"></script>
<!-- magnific popup -->
<script src="{{ asset('frontend') }}/js/jquery-modal-video.min.js"></script>
<!-- filterizr js -->
<script src="{{ asset('frontend') }}/js/jquery.filterizr.min.js"></script>
<!-- way poin js-->
<script src="{{ asset('frontend') }}/js/waypoints.min.js"></script>
<!-- wow js-->
<script src="{{ asset('frontend') }}/js/wow.min.js"></script>
<!-- main -->
<script src="{{ asset('frontend') }}/js/main.js"></script>

@yield('script')
</body>

</html>