<?php

namespace App\Http\Controllers;

use App\Notifications;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class NotificationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function getList()
    {
        $notifications = Notifications::latest()->where('status', 'Unread')->get();

        return response()->json($notifications);
    }

    public function getListUser()
    {
        $notifications = Notifications::latest()->where('triggered_user_to', auth()->user()->id)->where('status', 'Unread')->get();

        return response()->json($notifications);
    }

    public function markAllRead()
    {

        DB::beginTransaction();

        try {
            //


            Notifications::where('status', 'Unread')->update([
                'status' => 'Read'
            ]);

            DB::commit();
            $notification = array(
                'message' => 'Notification Mark as Read successfully',
                'type' => 'success',
            );

            return response()->json($notification);
        } catch (\Exception $e) {

            DB::rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger',
            );

            return response()->json($notification);
        }




    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Notifications  $notifications
     * @return \Illuminate\Http\Response
     */
    public function show(Notifications $notifications)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Notifications  $notifications
     * @return \Illuminate\Http\Response
     */
    public function edit(Notifications $notifications)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Notifications  $notifications
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Notifications $notifications)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Notifications  $notifications
     * @return \Illuminate\Http\Response
     */
    public function destroy(Notifications $notifications)
    {
        //
    }
}
