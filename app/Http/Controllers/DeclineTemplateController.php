<?php

namespace App\Http\Controllers;

use App\DeclineTemplate;
use App\Http\Requests\DeclineTemplateRequest;
use App\UserLogs;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DeclineTemplateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if(!auth()->user()->can('decline-templates')){
            abort(403, 'Unauthorized');
        }
        return view('Configuration.Template.decline-template');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DeclineTemplateRequest $request)
    {
        //

        DB::beginTransaction();

        try {
            //

            UserLogs::create([
                'user_id' => \auth()->user()->id,
                'type' => 'Decline_Template',
                'content' => "Decline Template has been created"
            ]);

            DeclineTemplate::create($request->all());

            DB::commit();
            $notification = array(
                'message' => 'Decline Template Created Successfully',
                'type' => 'success',
            );

            return response()->json($notification);
        } catch (\Exception $e) {

            DB::rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger',
            );

            return response()->json($notification);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DeclineTemplate  $declineTemplate
     * @return \Illuminate\Http\Response
     */
    public function show(DeclineTemplate $declineTemplate)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DeclineTemplate  $declineTemplate
     * @return \Illuminate\Http\Response
     */
    public function edit(DeclineTemplate $declineTemplate)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DeclineTemplate  $declineTemplate
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        DB::beginTransaction();

        try {
            //

            DeclineTemplate::find($id)->update($request->all());

            UserLogs::create([
                'user_id' => \auth()->user()->id,
                'type' => 'Decline_Template',
                'content' => "Decline Template has been updated"
            ]);

            DB::commit();
            $notification = array(
                'message' => 'Decline Template Updated Successfully',
                'type' => 'success',
            );

            return response()->json($notification);
        } catch (\Exception $e) {

            DB::rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger',
            );

            return response()->json($notification);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DeclineTemplate  $declineTemplate
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        DB::beginTransaction();

        try {
            //

            DeclineTemplate::destroy($id);
            UserLogs::create([
                'user_id' => \auth()->user()->id,
                'type' => 'Decline_Template',
                'content' => "Decline Template has been deleted"
            ]);

            DB::commit();
            $notification = array(
                'message' => 'Decline Template Deleted successfully',
                'type' => 'warning',
            );

            return response()->json($notification);
        } catch (\Exception $e) {

            DB::rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger',
            );

            return response()->json($notification);
        }
    }

    public function getList()
    {
        $template = DeclineTemplate::latest()->get();

        return response()->json($template);
    }
}
