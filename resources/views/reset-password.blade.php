@isset($pageConfigs)
    {!! Helper::updatePageConfig($pageConfigs) !!}
@endisset

<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" data-textdirection="{{ env('MIX_CONTENT_DIRECTION') === 'rtl' ? 'rtl' : 'ltr' }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width,initial-scale=1.0">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="user_id" content="{{ (Auth()->check() ? Auth::user()->id:'') }}" />

        <title>@yield('title') - Alpha PH</title>
        <link rel="shortcut icon" type="image/x-icon" href="/landing/images/dw2.png">
        <!-- choose one -->
        <script src="http://unpkg.com/feather-icons"></script>

        {{-- Include core + vendor Styles --}}
        @include('panels/styles')

    </head>

    {{-- {!! Helper::applClasses() !!} --}}
    @php
    $configData = Helper::applClasses();
    @endphp

    <body class="vertical-layout vertical-menu-modern 1-column {{ $configData['blankPageClass'] }} {{ $configData['bodyClass'] }} {{($configData['theme'] === 'light') ? '' : $configData['theme'] }} data-menu="vertical-menu-modern" data-col="1-column"  data-layout="{{ $configData['theme'] }}">

        <!-- BEGIN: Content-->
        <div class="app-content content">
            <div class="content-wrapper">
                <div class="content-body">
                    <div class="container">
                        <div class="page">

                        <div class="container-fluid">
                            <div class="row no-gutter">
                                <!-- The image half -->
                                <div class="col-md-6 col-lg-6 col-xl-7 d-none d-md-flex bg-primary-transparent">
                                    <div class="row wd-100p mx-auto text-center">
                                        <div class="col-md-12 col-lg-12 col-xl-12 my-auto mx-auto wd-100p">
                                            <img src="/images/forgot-password.png" class="my-auto ht-xl-80p wd-md-100p wd-xl-80p mx-auto" alt="logo">
                                        </div>
                                    </div>
                                </div>
                                <!-- The content half -->
                                <div class="col-md-6 col-lg-6 col-xl-5">
                                    <div class="login d-flex align-items-center py-2">
                                        <!-- Demo content-->
                                        <div class="container p-0">
                                            <div class="row">
                                                <div class="col-md-10 col-lg-10 col-xl-9 mx-auto">
                                                    <div class="main-card-signin d-md-flex">
                                                        <div class="wd-100p">
                                                        @if($errors->any())
                                                            @if($errors->first('type') == 'error')
                                                                <div class="alert alert-danger" role="alert">
                                                                {{ $errors->first('message')  }}
                                                                </div>
                                                            @endif
                                                            @if($errors->first('type') == 'success')
                                                                <div class="alert alert-success" role="success">
                                                                {{ $errors->first('message')  }}
                                                                </div>
                                                            @endif
                                                        @endif
                                                            <div class="main-signin-header">
                                                                <h2 class="text-danger">Reset Your Password!</h2>
                                                                <form method="POST" action="/reset-password">
                                                                @csrf
                                                                    <input type="hidden" name="id" value="{{$get->id}}">
                                                                    <input type="hidden" name="reset_token" value="{{$get->reset_token}}">
                                                                    <div class="form-group">
                                                                        <label>Activation Code:</label>
                                                                        <input class="form-control" placeholder="Enter Activation Code" type="number" name="otp">
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>New Password:</label>
                                                                        <input class="form-control" placeholder="Enter New Password" type="password" name="password">
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>Confirm Password:</label>
                                                                        <input class="form-control" placeholder="Enter Confirm Password" type="password" name="confirm_password">
                                                                    </div>
                                                                    <button class="btn btn-primary btn-block" type="submit">Send</button>
                                                                </form>
                                                            </div>
                                                            <div class="main-signup-footer mg-t-20">
                                                                <p>Forget it, <a href="/login"> Send me back</a> to the sign in screen.</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div><!-- End -->
                                    </div>
                                </div><!-- End -->
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>


