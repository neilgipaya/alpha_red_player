<?php

use Illuminate\Database\Seeder;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        $permissions = [
            'dashboard-user',
            'play-game',
            'host-game-module',
            'create-game-session',
            'manage-game-session',
            'session-history',
            'user-management',
            'user-create',
            'user-edit',
            'user-delete',
            'top-up-module',
            'top-up-request',
            'top-up-recent',
            'top-up-approve',
            'top-up-decline',
            'top-up-history',
            'funds-transfer-module',
            'funds-request',
            'funds-recent',
            'funds-approve',
            'funds-decline',
            'funds-history',
            'withdrawal-module',
            'withdrawal-request',
            'withdrawal-recent',
            'withdrawal-approve',
            'withdrawal-decline',
            'withdrawal-history',
            'configuration-module',
            'fund-accounts',
            'decline-templates',
            'partner-settings',
            'roles',
        ];

        foreach ($permissions as $permission){
            \Spatie\Permission\Models\Permission::create([
                'name' => $permission,
                'guard_name' => 'web'
            ]);
        }
    }
}
