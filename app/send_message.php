<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class send_message extends Model
{
    public $timestamps = true;
    protected $table = "chat_message";
    protected $fillable = [
        'user_id', 'message', 'name','type'
    ];
}
