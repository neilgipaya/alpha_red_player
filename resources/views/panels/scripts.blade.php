{{-- Vendor Scripts --}}
<script>
    const userType = "{{ auth()->user()->type }}";
    const userID = "{{ auth()->user()->id }}";
    var userRate = parseFloat("{{ auth()->user()->agent_rate }}");
    const userCommission = parseFloat("{{ auth()->user()->agent_commission_balance}}");
    const agentRefferal = "{{ request()->root() }}/register?r={{ auth()->user()->id }}";
</script>


<script src="{{ asset(mix('js/app.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/vendors.min.js')) }}"></script>
<script src="{{ asset(mix('js/scripts/feather/feather.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/ui/prism.min.js')) }}"></script>
@yield('vendor-script')
{{-- Theme Scripts --}}
<script src="{{ asset(mix('js/core/app-menu.js')) }}"></script>
<script src="{{ asset(mix('js/core/app.js')) }}"></script>
<script src="{{ asset(mix('js/scripts/components.js')) }}"></script>
@if($configData['blankPage'] == false)
<script src="{{ asset(mix('js/scripts/customizer.js')) }}"></script>
<script src="{{ asset(mix('js/scripts/footer.js')) }}"></script>
@endif
{{-- page script --}}
@yield('page-script')
{{-- page script --}}
