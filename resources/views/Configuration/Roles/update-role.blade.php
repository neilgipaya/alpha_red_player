@extends('layouts/contentLayoutMaster')

@section('breadcrumb')
    <div class="clearfix"></div>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    @if(Session::has('message'))
                        <div class="alert alert-primary alert-dismissible mb-2" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            <div class="d-flex align-items-center">
                                <i class="bx bx-star"></i>
                                <span>
                                        {{ Session::get('message') }}
                                    </span>
                            </div>
                        </div>
                    @endif
                    <h4 class="card-title">Update Role & Permission </h4>
                    <a class="heading-elements-toggle">
                        <i class="bx bx-dots-vertical font-medium-3"></i>
                    </a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li>
                                <a data-action="collapse">
                                    <i class="bx bx-chevron-down"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="card-content collapse show">
                    <div class="card-body">
                        <form method="POST" action="/configuration/roles/{{ $role->id }}">
                            @csrf
                            @method('PATCH')

                            <div class="form-group row no-gutters">
                                <div class="col-md-10">
                                    <label>Role Name <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" value="{{ $role->name }}" name="role_name" placeholder="Enter Role Name"  required />
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-12 col-form-label">Permissions</label>
                                @foreach($permissions as $perms)
                                    <div class="col-lg-12">
                                        <div class="input-group m-input-group m-input-group--square">
                                            <input type="checkbox" name="permission[]" value="{{ $perms->id }}" @if(in_array($perms->id, $rolePermissions)) checked @endif /> &nbsp;&nbsp;&nbsp;
                                            <label>{{ ucwords(str_replace('-', ' ', $perms->name)) }}</label>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <button class="btn btn-success" type="submit"><i class="bx bx-save"></i> Save Changes</button>
                                    <button class="btn btn-warning" type="button" onclick="window.location.href='/configuration/roles'"><i class="bx bx-block"></i> Cancel</button>

                                </div>
                            </div>
                        </form>

                    </div>

                </div>
            </div>

        </div>
    </div>@endsection

@section('vendor-scripts')

@endsection
