@extends('layouts/contentLayoutMaster')

@section('title', 'Dashboard')

@section('page-style')
    {{-- Page Css files --}}
    <link rel="stylesheet" href="{{ asset(mix('css/pages/authentication.css')) }}">
@endsection

@section('page-script')

@endsection

@section('content')

    <div>
{{--        <form action="">--}}

{{--        <div class="row">--}}
{{--                @csrf--}}
{{--                @method('GET')--}}
{{--                <div class="col-md-6">--}}
{{--                    <label>Starting Date</label>--}}
{{--                    <input type="date" class="form-control" name="date_from" />--}}
{{--                </div>--}}
{{--                <div class="col-md-3" style="margin-top: 30px">--}}
{{--                    <button class="btn btn-primary " type="submit">Filter</button>--}}
{{--                </div>--}}
{{--        </div>--}}
{{--        </form>--}}


        <div class="row mt-2">
            <div class="col-sm-4 col-12 dashboard-users-success">
                <div class="card text-center">
                    <div class="card-content">
                        <div class="card-body py-1">
                            <div class="badge-circle badge-circle-lg badge-circle-light-danger mx-auto mb-50">
                                <i class="bx bx-wallet font-medium-5"></i>
                            </div>
                            <div class="text-muted line-ellipsis">Total Current Wallet Cash</div>
                            <h3 class="mb-0">{{ number_format($total_wallet_cash, 2) }}</h3>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 col-12 dashboard-users-success" onclick="window.location.href='/user/onlinePlayers'">
                <div class="card text-center">
                    <div class="card-content">
                        <div class="card-body py-1">
                            <div class="badge-circle badge-circle-lg badge-circle-light-danger mx-auto mb-50">
                                <i class="bx bx-user font-medium-5"></i>
                            </div>
                            <div class="text-muted line-ellipsis">Total Players Online</div>
                            <h3 class="mb-0">{{ number_format($total_players_online) }}</h3>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 col-12 dashboard-users-success">
                <div class="card text-center">
                    <div class="card-content">
                        <div class="card-body py-1">
                            <div class="badge-circle badge-circle-lg badge-circle-light-danger mx-auto mb-50">
                                <i class="bx bx-money font-medium-5"></i>
                            </div>
                            <div class="text-muted line-ellipsis">Average Bets / Game</div>
                            <h3 class="mb-0">{{ number_format($average_bets_per_game, 2) }}</h3>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 col-12 dashboard-users-success">
                <div class="card text-center">
                    <div class="card-content">
                        <div class="card-body py-1">
                            <div class="badge-circle badge-circle-lg badge-circle-light-danger mx-auto mb-50">
                                <i class="bx bx-user-circle font-medium-5"></i>
                            </div>
                            <div class="text-muted line-ellipsis">Total Players Registered</div>
                            <h3 class="mb-0">{{ number_format($total_players_registered) }}</h3>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 col-12 dashboard-users-success">
                <div class="card text-center">
                    <div class="card-content">
                        <div class="card-body py-1">
                            <div class="badge-circle badge-circle-lg badge-circle-light-danger mx-auto mb-50">
                                <i class="bx bx-user-minus font-medium-5"></i>
                            </div>
                            <div class="text-muted line-ellipsis">Inactive Players</div>
                            <h3 class="mb-0">{{ number_format($inactive_players) }}</h3>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 col-12 dashboard-users-success">
                <div class="card text-center">
                    <div class="card-content">
                        <div class="card-body py-1">
                            <div class="badge-circle badge-circle-lg badge-circle-light-danger mx-auto mb-50">
                                <i class="bx bx-user-check font-medium-5"></i>
                            </div>
                            <div class="text-muted line-ellipsis">New Players Today</div>
                            <h3 class="mb-0">{{ number_format($new_players_today) }}</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="col-12">
                    <div class="card">
                        <div class="card-content">
                            <!-- table strip dark -->
                            <div class="table-responsive">
                                <h5 class="card-header" style="margin-bottom: 15px">SUMMARY PENDING REQUEST</h5>
                                <table class="table table-striped table-dark mb-0">
                                    <thead>
                                    <tr>
                                        <th>CASH-IN</th>
                                        <th>WITHDRAWAL</th>
                                        <th>FUND TRANSFER</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>{{ number_format($summary_pending_cashin) }}</td>
                                        <td>{{ number_format($summary_pending_withdrawal) }}</td>
                                        <td>{{ number_format($summary_pending_fund_transfer) }}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="col-12">
                    <div class="card">
                        <div class="card-content">
                            <!-- table strip dark -->
                            <div class="table-responsive">
                                <h5 class="card-header" style="margin-bottom: 15px">LOAD - CASH-IN - TOPUP</h5>
                                <table class="table table-striped table-dark mb-0">
                                    <thead>
                                    <tr>
                                        <th></th>
                                        <th>DAILY ({{ \Carbon\Carbon::today()->format('Y-m-d') }})</th>
                                        <th>WEEKLY {{ \Carbon\Carbon::today()->startOfWeek()->format('Y-m-d') . ' - ' . \Carbon\Carbon::today()->endOfWeek()->format('Y-m-d') }})</th>
                                        <th>MONTHLY ({{ \Carbon\Carbon::today()->startOfMonth()->format('Y-m-d') . ' - ' . \Carbon\Carbon::today()->endOfMonth()->format('Y-m-d') }})</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>Total Load</td>
                                        <td>{{ number_format($total_load_daily, 2) }}</td>
                                        <td>{{ number_format($total_load_weekly, 2) }}</td>
                                        <td>{{ number_format($total_load_daily, 2) }}</td>
                                    </tr>
                                    <tr>
                                        <td>GCash</td>
                                        <td>{{ number_format($gcash_load_daily, 2) }}</td>
                                        <td>{{ number_format($gcash_load_weekly, 2) }}</td>
                                        <td>{{ number_format($gcash_load_daily, 2) }}</td>
                                    </tr>
                                    <tr>
                                        <td>PayMaya</td>
                                        <td>{{ number_format($paymaya_load_daily, 2) }}</td>
                                        <td>{{ number_format($paymaya_load_weekly, 2) }}</td>
                                        <td>{{ number_format($paymaya_load_monthly, 2) }}</td>
                                    </tr>
                                    <tr>
                                        <td>ML</td>
                                        <td>{{ number_format($ml_load_daily, 2) }}</td>
                                        <td>{{ number_format($ml_load_weekly, 2) }}</td>
                                        <td>{{ number_format($ml_load_monthly, 2) }}</td>
                                    </tr>
                                    <tr>
                                        <td>Cebuana</td>
                                        <td>{{ number_format($cebuana_load_daily, 2) }}</td>
                                        <td>{{ number_format($cebuana_load_weekly, 2) }}</td>
                                        <td>{{ number_format($cebuana_load_monthly, 2) }}</td>
                                    </tr>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="col-12">
                    <div class="card">
                        <div class="card-content">
                            <!-- table strip dark -->
                            <div class="table-responsive">
                                <h5 class="card-header" style="margin-bottom: 15px">Game Stats: Bet</h5>
                                <table class="table table-striped table-dark mb-0">
                                    <thead>
                                    <tr>
                                        <th></th>
                                        <th>DAILY ({{ \Carbon\Carbon::today()->format('Y-m-d') }})</th>
                                        <th>WEEKLY {{ \Carbon\Carbon::today()->startOfWeek()->format('Y-m-d') . ' - ' . \Carbon\Carbon::today()->endOfWeek()->format('Y-m-d') }})</th>
                                        <th>MONTHLY ({{ \Carbon\Carbon::today()->startOfMonth()->format('Y-m-d') . ' - ' . \Carbon\Carbon::today()->endOfMonth()->format('Y-m-d') }})</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>Amount</td>
                                        <td>{{ number_format($bets_daily_amount, 2) }}</td>
                                        <td>{{ number_format($bets_weekly_amount, 2) }}</td>
                                        <td>{{ number_format($bets_monthly_amount, 2) }}</td>
                                    </tr>
                                    <tr>
                                        <td>NUMBER</td>
                                        <td>{{ number_format($bets_daily_number, 2) }}</td>
                                        <td>{{ number_format($bets_weekly_number, 2) }}</td>
                                        <td>{{ number_format($bets_monthly_number, 2) }}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="col-12">
                    <div class="card">
                        <div class="card-content">
                            <!-- table strip dark -->
                            <div class="table-responsive">
                                <h5 class="card-header" style="margin-bottom: 15px">FUND TRANSFERS - WITHDRAWAL</h5>
                                <table class="table table-striped table-dark mb-0">
                                    <thead>
                                    <tr>
                                        <th>TOTAL WALLET PAYOUTS</th>
                                        <th>DAILY ({{ \Carbon\Carbon::today()->format('Y-m-d') }})</th>
                                        <th>WEEKLY {{ \Carbon\Carbon::today()->startOfWeek()->format('Y-m-d') . ' - ' . \Carbon\Carbon::today()->endOfWeek()->format('Y-m-d') }})</th>
                                        <th>MONTHLY ({{ \Carbon\Carbon::today()->startOfMonth()->format('Y-m-d') . ' - ' . \Carbon\Carbon::today()->endOfMonth()->format('Y-m-d') }})</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>PRINCIPAL BANK ACCOUNTS</td>
                                        <td>{{ number_format($funds_principal_daily, 2) }}</td>
                                        <td>{{ number_format($funds_principal_weekly, 2) }}</td>
                                        <td>{{ number_format($funds_principal_monthly, 2) }}</td>
                                    </tr>
                                    <tr>
                                        <td>SERVICE PROVIDER</td>
                                        <td>{{ number_format($funds_service_provider_daily, 2) }}</td>
                                        <td>{{ number_format($funds_service_provider_weekly, 2) }}</td>
                                        <td>{{ number_format($funds_service_provider_monthly, 2) }}</td>
                                    </tr>
                                    <tr>
                                        <td>PLAYER WITHDRAWALS</td>
                                        <td>{{ number_format($funds_service_provider_daily, 2) }}</td>
                                        <td>{{ number_format($funds_service_provider_weekly, 2) }}</td>
                                        <td>{{ number_format($funds_service_provider_monthly, 2) }}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="col-12">
                    <div class="card">
                        <div class="card-content">
                            <!-- table strip dark -->
                            <div class="table-responsive">
                                <h5 class="card-header" style="margin-bottom: 15px">TOP PLAYERS (LAST 30 DAYS)</h5>
                                <table class="table table-striped table-dark mb-0">
                                    <thead>
                                    <tr>
                                        <th></th>
                                        <th>LOAD</th>
                                        <th>BET</th>
                                        <th>WIN</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @for($i=0; $i<5; $i++)
                                        <tr>
                                            <td>{{ $i + 1 }}</td>
                                            <td>{{ $top_player_load[$i]->name ?? "NONE" }}</td>
                                            <td>{{ $top_player_bet[$i]->name ?? "NONE" }}</td>
                                            <td>{{ $top_player_winner[$i]->name  ?? "NONE" }}</td>
                                        </tr>
                                    @endfor
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="col-12">
                    <div class="card">
                        <div class="card-content">
                            <!-- table strip dark -->
                            <div class="table-responsive">
                                <h5 class="card-header">Rake {{ $total_partner_rake * 100 }}% OF WINS</h5>
                                <table class="table table-striped table-dark mb-0">
                                    <thead>
                                    <tr>
                                        <th></th>
                                        <th>DAILY ({{ \Carbon\Carbon::today()->format('Y-m-d') }})</th>
                                        <th>WEEKLY {{ \Carbon\Carbon::today()->startOfWeek()->format('Y-m-d') . ' - ' . \Carbon\Carbon::today()->endOfWeek()->format('Y-m-d') }})</th>
                                        <th>MONTHLY ({{ \Carbon\Carbon::today()->startOfMonth()->format('Y-m-d') . ' - ' . \Carbon\Carbon::today()->endOfMonth()->format('Y-m-d') }})</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>TOTAL RAKE {{ $total_partner_rake * 100 }}%</td>
                                        <td>{{ number_format($total_rake_daily, 2) }}</td>
                                        <td>{{ number_format($total_rake_weekly, 2) }}</td>
                                        <td>{{ number_format($total_rake_monthly, 2) }}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="col-12">
                    <div class="card">
                        <h5 class="card-header">RAKE DISTRIBUTION (<strong>PHP {{ number_format($total_rake_monthly, 2) }}</strong>) as of this month</h5>
{{--                        <p>Total Rake as of today {{ $ }}</p>--}}
                        <div class="card-content">
                            <!-- table strip dark -->
                            <div class="table-responsive">
                                <table class="table table-striped table-dark mb-0">
                                    <thead>
                                    <tr>
                                        <th>PARTNER</th>
                                        <th>DAILY ({{ \Carbon\Carbon::today()->format('Y-m-d') }})</th>
                                        <th>WEEKLY ({{ \Carbon\Carbon::today()->startOfWeek()->format('Y-m-d') . ' - ' . \Carbon\Carbon::today()->endOfWeek()->format('Y-m-d') }})</th>
                                        <th>MONTHLY ({{ \Carbon\Carbon::today()->startOfMonth()->format('Y-m-d') . ' - ' . \Carbon\Carbon::today()->endOfMonth()->format('Y-m-d') }})</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($principal as $p)
                                        <tr>
                                            <td>{{ $p->partner_name . " - " . $p->rake_percent * 100 . "%" }}</td>
                                            <td>{{ number_format($total_rake_daily * $p->rake_percent / $total_partner_rake, 2) }}</td>
                                            <td>{{ number_format($total_rake_weekly * $p->rake_percent / $total_partner_rake, 2) }}</td>
                                            <td>{{ number_format($total_rake_monthly * $p->rake_percent / $total_partner_rake, 2) }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- row -->

    <!-- /row -->
@endsection
