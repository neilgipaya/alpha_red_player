<<<<<<< HEAD
<?php

namespace App\Http\Controllers;

use App\Bets;
use App\Http\Requests\UserRequest;
use App\User;
use App\UserLogs;
use App\UserSession;
use App\WalletHistory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use function GuzzleHttp\default_ca_bundle;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        //
        if(!auth()->user()->can('user-management')){
            abort(403, 'Unauthorized');
        }

        $data['users'] = User::all()->sortByDesc('name');

        return view('User.index', $data);
    }
    public function deactivate(){
        if(!auth()->user()->can('user-management')){
            abort(403, 'Unauthorized');
        }
        return view('User.user_deactivate');
    }
    public function inactiveAccounts(){
        if(!auth()->user()->can('user-management')){
            abort(403, 'Unauthorized');
        }
        return view('User.myInactivePlayers');
    }
    public function agentBypassLimit($id)
    {

        $user = User::find($id);

        if ($user->agent_limit === 1){



            $user->update([
                'agent_limit' => 0
            ]);
        } else {
            $user->update([
                'agent_limit' => 1
            ]);
        }

    }
    public function getUserReportsFiltered(Request $request){
        $from = $request->from;
        $to = $request->to;
        $users = User::select('id','name','contact_number','wallet_balance','type','created_at')->where('wallet_balance','>',0)->whereBetween('created_at',[$from, $to])->get();
        $history = WalletHistory::get();
        $collection = collect($history);
        $output = [];
        for($i = 0 ; $i < count($users); $i++){
            $output[] = ['name' => $users[$i]->name,
            'contact_number' => $users[$i]->contact_number,
            'wallet_balance' => $users[$i]->wallet_balance,
            'type' => $users[$i]->type,
            'created_at' => $users[$i]->created_at];
            $id = $users[$i]->id;
            $filtered = $collection->filter(function ($value) use($id) {
                return $value->user_id == $id;
            });
            $output[$i]['history'] = $filtered;
        }
        return response()->json($output);
    }
    public function player()
    {
        //
        if(!auth()->user()->can('user-management')){
            abort(403, 'Unauthorized');
        }

        return view('User.player');
    }

    public function agent()
    {
        if(!auth()->user()->can('user-management')){
            abort(403, 'Unauthorized');
        }

        return view('User.agent');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        //

        DB::beginTransaction();

        try {
            $validation = User::where('contact_number',$request->post('contact_number'))
            ->orWhere('username',$request->post('username'))->get();
            if(count($validation) > 0){
                $notification = array(
                    'message' => 'Invalid request Username,Contact Number is already exist',
                    'type' => 'danger',
                );
                return response()->json($notification);
            }
            $request->merge([
                'otp' => random_int(1000, 9999)
            ]);
            $user = User::create($request->all());
            $user->syncRoles($request->post('role_id'));
            $name = $request->post('name');

            UserLogs::create([
                'user_id' => \auth()->user()->id,
                'type' => 'auth',
                'content' => "User $name Successfully Created"
            ]);

            DB::commit();
            $notification = array(
                'message' => 'User Created successfully',
                'type' => 'success',
            );

            return response()->json($notification);
        } catch (\Exception $e) {

            DB::rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger',
            );

            return response()->json($notification);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        //

        DB::beginTransaction();

        try {
            //

            $user = User::find($id);

            $role = $request->post('role_id');
            if (!empty($role)){

                UserLogs::create([
                    'user_id' => \auth()->user()->id,
                    'type' => 'auth',
                    'content' => "User $user->name Role has been updated"
                ]);
                $user->syncRoles($request->post('role_id'));

            }
            $user->update($request->all());

            UserLogs::create([
                'user_id' => \auth()->user()->id,
                'type' => 'auth',
                'content' => "User $user->name Information Successfully Updated"
            ]);

            DB::commit();
            $notification = array(
                'message' => 'User Updated successfully',
                'type' => 'warning',
            );

            return response()->json($notification);
        } catch (\Exception $e) {

            DB::rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'error',
            );

            return response()->json($notification);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        //

        DB::beginTransaction();

        try {
            //

            $user = User::find($id);

            UserLogs::create([
                'user_id' => \auth()->user()->id,
                'type' => 'auth',
                'content' => "User $user->name Successfully Deleted"
            ]);

            User::destroy($id);

            DB::commit();
            $notification = array(
                'message' => 'User Deleted successfully',
                'type' => 'warning',
            );

            return response()->json($notification);

        } catch (\Exception $e) {

            DB::rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'error',
            );

            return response()->json($notification);
        }
    }

    public function getUsers()
    {
        $user = User::with('roles')->latest()->where('type', 'Official')->get();

        return response()->json($user, 200);
    }

    public function getPlayers()
    {

        $user = User::with('session')->with('agent')->where('status','Active')->where('type','Player')->orderBy('wallet_balance','DESC')->latest()->get();

        return response()->json($user, 200);
    }
    public function getDeactivatePlayers(){
        $user = User::onlyTrashed()->latest()->get();

        return response()->json($user, 200);
    }
    public function restoreAccount(Request $request){
        try {
            if(auth()->user()->type == 'Official'){
                $update = User::onlyTrashed()->where('id',$request->id);
                $update->update([
                    'deleted_at' => null
                ]);
                UserLogs::create([
                    'user_id' => auth()->user()->id,
                    'type' => 'Restore auth',
                    'content' => 'Restored user '.$request->name.'('.$request->id.') successfully'
                ]);
                $notification = array(
                    'message' => 'User has been restored successfully',
                    'type' => 'success',
                );
                return response()->json($notification);
            }
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger',
            );
            return response()->json($notification);
        } catch (\Exception $e) {
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger',
            );
            return response()->json($notification);
        }
    }
    public function getAgents()
    {
        $user = User::with('agent')
            ->orderBy('agent_commission_balance','DESC')
            ->latest()
            ->where('status','Active')
            ->where('type', '!=', 'Player')
            ->where('type', '!=', 'Official')
            ->get();

        return response()->json($user);
    }

    public function showProfile()
    {
        return view('User.profile');
    }

    public function updateProfile(Request $request, $id)
    {
        DB::beginTransaction();

        try {
            //

            $user = User::find($id);
            $user->update($request->all());

            UserLogs::create([
                'user_id' => \auth()->user()->id,
                'type' => 'auth',
                'content' => "User $user->name Profile Successfully Updated"
            ]);

            DB::commit();
            $notification = array(
                'message' => 'Update Profile successfully',
                'type' => 'success',
                'content' => 'Profile'
            );

            return redirect()->back()->with($notification);
        } catch (\Exception $e) {

            DB::rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger',
            );

            return redirect()->back()->with($notification);
        }
    }

    public function updatePassword(Request $request)
    {
        DB::beginTransaction();


        try {
            //

            $request->validate([
                'current-password' => 'required',
                'password' => 'required|confirmed',
            ]);


            if(Hash::check($request->get('current-password'), auth()->user()->password)) {

                User::find(auth()->user()->id)->update($request->only('password'));

                UserLogs::create([
                    'user_id' => \auth()->user()->id,
                    'type' => 'auth',
                    'content' => "Password has been updated"
                ]);

                DB::commit();

                $notification = array(
                    'message' => 'Password Updated Successfully',
                    'type' => 'info',
                    'content' => 'Password'
                );
            }
            else
            {
                $notification = array(
                    'message' => 'There was an error updating your password',
                    'type' => 'warning',
                    'content' => 'Password'
                );

            }
            return redirect()->back()->with($notification);

        } catch (\Exception $e) {

            DB::rollback();
            dd($e);
        }
    }

    public function agentPlayers($id)
    {
        return view('User.agentPlayers');
    }
    public function getCommission(){
        $get = User::select('agent_commission_balance')->where('id',auth()->user()->id)->get();
        if(count($get) > 0){
            $amount = $get[0]->agent_commission_balance;
        }else{
            $amount = 0;
        }
        return response()->json($amount);
    }
    public function transferCommission(Request $request){
        DB::beginTransaction();
        try{
            $validation = User::select('agent_commission_balance','wallet_balance')->where('id',auth()->user()->id)->get();
            if($validation[0]->agent_commission_balance < $request->post('amount')){
                $notification = array(
                    'message' => 'insufficient balance',
                    'type' => 'danger',
                );
                return response()->json($notification);
            }
            DB::commit();
            $add_wallet = $validation[0]->wallet_balance + $request->post('amount');
            $deduc_agent_commission = $validation[0]->agent_commission_balance - $request->post('amount');
            User::where('id',auth()->user()->id)->update(['wallet_balance' => $add_wallet,'agent_commission_balance' => $deduc_agent_commission]);
            WalletHistory::create([
                'user_id' => auth()->user()->id,
                'amount' => $request->post('amount'),
                'type' => auth()->user()->name.' Transfer commission into wallet balance Prev Amount: '.$validation[0]->wallet_balance
            ]);
            UserLogs::create([
                'user_id' => auth()->user()->id,
                'type' => 'Converted Commission',
                'content' => $request->post('amount')
            ]);
            $notification = array(
                'message' => 'Transfer successfully',
                'type' => 'success',
            );
            return response()->json($notification);
        }
        catch (\Exception $e) {
            DB::rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger',
            );
            return response()->json($notification);
        }


        return $request->post('amount');
    } 

    public function myPlayers()
    {
        return view('User.myPlayers');
    }

    public function getMyPlayers()
    {
        $user = User::with('session')
        ->with(['bets' => function($query){
            $query->orderBy('created_at','DESC');
        }])
        ->where('status','Active')
        ->where('type','Player')
        ->where('agent_id', auth()->user()->id)
        ->orderBy('wallet_balance','DESC')->latest()->get();

        return response()->json($user);
    }
    public function getMyInactivePlayers(){
        if(auth()->user()->type == 'Master Agent' || auth()->user()->type == 'Gold Agent' || auth()->user()->type == 'Silver Agent'){
            $user = User::with('session')->where('status','Inactive')->where('type','!=','Official')->where('agent_id', auth()->user()->id)->latest()->get();
        }else if(auth()->user()->type == 'Official'){
            $user = User::where('status','Inactive')
            ->where('type','!=','Official')
            ->where('agent_id','<=',0)
            ->orWhereNull('agent_id')
            ->where('status','Inactive')->latest()->get();
        }
        return response()->json($user);
    }
    public function getDownlineAgents(){
        // $user = User::with('session')->with(['comm'=> function($q){
        //     $q->join('bets','bets.user_id','=','users.id')
        //     ->select('*',DB::raw('SUM(bets.bet_amount) as bet_amount'),DB::raw('SUM(bets.agent_commission_payout) as agent_commission_payout'))
        //     ->groupBy('name');
        // }]
        // )->where('type','!=','Player')->where('agent_id', auth()->user()->id)->orderBy('agent_commission_balance','DESC')->latest()->get();
        $user = \DB::table('users as a')
        ->select('a.*',\DB::raw('COUNT(b.id) as player_count'))
        ->leftjoin('users as b','a.id','=','b.agent_id')
        ->where('a.type','!=','Player')
        ->where('a.agent_id',auth()->user()->id)
        ->orderBy('player_count','DESC')
        ->groupBy('a.id')
        ->get();

        return response()->json($user);
    }

    public function getAgentPlayers($id)
    {
        $user = User::with('session')->where('agent_id', $id)->latest()->get();

        return response()->json($user);
    }

    public function getOtherUsers()
    {
        if(auth()->user()->type === 'Silver Agent'){
            $user = User::where('agent_id', auth()->user()->id)->latest()->get();
        } else {
            $user = User::latest()->get();
        }

        return response()->json($user);
    }

    public function promoteUser(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            //

            $user = User::find($id);
            if($request->type === 'Master Agent'){
                if($request->agent_rate > 0.05){
                    $notification = array(
                        'message' => 'Agent rate shouldn`t greater than 5%',
                        'type' => 'danger',
                    );
                    return response()->json($notification);
                }
            }
            else{
                $agent = User::find($request->agent_id);
                if($request->agent_rate > $agent->agent_rate){
                    $notification = array(
                        'message' => 'Agent rate shouldn`t greater than selected agent',
                        'type' => 'danger',
                    );
                    return response()->json($notification);
                }
            }
            $user->update($request->all());

            UserLogs::create([
                'user_id' => \auth()->user()->id,
                'type' => 'promote',
                'content' => "User $user->name Successfully Promoted to $request->type"
            ]);

            DB::commit();
            $notification = array(
                'message' => 'Promote User successfully',
                'type' => 'success',
            );

            return response()->json($notification);
        } catch (\Exception $e) {

            DB::rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger',
            );

            return response()->json($notification);
        }
    }

    public function walletHistory($id)
    {
        if(auth()->user()->type != 'Official'){
            if(auth()->user()->id != $id){
                abort(404,'Not found');
            }
        }
        return view('User.wallet-history');
    }

    public function onlinePlayers()
    {
        if(auth()->user()->type != 'Official'){
            abort(404,'Not found');
        }
        return view('User.online-players');
    }

    public function getOnlinePlayers()
    {
        $user = UserSession::with('user.agent')->get();


        return response()->json($user);
    }

    public function playerBets($user_id)
    {
        return view('User.player-bets');
    }
    public function DemoteToPlayer(Request $request)
    {
        DB::beginTransaction();
        try {
            $user = User::find($request->user_id);
            $prev_type = $user->type;
            if($user->type == 'Player' || $user->type == 'Official'){
                $notification = array(
                    'message' => 'Selected User cannot be demoted',
                    'type' => 'danger',
                );
                return response()->json($notification);
            }
            //check if agent is available to demote
            $check = User::where('agent_id',$user->id)->get();
            if(count($check) > 0){
                $notification = array(
                    'message' => 'Selected User has downline user it cannot be demoted ',
                    'type' => 'danger',
                );
                return response()->json($notification);
            }
            $user->update(['type' => 'Player','agent_rate' => 0]);
            UserLogs::create([
                'user_id' => auth()->user()->id,
                'type' => 'Demote',
                'content' => $user->name.'('.$user->id.') has been demoted from: '.$prev_type.' to: '.$user->type.' By: '.auth()->user()->name.'('.auth()->user()->id.')'
            ]);
            $notification = array(
                'message' => $user->name.' successfully demoted to '.$user->type,
                'type' => 'success',
            );
            return response()->json($notification);
        } catch (\Exception $e) {
            DB::rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger',
            );
            return response()->json($notification);
        }
        return response()->json();
    }

    public function getWalletHistory($id)
    {
        $walletHistory = WalletHistory::where('user_id', $id)
            ->latest()->get();

        return response()->json($walletHistory);
    }
    public function getUserReports($id){
        $var = $id;
        $ids =[$var];
        $data = [];
        $master = User::where('type','Master Agent')->where('id',$var)->get();
        if(count($master) <= 0){
            abort(404);
        }
        $gold = \DB::table('users')
        ->where('status','active')
        ->where('type','Gold Agent')
        ->where('agent_id',$var)
        ->get();
        $silver = \DB::table('users')
        ->where('type','Silver Agent')
        ->where('status','active')
        ->whereIn('agent_id',function($q)use($var){
            $q->select('b.id')
            ->from('users as a')
            ->leftjoin('users as b','a.id','=','b.agent_id')
            ->where('b.type','Gold Agent')
            ->where('a.id',$var);
        })
        ->get();
        $player = \DB::table('users')
        ->where('type','Player')
        ->where('status','active')
        ->whereIn('agent_id',function($q)use($var){
            $q->select('b.id')
            ->from('users as a')
            ->leftjoin('users as b','a.id','=','b.agent_id')
            ->where('b.type','!=','Player')
            ->where('a.id',$var);
        })->get();
        foreach($master as $v){
            $data[] = ['User ID' => $v->id,'Agent ID' => '','Master Agent' => $v->username,'Gold Agent' => '','Silver Agent'=>'','Player' =>''];
        };
        foreach($gold as $v){
            $data[] =  ['User ID' => $v->id,'Agent ID' => $v->agent_id,'Master Agent' => '','Gold Agent' => $v->username,'Silver Agent'=>'','Player' =>''];
        };
        foreach($silver as $v){
            $data[] =  ['User ID' => $v->id,'Agent ID' => $v->agent_id,'Master Agent' => '','Gold Agent' => '','Silver Agent'=> $v->username,'Player' =>''];
        };
        foreach($player as $v){
            $data[] =  ['User ID' => $v->id,'Agent ID' => $v->agent_id,'Master Agent' => '','Gold Agent' => '','Silver Agent'=>'','Player' => $v->username];
        };
        
        // $data = User::whereIn('id',$ids)->get();

        return response()->json($data);
    }
    public function getAgentCommission()
    {
        $agent = \DB::table('users')
        ->where('type','Silver Agent')
        ->orWhere('type','Gold Agent')
        ->orWhere('type','Master Agent')->get();
        $users = \DB::table('users as a')
        ->select('a.id','a.name','a.agent_id','b.id as bet_id','b.bet_amount','b.bet_result','b.agent_commission_payout')
        ->join('bets as b','b.user_id','=','a.id')
        ->where('a.agent_id','>',0)
        ->where('b.agent_commission_payout','>',0)
        ->get();
        $output = [];
        $output1 = [];
        $test = [];
        $users_collection = collect($users);
        $agent_collection = collect($agent);
        $incorporator = collect($agent_collection->filter(function ($value) {
            return $value->type == 'Master Agent';
        })->values())->toArray();
        for($i= 0; $i < count($incorporator); $i++){
            $master_id = $incorporator[$i]->id;
            $master = collect($agent_collection->filter(function($value) use($master_id){
                return $value->agent_id == $master_id && $value->type == 'Gold Agent';
            })->values())->toArray();
            $inc_players_collection = collect($users_collection->filter(function($value) use($master_id){
                return $value->agent_id == $master_id;
            })->values())->toArray();
            $output[] = [
                'inc_id' => $incorporator[$i]->id,
                'agent_name' => $incorporator[$i]->name,
                'rate' => $incorporator[$i]->agent_rate,
                'type' => $incorporator[$i]->type,
                'rows' => [
                    'master' => $master,
                    'players' => $inc_players_collection,
                ],
                'agent_commision' =>  $incorporator[$i]->agent_commission_balance,
                'total' => 0
            ];
            if(count($output[$i]['rows']['master']) > 0){
                for($x =0 ; $x < count($output[$i]['rows']['master']); $x++){
                    $ag_id = $output[$i]['rows']['master'][$x]->id;
                    $agent = collect($agent_collection->filter(function($value) use($ag_id){
                        return $value->agent_id == $ag_id && $value->type == 'Silver Agent';
                    })->values())->toArray();
                    $master_players_collection = collect($users_collection->filter(function($value) use($ag_id){
                        return $value->agent_id == $ag_id;
                    })->values())->toArray();
                    $output[$i]['rows']['master'][$x] = [
                        'ma_id' => $output[$i]['rows']['master'][$x]->id,
                        'agent_name' => $output[$i]['rows']['master'][$x]->name,
                        'rate' => $output[$i]['rows']['master'][$x]->agent_rate,
                        'type'=> $output[$i]['rows']['master'][$x]->type,
                        'agent_commision' => $output[$i]['rows']['master'][$x]->agent_commission_balance,
                        'rows' => [
                            'Silver Agent' => [],
                            'players' => $master_players_collection,
                        ],
                        'total' => 0
                    ];
                    if(count($agent) > 0){
                        for($j = 0 ; $j < count($agent); $j++){
                            $agent_id = $agent[$j]->id;
                            $players_collection = collect($users_collection->filter(function($value) use($agent_id){
                                return $value->agent_id == $agent_id;
                            })->values())->toArray();
                            $output[$i]['rows']['master'][$x]['rows']['Silver Agent'] = array(
                                ['agent_id' => $agent[$j]->id,
                                'agent_name' => $agent[$j]->name,
                                'rate' => $agent[$j]->agent_rate,
                                'type'=> $agent[$j]->type,
                                'agent_commision' => $agent[$j]->agent_commission_balance,
                                'players' => $players_collection,
                                'total' => 0]
                            );
                        }
                    }
                    // if(count($output[$i]['master'][$x]['Silver Agent']) > 0){
                    //     for($j = 0 ; $j < count($output[$i]['master'][$x]['Silver Agent']); $j++){

                    //     }
                    // }
                }
            }
        }

        // $output[3]['master'][0]['Silver Agent'][0]['test'] = 1;
        // $output2 = $output[3]['master'][0]['Silver Agent'][0]['test'];


        return response()->json($output);
        // for($i = 0 ; $i < count($agent); $i++){
        //     $output[] = ['agent_name' => $agent[$i]->name,
        //     'rate' => $agent[$i]->agent_rate,
        //     'type' => $agent[$i]->type,
        //     'total' => 0
        //     ];
        //     $id = $agent[$i]->id;
        //     $filtered = $collection->filter(function ($value) use($id) {
        //         return $value->agent_id == $id;
        //     });
        //     foreach($filtered as $val){
        //         $output[$i]['total'] += $val->agent_commission_payout;
        //     }
        //     $output[$i]['players'] = $filtered;
        // }
        // $total = array();
        // foreach ($output as $key => $row)
        // {
        //     $total[$key] = $row['total'];
        // }
        // $result = array_multisort($total, SORT_DESC, $output);

        // return response()->json($output);
    }



    public function getAgentCommissionFilter(Request $request){
        $agent = \DB::table('users')
        ->where('type','Silver Agent')->get();
        $users = \DB::table('users as a')
        ->select('a.id','a.name','a.agent_id','b.id as bet_id','b.bet_amount','b.agent_commission_payout')
        ->join('bets as b','b.user_id','=','a.id')
        ->where('a.agent_id','>',0)
        ->where('b.agent_commission_payout','>',0)
        ->whereBetween('b.created_at',[$request->post('from'), $request->post('to')])
        ->get();
        $output = [];
        $collection = collect($users);
        for($i = 0 ; $i < count($agent); $i++){
            $output[] = ['agent_name' => $agent[$i]->name,
            'rate' => $agent[$i]->agent_rate,
            'total' => 0
            ];
            $id = $agent[$i]->id;
            $filtered = $collection->filter(function ($value) use($id) {
                return $value->agent_id == $id;
            });
            foreach($filtered as $val){
                $output[$i]['total'] += $val->agent_commission_payout;
            }
            $output[$i]['players'] = $filtered;
        }
        $total = array();
        foreach ($output as $key => $row)
        {
            $total[$key] = $row['total'];
        }
        array_multisort($total, SORT_DESC, $output);

        return response()->json($output);

    }
    public function myAgentsByType(Request $request){
        return User::where('agent_id',auth()->user()->id)->where('status','Active')->where('type',$request->type)->get();
    }
    public function getmyagents(){
        $agent1 = null;
        $agent2 = null;
        $agent3 = null;
        $data = [];
        // if(!auth()->user()->agent_id){
        //     return response()->json($data);
        // }
        $agent1 = User::where('id',auth()->user()->agent_id)->first();
        if($agent1){
            array_push($data,$agent1);
            // $agent2 = User::where('id',$agent1->agent_id)->first();
            // if($agent2){
            //     array_push($data,$agent2);
            //     $agent3 = User::where('id',$agent2->agent_id)->first();
            //     if($agent3){
            //         array_push($data,$agent3);
            //     }
            // }
        }
        return response()->json($data);
    }
    public function getAgentByType(Request $request)
    {

        $user = User::where('type', $request->type)
                    ->latest()
                    ->get();
        return response()->json($user);
    }
    public function getSelectedAgent(Request $request)
    {
        $user = User::where('type', $request->type)
                    ->where('status','Active')
                    ->latest()
                    ->get();
        return response()->json($user);
    }
    public function transferUser(Request $request,$id){
        DB::beginTransaction();
        try {
            DB::commit();
            $user = User::find($id);
            $user->update(['agent_id' => $request->agent_id]);
            $notification = array(
                'message' => 'Transferring user successfully',
                'type' => 'success',
            );
            UserLogs::create([
                'user_id' => auth()->user()->id,
                'type' => 'Transfer_User',
                'content' => $user->name.'('.$id.')'.' Successfully transferred Inc/MA/A to Agent Id: ('.$request->agent_id.')'
            ]);
            return response()->json($notification);
        } catch (\Exception $e) {
            DB::rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger',
            );

            return response()->json($notification);
        }

    }

    public function getAllPlayers()
    {
        if(auth()->user()->type == 'Player'){
            $players = User::latest()->where('type', '!=', 'Official')->get();
        }else{
            $players = User::latest()->where('type', '!=', 'Official')->where('agent_id',auth()->user()->id)->get();
        }
        return response()->json($players);
    }

    public function walletTransfer()
    {
        return view('User.wallet-transfer');
    }

    public function transferWallet(Request $request)
    {
        DB::beginTransaction();

        try {
            
            $wallet = User::find(auth()->user()->id);
            $check_password = \Hash::check($request->password, $wallet->password);
            if(!$check_password){
                $notification = array(
                    'message' => 'Password Doesn`t match' ,
                    'type' => 'danger',
                );
                return response()->json($notification);
            }
            if ($wallet->wallet_balance >= $request->post('amount')) {

                $user = User::find($request->post('user_id'));
                $prev = $user->wallet_balance;
                $wallet->decrement('wallet_balance', $request->post('amount'));
                if($user->increment('wallet_balance', $request->post('amount'))){
                    UserLogs::create([
                        'user_id' => \auth()->user()->id,
                        'type' => 'Wallet_Transfer',
                        'content' => "Wallet has been transferred from Id:($wallet->id) $wallet->name to Id:($user->id) $user->name with amount of ".$request->post('amount')
                        .' Bef: '.$prev.' Aft:'.$user->wallet_balance
                    ]);
                    WalletHistory::insert([[
                        'user_id'=> \auth()->user()->id,
                        'amount' => $request->post('amount'),
                        'type' => "Wallet has been transferred from Id:($wallet->id) $wallet->name to Id:($user->id) $user->name with amount of ".$request->post('amount'),
                        'details' => $request->post('details'),
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                        ],[
                        'user_id' => $request->post('user_id'),
                        'amount' => $request->post('amount'),
                        'type' => $wallet->name.'( '.$wallet->id.' ) has transferred wallet balance to your account',
                        'details' => $request->post('details'),
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                        ]]
                    );
                }else{
                    UserLogs::create([
                        'user_id' => \auth()->user()->id,
                        'type' => 'Wallet_Transfer',
                        'content' => "Wallet Failed to transferred from Id:($wallet->id) $wallet->name to Id:($user->id) $user->name with amount of ".$request->post('amount')
                        .' Bef: '.$prev.' Aft:'.$user->wallet_balance
                    ]);
                    WalletHistory::create([
                        'user_id'=> \auth()->user()->id,
                        'amount' => $request->post('amount'),
                        'type' => "Wallet Failed to transferred from Id:($wallet->id) $wallet->name to Id:($user->id) $user->name with amount of ".$request->post('amount')
                    ]);
                }
                DB::commit();
                $notification = array(
                    'message' => 'Wallet Balance Transferred Successfully',
                    'type' => 'success',
                );

                return response()->json($notification);
            }

            $notification = array(
                'message' => 'Insufficient Wallet Balance',
                'type' => 'danger',
            );

            return response()->json($notification);


        } catch (\Exception $e) {

            DB::rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger',
            );

            return response()->json($notification);
        }
    }
    public function view_agent_players($id){
        if(auth()->user()->type != 'Master Agent' && auth()->user()->type != 'Gold Agent' && auth()->user()->type != 'Official'){
            abort(403,'Unauthorized');
        }
        $data = \DB::table('users as a')
        ->select('a.*',\DB::raw('COUNT(b.user_id) as bet_count'))
        ->leftjoin('bets as b','b.user_id','=','a.id')
        ->where('type','Player')
        ->where('agent_id',$id)
        ->groupBy('a.id')
        ->get();
        return view('User.view_agent_players',compact('data'));
    }
    public function view_agent_downline($id){
        if(auth()->user()->type != 'Master Agent' && auth()->user()->type != 'Gold Agent' && auth()->user()->type != 'Official'){
            abort(403,'Unauthorized');
        }
        $data = \DB::table('users as a')
        ->select('a.*',\DB::raw('COUNT(b.id) as player_count'))
        ->leftjoin('users as b','b.agent_id','=','a.id')
        ->where('a.type','!=','Player')
        ->where('a.status','Active')
        ->where('a.agent_id',$id)
        ->groupBy('a.id')
        ->get();
        return view('User.view_agent_downline',compact('data'));
    }
    public function view_agent_players_bet($id){
        if(auth()->user()->type != 'Master Agent' && auth()->user()->type != 'Gold Agent' && auth()->user()->type != 'Official'){
            abort(403,'Unauthorized');
        }
        $data = \DB::table('bets as a')
        ->select('a.*','c.game_title','b.game_number','d.left_win','d.middle_win','d.right_win')
        ->join('games as b','a.game_id','=','b.id')
        ->join('game_sessions as c','b.game_session_id','=','c.id')
        ->join('matches as d','d.game_id','=','a.game_id')
        ->orderBy('a.id','DESC')
        ->where('a.user_id',$id)->get();
        return view('User.view_agent_players_bet',compact('data'));
    }
}
=======
<?php

namespace App\Http\Controllers;

use App\Bets;
use App\Http\Requests\UserRequest;
use App\User;
use App\UserLogs;
use App\UserSession;
use App\WalletHistory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use function GuzzleHttp\default_ca_bundle;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        //
        if(!auth()->user()->can('user-management')){
            abort(403, 'Unauthorized');
        }

        $data['users'] = User::all()->sortByDesc('name');

        return view('User.index', $data);
    }
    public function deactivate(){
        if(!auth()->user()->can('user-management')){
            abort(403, 'Unauthorized');
        }
        return view('User.user_deactivate');
    }
    public function inactiveAccounts(){
        if(!auth()->user()->can('user-management')){
            abort(403, 'Unauthorized');
        }
        return view('User.myInactivePlayers');
    }
    public function agentBypassLimit($id)
    {

        $user = User::find($id);

        if ($user->agent_limit === 1){



            $user->update([
                'agent_limit' => 0
            ]);
        } else {
            $user->update([
                'agent_limit' => 1
            ]);
        }

    }
    public function getUserReportsFiltered(Request $request){
        $from = $request->from;
        $to = $request->to;
        $users = User::select('id','name','contact_number','wallet_balance','type','created_at')->where('wallet_balance','>',0)->whereBetween('created_at',[$from, $to])->get();
        $history = WalletHistory::get();
        $collection = collect($history);
        $output = [];
        for($i = 0 ; $i < count($users); $i++){
            $output[] = ['name' => $users[$i]->name,
            'contact_number' => $users[$i]->contact_number,
            'wallet_balance' => $users[$i]->wallet_balance,
            'type' => $users[$i]->type,
            'created_at' => $users[$i]->created_at];
            $id = $users[$i]->id;
            $filtered = $collection->filter(function ($value) use($id) {
                return $value->user_id == $id;
            });
            $output[$i]['history'] = $filtered;
        }
        return response()->json($output);
    }
    public function player()
    {
        //
        if(!auth()->user()->can('user-management')){
            abort(403, 'Unauthorized');
        }

        return view('User.player');
    }

    public function agent()
    {
        if(!auth()->user()->can('user-management')){
            abort(403, 'Unauthorized');
        }

        return view('User.agent');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        //

        DB::beginTransaction();

        try {
            $validation = User::where('contact_number',$request->post('contact_number'))
            ->orWhere('username',$request->post('username'))->get();
            if(count($validation) > 0){
                $notification = array(
                    'message' => 'Invalid request Username,Contact Number is already exist',
                    'type' => 'danger',
                );
                return response()->json($notification);
            }
            $request->merge([
                'otp' => random_int(1000, 9999)
            ]);
            $user = User::create($request->all());
            $user->syncRoles($request->post('role_id'));
            $name = $request->post('name');

            UserLogs::create([
                'user_id' => \auth()->user()->id,
                'type' => 'auth',
                'content' => "User $name Successfully Created"
            ]);

            DB::commit();
            $notification = array(
                'message' => 'User Created successfully',
                'type' => 'success',
            );

            return response()->json($notification);
        } catch (\Exception $e) {

            DB::rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger',
            );

            return response()->json($notification);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        //

        DB::beginTransaction();

        try {
            //

            $user = User::find($id);

            $role = $request->post('role_id');
            if (!empty($role)){

                UserLogs::create([
                    'user_id' => \auth()->user()->id,
                    'type' => 'auth',
                    'content' => "User $user->name Role has been updated"
                ]);
                $user->syncRoles($request->post('role_id'));

            }
            $user->update($request->all());

            UserLogs::create([
                'user_id' => \auth()->user()->id,
                'type' => 'auth',
                'content' => "User $user->name Information Successfully Updated"
            ]);

            DB::commit();
            $notification = array(
                'message' => 'User Updated successfully',
                'type' => 'warning',
            );

            return response()->json($notification);
        } catch (\Exception $e) {

            DB::rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'error',
            );

            return response()->json($notification);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        //

        DB::beginTransaction();

        try {
            //

            $user = User::find($id);

            UserLogs::create([
                'user_id' => \auth()->user()->id,
                'type' => 'auth',
                'content' => "User $user->name Successfully Deleted"
            ]);

            User::destroy($id);

            DB::commit();
            $notification = array(
                'message' => 'User Deleted successfully',
                'type' => 'warning',
            );

            return response()->json($notification);

        } catch (\Exception $e) {

            DB::rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'error',
            );

            return response()->json($notification);
        }
    }

    public function getUsers()
    {
        $user = User::with('roles')->latest()->where('type', 'Official')->get();

        return response()->json($user, 200);
    }

    public function getPlayers()
    {

        $user = User::with('session')->with('agent')->where('status','Active')->where('type','Player')->orderBy('wallet_balance','DESC')->latest()->get();

        return response()->json($user, 200);
    }
    public function getDeactivatePlayers(){
        $user = User::onlyTrashed()->latest()->get();

        return response()->json($user, 200);
    }
    public function restoreAccount(Request $request){
        try {
            if(auth()->user()->type == 'Official'){
                $update = User::onlyTrashed()->where('id',$request->id);
                $update->update([
                    'deleted_at' => null
                ]);
                UserLogs::create([
                    'user_id' => auth()->user()->id,
                    'type' => 'Restore auth',
                    'content' => 'Restored user '.$request->name.'('.$request->id.') successfully'
                ]);
                $notification = array(
                    'message' => 'User has been restored successfully',
                    'type' => 'success',
                );
                return response()->json($notification);
            }
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger',
            );
            return response()->json($notification);
        } catch (\Exception $e) {
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger',
            );
            return response()->json($notification);
        }
    }
    public function getAgents()
    {
        $user = User::with('agent')
            ->orderBy('agent_commission_balance','DESC')
            ->latest()
            ->where('status','Active')
            ->where('type', '!=', 'Player')
            ->where('type', '!=', 'Official')
            ->get();

        return response()->json($user);
    }

    public function showProfile()
    {
        return view('User.profile');
    }

    public function updateProfile(Request $request, $id)
    {
        DB::beginTransaction();

        try {
            //

            $user = User::find($id);
            $user->update($request->all());

            UserLogs::create([
                'user_id' => \auth()->user()->id,
                'type' => 'auth',
                'content' => "User $user->name Profile Successfully Updated"
            ]);

            DB::commit();
            $notification = array(
                'message' => 'Update Profile successfully',
                'type' => 'success',
                'content' => 'Profile'
            );

            return redirect()->back()->with($notification);
        } catch (\Exception $e) {

            DB::rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger',
            );

            return redirect()->back()->with($notification);
        }
    }

    public function updatePassword(Request $request)
    {
        DB::beginTransaction();


        try {
            //

            $request->validate([
                'current-password' => 'required',
                'password' => 'required|confirmed',
            ]);


            if(Hash::check($request->get('current-password'), auth()->user()->password)) {

                User::find(auth()->user()->id)->update($request->only('password'));

                UserLogs::create([
                    'user_id' => \auth()->user()->id,
                    'type' => 'auth',
                    'content' => "Password has been updated"
                ]);

                DB::commit();

                $notification = array(
                    'message' => 'Password Updated Successfully',
                    'type' => 'info',
                    'content' => 'Password'
                );
            }
            else
            {
                $notification = array(
                    'message' => 'There was an error updating your password',
                    'type' => 'warning',
                    'content' => 'Password'
                );

            }
            return redirect()->back()->with($notification);

        } catch (\Exception $e) {

            DB::rollback();
            dd($e);
        }
    }

    public function agentPlayers($id)
    {
        return view('User.agentPlayers');
    }
    public function getCommission(){
        $get = User::select('agent_commission_balance')->where('id',auth()->user()->id)->get();
        if(count($get) > 0){
            $amount = $get[0]->agent_commission_balance;
        }else{
            $amount = 0;
        }
        return response()->json($amount);
    }
    public function transferCommission(Request $request){
        DB::beginTransaction();
        try{
            $validation = User::select('agent_commission_balance','wallet_balance')->where('id',auth()->user()->id)->get();
            if($validation[0]->agent_commission_balance < $request->post('amount')){
                $notification = array(
                    'message' => 'insufficient balance',
                    'type' => 'danger',
                );
                return response()->json($notification);
            }
            DB::commit();
            $add_wallet = $validation[0]->wallet_balance + $request->post('amount');
            $deduc_agent_commission = $validation[0]->agent_commission_balance - $request->post('amount');
            User::where('id',auth()->user()->id)->update(['wallet_balance' => $add_wallet,'agent_commission_balance' => $deduc_agent_commission]);
            WalletHistory::create([
                'user_id' => auth()->user()->id,
                'amount' => $request->post('amount'),
                'type' => auth()->user()->name.' Transfer commission into wallet balance Prev Amount: '.$validation[0]->wallet_balance
            ]);
            UserLogs::create([
                'user_id' => auth()->user()->id,
                'type' => 'Converted Commission',
                'content' => $request->post('amount')
            ]);
            $notification = array(
                'message' => 'Transfer successfully',
                'type' => 'success',
            );
            return response()->json($notification);
        }
        catch (\Exception $e) {
            DB::rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger',
            );
            return response()->json($notification);
        }


        return $request->post('amount');
    } 

    public function myPlayers()
    {
        if(auth()->user()->type == 'Official' || auth()->user()->type == 'Player'){
            abort(404);
        }
        return view('User.myPlayers');
    }

    public function getMyPlayers()
    {
        $user = User::with('session')
        ->with(['bets' => function($query){
            $query->orderBy('created_at','DESC');
        }])
        ->where('status','Active')
        ->where('type','Player')
        ->where('agent_id', auth()->user()->id)
        ->orderBy('wallet_balance','DESC')->latest()->get();

        return response()->json($user);
    }
    public function getMyInactivePlayers(){
        if(auth()->user()->type == 'Master Agent' || auth()->user()->type == 'Gold Agent' || auth()->user()->type == 'Silver Agent'){
            $user = User::with('session')->where('status','Inactive')->where('type','!=','Official')->where('agent_id', auth()->user()->id)->latest()->get();
        }else if(auth()->user()->type == 'Official'){
            $user = User::where('status','Inactive')
            ->where('type','!=','Official')
            ->where('agent_id','<=',0)
            ->orWhereNull('agent_id')
            ->where('status','Inactive')->latest()->get();
        }
        return response()->json($user);
    }
    public function getDownlineAgents(){
        // $user = User::with('session')->with(['comm'=> function($q){
        //     $q->join('bets','bets.user_id','=','users.id')
        //     ->select('*',DB::raw('SUM(bets.bet_amount) as bet_amount'),DB::raw('SUM(bets.agent_commission_payout) as agent_commission_payout'))
        //     ->groupBy('name');
        // }]
        // )->where('type','!=','Player')->where('agent_id', auth()->user()->id)->orderBy('agent_commission_balance','DESC')->latest()->get();
        $user = \DB::table('users as a')
        ->select('a.*',\DB::raw('COUNT(b.id) as player_count'))
        ->leftjoin('users as b','a.id','=','b.agent_id')
        ->where('a.type','!=','Player')
        ->where('a.agent_id',auth()->user()->id)
        ->orderBy('player_count','DESC')
        ->groupBy('a.id')
        ->get();

        return response()->json($user);
    }

    public function getAgentPlayers($id)
    {
        $user = User::with('session')->where('agent_id', $id)->latest()->get();

        return response()->json($user);
    }

    public function getOtherUsers()
    {
        if(auth()->user()->type === 'Silver Agent'){
            $user = User::where('agent_id', auth()->user()->id)->latest()->get();
        } else {
            $user = User::latest()->get();
        }

        return response()->json($user);
    }

    public function promoteUser(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            //

            $user = User::find($id);
            if($request->type === 'Master Agent'){
                if($request->agent_rate > 0.05){
                    $notification = array(
                        'message' => 'Agent rate shouldn`t greater than 5%',
                        'type' => 'danger',
                    );
                    return response()->json($notification);
                }
            }
            else{
                $agent = User::find($request->agent_id);
                if($request->agent_rate > $agent->agent_rate){
                    $notification = array(
                        'message' => 'Agent rate shouldn`t greater than selected agent',
                        'type' => 'danger',
                    );
                    return response()->json($notification);
                }
            }
            $user->update($request->all());

            UserLogs::create([
                'user_id' => \auth()->user()->id,
                'type' => 'promote',
                'content' => "User $user->name Successfully Promoted to $request->type"
            ]);

            DB::commit();
            $notification = array(
                'message' => 'Promote User successfully',
                'type' => 'success',
            );

            return response()->json($notification);
        } catch (\Exception $e) {

            DB::rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger',
            );

            return response()->json($notification);
        }
    }

    public function walletHistory($id)
    {
        if(auth()->user()->type != 'Official'){
            if(auth()->user()->id != $id){
                abort(403,'Not found');
            }
        }
        if(auth()->user()->type == 'Official'){
            abort(403);
        }
        return view('User.wallet-history');
    }

    public function onlinePlayers()
    {
        if(auth()->user()->type != 'Official'){
            abort(404,'Not found');
        }
        return view('User.online-players');
    }

    public function getOnlinePlayers()
    {
        $user = UserSession::with('user.agent')->get();


        return response()->json($user);
    }

    public function playerBets($user_id)
    {
        return view('User.player-bets');
    }
    public function DemoteToPlayer(Request $request)
    {
        DB::beginTransaction();
        try {
            $user = User::find($request->user_id);
            $prev_type = $user->type;
            if($user->type == 'Player' || $user->type == 'Official'){
                $notification = array(
                    'message' => 'Selected User cannot be demoted',
                    'type' => 'danger',
                );
                return response()->json($notification);
            }
            //check if agent is available to demote
            $check = User::where('agent_id',$user->id)->get();
            if(count($check) > 0){
                $notification = array(
                    'message' => 'Selected User has downline user it cannot be demoted ',
                    'type' => 'danger',
                );
                return response()->json($notification);
            }
            $user->update(['type' => 'Player','agent_rate' => 0]);
            UserLogs::create([
                'user_id' => auth()->user()->id,
                'type' => 'Demote',
                'content' => $user->name.'('.$user->id.') has been demoted from: '.$prev_type.' to: '.$user->type.' By: '.auth()->user()->name.'('.auth()->user()->id.')'
            ]);
            $notification = array(
                'message' => $user->name.' successfully demoted to '.$user->type,
                'type' => 'success',
            );
            return response()->json($notification);
        } catch (\Exception $e) {
            DB::rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger',
            );
            return response()->json($notification);
        }
        return response()->json();
    }

    public function getWalletHistory($id)
    {
        $walletHistory = WalletHistory::where('user_id', $id)
            ->latest()->get();

        return response()->json($walletHistory);
    }
    public function getUserReports($id){
        $var = $id;
        $ids =[$var];
        $data = [];
        $master = User::where('type','Master Agent')->where('id',$var)->get();
        if(count($master) <= 0){
            abort(404);
        }
        $gold = \DB::table('users')
        ->where('status','active')
        ->where('type','Gold Agent')
        ->where('agent_id',$var)
        ->get();
        $silver = \DB::table('users')
        ->where('type','Silver Agent')
        ->where('status','active')
        ->whereIn('agent_id',function($q)use($var){
            $q->select('b.id')
            ->from('users as a')
            ->leftjoin('users as b','a.id','=','b.agent_id')
            ->where('b.type','Gold Agent')
            ->where('a.id',$var);
        })
        ->get();
        $player = \DB::table('users')
        ->where('type','Player')
        ->where('status','active')
        ->whereIn('agent_id',function($q)use($var){
            $q->select('b.id')
            ->from('users as a')
            ->leftjoin('users as b','a.id','=','b.agent_id')
            ->where('b.type','!=','Player')
            ->where('a.id',$var);
        })->get();
        foreach($master as $v){
            $data[] = ['User ID' => $v->id,'Agent ID' => '','Master Agent' => $v->username,'Gold Agent' => '','Silver Agent'=>'','Player' =>''];
        };
        foreach($gold as $v){
            $data[] =  ['User ID' => $v->id,'Agent ID' => $v->agent_id,'Master Agent' => '','Gold Agent' => $v->username,'Silver Agent'=>'','Player' =>''];
        };
        foreach($silver as $v){
            $data[] =  ['User ID' => $v->id,'Agent ID' => $v->agent_id,'Master Agent' => '','Gold Agent' => '','Silver Agent'=> $v->username,'Player' =>''];
        };
        foreach($player as $v){
            $data[] =  ['User ID' => $v->id,'Agent ID' => $v->agent_id,'Master Agent' => '','Gold Agent' => '','Silver Agent'=>'','Player' => $v->username];
        };
        
        // $data = User::whereIn('id',$ids)->get();

        return response()->json($data);
    }
    public function getAgentCommission()
    {
        $agent = \DB::table('users')
        ->where('type','Silver Agent')
        ->orWhere('type','Gold Agent')
        ->orWhere('type','Master Agent')->get();
        $users = \DB::table('users as a')
        ->select('a.id','a.name','a.agent_id','b.id as bet_id','b.bet_amount','b.bet_result','b.agent_commission_payout')
        ->join('bets as b','b.user_id','=','a.id')
        ->where('a.agent_id','>',0)
        ->where('b.agent_commission_payout','>',0)
        ->get();
        $output = [];
        $output1 = [];
        $test = [];
        $users_collection = collect($users);
        $agent_collection = collect($agent);
        $incorporator = collect($agent_collection->filter(function ($value) {
            return $value->type == 'Master Agent';
        })->values())->toArray();
        for($i= 0; $i < count($incorporator); $i++){
            $master_id = $incorporator[$i]->id;
            $master = collect($agent_collection->filter(function($value) use($master_id){
                return $value->agent_id == $master_id && $value->type == 'Gold Agent';
            })->values())->toArray();
            $inc_players_collection = collect($users_collection->filter(function($value) use($master_id){
                return $value->agent_id == $master_id;
            })->values())->toArray();
            $output[] = [
                'inc_id' => $incorporator[$i]->id,
                'agent_name' => $incorporator[$i]->name,
                'rate' => $incorporator[$i]->agent_rate,
                'type' => $incorporator[$i]->type,
                'rows' => [
                    'master' => $master,
                    'players' => $inc_players_collection,
                ],
                'agent_commision' =>  $incorporator[$i]->agent_commission_balance,
                'total' => 0
            ];
            if(count($output[$i]['rows']['master']) > 0){
                for($x =0 ; $x < count($output[$i]['rows']['master']); $x++){
                    $ag_id = $output[$i]['rows']['master'][$x]->id;
                    $agent = collect($agent_collection->filter(function($value) use($ag_id){
                        return $value->agent_id == $ag_id && $value->type == 'Silver Agent';
                    })->values())->toArray();
                    $master_players_collection = collect($users_collection->filter(function($value) use($ag_id){
                        return $value->agent_id == $ag_id;
                    })->values())->toArray();
                    $output[$i]['rows']['master'][$x] = [
                        'ma_id' => $output[$i]['rows']['master'][$x]->id,
                        'agent_name' => $output[$i]['rows']['master'][$x]->name,
                        'rate' => $output[$i]['rows']['master'][$x]->agent_rate,
                        'type'=> $output[$i]['rows']['master'][$x]->type,
                        'agent_commision' => $output[$i]['rows']['master'][$x]->agent_commission_balance,
                        'rows' => [
                            'Silver Agent' => [],
                            'players' => $master_players_collection,
                        ],
                        'total' => 0
                    ];
                    if(count($agent) > 0){
                        for($j = 0 ; $j < count($agent); $j++){
                            $agent_id = $agent[$j]->id;
                            $players_collection = collect($users_collection->filter(function($value) use($agent_id){
                                return $value->agent_id == $agent_id;
                            })->values())->toArray();
                            $output[$i]['rows']['master'][$x]['rows']['Silver Agent'] = array(
                                ['agent_id' => $agent[$j]->id,
                                'agent_name' => $agent[$j]->name,
                                'rate' => $agent[$j]->agent_rate,
                                'type'=> $agent[$j]->type,
                                'agent_commision' => $agent[$j]->agent_commission_balance,
                                'players' => $players_collection,
                                'total' => 0]
                            );
                        }
                    }
                    // if(count($output[$i]['master'][$x]['Silver Agent']) > 0){
                    //     for($j = 0 ; $j < count($output[$i]['master'][$x]['Silver Agent']); $j++){

                    //     }
                    // }
                }
            }
        }

        // $output[3]['master'][0]['Silver Agent'][0]['test'] = 1;
        // $output2 = $output[3]['master'][0]['Silver Agent'][0]['test'];


        return response()->json($output);
        // for($i = 0 ; $i < count($agent); $i++){
        //     $output[] = ['agent_name' => $agent[$i]->name,
        //     'rate' => $agent[$i]->agent_rate,
        //     'type' => $agent[$i]->type,
        //     'total' => 0
        //     ];
        //     $id = $agent[$i]->id;
        //     $filtered = $collection->filter(function ($value) use($id) {
        //         return $value->agent_id == $id;
        //     });
        //     foreach($filtered as $val){
        //         $output[$i]['total'] += $val->agent_commission_payout;
        //     }
        //     $output[$i]['players'] = $filtered;
        // }
        // $total = array();
        // foreach ($output as $key => $row)
        // {
        //     $total[$key] = $row['total'];
        // }
        // $result = array_multisort($total, SORT_DESC, $output);

        // return response()->json($output);
    }



    public function getAgentCommissionFilter(Request $request){
        $agent = \DB::table('users')
        ->where('type','Silver Agent')->get();
        $users = \DB::table('users as a')
        ->select('a.id','a.name','a.agent_id','b.id as bet_id','b.bet_amount','b.agent_commission_payout')
        ->join('bets as b','b.user_id','=','a.id')
        ->where('a.agent_id','>',0)
        ->where('b.agent_commission_payout','>',0)
        ->whereBetween('b.created_at',[$request->post('from'), $request->post('to')])
        ->get();
        $output = [];
        $collection = collect($users);
        for($i = 0 ; $i < count($agent); $i++){
            $output[] = ['agent_name' => $agent[$i]->name,
            'rate' => $agent[$i]->agent_rate,
            'total' => 0
            ];
            $id = $agent[$i]->id;
            $filtered = $collection->filter(function ($value) use($id) {
                return $value->agent_id == $id;
            });
            foreach($filtered as $val){
                $output[$i]['total'] += $val->agent_commission_payout;
            }
            $output[$i]['players'] = $filtered;
        }
        $total = array();
        foreach ($output as $key => $row)
        {
            $total[$key] = $row['total'];
        }
        array_multisort($total, SORT_DESC, $output);

        return response()->json($output);

    }
    public function myAgentsByType(Request $request){
        return User::where('agent_id',auth()->user()->id)->where('status','Active')->where('type',$request->type)->get();
    }
    public function getmyagents(){
        $agent1 = null;
        $agent2 = null;
        $agent3 = null;
        $data = [];
        // if(!auth()->user()->agent_id){
        //     return response()->json($data);
        // }
        $agent1 = User::where('id',auth()->user()->agent_id)->first();
        if($agent1){
            array_push($data,$agent1);
            // $agent2 = User::where('id',$agent1->agent_id)->first();
            // if($agent2){
            //     array_push($data,$agent2);
            //     $agent3 = User::where('id',$agent2->agent_id)->first();
            //     if($agent3){
            //         array_push($data,$agent3);
            //     }
            // }
        }
        return response()->json($data);
    }
    public function getAgentByType(Request $request)
    {

        $user = User::where('type', $request->type)
                    ->latest()
                    ->get();
        return response()->json($user);
    }
    public function getSelectedAgent(Request $request)
    {
        $user = User::where('type', $request->type)
                    ->where('status','Active')
                    ->latest()
                    ->get();
        return response()->json($user);
    }
    public function transferUser(Request $request,$id){
        DB::beginTransaction();
        try {
            DB::commit();
            $user = User::find($id);
            $user->update(['agent_id' => $request->agent_id]);
            $notification = array(
                'message' => 'Transferring user successfully',
                'type' => 'success',
            );
            UserLogs::create([
                'user_id' => auth()->user()->id,
                'type' => 'Transfer_User',
                'content' => $user->name.'('.$id.')'.' Successfully transferred Inc/MA/A to Agent Id: ('.$request->agent_id.')'
            ]);
            return response()->json($notification);
        } catch (\Exception $e) {
            DB::rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger',
            );

            return response()->json($notification);
        }

    }

    public function getAllPlayers()
    {
        if(auth()->user()->type == 'Player'){
            $players = User::latest()->where('type', '!=', 'Official')->get();
        }else{
            $players = User::latest()->where('type', '!=', 'Official')->where('agent_id',auth()->user()->id)->get();
        }
        return response()->json($players);
    }

    public function walletTransfer()
    {
        if(auth()->user()->type == 'Official'){
            abort(403);
        }
        return view('User.wallet-transfer');
    }

    public function transferWallet(Request $request)
    {
        DB::beginTransaction();

        try {
            
            $wallet = User::find(auth()->user()->id);
            $check_password = \Hash::check($request->password, $wallet->password);
            if(!$check_password){
                $notification = array(
                    'message' => 'Password Doesn`t match' ,
                    'type' => 'danger',
                );
                return response()->json($notification);
            }
            if ($wallet->wallet_balance >= $request->post('amount')) {

                $user = User::find($request->post('user_id'));
                $prev = $user->wallet_balance;
                $wallet->decrement('wallet_balance', $request->post('amount'));
                if($user->increment('wallet_balance', $request->post('amount'))){
                    UserLogs::create([
                        'user_id' => \auth()->user()->id,
                        'type' => 'Wallet_Transfer',
                        'content' => "Wallet has been transferred from Id:($wallet->id) $wallet->name to Id:($user->id) $user->name with amount of ".$request->post('amount')
                        .' Bef: '.$prev.' Aft:'.$user->wallet_balance
                    ]);
                    WalletHistory::insert([[
                        'user_id'=> \auth()->user()->id,
                        'amount' => $request->post('amount'),
                        'type' => "Wallet has been transferred from Id:($wallet->id) $wallet->name to Id:($user->id) $user->name with amount of ".$request->post('amount'),
                        'details' => $request->post('details'),
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                        ],[
                        'user_id' => $request->post('user_id'),
                        'amount' => $request->post('amount'),
                        'type' => $wallet->name.'( '.$wallet->id.' ) has transferred wallet balance to your account',
                        'details' => $request->post('details'),
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                        ]]
                    );
                }else{
                    UserLogs::create([
                        'user_id' => \auth()->user()->id,
                        'type' => 'Wallet_Transfer',
                        'content' => "Wallet Failed to transferred from Id:($wallet->id) $wallet->name to Id:($user->id) $user->name with amount of ".$request->post('amount')
                        .' Bef: '.$prev.' Aft:'.$user->wallet_balance
                    ]);
                    WalletHistory::create([
                        'user_id'=> \auth()->user()->id,
                        'amount' => $request->post('amount'),
                        'type' => "Wallet Failed to transferred from Id:($wallet->id) $wallet->name to Id:($user->id) $user->name with amount of ".$request->post('amount')
                    ]);
                }
                DB::commit();
                $notification = array(
                    'message' => 'Wallet Balance Transferred Successfully',
                    'type' => 'success',
                );

                return response()->json($notification);
            }

            $notification = array(
                'message' => 'Insufficient Wallet Balance',
                'type' => 'danger',
            );

            return response()->json($notification);


        } catch (\Exception $e) {

            DB::rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger',
            );

            return response()->json($notification);
        }
    }
    public function view_agent_players($id){
        if(auth()->user()->type != 'Master Agent' && auth()->user()->type != 'Gold Agent' && auth()->user()->type != 'Official'){
            abort(403,'Unauthorized');
        }
        $data = \DB::table('users as a')
        ->select('a.*',\DB::raw('COUNT(b.user_id) as bet_count'))
        ->leftjoin('bets as b','b.user_id','=','a.id')
        ->where('type','Player')
        ->where('agent_id',$id)
        ->groupBy('a.id')
        ->get();
        return view('User.view_agent_players',compact('data'));
    }
    public function view_agent_downline($id){
        if(auth()->user()->type != 'Master Agent' && auth()->user()->type != 'Gold Agent' && auth()->user()->type != 'Official'){
            abort(403,'Unauthorized');
        }
        $data = \DB::table('users as a')
        ->select('a.*',\DB::raw('COUNT(b.id) as player_count'))
        ->leftjoin('users as b','b.agent_id','=','a.id')
        ->where('a.type','!=','Player')
        ->where('a.status','Active')
        ->where('a.agent_id',$id)
        ->groupBy('a.id')
        ->get();
        return view('User.view_agent_downline',compact('data'));
    }
    public function view_agent_players_bet($id){
        if(auth()->user()->type != 'Master Agent' && auth()->user()->type != 'Gold Agent' && auth()->user()->type != 'Official'){
            abort(403,'Unauthorized');
        }
        $data = \DB::table('bets as a')
        ->select('a.*','c.game_title','b.game_number','d.left_win','d.middle_win','d.right_win')
        ->join('games as b','a.game_id','=','b.id')
        ->join('game_sessions as c','b.game_session_id','=','c.id')
        ->join('matches as d','d.game_id','=','a.game_id')
        ->orderBy('a.id','DESC')
        ->where('a.user_id',$id)->get();
        return view('User.view_agent_players_bet',compact('data'));
    }
}
>>>>>>> ae5b3d3ce322a6f5ff74d79fe1c0fb883afacf4c
