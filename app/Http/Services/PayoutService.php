<?php


namespace App\Http\Services;


use App\Bets;
use App\Game;
use App\Matches;
use App\User;
use App\WinningStreak;
use Carbon\Carbon;
use App\BankAccounts;
use function Sodium\increment;

class PayoutService
{
    /**
     * @var BetService
     */
    private $betService;
    private $bulkService;

    /**
     * PayoutService constructor.
     * @param BetService $betService
     */
    public function __construct(BetService $betService,BulkUpdateService $bulkService)
    {
        $this->betService = $betService;
        $this->bulkService = $bulkService;
    }

    /**
     * @param $request
     * @param $id
     */
    public function processWinner($request, $id)
    {
        $request->merge([
            'status' => 'Pending',
            'created_by' => auth()->user()->id
        ]);
        $this->updateGameStatus($request);
        $this->updateMatches($request, $id);
        $this->updateBets($request->post('id'));
        $this->updateWinningStreak($request->post('id'), $request);
    }

    public function cancelGame($request, $id)
    {
        $request->merge([
            'created_by' => auth()->user()->id
        ]);
        $this->backBets($request->post('id'));
    }
    public function drawGame($request,$id){
        $partner_rake = 0;
        $net_payout = 0;
        $deduction_banker = 0;
        $agent_commission_payout=0;
        $request->merge([
            'created_by' => auth()->user()->id
        ]);
        
        $check = BankAccounts::where('category','Banker')->where('banker',1)->first();
        if($check->rake){
            $partner_rake = $this->betService->getRake();
        }
        // $bets = $this->getBets($request->post('id'));
        $bets = Bets::select('bets.*','users.wallet_balance')->where('bets.game_id', $request->post('id'))->join('users','users.id','=','bets.user_id')->latest()->get();
        $updateBets = [];
        $updateUsers = [];
        foreach ($bets as $bet){
            $prev = $bet->wallet_balance;
            $drawprize = $bet->bet_middle * 8;
            $bet_payout = $drawprize;
            $bet_rake = 0;
            $total = $bet->bet_left + $bet->bet_right + $drawprize;
            $net_payout += $total;
            $aft = $bet->wallet_balance + $total;
            $deduction_banker += $bet_payout;
            if($bet->bet_middle > 0){
                $bet_result = 'Win';
            }else{
                $bet_result = 'Draw';
            }
            $updateBets[] =[
                'id' => $bet->id,
                'bet_result' => $bet_result,
                'bet_rake' => $bet_rake,
                'bet_winnings' => $drawprize,
                'bet_payout' => $bet_payout,
                'before_balance' => $prev,
                'agent_commission_payout' => $agent_commission_payout,
                'after_balance' => $aft
            ];
            $updateUsers[] =[
                'id' => $bet->user_id,
                'wallet_balance' => $aft
            ];
        }
        $this->bulkService->process('bets',$updateBets,'id');
        $this->bulkService->process('users',$updateUsers,'id');
        $check->decrement('balance',$deduction_banker);
        Matches::find($id)->update
        (
            [
                'middle_win' => 1,
                'match_status' => 'Completed',
                'draw_net_payout' => $net_payout,
            ]
        );
    }

    private function backBets($game_id)
    {
        // $bets = $this->getBets($game_id);
        $bets = Bets::select('bets.*','users.wallet_balance')->where('bets.game_id', $game_id)->join('users','users.id','=','bets.user_id')->latest()->get();
        $updateUsers = [];
        $updateBets = [];
        foreach ($bets as $bet){
            $prev = $bet->wallet_balance;
            $aft = $bet->bet_amount + $bet->wallet_balance;
            $updateUsers[]=[
                'id'=>$bet->user_id,
                'wallet_balance' => $aft
            ];
            $updateBets[]=[
                'id' => $bet->id,
                'bet_result' => 'Cancelled',
                'before_balance' => $prev,
                'after_balance' => $aft
            ];
        }
        $this->bulkService->process('bets',$updateBets,'id');
        $this->bulkService->process('users',$updateUsers,'id');
        $update_matches = Matches::where('game_id',$game_id)
        ->update(['total_rake' => 0,'net_payout' => 0,'match_status' => 'Cancelled','draw_net_payout' => 0]);
    }



    private function updateWinningStreak($id, $request){
        //check for streak today

        $streak = WinningStreak::where('game_session_id', $request->post('game_session_id'))->latest()->first();

        $match = Matches::latest()->first();

        $streak_increment = $request->post('winner') === 'left' ? 'left_streak' : 'right_streak';
        $streak_decrement = $request->post('winner') === 'left' ? 'right_streak' : 'left_streak';

        if ($streak){

            WinningStreak::find($streak->id)->increment(
                $streak_increment, 1
            );

            WinningStreak::find($streak->id)->update([
                $streak_decrement => 0
            ]);

        } else {

            WinningStreak::create([
                'left_streak' => $request->post('winner') === 'left' ? 1 : 0,
                'right_streak' => $request->post('winner') === 'right' ? 1 : 0,
                'game_session_id' => $request->post('game_session_id')
            ]);
        }
    }

    /**
     * @param $game_id
     */
    private function updateBets($game_id)
    {
        // $bets = $this->getBets($game_id);
        $bets = Bets::select('bets.*','users.wallet_balance','users.agent_id')->where('bets.game_id', $game_id)->join('users','users.id','=','bets.user_id')->latest()->get();
        $match = $this->betService->checkMatch($game_id);
        $partner_rake = $this->betService->getRake();
        $updateUsers = [];
        $updateBets =[];
        if ($match->right_win === 1){
            //process right win
            $right_odds = $match->right_odds;
            $bet_odds_less_rake = $match->right_percent_less_rake;
            foreach ($bets as $bet){
                //process bet right
                $right_payout = $bet->bet_right * $right_odds;
                $rake_payout = $right_payout * $partner_rake;
                $right_takehome = $right_payout - $rake_payout;
                $agent_commission_payout = ($bet->bet_left + $bet->bet_right) * $bet->agent_commission;
                if ($bet->bet_right > 0){
                    $user = User::find($bet->user_id);
                    $prev = $bet->wallet_balance;
                    $aft = $right_takehome + $bet->wallet_balance;
                    $updateBets[]=[
                        'id' => $bet->id,
                        'bet_odds' => round($right_odds, 2),
                        'bet_result' => 'Win',
                        'bet_rake' => round($rake_payout, 2),
                        'bet_winnings' => round($right_payout, 2),
                        'bet_payout' => round($right_takehome, 2),
                        'agent_commission_payout' => $agent_commission_payout,
                        'bet_odds_less_rake' => $bet_odds_less_rake,
                        'before_balance' => $prev,
                        'after_balance' => $aft
                    ];
                    $updateUsers[]=[
                        'id' => $bet->user_id,
                        'wallet_balance' => $aft
                    ];
                    if (!empty($user->agent_id)) {
                        //get agent
                        $agent = User::find($user->agent_id);
                        if (isset($agent)) {
                            $this->calculateAgentCommission($user, $bet);
                        }
                    }
                } else {
                    $agent_commission_payout = ($bet->bet_left + $bet->bet_right) * $bet->agent_commission;
                    $updateBets[]=[
                        'id' => $bet->id,
                        'bet_result' => 'Loss',
                        'agent_commission_payout' => $agent_commission_payout,
                        'bet_rake' => round($rake_payout, 2),
                        'bet_odds_less_rake' => $match->left_percent_less_rake
                    ];
                    $user = User::find($bet->user_id);
                    if (!empty($user->agent_id)) {
                        //get agent
                        $agent = User::find($user->agent_id);
                        if (isset($agent)) {
                            $this->calculateAgentCommission($user, $bet);
                        }
                    }
                }
            }
        }
        else {
            //process left win
            $left_odds = $match->left_odds;
            $bet_odds_less_rake = $match->left_percent_less_rake;
            foreach ($bets as $bet){
                //process bet right
                $left_payout = $bet->bet_left * $left_odds;
                $rake_payout = $left_payout * $partner_rake;
                $left_takehome = $left_payout - $rake_payout;
                $agent_commission_payout = ($bet->bet_left + $bet->bet_right) * $bet->agent_commission;
                if ($bet->bet_left > 0){
                    $user = User::find($bet->user_id);
                    $prev = $bet->wallet_balance;
                    $aft = $left_takehome + $bet->wallet_balance;
                    $updateBets[]=[
                        'id'=>$bet->id,
                        'bet_odds' => round($left_odds, 2),
                        'bet_result' => 'Win',
                        'bet_rake' => round($rake_payout, 2),
                        'bet_winnings' => round($left_payout, 2),
                        'bet_payout' => round($left_takehome, 2),
                        'agent_commission_payout' => $agent_commission_payout,
                        'bet_odds_less_rake' => $bet_odds_less_rake,
                        'before_balance' => $prev,
                        'after_balance' => $aft
                    ];
                    $updateUsers[]=[
                        'id' => $bet->user_id,
                        'wallet_balance' => $aft
                    ];
                    if (!empty($user->agent_id)) {
                        //get agent
                        $agent = User::find($user->agent_id);
                        if (isset($agent)) {
                            $this->calculateAgentCommission($user, $bet);
                        }
                    }
                } else {
                    $agent_commission_payout = ($bet->bet_left + $bet->bet_right) * $bet->agent_commission;
                    $updateBets[]=[
                        'id' => $bet->id,
                        'bet_result' => 'Loss',
                        'agent_commission_payout' => $agent_commission_payout,
                        'bet_rake' => round($rake_payout, 2),
                        'bet_odds_less_rake' => $match->right_percent_less_rake
                    ];
                    $user = User::find($bet->user_id);
                    if (!empty($user->agent_id)) {
                        //get agent
                        $agent = User::find($user->agent_id);
                        if (isset($agent)) {
                            $this->calculateAgentCommission($user, $bet);
                        }
                    }
                }
            }
        }
        $this->bulkService->process('bets',$updateBets,'id');
        $this->bulkService->process('users',$updateUsers,'id');
    }

    private function calculateAgentCommission($agent, $bet){
        $bet_rake = ($bet->bet_left + $bet->bet_right) * $this->betService->getRake();
        $userAgent = $this->getUserByType('Silver Agent', $agent->agent_id ?? '');
        $masterAgent = $this->getUserByType('Gold Agent', $userAgent->agent_id ?? $agent->agent_id);
        $incorporator = $this->getUserByType('Master Agent', $masterAgent->agent_id ?? $agent->agent_id);

        //get percentage
        $ar = $userAgent->agent_rate ?? 0;
        $mr = $masterAgent->agent_rate ?? 0;
        $ir = $incorporator->agent_rate ?? 0;
        $dw = $this->betService->getRake() - $ir ?? 0;

        //calculate agent commission
        //calculate for incorporator
        $ic = ($ir - ($mr - $ar) - $ar) / $this->betService->getRake();
        $mc = ($mr - $ar) / $this->betService->getRake();
        $ac = $ar / $this->betService->getRake();
        $dwr = ($this->betService->getRake() - $ir) / $this->betService->getRake();

        if (!empty($userAgent)) {
            User::find($userAgent->id)->increment('agent_commission_balance', $ac * $bet_rake);
        }

        if (!empty($masterAgent->id)) {
            User::find($masterAgent->id)->increment('agent_commission_balance', $mc * $bet_rake);
        }

        if (!empty($incorporator->id)){
            User::find($incorporator->id)->increment('agent_commission_balance', $ic * $bet_rake);
        }

        Bets::find($bet->id)->update([
            'bet_dw_rake' => $dwr
        ]);
    }

    private function getUserByType($type, $agent_id){

        if (!empty($agent_id)){
            $user = User::where('id', $agent_id)->where('type', $type)->first();

            if (isset($user)){
                return $user;
            }

            return null;
        }

        return null;
    }

    private function getBets($game_id)
    {
        return Bets::where('game_id', $game_id)->latest()->get();
    }

    /**
     * @param $request
     * @return mixed
     */
    private function updateGameStatus($request)
    {
        $game = Game::find($request->post('id'))->update([
            'status' => 'Completed'
        ]);

        return $game;
    }

    /**
     * @param $request
     * @param $id
     */
    private function updateMatches($request, $id)
    {

        Matches::find($id)->update
        (
            [
                'left_win' => $request->post('winner') === 'left' ? 1 : 0,
                'right_win' => $request->post('winner') === 'right' ? 1 : 0,
                'match_status' => 'Completed'
            ]
        );

    }
}
