@php
    $configData = Helper::applClasses();
@endphp
<div class="main-menu menu-fixed {{($configData['theme'] === 'light') ? "menu-light" : "menu-dark"}} menu-accordion menu-shadow" data-scroll-to-active="true"
style="background: #1e2022 !important;">
    <div class="navbar-header" style="margin-left: -20px;">
        <ul class="nav navbar-nav flex-row">
            <li class="nav-item mr-auto">
                <a class="navbar-brand" href="dashboard">
                    <div class="brand-logo"></div>
                    <h2 class="brand-text mb-0" style="color: white !important; font-size: 18px !important; ">Alpha PH</h2>
                </a>
            </li>
            <li class="nav-item nav-toggle"><a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse">
{{--                    <i class="feather icon-x d-block d-xl-none font-medium-4 primary toggle-icon"></i>--}}
                    <i class="toggle-icon feather icon-disc font-medium-4 d-none d-xl-block primary collapse-toggle-icon" data-ticon="icon-disc"></i></a></li>
        </ul>
    </div>
    <div class="shadow-bottom"></div>
    <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
            {{-- Foreach menu item starts --}}
            @if(auth()->user()->type === "Silver Agent" or auth()->user()->type === "Gold Agent" or auth()->user()->type === "Master Agent")
                <li class="nav-item {{ (request()->is('agentDashboard')) ? 'active' : '' }}">
                    <a href="/agentDashboard">
                        <i class="fa fa-dashboard"></i>
                        <span class="menu-title" data-i18n="">Agent Dashboard</span>
                    </a>
                </li>
                <li class="nav-item {{ (request()->is('myPlayers')) ? 'active' : '' }}">
                    <a href="/user/myPlayers">
                        <i class="fa fa-users"></i>
                        <span class="menu-title" data-i18n="">My Players</span>
                    </a>
                </li>
                <li class="nav-item {{ (request()->is('walletTransfer')) ? 'active' : '' }}">
                    <a href="/user/walletTransfer">
                        <i class="fa fa-users"></i>
                        <span class="menu-title" data-i18n="">Wallet Transfer</span>
                    </a>
                </li>
            @endif
            @foreach($menuData[0]->menu as $menu)
                @if(isset($menu->navheader))
                    <li class="navigation-header">
                        <span>{{ $menu->navheader }}</span>
                    </li>
                @else
                  {{-- Add Custom Class with nav-item --}}
                  @php
                    $custom_classes = "";
                    if(isset($menu->classlist)) {
                      $custom_classes = $menu->classlist;
                    }
                    $translation = "";
                    if(isset($menu->i18n)){
                        $translation = $menu->i18n;
                    }
                  @endphp

                    @if($menu->permission !== '')
                        @can($menu->permission)
                            @if($menu->userType !== '')
                                @if($menu->userType === auth()->user()->type || $menu->userType2 === auth()->user()->type)

                                  <li class="nav-item {{ (request()->is($menu->url)) ? 'active' : '' }} {{ $custom_classes }}">
                                        <a href="/{{ $menu->url }}">
                                            <i class="{{ $menu->icon }}"></i>
                                            <span class="menu-title" data-i18n="{{ $translation }}">{{ $menu->name }}</span>
                                            @if (isset($menu->badge))
                                                <?php $badgeClasses = "badge badge-pill badge-primary float-right" ?>
                                                <span class="{{ isset($menu->badgeClass) ? $menu->badgeClass.' test' : $badgeClasses.' notTest' }} ">{{$menu->badge}}</span>
                                            @endif
                                        </a>
                                        @if(isset($menu->submenu))
                                            @include('panels/submenu', ['menu' => $menu->submenu])
                                        @endif
                                    </li>
                                @endif
                            @else
                                <li class="nav-item {{ (request()->is($menu->url)) ? 'active' : '' }} {{ $custom_classes }}">
                                    <a href="/{{ $menu->url }} @if(request()->segment(2) == 'agentPlayers') '/{{ auth()->user()->id }}' @endif">
                                        <i class="{{ $menu->icon }}"></i>
                                        <span class="menu-title" data-i18n="{{ $translation }}">{{ $menu->name }}</span>
                                        @if (isset($menu->badge))
                                            <?php $badgeClasses = "badge badge-pill badge-primary float-right" ?>
                                            <span class="{{ isset($menu->badgeClass) ? $menu->badgeClass.' test' : $badgeClasses.' notTest' }} ">{{$menu->badge}}</span>
                                        @endif
                                    </a>
                                    @if(isset($menu->submenu))
                                        @include('panels/submenu', ['menu' => $menu->submenu])
                                    @endif
                                </li>
                            @endif
                        @endcan
                    @else
                        @if($menu->userType === auth()->user()->type)
                            <li class="nav-item {{ (request()->is($menu->url)) ? 'active' : '' }} {{ $custom_classes }}">
                                <a href="/{{ $menu->url }}">
                                    <i class="{{ $menu->icon }}"></i>
                                    <span class="menu-title" data-i18n="{{ $translation }}">{{ $menu->name }}</span>
                                    @if (isset($menu->badge))
                                        <?php $badgeClasses = "badge badge-pill badge-primary float-right" ?>
                                        <span class="{{ isset($menu->badgeClass) ? $menu->badgeClass.' test' : $badgeClasses.' notTest' }} ">{{$menu->badge}}</span>
                                    @endif
                                </a>
                                @if(isset($menu->submenu))
                                    @include('panels/submenu', ['menu' => $menu->submenu])
                                @endif
                            </li>
                        @endif

                            @if($menu->userType2 === auth()->user()->type)
                                <li class="nav-item {{ (request()->is($menu->url)) ? 'active' : '' }} {{ $custom_classes }}">
                                    <a href="/{{ $menu->url }}">
                                        <i class="{{ $menu->icon }}"></i>
                                        <span class="menu-title" data-i18n="{{ $translation }}">{{ $menu->name }}</span>
                                        @if (isset($menu->badge))
                                            <?php $badgeClasses = "badge badge-pill badge-primary float-right" ?>
                                            <span class="{{ isset($menu->badgeClass) ? $menu->badgeClass.' test' : $badgeClasses.' notTest' }} ">{{$menu->badge}}</span>
                                        @endif
                                    </a>
                                    @if(isset($menu->submenu))
                                        @include('panels/submenu', ['menu' => $menu->submenu])
                                    @endif
                                </li>
                            @endif
                    @endif
                @endif
            @endforeach
            <li class="nav-item {{ (request()->is('help')) ? 'active' : '' }}">
                <a href="/help">
                    <i class="fa fa-question-circle"></i>
                    <span class="menu-title" data-i18n="">Help Center</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="/logout">
                    <i class="fa fa-sign-out"></i>
                    <span class="menu-title" data-i18n="">Logout</span>
                </a>
            </li>
            {{-- Foreach menu item ends --}}
        </ul>
    </div>
</div>
<!-- END: Main Menu-->
