<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/clappr/latest/clappr.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/dash-shaka-playback/2.0.4/dash-shaka-playback.js"></script>
    <style>
    body{
        margin:0;
    }
    </style>
</head>
<body>
<div id="player"></div>
<script>
var r = 3;
var player = new Clappr.Player({
  parentId: '#player',
  source: '/stream/cam.m3u8',
  disableErrorScreen: false,
  //source: 'http://wowza.peer5.com/vod/mp4:cartoon.mp4/playlist.m3u',
  //source: '//storage.googleapis.com/shaka-demo-assets/angel-one/dash.mpd',
  //source: 'nfWlot6h_JM',
  mute:false,
  playback: { playInline: true },
  // plugins: [DashShakaPlayback],
  //plugins: {playback: [YoutubePlayback]},
  //plugins: [DashShakaPlayback,YoutubePlayback],
  events: {
    // onStop:  function() { 
    //     player.configure({
    //         parentId: '#player',
    //         autoPlay: true,
    //         mute:true,
    //         source: '/stream/cam.m3u8',
    //     }); 
    // },
    //   onEnded:  function() { 
    //         player.configure({
    //         parentId: '#player',
    //         autoPlay: true,
    //         mute:true,
    //         source: '/stream/cam.m3u8'
    //         }); 
        
    // },
    // onError: function(e) {
    //     r--;
    //   var s = player.options.source;
    //   // Replace previous line by the following line to simulate successful recovery
    //   // var s = (r > 2) ? player.options.source : 'http://clappr.io/highline.mp4';
    //   var t = 10;
    //   var retry = function() {
    //     if (t === 0) {
    //       var o = player.options;
    //       o.source = s;
    //       player.configure(o);
    //       return;
    //     }
    //     Clappr.$('#retryCounter').text(t);
    //     t--;
    //     setTimeout(retry, 1000);
    //   };
    //   player.configure({
    //     parentId: '#player',
    //     autoPlay: true,
    //     mute:true,
    //     source: '/stream/cam.m3u8',
    //   });
    //   if (r > 0) {
    //     retry();
    //   }
    // }
  },
})
function resizePlayer(){
    var aspectRatio = 9/16,
    newWidth = document.getElementById('player').parentElement.offsetWidth,
    newHeight = 2 * Math.round(newWidth * aspectRatio/2);
    player.resize({width: newWidth, height: newHeight});
  }

  resizePlayer();
  window.onresize = resizePlayer; 

</script>
</div>
    </div>
</div>
</body>
</html>