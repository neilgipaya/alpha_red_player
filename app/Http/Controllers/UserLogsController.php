<?php

namespace App\Http\Controllers;

use App\UserLogs;
use Illuminate\Http\Request;
use App\User;

class UserLogsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!auth()->user()->can('user-logs')){
            abort(403, 'Unauthorized');
        }
        return view('User.user-logs');
    }

    public function getLogs()
    {
        // $logs = UserLogs::with('user')->latest()->get();
        $logs = \DB::table('users as a')
        ->select('a.name','a.id','b.*')
        ->join('user_logs as b','a.id','=','b.user_id')
        ->latest()->get();
        $unique = $logs->unique('name');
        $data['logs'] = $logs;
        $data['unique'] = $unique->values()->all();

        // $logs = User::with('logs')->latest()->get();

        return response()->json($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UserLogs  $userLogs
     * @return \Illuminate\Http\Response
     */
    public function show(UserLogs $userLogs)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UserLogs  $userLogs
     * @return \Illuminate\Http\Response
     */
    public function edit(UserLogs $userLogs)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UserLogs  $userLogs
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserLogs $userLogs)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UserLogs  $userLogs
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserLogs $userLogs)
    {
        //
    }
}
