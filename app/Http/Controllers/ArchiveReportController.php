<?php

namespace App\Http\Controllers;

use App\ArchiveReport;
use Illuminate\Http\Request;

class ArchiveReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return response()->json(ArchiveReport::with('users')->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return 'craete';
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        \DB::beginTransaction();
        try{
            $json_data = json_encode($request->report_data,JSON_PRETTY_PRINT);
            $filename = str_replace(" ", "-",$request->archived_name).'-'.uniqid().'.json';
            $save = ArchiveReport::create([
                'archive_name' => $request->archived_name,
                'archive_by' => auth()->user()->id,
                'archive_file' => $filename
            ]);
            \DB::commit();
            if($save){
                \Storage::disk('public')->put($filename, $json_data);
            }
            $m = array(
                'message' => 'Save To Archived Report',
                'type' => 'success'
            );
            return response()->json($m);
        }catch(\Exception $e){
            \DB::rollback();
            $m = array(
                'message' => $e->getMessage(),
                'type' => 'danger'
            );
            return response()->json($m);
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ArchiveReport  $archiveReport
     * @return \Illuminate\Http\Response
     */
    public function show(ArchiveReport $archiveReport,$id)
    {
        //
        $filename = $archiveReport->where('id',$id)->first()->archive_file;
        $contents = \Storage::disk('public')->get($filename);
        return $contents;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ArchiveReport  $archiveReport
     * @return \Illuminate\Http\Response
     */
    public function edit(ArchiveReport $archiveReport)
    {
        //
        return 'edit';
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ArchiveReport  $archiveReport
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ArchiveReport $archiveReport)
    {
        //
        return 'update';
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ArchiveReport  $archiveReport
     * @return \Illuminate\Http\Response
     */
    public function destroy(ArchiveReport $archiveReport,$id)
    {
        \DB::beginTransaction();
        try{
            $data = $archiveReport->where('id',$id)->first();
            if(\Storage::disk('public')->delete($data->archive_file)){
                $data->delete();
            }else{
                $m = array(
                    'message' => 'Selected Archive Doesn`t Exist ',
                    'type' => 'warning'
                );
                return response()->json($m);
            }
            \DB::commit();
            $m = array(
                'message' => 'Archive Deleted successfully',
                'type' => 'success'
            );
            return response()->json($m);
        }catch(\Exception $e){
           \ DB::rollback();
            $m = array(
                'message' => $e->getMessage(),
                'type' => 'danger'
            );
            return response()->json($m);
        }
        
    }
}
