<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="UTF-8">
    <meta name='viewport' content='width=device-width, initial-scale=1.0, user-scalable=0'>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="Description" content="Bootstrap Responsive Admin Web Dashboard HTML5 Template">
    <meta name="Author" content="Spruko Technologies Private Limited">
    <meta name="Keywords" content="admin,admin dashboard,admin dashboard template,admin panel template,admin template,admin theme,bootstrap 4 admin template,bootstrap 4 dashboard,bootstrap admin,bootstrap admin dashboard,bootstrap admin panel,bootstrap admin template,bootstrap admin theme,bootstrap dashboard,bootstrap form template,bootstrap panel,bootstrap ui kit,dashboard bootstrap 4,dashboard design,dashboard html,dashboard template,dashboard ui kit,envato templates,flat ui,html,html and css templates,html dashboard template,html5,jquery html,premium,premium quality,sidebar bootstrap 4,template admin bootstrap 4"/>

    <!-- Title -->
    <title> Sabong World -  Login to your Account </title>

    <!-- Favicon -->

    <!-- Icons css -->
    <link href="{{ asset('assets') }}/css/icons.css" rel="stylesheet">

    <!--  Right-sidemenu css -->
    <link href="{{ asset('assets') }}/plugins/sidebar/sidebar.css" rel="stylesheet">

    <!--  Custom Scroll bar-->
    <link href="{{ asset('assets') }}/plugins/mscrollbar/jquery.mCustomScrollbar.css" rel="stylesheet"/>

    <!--  Left-Sidebar css -->
    <link rel="stylesheet" href="{{ asset('assets') }}/css/sidemenu.css">

    <!--- Style css --->
    <link href="{{ asset('assets') }}/css/style.css" rel="stylesheet">

    <!--- Dark-mode css --->
    <link href="{{ asset('assets') }}/css/style-dark.css" rel="stylesheet">

    <!---Skinmodes css-->
    <link href="{{ asset('assets') }}/css/skin-modes.css" rel="stylesheet" />

    <!--- Animations css-->
    <link href="{{ asset('assets') }}/css/animate.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>


    <style>
        .text-gold {
            color: #FFCC00;
        }

        .bg-gold {
            background: #FFCC00;
            -webkit-box-shadow: inset 1px 6px 12px #fff61a, inset -1px -10px 5px #fff61a, 1px 2px 1px #FFCC00;
            -moz-box-shadow: inset 1px 6px 12px #fff61a, inset -1px -10px 5px #fff61a, 1px 2px 1px #FFCC00;
            box-shadow: inset 1px 6px 12px #fff61a, inset -1px -10px 5px #fff61a, 1px 2px 1px #FFCC00;
        }

        .btn-red {
            background: #FF0000;
            -webkit-box-shadow: inset 1px 6px 12px #bc0800, inset -1px -10px 5px #a80009, 1px 2px 1px #a80009;
            -moz-box-shadow: inset 1px 6px 12px #bc0800, inset -1px -10px 5px #a80009, 1px 2px 1px #a80009;
            box-shadow: inset 1px 6px 12px #bc0800, inset -1px -10px 5px #a80009, 1px 2px 1px #a80009;
        }

        body {
            font-family: Roboto, sans-serif !important;
        }

    </style>
</head>
<body class="main-body" style="background: black !important">

<!-- Loader -->
<div id="global-loader">
    <img src="{{ asset('assets') }}/img/loader.svg" class="loader-img" alt="Loader">
</div>
<!-- /Loader -->

<!-- Page -->
<div class="page">

    <div class="container-fluid">
        <div class="row no-gutter">
            <!-- The content half -->
            <div class="col-md-6 col-lg-6 col-xl-5">
                <div class="login d-flex align-items-center py-2">
                    <!-- Demo content-->
                    <div class="container p-0">
                        <div class="row">
                                <div class="col-md-10 col-lg-10 col-xl-9 mx-5 mt-5">
                                    <form action="/register-account" method="POST">
                                        @csrf
                                        <h4 class="text-center text-gold"><strong>WELCOME</strong></h4>
                                        <div class="text-center">
                                            <img src="{{ asset('images/sabong-logo.png') }}" class="img img-fluid text-center" style="height: 220px; width: 220px" />
                                        </div>
                                        <div class="text-center">
                                            <h4 class="text-center text-gold"><strong>PLAYER REGISTER</strong></h4>
                                        </div>
                                        <div class="form-group">
                                            <input type="text" name="name" placeholder="Full Name" class="form-control" style="border-radius: 1rem" />
                                        </div>
                                        <div class="form-group">
                                            <input type="number" name="contact_number" placeholder="Phone Number" class="form-control" style="border-radius: 1rem" />
                                        </div>
                                        <div class="form-group">
                                            <input type="password" name="password" placeholder="Password" class="form-control" style="border-radius: 1rem" />
                                        </div>
                                        <div class="form-group">
                                            <label style="color: white">Do you have an agent?</label>
                                            <select class="form-control" name="agent_id" style="border-radius: 1rem">
                                                <option value="">No Agent</option>
                                                @foreach($agent as $a)
                                                    <option value="{{ $a->id }}">{{ $a->name . " (" . $a->contact_number . ")" }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
<!--                                             <button type="submit" class="btn btn-rounded bg-gold btn-block" style="border-radius: 1rem"><strong>REGISTER</strong></button> -->
                                        </div>
                                    </form>
                            </div>

                            <div class="col-md-10 col-lg-10 col-xl-9 mx-5" style="margin-top: 30px">
                                <h5 class="text-center text-gold">
                                    ALREADY HAVE AN ACCOUNT?
                                </h5>
                                <div class="form-group text-center">
                                    <a href="/" class="btn btn-red btn-block btn-rounded text-white" type="submit"><strong>Login Here</strong></a>
                                </div>
                            </div>
                        </div><!-- End -->
                    </div>
                </div><!-- End -->
            </div>
        </div>

    </div>
    <!-- End Page -->

    <!-- JQuery min js -->
    <script src="{{ asset('assets') }}/plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Bundle js -->
    <script src="{{ asset('assets') }}/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Ionicons js -->
    <script src="{{ asset('assets') }}/plugins/ionicons/ionicons.js"></script>

    <!-- Moment js -->
    <script src="{{ asset('assets') }}/plugins/moment/moment.js"></script>

    <!-- eva-icons js -->
    <script src="{{ asset('assets') }}/js/eva-icons.min.js"></script>

    <!-- Rating js-->
    <script src="{{ asset('assets') }}/plugins/rating/jquery.rating-stars.js"></script>
    <script src="{{ asset('assets') }}/plugins/rating/jquery.barrating.js"></script>

    <!-- custom js -->
    <script src="{{ asset('assets') }}/js/custom.js"></script>
    <script>


            @if(Session::has('message'))

        let type = "{{ Session::get('alert-type', 'info') }}";

        switch(type){

            case 'info':
                Swal.fire({

                    type: 'info',
                    title: "{{ Session::get('message') }}",
                    toast: true,
                    position: 'top-end',
                    showConfirmButton: false,
                    timer: 3000

                });

                break;

            case 'warning':

                Swal.fire({

                    type: 'warning',
                    title: "{{ Session::get('message') }}",
                    toast: true,
                    position: 'top-end',
                    showConfirmButton: false,
                    timer: 3000

                });

                break;

            case 'success':
                Swal.fire({

                    type: 'success',
                    title: "{{ Session::get('message') }}",
                    toast: true,
                    position: 'top-end',
                    showConfirmButton: false,
                    timer: 3000

                });
                break;

            case 'error':
                Swal.fire({

                    type: 'error',
                    title: "{{ Session::get('message') }}",
                    toast: true,
                    position: 'top-end',
                    showConfirmButton: false,
                    timer: 3000

                });
                break;
        }
        @endif

    </script>
    @if ($errors->any())
        @foreach ($errors->all() as $error)
            <script>
                toastr["warning"]("{{ $error }}");

                {{--new Noty({--}}
                {{--    type: 'warning',--}}
                {{--    layout: 'topRight',--}}
                {{--    text: "{{ $error }}"--}}
                {{--}).show();--}}
            </script>
@endforeach
@endif
</body>
</html>
