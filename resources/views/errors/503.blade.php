<html lang="en" class="no-js"><head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Marshall v24 - Ultimate Coming Soon Template</title>
    <meta name="description" content="Marshall - Ultimate Coming Soon Template by pixiefy">
    <meta name="author" content="pixiefy">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	
	<!-- Google Font -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,800%7CWork+Sans:500,900" rel="stylesheet">
    <style>
    /*! CSS Used from: https://pixiefy.com/themes/marshall/html/v24/css/normalize.css */
html{font-family:sans-serif;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;}
body{margin:0;}
a{background-color:transparent;-webkit-text-decoration-skip:objects;}
a:active,a:hover{outline-width:0;}
img{border-style:none;}
/*! CSS Used from: https://pixiefy.com/themes/marshall/html/v24/style.css */
*,*:after,*:before{-webkit-box-sizing:border-box;box-sizing:border-box;}
body{color:#FFF;font-size:16px;font-family:'Open Sans', Arial, sans-serif;-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale;background-color:#131623;line-height:1.5;font-weight:400;overflow-x:hidden;}
a{color:#FFF;text-decoration:none;outline:none;}
a:hover{color:inherit;}
a:hover,a:focus{outline:none;}
img{max-width:100%;border:none;}
h2,h4{font-family:inherit;font-weight:normal;line-height:1.1;color:inherit;}
h2{margin-top:20px;margin-bottom:10px;}
h4{margin-top:10px;margin-bottom:10px;}
h2{font-size:1.875em;}
h4{font-size:1.125em;}
.marshall-container::before,.marshall-container::after,[class^="marshall-col-"]::before,[class^="marshall-col-"]::after{content:"";display:block;clear:both;}
.marshall-container{width:100%;}
.marshall-col-content{position:relative;z-index:3;}
.marshall-col-10{float:left;min-height:1;height:100vh;}
.marshall-col-10{width:83.3333%;}
.marshall-col-10.marshall-middle-10.marshall-col-content{margin-left:8.3%;text-align:center;}
.marshall-loading-screen{background-color:#333;bottom:0;left:0;position:fixed;right:0;text-align:center;top:0;z-index:99999;}
.marshall-loading-icon{display:table;height:100%;position:absolute;width:100%;}
.marshall-loading-inner{display:table-cell;vertical-align:middle;}
.marshall-load{background-color:#f61067;border-radius:50%;height:60px;margin:auto;position:relative;width:60px;}
.marshall-load::before{background-color:transparent;border:2px dashed #fff;border-radius:50%;bottom:0;content:"";display:block;left:0;position:absolute;right:0;top:0;transition-duration:0.4s;transition-property:opacity;transition-timing-function:linear;z-index:2;opacity:1;visibility:visible;transition:top 0.4s,left 0.4s,right 0.4s,bottom 0.4s, border-width 0.4s ease 0.4s;-webkit-transition:top 0.4s,left 0.4s,right 0.4s,bottom 0.4s, border-width 0.4s ease 0.4s;animation:4s linear 0s normal none infinite running spinnerRotate;-webkit-animation:4s linear 0s normal none infinite running spinnerRotate;}
.marshall-load::after{content:attr(data-name);font-size:1.8em;font-weight:700;line-height:57px;color:#FFF;}
.marshall-content{display:block;margin-left:15%;position:absolute;top:50%;visibility:visible;width:70%;}
.marhall-maintain-thumb{margin:auto;width:300px;margin-bottom:20px;}
.marhall-maintain-thumb img{display:block;}
body.mrs-v24 .marshall-content h4{font-size:1.4em;line-height:1.5;}
body.mrs-v24{background:#F1F1F1;color:#969696;}
.marshall-bg-overley{height:100%;width:100%;position:absolute;top:0;left:0;background:rgba(0, 0, 0, 0) url("https://pixiefy.com/themes/marshall/html/v24/images/v24/confetti.svg") repeat scroll center center;}
body.mrs-v24 .marshall-content h2{color:#424242;}
body.mrs-v24 .marshall-content h2{color:#424242;font-size:1.6em;font-weight:bold;margin-bottom:35px;margin-top:0;}
.marhall-maintain-thumb{margin:auto;width:300px;margin-bottom:20px;}
.marhall-maintain-thumb img{display:block;}
body.mrs-v24 .marshall-content h4{font-size:1.4em;line-height:1.5;}
body.mrs-v24 .marshall-col-content{min-height:600px;}
.simple-logo{margin:40px auto auto;opacity:0.3;width:100px;}
body.mrs-v24 .marshall-content a,body.mrs-v24 .marshall-content a:hover,body.mrs-v24 .marshall-content a:focus,body.mrs-v24 .marshall-content a:active{color:#90C145;}
.marshall-col-content{transition:all 0.4s ease 0s;}
/*! CSS Used from: https://pixiefy.com/themes/marshall/html/v24/css/responsive.css */
@media only screen and (max-width: 1200px){
.marshall-col-content{height:auto;padding:70px 0;position:relative;width:100%;}
.marshall-content{position:relative;top:inherit!important;}
.marshall-content{margin-left:50px;}
.marshall-container{position:relative;width:100%;}
}
@media only screen and (max-width: 767px){
body{font-size:14px;}
.mrs-v24 .marshall-col-10.marshall-middle-10.marshall-col-content{margin-left:0;}
}
@media only screen and (max-width: 479px){
.marshall-content{margin-left:20px;width:calc(100% - 40px);}
}
/*! CSS Used fontfaces */
@font-face{font-family:'Open Sans';font-style:normal;font-weight:300;src:local('Open Sans Light'), local('OpenSans-Light'), url(https://fonts.gstatic.com/s/opensans/v17/mem5YaGs126MiZpBA-UN_r8OX-hpOqc.woff2) format('woff2');unicode-range:U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;}
@font-face{font-family:'Open Sans';font-style:normal;font-weight:300;src:local('Open Sans Light'), local('OpenSans-Light'), url(https://fonts.gstatic.com/s/opensans/v17/mem5YaGs126MiZpBA-UN_r8OVuhpOqc.woff2) format('woff2');unicode-range:U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;}
@font-face{font-family:'Open Sans';font-style:normal;font-weight:300;src:local('Open Sans Light'), local('OpenSans-Light'), url(https://fonts.gstatic.com/s/opensans/v17/mem5YaGs126MiZpBA-UN_r8OXuhpOqc.woff2) format('woff2');unicode-range:U+1F00-1FFF;}
@font-face{font-family:'Open Sans';font-style:normal;font-weight:300;src:local('Open Sans Light'), local('OpenSans-Light'), url(https://fonts.gstatic.com/s/opensans/v17/mem5YaGs126MiZpBA-UN_r8OUehpOqc.woff2) format('woff2');unicode-range:U+0370-03FF;}
@font-face{font-family:'Open Sans';font-style:normal;font-weight:300;src:local('Open Sans Light'), local('OpenSans-Light'), url(https://fonts.gstatic.com/s/opensans/v17/mem5YaGs126MiZpBA-UN_r8OXehpOqc.woff2) format('woff2');unicode-range:U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB;}
@font-face{font-family:'Open Sans';font-style:normal;font-weight:300;src:local('Open Sans Light'), local('OpenSans-Light'), url(https://fonts.gstatic.com/s/opensans/v17/mem5YaGs126MiZpBA-UN_r8OXOhpOqc.woff2) format('woff2');unicode-range:U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;}
@font-face{font-family:'Open Sans';font-style:normal;font-weight:300;src:local('Open Sans Light'), local('OpenSans-Light'), url(https://fonts.gstatic.com/s/opensans/v17/mem5YaGs126MiZpBA-UN_r8OUuhp.woff2) format('woff2');unicode-range:U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;}
@font-face{font-family:'Open Sans';font-style:normal;font-weight:400;src:local('Open Sans Regular'), local('OpenSans-Regular'), url(https://fonts.gstatic.com/s/opensans/v17/mem8YaGs126MiZpBA-UFWJ0bbck.woff2) format('woff2');unicode-range:U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;}
@font-face{font-family:'Open Sans';font-style:normal;font-weight:400;src:local('Open Sans Regular'), local('OpenSans-Regular'), url(https://fonts.gstatic.com/s/opensans/v17/mem8YaGs126MiZpBA-UFUZ0bbck.woff2) format('woff2');unicode-range:U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;}
@font-face{font-family:'Open Sans';font-style:normal;font-weight:400;src:local('Open Sans Regular'), local('OpenSans-Regular'), url(https://fonts.gstatic.com/s/opensans/v17/mem8YaGs126MiZpBA-UFWZ0bbck.woff2) format('woff2');unicode-range:U+1F00-1FFF;}
@font-face{font-family:'Open Sans';font-style:normal;font-weight:400;src:local('Open Sans Regular'), local('OpenSans-Regular'), url(https://fonts.gstatic.com/s/opensans/v17/mem8YaGs126MiZpBA-UFVp0bbck.woff2) format('woff2');unicode-range:U+0370-03FF;}
@font-face{font-family:'Open Sans';font-style:normal;font-weight:400;src:local('Open Sans Regular'), local('OpenSans-Regular'), url(https://fonts.gstatic.com/s/opensans/v17/mem8YaGs126MiZpBA-UFWp0bbck.woff2) format('woff2');unicode-range:U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB;}
@font-face{font-family:'Open Sans';font-style:normal;font-weight:400;src:local('Open Sans Regular'), local('OpenSans-Regular'), url(https://fonts.gstatic.com/s/opensans/v17/mem8YaGs126MiZpBA-UFW50bbck.woff2) format('woff2');unicode-range:U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;}
@font-face{font-family:'Open Sans';font-style:normal;font-weight:400;src:local('Open Sans Regular'), local('OpenSans-Regular'), url(https://fonts.gstatic.com/s/opensans/v17/mem8YaGs126MiZpBA-UFVZ0b.woff2) format('woff2');unicode-range:U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;}
@font-face{font-family:'Open Sans';font-style:normal;font-weight:800;src:local('Open Sans ExtraBold'), local('OpenSans-ExtraBold'), url(https://fonts.gstatic.com/s/opensans/v17/mem5YaGs126MiZpBA-UN8rsOX-hpOqc.woff2) format('woff2');unicode-range:U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;}
@font-face{font-family:'Open Sans';font-style:normal;font-weight:800;src:local('Open Sans ExtraBold'), local('OpenSans-ExtraBold'), url(https://fonts.gstatic.com/s/opensans/v17/mem5YaGs126MiZpBA-UN8rsOVuhpOqc.woff2) format('woff2');unicode-range:U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;}
@font-face{font-family:'Open Sans';font-style:normal;font-weight:800;src:local('Open Sans ExtraBold'), local('OpenSans-ExtraBold'), url(https://fonts.gstatic.com/s/opensans/v17/mem5YaGs126MiZpBA-UN8rsOXuhpOqc.woff2) format('woff2');unicode-range:U+1F00-1FFF;}
@font-face{font-family:'Open Sans';font-style:normal;font-weight:800;src:local('Open Sans ExtraBold'), local('OpenSans-ExtraBold'), url(https://fonts.gstatic.com/s/opensans/v17/mem5YaGs126MiZpBA-UN8rsOUehpOqc.woff2) format('woff2');unicode-range:U+0370-03FF;}
@font-face{font-family:'Open Sans';font-style:normal;font-weight:800;src:local('Open Sans ExtraBold'), local('OpenSans-ExtraBold'), url(https://fonts.gstatic.com/s/opensans/v17/mem5YaGs126MiZpBA-UN8rsOXehpOqc.woff2) format('woff2');unicode-range:U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB;}
@font-face{font-family:'Open Sans';font-style:normal;font-weight:800;src:local('Open Sans ExtraBold'), local('OpenSans-ExtraBold'), url(https://fonts.gstatic.com/s/opensans/v17/mem5YaGs126MiZpBA-UN8rsOXOhpOqc.woff2) format('woff2');unicode-range:U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;}
@font-face{font-family:'Open Sans';font-style:normal;font-weight:800;src:local('Open Sans ExtraBold'), local('OpenSans-ExtraBold'), url(https://fonts.gstatic.com/s/opensans/v17/mem5YaGs126MiZpBA-UN8rsOUuhp.woff2) format('woff2');unicode-range:U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;}
    </style>

</head>
<body class="mrs-v24">
	<!-- Page Loader Start -->
	<div class="marshall-loading-screen" style="display: none;">
	    <div class="marshall-loading-icon">
	        <div class="marshall-loading-inner">
	        	<div class="marshall-load" data-name="M"></div>
	        </div>
	    </div>
	</div><!-- End .loading-screen -->
	
	<div class="marshall-container">
		<div class="marshall-bg-overley js-plaxify" data-xrange="50" data-yrange="25" style="top: 0px; left: 0px; transform: translate3d(22.8277px, -10.079px, 0px);"></div>
		<div class="marshall-col-10 marshall-middle-10 marshall-col-content">
			<div class="marshall-content jquery-center" style="top: calc(50% - 196.797px);">
				<div class="marhall-maintain-thumb">
					<img src="/images/workers.png" alt="" class="js-plaxify" data-xrange="10" data-yrange="10" style="top: 0px; left: 239.359px; transform: translate3d(4.56554px, -4.23159px, 0px);">
				</div>
				<h2 class="js-plaxify" data-xrange="20" data-yrange="10" style="top: 200.594px; left: 0px; transform: translate3d(9.13109px, -4.23159px, 0px);">Sorry, We are down for maintenance</h2>
				<h4 class="js-plaxify" data-xrange="40" data-yrange="15" style="top: 253.594px; left: 0px; transform: translate3d(18.2622px, -5.84739px, 0px);">We'll be back up shortly, Sorry for inconvenience</h4>

				<div class="simple-logo js-plaxify" data-xrange="50" data-yrange="20" style="top: 329.594px; left: 0.000375px; transform: translate3d(22.8277px, -8.46318px, 0px);">
					<img src="images/logo-dark.png" alt="">
				</div>
			</div>
		</div>

	</div>

	

	<!-- All marshall js files -->

	

</body></html>