<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BankerTopup extends Model
{
    protected $table = "banker_topup";
    protected $fillable = ['banker_id','status','details','amount','request_by','processed_by','decline_reason'];
    public $timestamps = true;
    protected $guarded = [];
    public function banker(){
        return $this->belongsTo('\App\BankAccounts','banker_id');
    }
    public function users(){
        return $this->belongsTo('\App\User','request_by');
    }
    public function processed_by(){
        return $this->belongsTo('\App\User','processed_by');
    }
}
