<<<<<<< HEAD
<?php

namespace App\Http\Controllers;

use App\BankAccounts;
use App\BankTransactions;
use App\Events\NotificationEvent;
use App\User;
use App\UserLogs;
use App\Wallet;
use App\WalletHistory;
use App\Withdrawal;
use App\WithdrawalTemplate;
use Illuminate\Http\Request;
use App\WithdrawalPlayer;
use App\GameSession;
use Illuminate\Support\Facades\DB;

class WithdrawalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if(!auth()->user()->can('withdrawal-recent')){
            abort(403, 'Unauthorized');
        }else if(auth()->user()->type == 'Player'){
            abort(403, 'Unauthorized');
        }
        return view('Withdrawal.withdrawal-recent');
    }

    public function getRecent()
    {
        $records = Withdrawal::with('users')->where('status', 'Pending')->latest()->get();

        return response()->json($records);
    }
    public function getRecentAgents()
    {
        $records = WithdrawalPlayer::with('users')->where('status', 'Pending')->where('agent_id',auth()->user()->id)->latest()->get();

        return response()->json($records);
    }

    public function logs()
    {
        if(!auth()->user()->can('withdrawal-history')){
            abort(403, 'Unauthorized');
        }else if(auth()->user()->type == 'Player'){
            abort(403, 'Unauthorized');
        }
        return view('Withdrawal.withdrawal-logs');
    }

    public function getLogs()
    {
        $records = Withdrawal::with('processed_by')->with('users')->where('status','!=','Pending')->with('withdrawal_template')->latest()->get();
        return response()->json($records);
    }
    public function markpaid(Request $request,$id){
        $paid = WithdrawalPlayer::find($id);
        $paid->update($request->all());
        $notification = array(
            'message' => 'Marked as paid',
            'type' => 'success',
        );
        return response()->json($notification);
    }
    public function getLogsAgents(){
        $records = WithdrawalPlayer::with('processed_by')
        ->with('users')
        ->where('status','!=','Pending')
        ->with('withdrawal_template')
        ->where('agent_id',auth()->user()->id)
        ->latest()
        ->get();
        return response()->json($records);
    }

    public function request()
    {
        return view('Withdrawal.request');
    }

    public function getWithdrawalTemplate()
    {
        $templates = WithdrawalTemplate::where('user_id', auth()->user()->id)->latest()->get();

        return response()->json($templates);
    }
    public function deleteWithTemplate($id){
        DB::beginTransaction();
        try {
            WithdrawalTemplate::where('id',$id)->where('user_id',auth()->user()->id)->delete();
            DB::commit();
            $notification = array(
                'message' => ' Deleting Withdrawal template successfully',
                'type' => 'success',
            );

            return response()->json($notification);
        } catch (\Exception $e) {
            DB::rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger',
            );

            return response()->json($notification);
        }

    }
    public function updateWithTemplate(Request $request,$id){
        DB::beginTransaction();
        try {
            $template = WithdrawalTemplate::find($id);
            $template->update($request->all());
            UserLogs::create([
                'user_id' => \auth()->user()->id,
                'type' => 'Withdrawal_Template',
                'content' => "Withdrawal Template has been Updated"
            ]);
            DB::commit();
            $notification = array(
                'message' => ' Updating Withdrawal template successfully',
                'type' => 'success',
            );

            return response()->json($notification);
        } catch (\Exception $e) {
            DB::rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger',
            );

            return response()->json($notification);
        }
    }

    public function createWithdrawalTemplate(Request $request)
    {
        DB::beginTransaction();

        try {
            //

            $withdrawal = WithdrawalTemplate::create([
                'account_name' => $request->post('account_name'),
                'account_number' => $request->post('account_number'),
                'payment_type' => $request->post('payment_type'),
                'bank_type' => $request->post('bank_type'),
                'user_id' => auth()->user()->id,
            ]);


            UserLogs::create([
                'user_id' => \auth()->user()->id,
                'type' => 'Withdrawal_Template',
                'content' => "Withdrawal Template has been created"
            ]);

            DB::commit();
            $notification = array(
                'message' => ' successfully',
                'type' => 'success',
                'id' => $withdrawal->id
            );

            return response()->json($notification);
        } catch (\Exception $e) {

            DB::rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger',
            );

            return response()->json($notification);
        }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    public function getmywithdrawal(){
        $cashout = Withdrawal::with('decline_template')->where('user_id', auth()->user()->id)->latest()->get();
        return response()->json($cashout);
    }
    public function getmywithdrawalAgent(){
        $cashout = WithdrawalPlayer::with('processed_by')->with('decline_template')->where('user_id', auth()->user()->id)->latest()->get();
        return response()->json($cashout);
    }
    public function cancel_mycashout(Request $request){
        DB::begintransaction();
        try {
            if(auth()->user()->type == 'Master Agent'){
                $with = Withdrawal::find($request->with_id);
            }else{
                $with = WithdrawalPlayer::find($request->with_id);
            }
            if($with->status == 'Pending'){
                if($with->user_id === auth()->user()->id){
                    $with->update(['status' => 'Cancelled']);
                    $user = User::find(auth()->user()->id);
                    $user->increment('wallet_balance',$with->withdrawal_amount);
                    $notification = array(
                        'message' => 'Cancel Cashout Request Successfully !',
                        'type' => 'success',
                    );
                    UserLogs::create([
                        'user_id' => \auth()->user()->id,
                        'type' => 'Cashout',
                        'content' => "Cash-out ($request->with_id) has been cancelled"
                    ]);
                    DB::commit();
                    return response()->json($notification);
                }
            }
            $notification = array(
                'message' => 'Invalid Request !',
                'type' => 'danger',
            );
            return response()->json($notification);
        } catch (\Exception $e) {
            DB::rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger',
            );
            return response()->json($notification);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $get_withdrawal_template_info = WithdrawalTemplate::find($request->withdrawal_template_id);
            $request->merge([
                'user_id' => auth()->user()->id,
                'account_name' => $get_withdrawal_template_info->account_name,
                'account_number' => $get_withdrawal_template_info->account_number,
                'bank_type' => $get_withdrawal_template_info->bank_type,
                'payment_type' => $get_withdrawal_template_info->payment_type,
                'reference_number' => "WT-".$this->generateRandomString(20)
            ]);
            $reference = $request->post('reference_number');
            if ($request->post('type') === 'Wallet'){
                $user = User::find($request->post('user_id'));
                if ($user->wallet_balance >= $request->post('withdrawal_amount')){
                    User::where('id', $user->id)->decrement('wallet_balance', $request->post('withdrawal_amount'));
                    WalletHistory::create([
                        'user_id' => $user->id,
                        'amount' => $request->post('withdrawal_amount'),
                        'type' => 'Request for Cash-out'
                    ]);
                    UserLogs::create([
                        'user_id' => \auth()->user()->id,
                        'type' => 'Cashout',
                        'content' => "Cash-out $reference has been requested"
                    ]);
                } else {
                    $notification = array(
                        'message' => 'Insufficient Withdrawal Amount',
                        'type' => 'success',
                    );
                    return response()->json($notification);
                }
            }
            else {
                $user = User::find($request->post('user_id'));
                if ($user->agent_commission_balance >= $request->post('agent_commission_balance')) {
                    User::where('id', $user->id)->decrement('agent_commission_balance', $request->post('withdrawal_amount'));
                    UserLogs::create([
                        'user_id' => \auth()->user()->id,
                        'type' => 'Cashout',
                        'content' => "Cash-out from commission balance with reference $reference has been requested"
                    ]);
                } else {
                    $notification = array(
                        'message' => 'Insufficient Withdrawal Amount',
                        'type' => 'success',
                    );
                    return response()->json($notification);
                }
            }
            $message = "You have a New Withdrawal Request";
            event(new NotificationEvent($message, auth()->user()->id, '/withdrawal', $request->post('user_id')));
            Withdrawal::create($request->all());
            DB::commit();
            $notification = array(
                'message' => 'Withdrawal Submitted successfully',
                'type' => 'success',
            );
            return response()->json($notification);
        } catch (\Exception $e) {
            DB::rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger',
            );
            return response()->json($notification);
        }
    }
    public function playerWithdrawal(Request $request){
        DB::beginTransaction();
        // try {
            $get_withdrawal_template_info = WithdrawalTemplate::find($request->withdrawal_template_id);
            $request->merge([
                'user_id' => auth()->user()->id,
                'agent_id' => $request->post('agent_id'),
                'account_name' => $get_withdrawal_template_info->account_name,
                'account_number' => $get_withdrawal_template_info->account_number,
                'bank_type' => $get_withdrawal_template_info->bank_type,
                'payment_type' => $get_withdrawal_template_info->payment_type,
                'reference_number' => "WT-".$this->generateRandomString(20)
            ]);
            $reference = $request->post('reference_number');
            if ($request->post('type') === 'Wallet'){
                $user = User::find($request->post('user_id'));
                $agent = User::find($request->post('agent_id'));
                if ($user->wallet_balance >= $request->post('withdrawal_amount')){
                    User::where('id', $user->id)->decrement('wallet_balance', $request->post('withdrawal_amount'));
                    UserLogs::create([
                        'user_id' => \auth()->user()->id,
                        'type' => 'Cashout Agent',
                        'content' => "Cash-out $reference has been requested by $user->name($user->id) to Agent ID:$agent->name($agent->id)
                        with amount of (P ".$request->post('withdrawal_amount').")"
                    ]);
                    WalletHistory::create([
                        'user_id' => auth()->user()->id,
                        'type' => "You have created a cashout for Agent: $agent->name($agent->id)",
                        'amount' => $request->post('withdrawal_amount'),
                    ],[
                        'user_id' => $agent->id,
                        'type' => "User:: $user->name($user->id) has requested a cash-out to you with ref# of $reference",
                        'amount' => $request->post('withdrawal_amount'),
                    ]);
                } else {
                    $notification = array(
                        'message' => 'Insufficient Withdrawal Amount',
                        'type' => 'success',
                    );
                    return response()->json($notification);
                }
            }
            $message = "You have a New Withdrawal Request";
            event(new NotificationEvent($message, auth()->user()->id, '/withdrawal', $request->post('user_id')));
            WithdrawalPlayer::create($request->all());
            DB::commit();
            $notification = array(
                'message' => 'Withdrawal Submitted successfully',
                'type' => 'success',
            );
            return response()->json($notification);
        // } catch (\Exception $e) {
        //     DB::rollback();
        //     $notification = array(
        //         'message' => $e->getMessage(),
        //         'type' => 'danger',
        //     );
        //     return response()->json($notification);
        // }
    }

    public function storeAgent(Request $request)
    {
        //

        DB::beginTransaction();

        try {
            //

            $request->merge([
                'user_id' => auth()->user()->id,
                'reference_number' => "WT-".$this->generateRandomString(20),
                'type' => 'Commission'
            ]);


            $message = "You have a New Withdrawal Request";

            event(new NotificationEvent($message, auth()->user()->id, '/withdrawal', $request->post('user_id')));

            Withdrawal::create($request->all());

            DB::commit();
            $notification = array(
                'message' => 'Withdrawal Submitted successfully',
                'type' => 'success',
            );

            return response()->json($notification);
        } catch (\Exception $e) {

            DB::rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger',
            );

            return response()->json($notification);
        }
    }



    /**
     * Display the specified resource.
     *
     * @param  \App\Withdrawal  $withrawal
     * @return \Illuminate\Http\Response
     */
    public function show(Withdrawal $withrawal)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Withdrawal  $withrawal
     * @return \Illuminate\Http\Response
     */
    public function edit(Withdrawal $withrawal)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Withdrawal  $withrawal
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $request->merge([
                'status' => 'Transferred',
                'processed_by' => auth()->user()->id
            ]);
            $amount = BankAccounts::find($request->post('source_fund_id'));
            if ($amount->balance >= $request->post('withdrawal_amount')){
                BankAccounts::where('id', $request->post('source_fund_id'))->decrement('balance', $request->post('withdrawal_amount') - $request->post('processing_fee'));
                $withdrawal = Withdrawal::with('users')->find($id);
                if($withdrawal->status != 'Pending'){
                    $notification = array(
                        'message' => 'This request has been processed already !',
                        'type' => 'danger',
                    );
                    return response()->json($notification);
                }
                WalletHistory::create([
                    'user_id' => $withdrawal->user_id,
                    'amount' => $request->post('withdrawal_amount') - $request->post('processing_fee'),
                    'type' => 'Request for Cash-out has been Approved'
                ]);
                BankTransactions::create(
                    [
                        'bank_id' => $request->post('source_fund_id'),
                        'debit' => $request->post('withdrawal_amount'),
                        'credit' => 0,
                        'transaction_code' => $withdrawal->reference_number,
                        'transaction_details' => "Request for withdrawal has been credited to ". $withdrawal->users->id . ',' . $withdrawal->users->contact_number
                    ]
                );
                UserLogs::create([
                    'user_id' => \auth()->user()->id,
                    'type' => 'Cashout',
                    'content' => "Cash-out from $withdrawal->reference_number has been approved and deducted"
                ]);
                $withdrawal->update($request->only('status', 'source_fund_id', 'processed_by'));
                DB::commit();
                $notification = array(
                    'message' => 'Withdrawal Updated successfully',
                    'type' => 'success',
                );
                return response()->json($notification);
            } else {
                $notification = array(
                    'message' => 'Not Enough Wallet Balance',
                    'type' => 'danger',
                );
                return response()->json($notification);
            }
        } catch (\Exception $e) {
            DB::rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger',
            );
            return response()->json($notification);
        }
    }
    public function updateWithdrawalAgents(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $request->merge([
                'status' => 'Approved',
                'processed_by' => auth()->user()->id
            ]);
            $withdrawal = WithdrawalPlayer::with('users')->find($id);
            if($withdrawal->status != 'Pending'){
                $notification = array(
                    'message' => 'This request has been processed already !',
                    'type' => 'danger',
                );
                return response()->json($notification);
            }
            WalletHistory::create([
                'user_id' => $withdrawal->user_id,
                'amount' => $request->post('withdrawal_amount') - $request->post('processing_fee'),
                'type' => 'Request for Cash-out has been Approved'
            ]);
            UserLogs::create([
                'user_id' => \auth()->user()->id,
                'type' => 'Cashout',
                'content' => "Cash-out from $withdrawal->reference_number has been approved and deducted"
            ]);
            $users = User::find(auth()->user()->id);
            $users->increment('wallet_balance',$withdrawal->withdrawal_amount);
            $withdrawal->update($request->only('status', 'source_fund_id', 'processed_by'));
            DB::commit();
            $notification = array(
                'message' => 'Withdrawal Updated successfully',
                'type' => 'success',
            );
            return response()->json($notification);
        } catch (\Exception $e) {
            DB::rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger',
            );
            return response()->json($notification);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Withdrawal  $withrawal
     * @return \Illuminate\Http\Response
     */
    public function decline(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $decline = \App\DeclineTemplate::find($request->decline_template_id);

            $request->merge([
                'status' => 'Declined',
                'processed_by' => auth()->user()->id,
                'decline_reason' => $decline->description
            ]);
            $withdrawal = Withdrawal::find($id);
            if($withdrawal->status != 'Pending'){
                $notification = array(
                    'message' => 'This request has been processed already',
                    'type' => 'danger',
                );
                return response()->json($notification);
            }
            $withdrawal->update($request->only('status', 'processed_by', 'decline_reason', 'decline_reason_id'));
            User::find($withdrawal->user_id)->increment('wallet_balance', $withdrawal->withdrawal_amount);
            WalletHistory::create([
                'user_id' => $withdrawal->user_id,
                'amount' => $withdrawal->withdrawal_amount,
                'type' => 'Request for Cash-out has been Declined'
            ]);
            UserLogs::create([
                'user_id' => \auth()->user()->id,
                'type' => 'Cashout',
                'content' => "Cash-out with reference $withdrawal->reference_number has been declined"
            ]);
            DB::commit();
            $notification = array(
                'message' => 'Withdrawal Declined successfully',
                'type' => 'warning',
            );
            return response()->json($notification);
        } catch (\Exception $e) {
            DB::rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger',
            );
            return response()->json($notification);
        }
    }
    public function declineWithdrawalAgent(Request $request, $id){
        DB::beginTransaction();
        // try {
            $decline = \App\DeclineTemplate::find($request->decline_template_id);

            $request->merge([
                'status' => 'Declined',
                'processed_by' => auth()->user()->id,
                'decline_reason' => $decline->description
            ]);
            $withdrawal = WithdrawalPlayer::find($id);
            if($withdrawal->status != 'Pending'){
                $notification = array(
                    'message' => 'This request has been processed already',
                    'type' => 'danger',
                );
                return response()->json($notification);
            }
            $withdrawal->update($request->only('status', 'processed_by', 'decline_reason', 'decline_reason_id'));
            User::find($withdrawal->user_id)->increment('wallet_balance', $withdrawal->withdrawal_amount);
            WalletHistory::create([
                'user_id' => $withdrawal->user_id,
                'amount' => $withdrawal->withdrawal_amount,
                'type' => 'Request for Cash-out has been Declined'
            ]);
            UserLogs::create([
                'user_id' => \auth()->user()->id,
                'type' => 'Cashout',
                'content' => "Cash-out with reference $withdrawal->reference_number has been declined"
            ]);
            DB::commit();
            $notification = array(
                'message' => 'Withdrawal Declined successfully',
                'type' => 'warning',
            );
            return response()->json($notification);
        // } catch (\Exception $e) {
        //     DB::rollback();
        //     $notification = array(
        //         'message' => $e->getMessage(),
        //         'type' => 'danger',
        //     );
        //     return response()->json($notification);
        // }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Withdrawal  $withrawal
     * @return \Illuminate\Http\Response
     */
    public function destroy(Withdrawal $withrawal)
    {
        //
    }

    public function requestAgent()
    {
        return view('Withdrawal.request-agent');
    }

    private function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}
=======
<?php

namespace App\Http\Controllers;

use App\BankAccounts;
use App\BankTransactions;
use App\Events\NotificationEvent;
use App\User;
use App\UserLogs;
use App\Wallet;
use App\WalletHistory;
use App\Withdrawal;
use App\WithdrawalTemplate;
use Illuminate\Http\Request;
use App\WithdrawalPlayer;
use App\GameSession;
use Illuminate\Support\Facades\DB;

class WithdrawalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if(!auth()->user()->can('withdrawal-recent')){
            abort(403, 'Unauthorized');
        }else if(auth()->user()->type == 'Player'){
            abort(403, 'Unauthorized');
        }
        return view('Withdrawal.withdrawal-recent');
    }

    public function getRecent()
    {
        $records = Withdrawal::with('users')->where('status', 'Pending')->latest()->get();

        return response()->json($records);
    }
    public function getRecentAgents()
    {
        $records = WithdrawalPlayer::with('users')->where('status', 'Pending')->where('agent_id',auth()->user()->id)->latest()->get();

        return response()->json($records);
    }

    public function logs()
    {
        if(!auth()->user()->can('withdrawal-history')){
            abort(403, 'Unauthorized');
        }else if(auth()->user()->type == 'Player'){
            abort(403, 'Unauthorized');
        }
        return view('Withdrawal.withdrawal-logs');
    }

    public function getLogs()
    {
        $records = Withdrawal::with('processed_by')->with('users')->where('status','!=','Pending')->with('withdrawal_template')->latest()->get();
        return response()->json($records);
    }
    public function markpaid(Request $request,$id){
        $paid = WithdrawalPlayer::find($id);
        $paid->update($request->all());
        $notification = array(
            'message' => 'Marked as paid',
            'type' => 'success',
        );
        return response()->json($notification);
    }
    public function getLogsAgents(){
        $records = WithdrawalPlayer::with('processed_by')
        ->with('users')
        ->where('status','!=','Pending')
        ->with('withdrawal_template')
        ->where('agent_id',auth()->user()->id)
        ->latest()
        ->get();
        return response()->json($records);
    }

    public function request()
    {
        if(!auth()->user()->can('withdrawal-request')){
            abort(403);
        }
        return view('Withdrawal.request');
    }

    public function getWithdrawalTemplate()
    {
        $templates = WithdrawalTemplate::where('user_id', auth()->user()->id)->latest()->get();

        return response()->json($templates);
    }
    public function deleteWithTemplate($id){
        DB::beginTransaction();
        try {
            WithdrawalTemplate::where('id',$id)->where('user_id',auth()->user()->id)->delete();
            DB::commit();
            $notification = array(
                'message' => ' Deleting Withdrawal template successfully',
                'type' => 'success',
            );

            return response()->json($notification);
        } catch (\Exception $e) {
            DB::rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger',
            );

            return response()->json($notification);
        }

    }
    public function updateWithTemplate(Request $request,$id){
        DB::beginTransaction();
        try {
            $template = WithdrawalTemplate::find($id);
            $template->update($request->all());
            UserLogs::create([
                'user_id' => \auth()->user()->id,
                'type' => 'Withdrawal_Template',
                'content' => "Withdrawal Template has been Updated"
            ]);
            DB::commit();
            $notification = array(
                'message' => ' Updating Withdrawal template successfully',
                'type' => 'success',
            );

            return response()->json($notification);
        } catch (\Exception $e) {
            DB::rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger',
            );

            return response()->json($notification);
        }
    }

    public function createWithdrawalTemplate(Request $request)
    {
        DB::beginTransaction();

        try {
            //

            $withdrawal = WithdrawalTemplate::create([
                'account_name' => $request->post('account_name'),
                'account_number' => $request->post('account_number'),
                'payment_type' => $request->post('payment_type'),
                'bank_type' => $request->post('bank_type'),
                'user_id' => auth()->user()->id,
            ]);


            UserLogs::create([
                'user_id' => \auth()->user()->id,
                'type' => 'Withdrawal_Template',
                'content' => "Withdrawal Template has been created"
            ]);

            DB::commit();
            $notification = array(
                'message' => ' successfully',
                'type' => 'success',
                'id' => $withdrawal->id
            );

            return response()->json($notification);
        } catch (\Exception $e) {

            DB::rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger',
            );

            return response()->json($notification);
        }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    public function getmywithdrawal(){
        $cashout = Withdrawal::with('decline_template')->where('user_id', auth()->user()->id)->latest()->get();
        return response()->json($cashout);
    }
    public function getmywithdrawalAgent(){
        $cashout = WithdrawalPlayer::with('processed_by')->with('decline_template')->where('user_id', auth()->user()->id)->latest()->get();
        return response()->json($cashout);
    }
    public function cancel_mycashout(Request $request){
        DB::begintransaction();
        try {
            if(auth()->user()->type == 'Master Agent'){
                $with = Withdrawal::find($request->with_id);
            }else{
                $with = WithdrawalPlayer::find($request->with_id);
            }
            if($with->status == 'Pending'){
                if($with->user_id === auth()->user()->id){
                    $with->update(['status' => 'Cancelled']);
                    $user = User::find(auth()->user()->id);
                    $user->increment('wallet_balance',$with->withdrawal_amount);
                    $notification = array(
                        'message' => 'Cancel Cashout Request Successfully !',
                        'type' => 'success',
                    );
                    UserLogs::create([
                        'user_id' => \auth()->user()->id,
                        'type' => 'Cashout',
                        'content' => "Cash-out ($request->with_id) has been cancelled"
                    ]);
                    DB::commit();
                    return response()->json($notification);
                }
            }
            $notification = array(
                'message' => 'Invalid Request !',
                'type' => 'danger',
            );
            return response()->json($notification);
        } catch (\Exception $e) {
            DB::rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger',
            );
            return response()->json($notification);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $get_withdrawal_template_info = WithdrawalTemplate::find($request->withdrawal_template_id);
            $request->merge([
                'user_id' => auth()->user()->id,
                'account_name' => $get_withdrawal_template_info->account_name,
                'account_number' => $get_withdrawal_template_info->account_number,
                'bank_type' => $get_withdrawal_template_info->bank_type,
                'payment_type' => $get_withdrawal_template_info->payment_type,
                'reference_number' => "WT-".$this->generateRandomString(20)
            ]);
            $reference = $request->post('reference_number');
            if ($request->post('type') === 'Wallet'){
                $user = User::find($request->post('user_id'));
                if ($user->wallet_balance >= $request->post('withdrawal_amount')){
                    User::where('id', $user->id)->decrement('wallet_balance', $request->post('withdrawal_amount'));
                    WalletHistory::create([
                        'user_id' => $user->id,
                        'amount' => $request->post('withdrawal_amount'),
                        'type' => 'Request for Cash-out'
                    ]);
                    UserLogs::create([
                        'user_id' => \auth()->user()->id,
                        'type' => 'Cashout',
                        'content' => "Cash-out $reference has been requested"
                    ]);
                } else {
                    $notification = array(
                        'message' => 'Insufficient Withdrawal Amount',
                        'type' => 'danger',
                    );
                    return response()->json($notification);
                }
            }
            else {
                $user = User::find($request->post('user_id'));
                if ($user->agent_commission_balance >= $request->post('agent_commission_balance')) {
                    User::where('id', $user->id)->decrement('agent_commission_balance', $request->post('withdrawal_amount'));
                    UserLogs::create([
                        'user_id' => \auth()->user()->id,
                        'type' => 'Cashout',
                        'content' => "Cash-out from commission balance with reference $reference has been requested"
                    ]);
                } else {
                    $notification = array(
                        'message' => 'Insufficient Withdrawal Amount',
                        'type' => 'danger',
                    );
                    return response()->json($notification);
                }
            }
            $message = "You have a New Withdrawal Request";
            event(new NotificationEvent($message, auth()->user()->id, '/withdrawal', $request->post('user_id')));
            Withdrawal::create($request->all());
            DB::commit();
            $notification = array(
                'message' => 'Withdrawal Submitted successfully',
                'type' => 'success',
            );
            return response()->json($notification);
        } catch (\Exception $e) {
            DB::rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger',
            );
            return response()->json($notification);
        }
    }
    public function playerWithdrawal(Request $request){
        DB::beginTransaction();
        // try {
            $get_withdrawal_template_info = WithdrawalTemplate::find($request->withdrawal_template_id);
            $request->merge([
                'user_id' => auth()->user()->id,
                'agent_id' => $request->post('agent_id'),
                'account_name' => $get_withdrawal_template_info->account_name,
                'account_number' => $get_withdrawal_template_info->account_number,
                'bank_type' => $get_withdrawal_template_info->bank_type,
                'payment_type' => $get_withdrawal_template_info->payment_type,
                'reference_number' => "WT-".$this->generateRandomString(20)
            ]);
            $reference = $request->post('reference_number');
            if ($request->post('type') === 'Wallet'){
                $user = User::find($request->post('user_id'));
                $agent = User::find($request->post('agent_id'));
                if ($user->wallet_balance >= $request->post('withdrawal_amount')){
                    User::where('id', $user->id)->decrement('wallet_balance', $request->post('withdrawal_amount'));
                    UserLogs::create([
                        'user_id' => \auth()->user()->id,
                        'type' => 'Cashout Agent',
                        'content' => "Cash-out $reference has been requested by $user->name($user->id) to Agent ID:$agent->name($agent->id)
                        with amount of (P ".$request->post('withdrawal_amount').")"
                    ]);
                    WalletHistory::create([
                        'user_id' => auth()->user()->id,
                        'type' => "You have created a cashout for Agent: $agent->name($agent->id)",
                        'amount' => $request->post('withdrawal_amount'),
                    ],[
                        'user_id' => $agent->id,
                        'type' => "User:: $user->name($user->id) has requested a cash-out to you with ref# of $reference",
                        'amount' => $request->post('withdrawal_amount'),
                    ]);
                } else {
                    $notification = array(
                        'message' => 'Insufficient Withdrawal Amount',
                        'type' => 'danger',
                    );
                    return response()->json($notification);
                }
            }
            $message = "You have a New Withdrawal Request";
            event(new NotificationEvent($message, auth()->user()->id, '/withdrawal', $request->post('user_id')));
            WithdrawalPlayer::create($request->all());
            DB::commit();
            $notification = array(
                'message' => 'Withdrawal Submitted successfully',
                'type' => 'success',
            );
            return response()->json($notification);
        // } catch (\Exception $e) {
        //     DB::rollback();
        //     $notification = array(
        //         'message' => $e->getMessage(),
        //         'type' => 'danger',
        //     );
        //     return response()->json($notification);
        // }
    }

    public function storeAgent(Request $request)
    {
        //

        DB::beginTransaction();

        try {
            //

            $request->merge([
                'user_id' => auth()->user()->id,
                'reference_number' => "WT-".$this->generateRandomString(20),
                'type' => 'Commission'
            ]);


            $message = "You have a New Withdrawal Request";

            event(new NotificationEvent($message, auth()->user()->id, '/withdrawal', $request->post('user_id')));

            Withdrawal::create($request->all());

            DB::commit();
            $notification = array(
                'message' => 'Withdrawal Submitted successfully',
                'type' => 'success',
            );

            return response()->json($notification);
        } catch (\Exception $e) {

            DB::rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger',
            );

            return response()->json($notification);
        }
    }



    /**
     * Display the specified resource.
     *
     * @param  \App\Withdrawal  $withrawal
     * @return \Illuminate\Http\Response
     */
    public function show(Withdrawal $withrawal)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Withdrawal  $withrawal
     * @return \Illuminate\Http\Response
     */
    public function edit(Withdrawal $withrawal)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Withdrawal  $withrawal
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $request->merge([
                'status' => 'Transferred',
                'processed_by' => auth()->user()->id
            ]);
            $amount = BankAccounts::find($request->post('source_fund_id'));
            if ($amount->balance >= $request->post('withdrawal_amount')){
                BankAccounts::where('id', $request->post('source_fund_id'))->decrement('balance', $request->post('withdrawal_amount') - $request->post('processing_fee'));
                $withdrawal = Withdrawal::with('users')->find($id);
                if($withdrawal->status != 'Pending'){
                    $notification = array(
                        'message' => 'This request has been processed already !',
                        'type' => 'danger',
                    );
                    return response()->json($notification);
                }
                WalletHistory::create([
                    'user_id' => $withdrawal->user_id,
                    'amount' => $request->post('withdrawal_amount') - $request->post('processing_fee'),
                    'type' => 'Request for Cash-out has been Approved'
                ]);
                BankTransactions::create(
                    [
                        'bank_id' => $request->post('source_fund_id'),
                        'debit' => $request->post('withdrawal_amount'),
                        'credit' => 0,
                        'transaction_code' => $withdrawal->reference_number,
                        'transaction_details' => "Request for withdrawal has been credited to ". $withdrawal->users->id . ',' . $withdrawal->users->contact_number
                    ]
                );
                UserLogs::create([
                    'user_id' => \auth()->user()->id,
                    'type' => 'Cashout',
                    'content' => "Cash-out from $withdrawal->reference_number has been approved and deducted"
                ]);
                $withdrawal->update($request->only('status', 'source_fund_id', 'processed_by'));
                DB::commit();
                $notification = array(
                    'message' => 'Withdrawal Updated successfully',
                    'type' => 'success',
                );
                return response()->json($notification);
            } else {
                $notification = array(
                    'message' => 'Not Enough Wallet Balance',
                    'type' => 'danger',
                );
                return response()->json($notification);
            }
        } catch (\Exception $e) {
            DB::rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger',
            );
            return response()->json($notification);
        }
    }
    public function updateWithdrawalAgents(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $request->merge([
                'status' => 'Approved',
                'processed_by' => auth()->user()->id
            ]);
            $withdrawal = WithdrawalPlayer::with('users')->find($id);
            if($withdrawal->status != 'Pending'){
                $notification = array(
                    'message' => 'This request has been processed already !',
                    'type' => 'danger',
                );
                return response()->json($notification);
            }
            WalletHistory::create([
                'user_id' => $withdrawal->user_id,
                'amount' => $request->post('withdrawal_amount') - $request->post('processing_fee'),
                'type' => 'Request for Cash-out has been Approved'
            ]);
            UserLogs::create([
                'user_id' => \auth()->user()->id,
                'type' => 'Cashout',
                'content' => "Cash-out from $withdrawal->reference_number has been approved and deducted"
            ]);
            $users = User::find(auth()->user()->id);
            $users->increment('wallet_balance',$withdrawal->withdrawal_amount);
            $withdrawal->update($request->only('status', 'source_fund_id', 'processed_by'));
            DB::commit();
            $notification = array(
                'message' => 'Withdrawal Updated successfully',
                'type' => 'success',
            );
            return response()->json($notification);
        } catch (\Exception $e) {
            DB::rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger',
            );
            return response()->json($notification);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Withdrawal  $withrawal
     * @return \Illuminate\Http\Response
     */
    public function decline(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $decline = \App\DeclineTemplate::find($request->decline_template_id);

            $request->merge([
                'status' => 'Declined',
                'processed_by' => auth()->user()->id,
                'decline_reason' => $decline->description
            ]);
            $withdrawal = Withdrawal::find($id);
            if($withdrawal->status != 'Pending'){
                $notification = array(
                    'message' => 'This request has been processed already',
                    'type' => 'danger',
                );
                return response()->json($notification);
            }
            $withdrawal->update($request->only('status', 'processed_by', 'decline_reason', 'decline_reason_id'));
            User::find($withdrawal->user_id)->increment('wallet_balance', $withdrawal->withdrawal_amount);
            WalletHistory::create([
                'user_id' => $withdrawal->user_id,
                'amount' => $withdrawal->withdrawal_amount,
                'type' => 'Request for Cash-out has been Declined'
            ]);
            UserLogs::create([
                'user_id' => \auth()->user()->id,
                'type' => 'Cashout',
                'content' => "Cash-out with reference $withdrawal->reference_number has been declined"
            ]);
            DB::commit();
            $notification = array(
                'message' => 'Withdrawal Declined successfully',
                'type' => 'warning',
            );
            return response()->json($notification);
        } catch (\Exception $e) {
            DB::rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger',
            );
            return response()->json($notification);
        }
    }
    public function declineWithdrawalAgent(Request $request, $id){
        DB::beginTransaction();
        // try {
            $decline = \App\DeclineTemplate::find($request->decline_template_id);

            $request->merge([
                'status' => 'Declined',
                'processed_by' => auth()->user()->id,
                'decline_reason' => $decline->description
            ]);
            $withdrawal = WithdrawalPlayer::find($id);
            if($withdrawal->status != 'Pending'){
                $notification = array(
                    'message' => 'This request has been processed already',
                    'type' => 'danger',
                );
                return response()->json($notification);
            }
            $withdrawal->update($request->only('status', 'processed_by', 'decline_reason', 'decline_reason_id'));
            User::find($withdrawal->user_id)->increment('wallet_balance', $withdrawal->withdrawal_amount);
            WalletHistory::create([
                'user_id' => $withdrawal->user_id,
                'amount' => $withdrawal->withdrawal_amount,
                'type' => 'Request for Cash-out has been Declined'
            ]);
            UserLogs::create([
                'user_id' => \auth()->user()->id,
                'type' => 'Cashout',
                'content' => "Cash-out with reference $withdrawal->reference_number has been declined"
            ]);
            DB::commit();
            $notification = array(
                'message' => 'Withdrawal Declined successfully',
                'type' => 'warning',
            );
            return response()->json($notification);
        // } catch (\Exception $e) {
        //     DB::rollback();
        //     $notification = array(
        //         'message' => $e->getMessage(),
        //         'type' => 'danger',
        //     );
        //     return response()->json($notification);
        // }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Withdrawal  $withrawal
     * @return \Illuminate\Http\Response
     */
    public function destroy(Withdrawal $withrawal)
    {
        //
    }

    public function requestAgent()
    {
        if(!auth()->user()->can('withdrawal-request')){
            abort(403);
        }
        return view('Withdrawal.request-agent');
    }

    private function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}
>>>>>>> ae5b3d3ce322a6f5ff74d79fe1c0fb883afacf4c
