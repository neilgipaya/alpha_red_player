@extends('layouts/contentLayoutMaster')

@section('breadcrumb')
    <div class="clearfix"></div>
@endsection
@section('content')
    @if(auth()->user()->type == 'Master Agent' || auth()->user()->type == 'Gold Agent' || auth()->user()->type == 'Silver Agent')
    <withdrawal-recent-agents></withdrawal-recent-agents>
    @else
    <withdrawal-recent></withdrawal-recent>
    @endif
@endsection

@section('vendor-scripts')

@endsection
