@php
    $configData = Helper::applClasses();
@endphp
<div class="main-menu menu-fixed {{($configData['theme'] === 'light') ? "menu-light" : "menu-dark"}} menu-accordion menu-shadow" data-scroll-to-active="true"
 style="background: #5543FF !important;">
    <div class="navbar-header" style="margin-left: -20px;">
        <ul class="nav navbar-nav flex-row">
            <li class="nav-item mr-auto">
                <a class="navbar-brand" href="#">
                    <div class="brand-logo"></div>
                    <h2 class="brand-text mb-0" style="color:white !important; font-size: 18px !important; ">Alpha PH</h2>
                </a>
            </li>
            <li class="nav-item nav-toggle"><a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse">
                    <i style="color:white !important;" class="toggle-icon feather icon-disc font-medium-4 d-none d-xl-block primary collapse-toggle-icon" data-ticon="icon-disc"></i></a></li>
        </ul>
    </div>
    <div class="shadow-bottom"></div>
    <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" style="background: #5543FF !important;" data-menu="menu-navigation">
            {{-- Foreach menu item starts --}}
            <li aria-haspopup="true">
                <a href="/profile" class=" {{ (request()->is('profile')) ? 'active' : '' }}"><i class="fa fa-user"></i> Profile</a>
            </li>
            @if(auth()->user()->type === "Silver Agent" or auth()->user()->type === "Gold Agent" or auth()->user()->type === "Master Agent")
                <li aria-haspopup="true">
                    <a href="/agentDashboard" class=" {{ (request()->is('agentDashboard')) ? 'active' : '' }}">
                        <i class="fa fa-dashboard"></i> Agent Dashboard
                    </a>
                </li>
                <li aria-haspopup="true">
                    <a href="/user/myPlayers" class=" {{ (request()->is('user/myPlayers')) ? 'active' : '' }}">
                        <i class="fa fa-users"></i> My Players
                    </a>
                </li>
                @if(auth()->user()->type != 'Player')
                    @if(auth()->user()->type != 'Silver Agent')
                    <li aria-haspopup="true">
                        <a href="/user/myDownlineAgents" class=" {{ (request()->is('user/myDownlineAgents')) ? 'active' : '' }}">
                            <i class="fa fa-users"></i> My Agents
                        </a>
                    </li>
                    @endif
                @endif
                <li aria-haspopup="true">
                    <a href="/user/myInactivePlayers" class=" {{ (request()->is('user/myInactivePlayers')) ? 'active' : '' }}">
                        <i class="fa fa-users"></i> My Inactive Players
                    </a>
                </li>
                <li aria-haspopup="true">
                    <a href="/user/convert-commission" class=" {{ (request()->is('user/convert-commission')) ? 'active' : '' }}">
                        <i class="fa fa-money"></i> Convert Commission
                    </a>
                </li>
            @endif
            @if(auth()->user()->type != 'Official')
            <li aria-haspopup="true">
                <a href="/user/play-stats" class=" {{ (request()->is('user/play-stats')) ? 'active' : '' }}">
                    <i class="fa fa-industry"></i> Play Stats
                </a>
            </li>
            <li aria-haspopup="true">
                <a href="/user/walletTransfer" class=" {{ (request()->is('user/walletTransfer')) ? 'active' : '' }}">
                    <i class="fa fa-dollar"></i> Wallet Transfer
                </a>
            </li>
            <li aria-haspopup="true">
                <a href="/user/walletHistory/{{auth()->user()->id}}" class=" {{ (request()->is('user/walletHistory/'.auth()->user()->id)) ? 'active' : '' }}">
                    <i class="fa fa-dollar"></i> Wallet History
                </a>
            </li>
            @endif
            @can('dashboard-admin')
            <li aria-haspopup="true">
                <a href="/dashboard" class=" {{ (request()->is('dashboard')) ? 'active' : '' }}"><i class="fa fa-dashboard"></i> Dashboard</a>
            </li>
            @endcan
            @can('play-game')
            <li aria-haspopup="true">
                <a href="/game/play-game" class=" {{ (request()->is('game/play-game')) ? 'active' : '' }}"><i class="fa fa-gamepad"></i> Play Game</a>
            </li>
            @endcan
            @can('host-game-module')
            <li aria-haspopup="true" class="{{ (request()->segment(1) == 'game' && request()->segment(2) != 'play-game') ? 'open':'' }}"><a href="#" class="sub-icon  ">
                <i class="fa fa-keyboard-o"></i> Host Game<i class="fe fe-chevron-down horizontal-icon"></i></a>
                <ul class="sub-menu">
                    @can('manage-game-session')
                    <li aria-haspopup="true"><a href="/game" class="slide-item {{ (request()->is('game')) ? 'active' : '' }}"><i class="fa fa-circle-thin"></i>Game List</a></li>
                    <li aria-haspopup="true"><a href="/game/current-session" class="slide-item {{ (request()->is('game/current-session')) ? 'active' : '' }}"><i class="fa fa-circle-thin"></i>Manage Session</a></li>
                    @endcan
                    @can('session-history')
                    <li aria-haspopup="true"><a href="/game/logs" class="slide-item {{ (request()->is('game/logs')) ? 'active' : '' }}"><i class="fa fa-circle-thin"></i>Session History</a></li>
                    <li aria-haspopup="true"><a href="/game/bets" class="slide-item {{ (request()->is('game/bets')) ? 'active' : '' }}"><i class="fa fa-circle-thin"></i>Player Bets</a></li>
                    @endcan
                </ul>
            </li>
            @endcan
            @can('user-management')
            <li aria-haspopup="true">
                <a href="/user" class=" {{ (request()->is('user')) ? 'active' : '' }}"><i class="fa fa-user"></i> User Management</a>
            </li>
            <li aria-haspopup="true">
                <a href="/user/deactivate" class=" {{ (request()->is('user/deactivate')) ? 'active' : '' }}"><i class="fa fa-user"></i> Deactivated Accounts</a>
            </li>
            <li aria-haspopup="true">
                <a href="/user/inactiveAccounts" class=" {{ (request()->is('user/inactiveAccounts')) ? 'active' : '' }}"><i class="fa fa-user"></i> Inactive Accounts</a>
            </li>
            <li aria-haspopup="true">
                <a href="/user/player" class=" {{ (request()->is('user/player')) ? 'active' : '' }}"><i class="fa fa-user"></i> Player Management</a>
            </li>
            <li aria-haspopup="true">
                <a href="/user/agent" class=" {{ (request()->is('user/agent')) ? 'active' : '' }}"><i class="fa fa-user"></i> Agent Management</a>
            </li>
            @endcan
            @can('top-up-module')
            <li aria-haspopup="true" class="{{ (request()->segment(1) == 'topup') ? 'open':'' }}"><a href="#" class="sub-icon ">
                    <i class="fa fa-money"></i> Cash-in <i class="fe fe-chevron-down horizontal-icon"></i></a>
                <ul class="sub-menu">
                    @can('top-up-request')
                    <li aria-haspopup="true"><a href="/topup/request" class="slide-item {{ (request()->is('topup/request')) ? 'active' : '' }}"><i class="fa fa-circle-thin"></i>Request Cash-in</a></li>
                    @endcan
                    @if(auth()->user()->type != 'Official')
                    <li aria-haspopup="true"><a href="/topup/cash-in-history" class="slide-item {{ (request()->is('topup/cash-in-history')) ? 'active' : '' }}"><i class="fa fa-circle-thin"></i>My Cash-in</a></li>
                    @endif
                    @if(auth()->user()->type !== 'Player')
                    <!-- <li aria-haspopup="true"><a href="/topup/other" class="slide-item {{ (request()->is('topup/other')) ? 'active' : '' }}"><i class="fa fa-circle-thin"></i>Request for Other User</a></li> -->
                    @endif
                    @can('top-up-recent')
                    @if(auth()->user()->type !== 'Player')
                    <li aria-haspopup="true"><a href="/topup" class="slide-item {{ (request()->is('topup')) ? 'active' : '' }}"><i class="fa fa-circle-thin"></i>Pending Cash-in</a></li>
                    @endif
                    @endcan
                    @can('banker-topup-approval')
                    <li aria-haspopup="true"><a href="/topup/banker-topup-approval" class="slide-item {{ (request()->is('topup/banker-topup-approval')) ? 'active' : '' }}"><i class="fa fa-circle-thin"></i>Banker Approval</a></li>
                    @endcan
                    @can('top-up-history')
                    @if(auth()->user()->type !== 'Player')
                    <li aria-haspopup="true"><a href="/topup/logs" class="slide-item {{ (request()->is('topup/logs')) ? 'active' : '' }}"><i class="fa fa-circle-thin"></i>Cash-in List</a></li>
                    @endif
                    @endcan
                </ul>
            </li>
            @endcan
            @can('withdrawal-module')
            <li aria-haspopup="true" class="{{ (request()->segment(1) == 'withdrawal') ? 'open':'' }}"><a href="#" class="sub-icon  ">
                    <i class="fa fa-money"></i> Cash-out <i class="fe fe-chevron-down horizontal-icon"></i></a>
                <ul class="sub-menu">
                    @can('withdrawal-request')
                    <li aria-haspopup="true"><a href="/withdrawal/request" class="slide-item {{ (request()->is('withdrawal/request')) ? 'active' : '' }}"><i class="fa fa-circle-thin"></i>Request Cash-out</a></li>
                    @endcan
                    @if(auth()->user()->type === 'Silver Agent' or auth()->user()->type === 'Gold Agent' or auth()->user()->type === 'Master Agent')
                    <!-- <li aria-haspopup="true"><a href="/withdrawal/agent" class="slide-item {{ (request()->is('withdrawal/agent')) ? 'active' : '' }}"><i class="fa fa-circle-thin"></i>Commission Cash-out</a></li> -->
                    @endif
                    @can('play-game')
                    <li aria-haspopup="true"><a href="/withdrawal/cash-out-history" class="slide-item {{ (request()->is('withdrawal/cash-out-history')) ? 'active' : '' }}"><i class="fa fa-circle-thin"></i>My Cash-out</a></li>
                    @endcan
                    @can('withdrawal-recent')
                        @if(auth()->user()->type != 'Player')
                        <li aria-haspopup="true"><a href="/withdrawal" class="slide-item {{ (request()->is('withdrawal')) ? 'active' : '' }}"><i class="fa fa-circle-thin"></i>Pending Cash-out</a></li>
                        @endif
                    @endcan
                    @can('withdrawal-history')
                        @if(auth()->user()->type != 'Player')
                            <li aria-haspopup="true"><a href="/withdrawal/logs" class="slide-item {{ (request()->is('withdrawal/logs')) ? 'active' : '' }}"><i class="fa fa-circle-thin"></i>Cash-out List</a></li>
                        @endif
                    @endcan
                </ul>
            </li>
            @endcan
            @can('funds-transfer-module')
            <li aria-haspopup="true" class="{{ (request()->segment(1) == 'funds') ? 'open':'' }}"><a href="#" class="sub-icon  ">
                    <i class="fa fa-bank"></i> Transfers <i class="fe fe-chevron-down horizontal-icon"></i></a>
                <ul class="sub-menu">
                    @can('funds-request')
                    <li aria-haspopup="true"><a href="/funds/request" class="slide-item {{ (request()->is('funds/request')) ? 'active' : '' }}"><i class="fa fa-circle-thin"></i>Request Funds Transfer</a></li>
                    @endcan
                    @can('funds-recent')
                    <li aria-haspopup="true"><a href="/funds" class="slide-item {{ (request()->is('funds')) ? 'active' : '' }}"><i class="fa fa-circle-thin"></i>Pending Funds Transfer</a></li>
                    @endcan
                    @can('funds-history')
                    <li aria-haspopup="true"><a href="/funds/logs" class="slide-item {{ (request()->is('funds/logs')) ? 'active' : '' }}"><i class="fa fa-circle-thin"></i>Funds Transfer Logs</a></li>
                    @endcan
                </ul>
            </li>
            @endcan
            @can('reports')
                <li aria-haspopup="true" class="{{ (request()->segment(1) == 'reports') ? 'open':'' }}"><a href="#" class="sub-icon  ">
                        <i class="fa fa-file"></i> Reports <i class="fe fe-chevron-down horizontal-icon"></i></a>
                    <ul class="sub-menu">
                        <!-- <li aria-haspopup="true"><a href="/reports/rake" class="slide-item {{ (request()->is('reports/rake')) ? 'active' : '' }}"><i class="fa fa-circle-thin"></i>Rake Reports</a></li> -->
                        <li aria-haspopup="true"><a href="/reports/advance-reports" class="slide-item {{ (request()->is('reports/advance-reports')) ? 'active' : '' }}"><i class="fa fa-circle-thin"></i>Advance Reports</a></li>
                        <!-- <li aria-haspopup="true"><a href="/reports/agent-commission" class="slide-item {{ (request()->is('reports/agent-commission')) ? 'active' : '' }}"><i class="fa fa-circle-thin"></i>Agent Commission Reports</a></li>
                        <li aria-haspopup="true"><a href="/reports/cash_in" class="slide-item {{ (request()->is('reports/cash_in')) ? 'active' : '' }}"><i class="fa fa-circle-thin"></i>Cash-in Reports</a></li>
                        <li aria-haspopup="true"><a href="/reports/cash_out" class="slide-item {{ (request()->is('reports/cash_out')) ? 'active' : '' }}"><i class="fa fa-circle-thin"></i>Cash-out Reports</a></li>
                        <li aria-haspopup="true"><a href="/reports/fund_transfer" class="slide-item {{ (request()->is('reports/fund_transfer')) ? 'active' : '' }}"><i class="fa fa-circle-thin"></i>Funds Transfer Reports</a></li>
                        <li aria-haspopup="true"><a href="/reports/user_report" class="slide-item {{ (request()->is('reports/user_report')) ? 'active' : '' }}"><i class="fa fa-circle-thin"></i>User Player Reports</a></li>
                        <li aria-haspopup="true"><a href="/reports/fund_account" class="slide-item {{ (request()->is('reports/fund_account')) ? 'active' : '' }}"><i class="fa fa-circle-thin"></i>Fund Account Reports</a></li>
                        <li aria-haspopup="true"><a href="/reports/betting" class="slide-item {{ (request()->is('reports/betting')) ? 'active' : '' }}"><i class="fa fa-circle-thin"></i>Betting Reports</a></li> -->

                    </ul>
                </li>
            @endcan
            @can('configuration-module')
            <li aria-haspopup="true" class="{{ (request()->segment(1) == 'configuration') ? 'open':'' }}"><a href="#" class="sub-icon  ">
                <i class="fa fa-cog"></i> Configuration <i class="fe fe-chevron-down horizontal-icon"></i></a>
                <ul class="sub-menu">
                    @can('banker-module')
                    <li aria-haspopup="true"><a href="/configuration/banker" class="slide-item {{ (request()->is('configuration/banker')) ? 'active' : '' }}"><i class="fa fa-circle-thin"></i>Banker Settings</a></li>
                    @endcan
                    @can('fund-accounts')
                    <li aria-haspopup="true"><a href="/configuration/banks" class="slide-item {{ (request()->is('configuration/banks')) ? 'active' : '' }}"><i class="fa fa-circle-thin"></i>Fund Accounts</a></li>
                    @endcan
                    @can('decline-templates')
                    <li aria-haspopup="true"><a href="/configuration/decline-template" class="slide-item {{ (request()->is('configuration/decline-template')) ? 'active' : '' }}"><i class="fa fa-circle-thin"></i>Decline Template</a></li>
                    @endcan
                    @can('partner-settings')
                    <li aria-haspopup="true"><a href="/configuration/partners" class="slide-item {{ (request()->is('configuration/partners')) ? 'active' : '' }}"><i class="fa fa-circle-thin"></i>Partner Setting</a></li>
                    @endcan
                    @can('roles')
                    <li aria-haspopup="true"><a href="/configuration/roles" class="slide-item {{ (request()->is('configuration/roles')) ? 'active' : '' }}"><i class="fa fa-circle-thin"></i>Roles and Permissions</a></li>
                    @endcan
                    @can('user-logs')
                        <li aria-haspopup="true"><a href="/configuration/UserLogs" class="slide-item {{ (request()->is('configuration/UserLogs')) ? 'active' : '' }}"><i class="fa fa-circle-thin"></i>User Logs</a></li>
                    @endcan
                </ul>
            </li>
            @endcan
            <li class="nav-item {{ (request()->is('help')) ? 'active' : '' }}">
                <a href="/help">
                    <i class="fa fa-question-circle"></i>
                    <span class="menu-title" data-i18n="">Help Center</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="/logout">
                    <i class="fa fa-sign-out"></i>
                    <span class="menu-title" data-i18n="">Logout</span>
                </a>
            </li>
            {{-- Foreach menu item ends --}}
        </ul>
    </div>
</div>
<!-- END: Main Menu-->
