<?php

namespace App\Http\Controllers;

use App\Partner;
use App\UserLogs;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\GameSession;

class PartnerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if(!auth()->user()->can('roles')){
            abort(403, 'Unauthorized');
        }
        return view('Configuration.Partner.partner');
    }

    public function getList()
    {
        $partners = Partner::latest()->get();

        return response()->json($partners);
    }

    public function getPartnerTotal()
    {
        $partners = Partner::all()->sum('rake_percent');

        return response()->json($partners);
    }
    public function getPartnerListFiltered(Request $request)
    {
        $from = $request->from;
        $to = $request->to;
        $partners = Partner::whereBetween('created_at',[$from, $to])->latest()->get();

        return response()->json($partners);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        DB::beginTransaction();

        try {
            //

            $partner = Partner::create($request->all());
            $game = GameSession::where('status','Ongoing')->first();
            if($game){
                $logs = "Partner $request->partner_name ($partner->rake_percent) has been created during Game: Session ID: $game->id"." Game Title: $game->game_title";
            }else{
                $logs = "Partner $request->partner_name ($partner->rake_percent) has been created";
            }
            UserLogs::create([
                'user_id' => \auth()->user()->id,
                'type' => 'Partner',
                'content' => $logs
            ]);

            DB::commit();
            $notification = array(
                'message' => 'Partner Created successfully',
                'type' => 'success',
            );

            return response()->json($notification);
        } catch (\Exception $e) {

            DB::rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger',
            );

            return response()->json($notification);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Partner  $partner
     * @return \Illuminate\Http\Response
     */
    public function show(Partner $partner)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Partner  $partner
     * @return \Illuminate\Http\Response
     */
    public function edit(Partner $partner)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Partner  $partner
     * @return \Illuminate\Http\Response
     */
    public function getPartnerHistory(){
        return UserLogs::with('user')->where('type','Partner')->latest()->get();
    }
    public function update(Request $request, $id)
    {
        DB::beginTransaction();

        try {
            //
            $partner = Partner::find($id);
            $game = GameSession::where('status','Ongoing')->first();
            if($game){
                $logs = "Partner $request->partner_name ($partner->rake_percent) Rake has been updated from $partner->rake_percent to $request->rake_percent during Game: Session ID: $game->id"." Game Title: $game->game_title";
            }else{
                $logs = $request->partner_name.'('.$request->id.')'." Rake has been updated from $partner->rake_percent to ".$request->post('rake_percent');
            }
            UserLogs::create([
                'user_id' => \auth()->user()->id,
                'type' => 'Partner',
                'content' => $logs
            ]);
            $partner->update($request->all());

            DB::commit();
            $notification = array(
                'message' => 'Partner Updated successfully',
                'type' => 'success',
            );

            return response()->json($notification);
        } catch (\Exception $e) {

            DB::rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger',
            );

            return response()->json($notification);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Partner  $partner
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        DB::beginTransaction();

        try {
            //
            $partner = Partner::find($id);
            $game = GameSession::where('status','Ongoing')->first();
            if($game){
                $logs = "Partner $partner->partner_name ($partner->rake_percent) has been deleted during Game: Session ID: $game->id"." Game Title: $game->game_title";
            }else{
                $logs = "Partner $partner->partner_name ($partner->rake_percent) has been deleted";
            }
            UserLogs::create([
                'user_id' => \auth()->user()->id,
                'type' => 'Partner',
                'content' => $logs
            ]);

            Partner::destroy($id);

            DB::commit();
            $notification = array(
                'message' => 'Partner Deleted successfully',
                'type' => 'success',
            );

            return response()->json($notification);
        } catch (\Exception $e) {

            DB::rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger',
            );

            return response()->json($notification);
        }
    }
}
