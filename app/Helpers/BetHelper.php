<?php


namespace App\Helpers;


use App\Partner;
use App\User;

class BetHelper
{

    public static function totalRake($bets)
    {
        $rake = Partner::all()->sum('rake_percent');

        if ($bets > 0){
            $totalBet = $bets * $rake;

            return $totalBet;
        }

        return 0;
    }

    public static function commission()
    {
        $commission = auth()->user()->agent_rate;

        $percentage = User::where('agent_id', auth()->user()->id)->sum('agent_rate');
        $percentage = floatval(number_format($percentage, 3));

        if ($commission > 0){
            $totalCommission = $commission - $percentage;

            return $totalCommission;
        }

        return 0;
    }
}
