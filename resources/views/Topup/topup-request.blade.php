@extends('layouts/contentLayoutMaster')

@section('breadcrumb')
    <div class="clearfix"></div>
@endsection
@section('content')
    @if(auth()->user()->type == 'Player' || auth()->user()->type == 'Silver Agent' || auth()->user()->type == 'Gold Agent')
    <topup-player></topup-player>
    @else
    <top-up></top-up>
    @endif
@endsection

@section('vendor-scripts')

@endsection
