<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ReportsController extends Controller
{
    public function totalRake()
    {
        if(!auth()->user()->can('reports')){
            abort(403, 'Unauthorized');
        }
        return view('Report.total-rake');
    }
    public function cash_in()
    {
        if(!auth()->user()->can('reports')){
            abort(403, 'Unauthorized');
        }
        return view('Report.cash-in');
    }
    public function cash_out(){
        if(!auth()->user()->can('reports')){
            abort(403, 'Unauthorized');
        }
        return view('Report.cash-out');
    }
    public function fund_transfer(){
        if(!auth()->user()->can('reports')){
            abort(403, 'Unauthorized');
        }
        return view('Report.fund-transfer');
    }
    public function user_reports($id){
        if(!auth()->user()->can('reports')){
            abort(403, 'Unauthorized');
        }
        return view('Report.user_reports',array('id' => $id));
    }
    public function fund_account(){
        if(!auth()->user()->can('reports')){
            abort(403, 'Unauthorized');
        }
        return view('Report.fund-account');
    }
    public function betting(){
        if(!auth()->user()->can('reports')){
            abort(403, 'Unauthorized');
        }
        return view('Report.betting');
    }

    public function agent()
    {
        if(!auth()->user()->can('reports')){
            abort(403, 'Unauthorized');
        }
        return view('Report.agent');
    }
}
