@extends('layouts/contentLayoutMaster')


@section('breadcrumb')
    <div class="clearfix"></div>
@endsection
@section('content')
    <view-agent-players :data="{{$data}}"></view-agent-players>
@endsection

@section('vendor-scripts')

@endsection
