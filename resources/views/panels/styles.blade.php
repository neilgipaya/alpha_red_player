        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600">
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/vendors.min.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/ui/prism.min.css')) }}">
        {{-- Vendor Styles --}}
        @yield('vendor-style')
        {{-- Theme Styles --}}
        <link rel="stylesheet" href="{{ asset(mix('css/bootstrap.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('css/bootstrap-extended.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('css/colors.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('css/components.css')) }}">
        <!-- <link rel="stylesheet" href="{{ asset(mix('css/themes/dark-layout.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('css/themes/semi-dark-layout.css')) }}"> -->
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
              rel="stylesheet"><!-- END: Theme CSS-->
        <script src="https://js.pusher.com/6.0/pusher.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
        <style>
        /* body.dark-layout .card {
                background-color: #1e2022 !important;
        } */
        .main-menu.menu-light .navigation > li .active {
                background: #2C1ED6 !important;
                font-weight:600 !important;
                border-radius:0px !important;
                /* box-shadow: 0 0 10px 1px rgba(128,222,234, 0.7) !important; */
                /* border-radius: 4px !important; */
        }
        .sub-menu{
                background: #5543FF !important;
        }
        .main-menu.menu-light .navigation li a{
                color: #fff !important;
        }
        .main-menu.menu-light .navigation > li.open > a{
                background: #5543FF !important;
        }
        .main-menu.menu-light .navigation > li {
                padding:0 !important;
        }
        body.vertical-layout.vertical-menu-modern.menu-collapsed .main-menu{
                width: 44px !important;
        }
        body.vertical-layout.vertical-menu-modern.menu-collapsed .main-menu.expanded{
                width:260px !important;
        }
        /* .vs-textarea{
                color:white !important;
                height:150px;
                font-size:14px !important;
        }
        .vs-table--tbody-table tr{
                background:#16161d !important;
        }

        .vs-table--tbody-table .tr-expand td {
                padding: 10px !important;
        }

        .vs-con-table td.td-check .vs-icon {
                color: white !important;
        }
        .vue-treeselect__control {
                background: #232e48 !important;
        }

        .vue-treeselect__menu {
                background: #232e48 !important;
                color: #FFEB3B !important;

        }

        .vue-treeselect__menu:hover {
                background: #232e48 !important;

        }

        .con-vs-popup .vs-popup {
                background: #232e48 !important;
        }

        .vs-con-table.stripe .tr-values:nth-child(2n) {
                background: #16161d;
        }

        .vs-select--input {
                color: white !important;
        }

        .vs-popup--close {
                background: #FFEB3B !important;
        }
        .vs-table--pagination .item-pagination {
                color: inherit !important;
        }

        .vs-popup--content {
                background: #1e2022 !important;
        }

        .vs-popup--header {
                background: #b71c1c !important;
        }
        .vs-con-table{
                background:#16161d !important;
                border-radius: 5px;
                padding: 11px !important;
        }
        .vs-con-table.topplayer{
                background:#1E2022 !important;
                border-radius: 5px;
                padding: 11px !important;
        }
        .topplayer .vs-table--tbody-table tr{
                background:#1E2022 !important;
        }
        .vs-table--tbody-table tr:not('chat-td') {
                background: #16161d !important;
        }
        .vs-pagination--ul {
                padding: 0;
                padding-left: 5px;
                margin-top: 15px;
                padding-right: 5px;
                background: none !important;
        }
        .vs-select--options {
                background: #232e48 !important;
        }
        .vs-select--item:hover {
                background: #16161d !important;
        }
        .vs-select--item {
                border: 0 solid !important;
        }

        .bg-darker {
                background: #263238 !important;
        }

        .vs-icon {
                color: #232e48 !important;
        }

        .vs-select--input {
                background-color: #232e48;
        }

        .vs-select--input::placeholder {
                color: white;
        }


        .input-select-con {
                color: black !important;
        } */
        .vs-table--header, .vs-table--search{
                padding:5px !important;
        }
        .vs-table--search-input{
                padding:5px 20px !important;
                border: 1px solid rgb(47 47 47) !important;
        }
        body.vertical-layout.vertical-menu-modern.menu-collapsed .app-content{
                margin-left:20px !important;
        }
        .text-gold {
                color: #FFCC00;
        }
        .trends_border{
                border:1px solid #bdbdbd;
        }
        .bg-gold {
                background: #FFCC00;
                -webkit-box-shadow: inset 1px 6px 12px #fff61a, inset -1px -10px 5px #fff61a, 1px 2px 1px #FFCC00;
                -moz-box-shadow: inset 1px 6px 12px #fff61a, inset -1px -10px 5px #fff61a, 1px 2px 1px #FFCC00;
                box-shadow: inset 1px 6px 12px #fff61a, inset -1px -10px 5px #fff61a, 1px 2px 1px #FFCC00;
        }

        .btn-red {
                background: #FF0000;
                -webkit-box-shadow: inset 1px 6px 12px #bc0800, inset -1px -10px 5px #a80009, 1px 2px 1px #a80009;
                -moz-box-shadow: inset 1px 6px 12px #bc0800, inset -1px -10px 5px #a80009, 1px 2px 1px #a80009;
                box-shadow: inset 1px 6px 12px #bc0800, inset -1px -10px 5px #a80009, 1px 2px 1px #a80009;
        }
        .vue__time-picker input.display-time {
            width: 100% !important;
            height: calc(1.25em + 1.4rem + 1px);
            padding: 0.7rem 0.7rem;
            font-size: 0.96rem;
            font-weight: 400;
            line-height: 1.25;
            background-color: #262c49 !important;
            color: white !important;
            background-clip: padding-box;
            border: 1px solid rgba(0, 0, 0, 0.2);
            border-radius: 5px;
            transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
        }

        </style>
{{-- {!! Helper::applClasses() !!} --}}
@php
$configData = Helper::applClasses();
@endphp


@if($configData['mainLayoutType'] === 'horizontal')
        <link rel="stylesheet" href="{{ asset(mix('css/core/menu/menu-types/horizontal-menu.css')) }}">
@endif
        <link rel="stylesheet" href="{{ asset(mix('css/core/menu/menu-types/vertical-menu.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('css/core/colors/palette-gradient.css')) }}">
        {{-- Page Styles --}}
        @yield('page-style')
{{-- Laravel Style --}}
        <link rel="stylesheet" href="{{ asset(mix('css/custom-laravel.css')) }}">
{{-- Custom RTL Styles --}}
@if($configData['direction'] === 'rtl')
        <link rel="stylesheet" href="{{ asset(mix('css/custom-rtl.css')) }}">
@endif
