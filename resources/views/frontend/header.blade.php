<div class="row">
    <div class="overflow-hidden col-xl-3 col-lg-3 d-xl-flex d-lg-flex d-block align-items-center">
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-7 d-xl-block d-lg-block">
                <div class="logo">
                    <a href="index.html">
                        <!-- <img src="{{ asset('frontend') }}/img/dw2.png" alt="LOGO"> -->
                    </a>
                </div>
            </div>
            <div class="d-xl-none d-lg-none col-5 d-flex align-items-center justify-content-end">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fas fa-bars"></i>
                </button>
            </div>
        </div>
    </div>
    <div class="col-xl-9 col-lg-9">
{{--        <div class="top-header">--}}
{{--            <div class="row justify-content-end">--}}
{{--                <div class="col-xl-8 col-lg-8">--}}
{{--                    <div class="top-right">--}}
{{--                        <div class="buttons">--}}
{{--                            <a href="login">--}}
{{--                                <i class="fas fa-lock"></i>--}}
{{--                                Login</a>--}}
{{--                            <a href="#"><i class="fas fa-sign-in-alt"></i> signup</a>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
        <div class="bottom-header">
            <div class="row">
                <div class="col-xl-12 col-lg-12">
                    <nav class="navbar navbar-expand-lg navbar-light" id="navbar">
                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul class="navbar-nav ml-auto">
                                <li class="nav-item">
                                    <a class="nav-link active" href="#header">home</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link active" href="login">Login</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link active" href="register">Register</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#feature">rules</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#about">Terms and Conditions</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#contact">contact</a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>