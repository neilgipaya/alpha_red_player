<html lang="en"><head><style data-styles="">ion-icon{visibility:hidden}.hydrated{visibility:inherit}</style>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="Description" content="Bootstrap Responsive Admin Web Dashboard HTML5 Template">
    <meta name="Author" content="Spruko Technologies Private Limited">
    <meta name="Keywords" content="admin,admin dashboard,admin dashboard template,admin panel template,admin template,admin theme,bootstrap 4 admin template,bootstrap 4 dashboard,bootstrap admin,bootstrap admin dashboard,bootstrap admin panel,bootstrap admin template,bootstrap admin theme,bootstrap dashboard,bootstrap form template,bootstrap panel,bootstrap ui kit,dashboard bootstrap 4,dashboard design,dashboard html,dashboard template,dashboard ui kit,envato templates,flat ui,html,html and css templates,html dashboard template,html5,jquery html,premium,premium quality,sidebar bootstrap 4,template admin bootstrap 4">

    <!-- Title -->
    <title> Alpha PH -  Login to your Account </title>
    <link rel="SHORTCUT ICON" href="http://www.yoursite.com/favicon2.ico" />
  <style>
  /*! CSS Used from: https://23.111.164.242/assets/css/icons.css */
/*! @import https://23.111.164.242/assets/plugins/boxicons/css/boxicons.css */
.bx{font-family:'boxicons'!important;font-weight:normal;font-style:normal;font-variant:normal;line-height:1;display:inline-block;text-transform:none;speak:none;-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale;}
.bx-show:before{content:"\ebd1";}
/*! end @import */
/*! CSS Used from: https://23.111.164.242/assets/css/style.css */
body{margin:0;font-family:-apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";font-size:0.875rem;font-weight:400;line-height:1.5;color:#031b4e;text-align:left;background-color:#ecf0fa;}
h2,h5{margin-top:0;margin-bottom:0.5rem;}
p{margin-top:0;margin-bottom:1rem;}
a{color:#0162e8;text-decoration:none;background-color:transparent;}
a:hover{color:#0661e0;text-decoration:none;}
img{vertical-align:middle;border-style:none;max-width:100%;}
label{display:inline-block;margin-bottom:0.5rem;}
button{border-radius:0;}
button:focus{outline:1px dotted;outline:5px auto -webkit-focus-ring-color;}
input,button{margin:0;font-family:inherit;font-size:inherit;line-height:inherit;}
button,input{overflow:visible;}
button{text-transform:none;}
button{-webkit-appearance:button;}
button::-moz-focus-inner{padding:0;border-style:none;}
input[type="checkbox"]{box-sizing:border-box;padding:0;}
h2,h5{margin-bottom:0.5rem;font-weight:500;line-height:1.2;}
h2{font-size:1.75rem;}
h5{font-size:1.09375rem;}
.container,.container-fluid{width:100%;padding-right:15px;padding-left:15px;margin-right:auto;margin-left:auto;}
@media (min-width: 576px){
.container{max-width:540px;}
}
@media (min-width: 768px){
.container{max-width:720px;}
}
@media (min-width: 992px){
.container{max-width:960px;}
}
@media (min-width: 1200px){
.container{max-width:1200px;}
}
.row{display:flex;flex-wrap:wrap;margin-right:-0.75rem;margin-left:-0.75rem;}
.col-md-6,.col-md-10,.col-md-12,.col-lg-6,.col-lg-10,.col-lg-12,.col-xl-5,.col-xl-7,.col-xl-9,.col-xl-12{position:relative;width:100%;padding-right:0.75rem;padding-left:0.75rem;}
@media (min-width: 768px){
.col-md-6{flex:0 0 50%;max-width:50%;}
.col-md-10{flex:0 0 83.33333%;max-width:83.33333%;}
.col-md-12{flex:0 0 100%;max-width:100%;}
}
@media (min-width: 992px){
.col-lg-6{flex:0 0 50%;max-width:50%;}
.col-lg-10{flex:0 0 83.33333%;max-width:83.33333%;}
.col-lg-12{flex:0 0 100%;max-width:100%;}
}
@media (min-width: 1200px){
.col-xl-5{flex:0 0 41.66667%;max-width:41.66667%;}
.col-xl-7{flex:0 0 58.33333%;max-width:58.33333%;}
.col-xl-9{flex:0 0 75%;max-width:75%;}
.col-xl-12{flex:0 0 100%;max-width:100%;}
}
.d-none{display:none!important;}
.d-flex{display:flex!important;}
@media (min-width: 768px){
.d-md-flex{display:flex!important;}
}
.align-items-center{align-items:center!important;}
.btn{display:inline-block;font-weight:400;color:#031b4e;text-align:center;vertical-align:middle;user-select:none;background-color:transparent;border:1px solid transparent;padding:0.375rem 0.75rem;font-size:0.875rem;line-height:1.5;border-radius:3px;transition:color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;}
@media (prefers-reduced-motion: reduce){
.btn{transition:none;}
}
.btn:hover{color:#031b4e;text-decoration:none;}
.btn:focus{outline:0;box-shadow:none;}
.btn:disabled{opacity:0.65;}
.btn-block{display:block;width:100%;}
.btn{border-width:0;line-height:1.538;padding:9px 20px;transition:none;}
.btn:active,.btn:focus{box-shadow:none;}
.form-control{height:38px;border-radius:0;}
.form-control:focus{border-color:#949eb7;box-shadow:none;}
.form-control{display:block;width:100%;height:40px;padding:0.375rem 0.75rem;font-size:0.875rem;font-weight:400;line-height:1.5;color:#4d5875;background-color:#fff;background-clip:padding-box;border:1px solid #e1e5ef;border-radius:3px;transition:border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;}
.form-control::-ms-expand{background-color:transparent;border:0;}
.form-control:focus{color:#4d5875;background-color:#fff;border-color:#b3c6ff;outline:0;box-shadow:none;}
.form-control::placeholder{color:#737f9e;opacity:1;}
.form-control:disabled{background-color:#dde2ef;opacity:1;}
@media (prefers-reduced-motion: reduce){
.form-control{transition:none;}
}
.form-group{margin-bottom:1rem;}
.main-navbar-backdrop{position:fixed;top:0;left:0;right:0;bottom:0;background-color:rgba(0, 0, 0, 0.89);z-index:900;visibility:hidden;opacity:0;transition:all 0.4s;}
@media (prefers-reduced-motion: reduce){
.main-navbar-backdrop{transition:none;}
}
*{box-sizing:border-box;}
*::before,*::after{box-sizing:border-box;}
#global-loader{position:fixed;z-index:50000;background:#fff;left:0;top:0;right:0;bottom:0;height:100%;width:100%;margin:0 auto;text-align:center;}
.loader-img{position:absolute;right:0;bottom:0;top:43%;left:0;margin:0 auto;text-align:center;}
@media print{
*{text-shadow:none!important;box-shadow:none!important;}
*::before,*::after{text-shadow:none!important;box-shadow:none!important;}
a:not(.btn){text-decoration:underline;}
img{page-break-inside:avoid;}
p,h2{orphans:3;widows:3;}
h2{page-break-after:avoid;}
body,.container{min-width:992px!important;}
}
body{font-family:"Roboto", sans-serif;}
.main-body{min-height:100vh;display:flex;flex-direction:column;justify-content:flex-start;position:relative;}
.main-body::before{content:'main';position:fixed;top:45%;left:70%;transform:translate3d(-50%, -58%, 0);font-size:1300px;font-weight:600;letter-spacing:-10px;line-height:.5;opacity:.02;z-index:-1;display:none;}
.font-weight-semibold{font-weight:500!important;}
*::-webkit-scrollbar{width:4px;height:4px;transition:.3s background;}
::-webkit-scrollbar-thumb{background:#e1e6f1;}
*:hover::-webkit-scrollbar-thumb{background:#adb5bd;}
.page{display:flex;flex-direction:column;min-height:100vh;}
.btn{padding:8px;}
p{font-size:13px;}
.btn{padding:8.5px;}
p{font-size:13px;}
.main-signin-footer p{color:#a5a0b1;}
.main-signin-footer p:first-child{margin-bottom:5px;}
.main-signin-footer p:last-child{margin-bottom:0;}
.main-signin-footer a{color:#14112d;font-weight:700;}
.main-signin-footer a:hover,.main-signin-footer a:focus{color:#0162e8;}
.main-signup-header h2{font-weight:500;color:#0162e8;letter-spacing:-1px;}
.main-signup-header label{color:#a5a0b1;}
.main-signup-header .form-control{color:#14112d;font-weight:500;border-width:2px;border-color:#e3e3e3;}
.main-signup-header .form-control:focus{border-color:#b9c2d8;box-shadow:none;}
.main-signup-header .form-control::placeholder{font-weight:400;color:#a5a0b1;}
.main-signup-header .row{margin-top:20px;}
.login{min-height:100vh;}
@media (max-width: 767px){
.main-signup-header{padding:1.5rem;border:1px solid #e1e5ef;border-radius:6px;}
}
@media (min-width: 1200px){
.ht-xl-80p{height:80%;}
}
.mb-4{margin-bottom:1.5rem!important;}
.mt-5{margin-top:3rem!important;}
.my-auto{margin-top:auto!important;}
.mx-auto{margin-right:auto!important;}
.my-auto{margin-bottom:auto!important;}
.mx-auto{margin-left:auto!important;}
.p-0{padding:0!important;}
.py-2{padding-top:0.5rem!important;}
.py-2{padding-bottom:0.5rem!important;}
.text-center{text-align:center!important;}
.wd-100p{width:100%;}
@media (min-width: 768px){
.wd-md-100p{width:100%;}
}
@media (min-width: 1200px){
.wd-xl-50p{width:50%;}
}
/*! CSS Used from: https://23.111.164.242/assets/css/style-dark.css */
body.dark-theme{color:#fff;background:#141b2d;}
.dark-theme .form-control{color:#fff;background-color:#232e48;border:1px solid rgba(226, 232, 245, 0.1);}
.dark-theme #global-loader{background:#1a233a;}
.dark-theme .main-signin-footer a{color:rgba(255, 255, 255, 0.8);}
.dark-theme .form-control::placeholder{color:rgba(212, 218, 236, 0.3)!important;opacity:1;}
/*! CSS Used from: Embedded */
.text-gold{color:#FFCC00!important;}
.bg-gold{background:#FFCC00;-webkit-box-shadow:inset 1px 6px 12px #fff61a, inset -1px -10px 5px #fff61a, 1px 2px 1px #FFCC00;-moz-box-shadow:inset 1px 6px 12px #fff61a, inset -1px -10px 5px #fff61a, 1px 2px 1px #FFCC00;box-shadow:inset 1px 6px 12px #fff61a, inset -1px -10px 5px #fff61a, 1px 2px 1px #FFCC00;}
body{font-family:Roboto, sans-serif!important;}
.field-icon{float:right;margin-top:-30px;position:relative;z-index:2;margin-right:10px;}
/*! CSS Used fontfaces */
@font-face{font-family:'boxicons';font-weight:normal;font-style:normal;src:url('https://23.111.164.242/assets/plugins/boxicons/fonts/boxicons.eot');src:url('https://23.111.164.242/assets/plugins/boxicons/fonts/boxicons.eot') format('embedded-opentype'),     url('https://23.111.164.242/assets/plugins/boxicons/fonts/boxicons.woff2') format('woff2'),     url('https://23.111.164.242/assets/plugins/boxicons/fonts/boxicons.woff') format('woff'),     url('https://23.111.164.242/assets/plugins/boxicons/fonts/boxicons.ttf') format('truetype'),     url('https://23.111.164.242/assets/plugins/boxicons/fonts/boxicons.svg#boxicons') format('svg');}
@font-face{font-family:'Roboto';font-style:normal;font-weight:300;src:local('Roboto Light'), local('Roboto-Light'), url(https://fonts.gstatic.com/s/roboto/v20/KFOlCnqEu92Fr1MmSU5fCRc4EsA.woff2) format('woff2');unicode-range:U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;}
@font-face{font-family:'Roboto';font-style:normal;font-weight:300;src:local('Roboto Light'), local('Roboto-Light'), url(https://fonts.gstatic.com/s/roboto/v20/KFOlCnqEu92Fr1MmSU5fABc4EsA.woff2) format('woff2');unicode-range:U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;}
@font-face{font-family:'Roboto';font-style:normal;font-weight:300;src:local('Roboto Light'), local('Roboto-Light'), url(https://fonts.gstatic.com/s/roboto/v20/KFOlCnqEu92Fr1MmSU5fCBc4EsA.woff2) format('woff2');unicode-range:U+1F00-1FFF;}
@font-face{font-family:'Roboto';font-style:normal;font-weight:300;src:local('Roboto Light'), local('Roboto-Light'), url(https://fonts.gstatic.com/s/roboto/v20/KFOlCnqEu92Fr1MmSU5fBxc4EsA.woff2) format('woff2');unicode-range:U+0370-03FF;}
@font-face{font-family:'Roboto';font-style:normal;font-weight:300;src:local('Roboto Light'), local('Roboto-Light'), url(https://fonts.gstatic.com/s/roboto/v20/KFOlCnqEu92Fr1MmSU5fCxc4EsA.woff2) format('woff2');unicode-range:U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB;}
@font-face{font-family:'Roboto';font-style:normal;font-weight:300;src:local('Roboto Light'), local('Roboto-Light'), url(https://fonts.gstatic.com/s/roboto/v20/KFOlCnqEu92Fr1MmSU5fChc4EsA.woff2) format('woff2');unicode-range:U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;}
@font-face{font-family:'Roboto';font-style:normal;font-weight:300;src:local('Roboto Light'), local('Roboto-Light'), url(https://fonts.gstatic.com/s/roboto/v20/KFOlCnqEu92Fr1MmSU5fBBc4.woff2) format('woff2');unicode-range:U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;}
@font-face{font-family:'Roboto';font-style:normal;font-weight:400;src:local('Roboto'), local('Roboto-Regular'), url(https://fonts.gstatic.com/s/roboto/v20/KFOmCnqEu92Fr1Mu72xKOzY.woff2) format('woff2');unicode-range:U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;}
@font-face{font-family:'Roboto';font-style:normal;font-weight:400;src:local('Roboto'), local('Roboto-Regular'), url(https://fonts.gstatic.com/s/roboto/v20/KFOmCnqEu92Fr1Mu5mxKOzY.woff2) format('woff2');unicode-range:U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;}
@font-face{font-family:'Roboto';font-style:normal;font-weight:400;src:local('Roboto'), local('Roboto-Regular'), url(https://fonts.gstatic.com/s/roboto/v20/KFOmCnqEu92Fr1Mu7mxKOzY.woff2) format('woff2');unicode-range:U+1F00-1FFF;}
@font-face{font-family:'Roboto';font-style:normal;font-weight:400;src:local('Roboto'), local('Roboto-Regular'), url(https://fonts.gstatic.com/s/roboto/v20/KFOmCnqEu92Fr1Mu4WxKOzY.woff2) format('woff2');unicode-range:U+0370-03FF;}
@font-face{font-family:'Roboto';font-style:normal;font-weight:400;src:local('Roboto'), local('Roboto-Regular'), url(https://fonts.gstatic.com/s/roboto/v20/KFOmCnqEu92Fr1Mu7WxKOzY.woff2) format('woff2');unicode-range:U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB;}
@font-face{font-family:'Roboto';font-style:normal;font-weight:400;src:local('Roboto'), local('Roboto-Regular'), url(https://fonts.gstatic.com/s/roboto/v20/KFOmCnqEu92Fr1Mu7GxKOzY.woff2) format('woff2');unicode-range:U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;}
@font-face{font-family:'Roboto';font-style:normal;font-weight:400;src:local('Roboto'), local('Roboto-Regular'), url(https://fonts.gstatic.com/s/roboto/v20/KFOmCnqEu92Fr1Mu4mxK.woff2) format('woff2');unicode-range:U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;}
@font-face{font-family:'Roboto';font-style:normal;font-weight:500;src:local('Roboto Medium'), local('Roboto-Medium'), url(https://fonts.gstatic.com/s/roboto/v20/KFOlCnqEu92Fr1MmEU9fCRc4EsA.woff2) format('woff2');unicode-range:U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;}
@font-face{font-family:'Roboto';font-style:normal;font-weight:500;src:local('Roboto Medium'), local('Roboto-Medium'), url(https://fonts.gstatic.com/s/roboto/v20/KFOlCnqEu92Fr1MmEU9fABc4EsA.woff2) format('woff2');unicode-range:U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;}
@font-face{font-family:'Roboto';font-style:normal;font-weight:500;src:local('Roboto Medium'), local('Roboto-Medium'), url(https://fonts.gstatic.com/s/roboto/v20/KFOlCnqEu92Fr1MmEU9fCBc4EsA.woff2) format('woff2');unicode-range:U+1F00-1FFF;}
@font-face{font-family:'Roboto';font-style:normal;font-weight:500;src:local('Roboto Medium'), local('Roboto-Medium'), url(https://fonts.gstatic.com/s/roboto/v20/KFOlCnqEu92Fr1MmEU9fBxc4EsA.woff2) format('woff2');unicode-range:U+0370-03FF;}
@font-face{font-family:'Roboto';font-style:normal;font-weight:500;src:local('Roboto Medium'), local('Roboto-Medium'), url(https://fonts.gstatic.com/s/roboto/v20/KFOlCnqEu92Fr1MmEU9fCxc4EsA.woff2) format('woff2');unicode-range:U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB;}
@font-face{font-family:'Roboto';font-style:normal;font-weight:500;src:local('Roboto Medium'), local('Roboto-Medium'), url(https://fonts.gstatic.com/s/roboto/v20/KFOlCnqEu92Fr1MmEU9fChc4EsA.woff2) format('woff2');unicode-range:U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;}
@font-face{font-family:'Roboto';font-style:normal;font-weight:500;src:local('Roboto Medium'), local('Roboto-Medium'), url(https://fonts.gstatic.com/s/roboto/v20/KFOlCnqEu92Fr1MmEU9fBBc4.woff2) format('woff2');unicode-range:U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;}
@font-face{font-family:'Roboto';font-style:normal;font-weight:700;src:local('Roboto Bold'), local('Roboto-Bold'), url(https://fonts.gstatic.com/s/roboto/v20/KFOlCnqEu92Fr1MmWUlfCRc4EsA.woff2) format('woff2');unicode-range:U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;}
@font-face{font-family:'Roboto';font-style:normal;font-weight:700;src:local('Roboto Bold'), local('Roboto-Bold'), url(https://fonts.gstatic.com/s/roboto/v20/KFOlCnqEu92Fr1MmWUlfABc4EsA.woff2) format('woff2');unicode-range:U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;}
@font-face{font-family:'Roboto';font-style:normal;font-weight:700;src:local('Roboto Bold'), local('Roboto-Bold'), url(https://fonts.gstatic.com/s/roboto/v20/KFOlCnqEu92Fr1MmWUlfCBc4EsA.woff2) format('woff2');unicode-range:U+1F00-1FFF;}
@font-face{font-family:'Roboto';font-style:normal;font-weight:700;src:local('Roboto Bold'), local('Roboto-Bold'), url(https://fonts.gstatic.com/s/roboto/v20/KFOlCnqEu92Fr1MmWUlfBxc4EsA.woff2) format('woff2');unicode-range:U+0370-03FF;}
@font-face{font-family:'Roboto';font-style:normal;font-weight:700;src:local('Roboto Bold'), local('Roboto-Bold'), url(https://fonts.gstatic.com/s/roboto/v20/KFOlCnqEu92Fr1MmWUlfCxc4EsA.woff2) format('woff2');unicode-range:U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB;}
@font-face{font-family:'Roboto';font-style:normal;font-weight:700;src:local('Roboto Bold'), local('Roboto-Bold'), url(https://fonts.gstatic.com/s/roboto/v20/KFOlCnqEu92Fr1MmWUlfChc4EsA.woff2) format('woff2');unicode-range:U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;}
@font-face{font-family:'Roboto';font-style:normal;font-weight:700;src:local('Roboto Bold'), local('Roboto-Bold'), url(https://fonts.gstatic.com/s/roboto/v20/KFOlCnqEu92Fr1MmWUlfBBc4.woff2) format('woff2');unicode-range:U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;}
@font-face{font-family:'Roboto';font-style:normal;font-weight:900;src:local('Roboto Black'), local('Roboto-Black'), url(https://fonts.gstatic.com/s/roboto/v20/KFOlCnqEu92Fr1MmYUtfCRc4EsA.woff2) format('woff2');unicode-range:U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;}
@font-face{font-family:'Roboto';font-style:normal;font-weight:900;src:local('Roboto Black'), local('Roboto-Black'), url(https://fonts.gstatic.com/s/roboto/v20/KFOlCnqEu92Fr1MmYUtfABc4EsA.woff2) format('woff2');unicode-range:U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;}
@font-face{font-family:'Roboto';font-style:normal;font-weight:900;src:local('Roboto Black'), local('Roboto-Black'), url(https://fonts.gstatic.com/s/roboto/v20/KFOlCnqEu92Fr1MmYUtfCBc4EsA.woff2) format('woff2');unicode-range:U+1F00-1FFF;}
@font-face{font-family:'Roboto';font-style:normal;font-weight:900;src:local('Roboto Black'), local('Roboto-Black'), url(https://fonts.gstatic.com/s/roboto/v20/KFOlCnqEu92Fr1MmYUtfBxc4EsA.woff2) format('woff2');unicode-range:U+0370-03FF;}
@font-face{font-family:'Roboto';font-style:normal;font-weight:900;src:local('Roboto Black'), local('Roboto-Black'), url(https://fonts.gstatic.com/s/roboto/v20/KFOlCnqEu92Fr1MmYUtfCxc4EsA.woff2) format('woff2');unicode-range:U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB;}
@font-face{font-family:'Roboto';font-style:normal;font-weight:900;src:local('Roboto Black'), local('Roboto-Black'), url(https://fonts.gstatic.com/s/roboto/v20/KFOlCnqEu92Fr1MmYUtfChc4EsA.woff2) format('woff2');unicode-range:U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;}
@font-face{font-family:'Roboto';font-style:normal;font-weight:900;src:local('Roboto Black'), local('Roboto-Black'), url(https://fonts.gstatic.com/s/roboto/v20/KFOlCnqEu92Fr1MmYUtfBBc4.woff2) format('woff2');unicode-range:U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;}
  </style>
  </head>
<body class="main-body dark-theme" style="background: black !important">

<!-- Loader -->
<div id="global-loader" style="display: none;">
    <img src="http://23.111.164.242/assets/img/loader.svg" class="loader-img" alt="Loader">
</div>
<!-- /Loader -->

<!-- Page -->
<div class="page">

    <div class="container-fluid">
        <div class="row no-gutter">
            <!-- The content half -->
            <div class="col-md-12 text-center">
                <div class="login d-flex align-items-center py-2">
                    <!-- Demo content-->
                    <div class="container p-0">
                        <div class="row">
                            <div class="col-md-10 col-lg-10 col-xl-9 mx-auto">
                                <div class="card-sigin">



                                    <div class="card-sigin">
                                        <div class="main-signup-header">                                            
                                            <h2 class="text-gold">Welcome to Alpha PH!</h2>
                                            <h5 class="font-weight-semibold mb-4">Please sign in to continue.</h5>
                                            @if(Session::has('type') == 'error')
                                                <div class="alert-danger">
                                                    <h2 style="color:red;">{{ Session::get('message') }}</h2>
                                                </div>
                                            @endif
                                            @if(Session::has('type1') == 'success')
                                                <div class="alert-success">
                                                    <h2 style="color:green;">{{ Session::get('message') }}</h2>
                                                </div>
                                            @endif
                                            
                                            <form method="POST" action="{{ route('login') }}">
                                            @csrf
                                            <div class="form-group">
                                            <input id="email" type="text" class="form-control @error('username') is-invalid @enderror" name="username" placeholder="Username" value="{{ old('username') }}" required autocomplete="email" autofocus>
                                            <label for="email">Username</label>
                                            @error('username')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-md-12">
                                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="Password"required autocomplete="current-password">
                                                    <label for="password">Password</label>
                                                    @error('password')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label>
                                                        <input type="checkbox" name="remember">
                                                        <span>
                                                            Keep me login
                                                        </span>
                                                    </label>
                                                </div>
                                                <div class="form-group">
                                                <a href="forgot-password">Forgot Password</a>
                                                </div>
                                                <button class="btn bg-gold btn-block">Sign In</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!-- End -->
                </div>
            </div><!-- End -->
        </div>
    </div>

</div>
<!-- End Page -->

<script>
    $(".toggle-password").click(function() {


        var input = $($(this).attr("toggle"));
        if (input.attr("type") == "password") {
            input.attr("type", "text");
            $(this).removeClass("bx bx-show");
            $(this).toggleClass("bx bx-hide");
        } else {
            input.attr("type", "password");
            $(this).removeClass("bx bx-hide");
            $(this).toggleClass("bx bx-show");
        }
    });
</script>
<script>


    
</script>


<div class="main-navbar-backdrop"></div></body></html>