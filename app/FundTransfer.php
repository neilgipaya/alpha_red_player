<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FundTransfer extends Model
{
    //

    protected $guarded = [];

    public function transaction_from()
    {
        return $this->belongsTo('App\BankAccounts', 'from_id');
    }

    public function transaction_to()
    {
        return $this->belongsTo('App\BankAccounts', 'to_id');
    }

    public function users()
    {
        return $this->belongsTo('App\User', 'requested_by');
    }
}
