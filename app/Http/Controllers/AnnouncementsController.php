<?php

namespace App\Http\Controllers;

use App\Announcements;
use App\Events\AnnouncementEvent;
use App\Events\AnnouncementClear;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AnnouncementsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $request->merge([
                'status' => 1
            ]);
            $create = Announcements::create($request->all());            
            DB::commit();
            event(new AnnouncementEvent($request->content));
            $notification = array(
                'message' => 'Announcement Created successfully',
                'type' => 'success',
            );

            return response()->json($notification);
        } catch (\Exception $e) {

            DB::rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger',
            );

            return response()->json($notification);
        }
    }
    public function remove(Request $request){
        DB::beginTransaction();
        try {
            $announcement = Announcements::where('status',1)->latest()->first();
            if(!$announcement){
                $notification = array(
                    'message' => 'There is no current announcement yet',
                    'type' => 'danger',
                );
                return response()->json($notification);
            }
            Announcements::where('id',$announcement->id)->update(['status' => 0]);          
            DB::commit();
            event(new AnnouncementClear());
            $notification = array(
                'message' => 'Announcement Cleared successfully',
                'type' => 'success',
            );

            return response()->json($notification);
        } catch (\Exception $e) {

            DB::rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger',
            );

            return response()->json($notification);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Announcements  $announcements
     * @return \Illuminate\Http\Response
     */
    public function show(Announcements $announcements)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Announcements  $announcements
     * @return \Illuminate\Http\Response
     */
    public function edit(Announcements $announcements)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Announcements  $announcements
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Announcements $announcements)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Announcements  $announcements
     * @return \Illuminate\Http\Response
     */
    public function destroy(Announcements $announcements)
    {
        //
    }

    public function getAnnouncement()
    {
        $announcement = Announcements::where('status',1)->latest()->first();

        return response()->json($announcement);
    }
}
