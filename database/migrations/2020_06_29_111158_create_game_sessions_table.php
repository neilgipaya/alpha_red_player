<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGameSessionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('game_sessions', function (Blueprint $table) {
            $table->id();
            $table->string('game_title');
            $table->unsignedInteger('game_start');
            $table->unsignedInteger('expected_game_number');
            $table->string('source_1');
            $table->string('source_2');
            $table->string('max_betting_time');
            $table->string('last_call');
            $table->string('close_bet_delay');
            $table->string('max_game_time');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('game_sessions');
    }
}
