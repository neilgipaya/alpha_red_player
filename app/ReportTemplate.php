<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReportTemplate extends Model
{
    public $timestamps = true;
    protected $table = "reports_template";
    protected $fillable = [
        'report_name', 'report_description', 'report_data',
    ];
}
