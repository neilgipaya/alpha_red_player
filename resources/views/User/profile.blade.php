@extends('layouts/contentLayoutMaster')

@section('breadcrumb')
    <div class="clearfix"></div>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="card-content">
                        <form method="POST" action="/updateProfile/{{ auth()->user()->id }}">
                            @csrf
                            @method('PATCH')
                            @if(Session::has('message'))
                                @if(Session::get('content') === 'Profile')
                                    <div class="alert alert-{{ Session::get('type') }} alert-dismissible mb-2" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                        <div class="d-flex align-items-center">
                                                            <span>
                                                                {{ Session::get('message') }}
                                                            </span>
                                        </div>
                                    </div>
                                @endif
                            @endif
                            @if(auth()->user()->type === 'Silver Agent')
                            <div class="form-group">
                                <label>Agent ID</label>
                                <input type="text" disabled class="form-control" name="name" value="{{ auth()->user()->id }}" required />
                            </div>
                            @endif
                            <div class="form-group">
                                <label>Full Name</label>
                                <input type="text" class="form-control" name="name" value="{{ auth()->user()->name }}" required />
                            </div>
                            <div class="form-group">
                                <button class="btn btn-success btn-block" type="submit">Update Profile</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="card-content">
                        <form method="POST" action="/updatePassword/{{ auth()->user()->id }}">
                            @csrf
                            @method('PATCH')
                            @if(Session::has('message'))
                                @if(Session::get('content') === 'Password')
                                    <div class="alert alert-{{ Session::get('type') }} alert-dismissible mb-2" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                        <div class="d-flex align-items-center">
                                            <span>
                                                {{ Session::get('message') }}
                                            </span>
                                        </div>
                                    </div>
                                @endif
                            @endif
                            <div class="form-group">
                                <label>Current Password</label>
                                <input type="password" class="form-control form-control-round" name="current-password" required />
                            </div>
                            <div class="form-group">
                                <label>New Password</label>
                                <input type="password" class="form-control form-control-round" name="password" required />
                            </div>
                            <div class="form-group">
                                <label>Retype Password</label>
                                <input type="password" class="form-control form-control-round" name="password_confirmation" required />
                            </div>
                            <div class="form-group">
                                <button class="btn btn-success btn-block" type="submit">Change Password</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('vendor-scripts')

@endsection
