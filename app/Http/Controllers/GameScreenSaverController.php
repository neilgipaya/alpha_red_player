<?php

namespace App\Http\Controllers;

use App\GameScreenSaver;
use Illuminate\Http\Request;

class GameScreenSaverController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\GameScreenSaver  $gameScreenSaver
     * @return \Illuminate\Http\Response
     */
    public function show(GameScreenSaver $gameScreenSaver)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\GameScreenSaver  $gameScreenSaver
     * @return \Illuminate\Http\Response
     */
    public function edit(GameScreenSaver $gameScreenSaver)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\GameScreenSaver  $gameScreenSaver
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, GameScreenSaver $gameScreenSaver)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\GameScreenSaver  $gameScreenSaver
     * @return \Illuminate\Http\Response
     */
    public function destroy(GameScreenSaver $gameScreenSaver)
    {
        //
    }
}
