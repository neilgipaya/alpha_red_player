<?php

use App\UserLogs;
use Illuminate\Support\Facades\Route;
use Jenssegers\Agent\Agent;
use Request as request;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/test',function(){
    // $data = \Spatie\Permission\Models\Role::get();
    $data = \App\User::Role('owner')->first();;
    return response()->json($data);
});
Route::get('optimize', function(){
    \Illuminate\Support\Facades\Artisan::call('optimize:clear');
});
Route::get('wala', function(){
    \Illuminate\Support\Facades\Artisan::call('down');
});
Route::get('meron', function(){
    \Illuminate\Support\Facades\Artisan::call('up');
});
Route::get('pusher', function (){
    event(new \App\Events\AnnouncementEvent("Sample"));
});
Route::post('/send-otp','ForgotController@send');
Route::get('/forgot-password',function(){
    return view('forgot-password');
});
Route::get('/activate-account',function(){
    if(!request::get('reset_token')){
        abort(404);
    }else{
        $get = \App\User::select('id','reset_token')->where('reset_token',request::get('reset_token'))->first();
        if(!$get){
            abort(403);
        }else{
            return view('reset-password',compact('get'));
        }
    }
});
Route::post('/reset-password','ForgotController@resetpassword');
Route::get('/otptest', 'OTPController@sendOTP');
Route::group(['middleware'=>'guest'], function (){


    Route::get('/', function (){
        // return view('frontend.welcome');
        return redirect('login');
    });
    Route::get('/register',function(){
        if(!request::get('r')){
            abort(404);
        }
        return view('register-frontend');
        
    })->name('register');

    Route::get('register-form', function (){
        $agent = new Agent();

        if ($agent->isMobile()){

            $agent = \App\User::where('type', 'Silver Agent')->latest()->get();

            return view('register-mobile', compact('agent'));
        } else {
            return view('register');
        }
    });

    Route::get('forgot-password-form', function (){

        $agent = new Agent();

        if ($agent->isMobile()){
            return view('forgot-password-mobile');
        } else {
            return view('forgot-password');
        }
    });

    Route::post('authenticate', 'AuthenticationController@authenticate');
    Route::post('register-account', 'AuthenticationController@register');
});

Route::group(['middleware' => 'auth'], function (){
    Route::get('/user/view-agent-players/{id}','UsersController@view_agent_players');
    Route::get('/user/view-agent-downline/{id}','UsersController@view_agent_downline');
    Route::get('/user/view-agent-players-bet/{id}','UsersController@view_agent_players_bet');
    // Route::get('dashboard' , 'DashboardController@index');
    Route::get('otp', 'AuthenticationController@otp');
    Route::post('verify', 'AuthenticationController@verify');
    Route::get('agentDashboard' , 'DashboardController@agentDashboard');
    Route::get('dashboard',function(){
        if (!auth()->user()->can('dashboard-admin')){
            if(auth()->user()->can('play-game')){
                return redirect('/game/play-game');
            }
            return redirect('/profile');
        }
        return view('dashboard2');
    });
    Route::get('250', function (){
        return view('Game.stream2');
    });
    Route::get('/lobby',function(){
        return view('Game.lobby');
    });

    Route::group(['prefix' => 'user'], function (){

        Route::resource('/', 'UsersController');
        Route::get('/deactivate', 'UsersController@deactivate');
        Route::get('/inactiveAccounts', 'UsersController@inactiveAccounts');
        Route::get('/player', 'UsersController@player');
        Route::get('/agent', 'UsersController@agent');
        Route::get('/agentPlayers/{id}', 'UsersController@agentPlayers');
        Route::get('/onlinePlayers', 'UsersController@onlinePlayers');
        Route::get('/playerBets/{id}', 'UsersController@playerBets');
        Route::get('/walletHistory/{id}', 'UsersController@walletHistory');
        Route::get('/myPlayers', 'UsersController@myPlayers');
        Route::get('/myInactivePlayers',function(){
            if(auth()->user()->type == 'Player'){
                abort(403,'Unauthorize');
            }
            return view('User.myInactivePlayers');
        });
        Route::get('/myDownlineAgents',function(){
            if(auth()->user()->type == 'Player' || auth()->user()->type == 'Official'){
                abort(403,'Unauthorize');
            }
            return view('User.myDownlineAgents');
        });
        Route::get('/play-stats',function(){
            if(auth()->user()->type != 'Player'){
                abort(403);
            }
            return view('User.play-stats');
        });
        Route::get('/convert-commission', function(){
            if(auth()->user()->type == 'Player' || auth()->user()->type == 'Official'){
                abort(403,'Unauthorize');
            }
            return view('User.convert-commission');
        });
        Route::get('/walletTransfer', 'UsersController@walletTransfer');
    });

    Route::group(['prefix' => 'topup'], function (){

        Route::resource('/', 'TopupController');
        Route::get('/request', 'TopupController@request');
        Route::get('/logs', 'TopupController@logs');
        Route::get('/banker-topup-approval', 'TopupController@banker');
        Route::get('cash-in-history', 'GameController@userCashin');
        Route::get('/my-topup', 'TopupController@myTopup');
        Route::get('/other', 'TopupController@other');
//        Route::post('/deposit', 'TopupController@deposit');
    });

    Route::group(['prefix' => 'configuration'], function (){
        Route::resource('banker','BankerController');
        Route::resource('banker-topup','BankerTopupController');
        Route::resource('payment-types', 'PaymentDetailsController');
        Route::resource('banks', 'BankAccountsController');
        Route::resource('decline-template', 'DeclineTemplateController');
        Route::resource('roles', 'RolesController');
        Route::post('createRole', 'RolesController@store');
        Route::resource('partners', 'PartnerController');
        Route::resource('UserLogs', 'UserLogsController');

    });

    Route::group(['prefix' => 'funds'], function (){

        Route::resource('/', 'FundTransferController');
        Route::get('request', 'FundTransferController@request');
        Route::get('logs', 'FundTransferController@logs');
    });

    Route::group(['prefix' => 'reports'], function (){
        Route::get('rake', 'ReportsController@totalRake');
        Route::get('cash_in', 'ReportsController@cash_in');
        Route::get('cash_out', 'ReportsController@cash_out');
        Route::get('fund_transfer', 'ReportsController@fund_transfer');
        Route::get('user_report/{id}','ReportsController@user_reports');
        Route::get('fund_account','ReportsController@fund_account');
        Route::get('betting','ReportsController@betting');
        Route::get('agent-commission','ReportsController@agent');
        Route::get('advance-reports/edit/{id}',function(){
            if(!auth()->user()->can('reports')){
                abort(403, 'Unauthorized');
            }
            return view('Report.edit-reports');
        });
        Route::get('advance-reports',function(){
            if(!auth()->user()->can('reports')){
                abort(403, 'Unauthorized');
            }
            return view('Report.advance');
        });
    });

    Route::group(['prefix' => 'game'], function (){

        Route::resource('/', 'GameController');
        Route::get('play-game', 'GameController@play');
        Route::get('current-session', 'GameController@currentSession');
        Route::get('create-game', 'GameController@create');
        Route::get('logs', 'GameController@logs');
        Route::get('bets', 'GameController@bets');
        Route::get('betting-reports/{id}','GameController@gameReports');
        Route::get('view-betting/{game_id}','GameController@viewBetting');

    });

    Route::group(['prefix' => 'withdrawal'], function (){

        Route::resource('/', 'WithdrawalController');
        Route::get('request', 'WithdrawalController@request');
        Route::get('logs', 'WithdrawalController@logs');
        Route::get('cash-out-history', 'GameController@userCashout');
        // Route::get('Sub Agent', 'WithdrawalController@requestAgent');
        Route::get('manageWithTemplate',function(){
            return view('Withdrawal.manageWithTemplate');
        });

    });

    Route::get('logout', function (){
        UserLogs::create([
            'user_id' => \auth()->user()->id,
            'type' => 'auth',
            'content' => 'User Successfully Logout'
        ]);
       auth()->logout();

       return redirect()->to('/');
    });

    Route::get('help',  function (){
       return view('Game.help');
    });
    // Route::get('/fill',function(){
    //     \DB::beginTransaction();
    //     try {
    //         $withdrawal = \App\Withdrawal::get();
    //         foreach($withdrawal as $val){
    //             $withdrawal_template = \App\WithdrawalTemplate::find($val->withdrawal_template_id);
    //             if($withdrawal_template){
    //                 $withdrawal_update = \App\Withdrawal::where('withdrawal_template_id',$withdrawal_template->id)
    //                 ->update([
    //                 'account_name' => $withdrawal_template->account_name,
    //                 'account_number' => $withdrawal_template->account_number,
    //                 'payment_type' => $withdrawal_template->payment_type,
    //                 'bank_type' => $withdrawal_template->bank_type
    //                 ]);
    //             }                
    //         }
    //         \DB::commit();
    //         return response()->json($withdrawal);
    //     } catch (\Exception $e) {
    //        \DB::rollback();
    //        return 'rollback';
    //     }
    // });

    Route::get('profile', 'UsersController@showProfile');
    Route::patch('updateProfile/{id}', 'UsersController@updateProfile');
    Route::patch('updatePassword/{id}', 'UsersController@updatePassword');

});
