<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="UTF-8">
    <meta name='viewport' content='width=device-width, initial-scale=1.0, user-scalable=0'>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="Description" content="Bootstrap Responsive Admin Web Dashboard HTML5 Template">
    <meta name="Author" content="Spruko Technologies Private Limited">
    <meta name="Keywords" content="admin,admin dashboard,admin dashboard template,admin panel template,admin template,admin theme,bootstrap 4 admin template,bootstrap 4 dashboard,bootstrap admin,bootstrap admin dashboard,bootstrap admin panel,bootstrap admin template,bootstrap admin theme,bootstrap dashboard,bootstrap form template,bootstrap panel,bootstrap ui kit,dashboard bootstrap 4,dashboard design,dashboard html,dashboard template,dashboard ui kit,envato templates,flat ui,html,html and css templates,html dashboard template,html5,jquery html,premium,premium quality,sidebar bootstrap 4,template admin bootstrap 4"/>

    <!-- Title -->
    <title> Sabong World -  Login to your Account </title>

    <!-- Favicon -->

    <!-- Icons css -->
    <link href="{{ asset('assets') }}/css/icons.css" rel="stylesheet">

    <!--  Right-sidemenu css -->
    <link href="{{ asset('assets') }}/plugins/sidebar/sidebar.css" rel="stylesheet">

    <!--  Custom Scroll bar-->
    <link href="{{ asset('assets') }}/plugins/mscrollbar/jquery.mCustomScrollbar.css" rel="stylesheet"/>

    <!--  Left-Sidebar css -->
    <link rel="stylesheet" href="{{ asset('assets') }}/css/sidemenu.css">

    <!--- Style css --->
    <link href="{{ asset('assets') }}/css/style.css" rel="stylesheet">

    <!--- Dark-mode css --->
    <link href="{{ asset('assets') }}/css/style-dark.css" rel="stylesheet">

    <!---Skinmodes css-->
    <link href="{{ asset('assets') }}/css/skin-modes.css" rel="stylesheet" />

    <!--- Animations css-->
    <link href="{{ asset('assets') }}/css/animate.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>

    <style>
        .text-gold {
            color: #FFCC00 !important;
        }

        .bg-gold {
            background: #FFCC00;
            -webkit-box-shadow: inset 1px 6px 12px #fff61a, inset -1px -10px 5px #fff61a, 1px 2px 1px #FFCC00;
            -moz-box-shadow: inset 1px 6px 12px #fff61a, inset -1px -10px 5px #fff61a, 1px 2px 1px #FFCC00;
            box-shadow: inset 1px 6px 12px #fff61a, inset -1px -10px 5px #fff61a, 1px 2px 1px #FFCC00;
        }

        .btn-red {
            background: #FF0000;
            -webkit-box-shadow: inset 1px 6px 12px #bc0800, inset -1px -10px 5px #a80009, 1px 2px 1px #a80009;
            -moz-box-shadow: inset 1px 6px 12px #bc0800, inset -1px -10px 5px #a80009, 1px 2px 1px #a80009;
            box-shadow: inset 1px 6px 12px #bc0800, inset -1px -10px 5px #a80009, 1px 2px 1px #a80009;
        }

        body {
            font-family: Roboto, sans-serif !important;
        }

        .field-icon {
            float: right;
            margin-top: -30px;
            position: relative;
            z-index: 2;
            margin-right: 10px;
        }

    </style>
</head>
<body class="main-body dark-theme" style="background: black !important">

<!-- Loader -->
<div id="global-loader">
    <img src="{{ asset('assets') }}/img/loader.svg" class="loader-img" alt="Loader">
</div>
<!-- /Loader -->

<!-- Page -->
<div class="page">

    <div class="container-fluid">
        <div class="row no-gutter">
            <!-- The image half -->
            <div class="col-md-6 col-lg-6 col-xl-7 d-none d-md-flex">
                <div class="row wd-100p mx-auto text-center">
                    <div class="col-md-12 col-lg-12 col-xl-12 my-auto mx-auto wd-100p">
                        <img src="{{ asset('images/sabong-logo.png') }}" class="my-auto ht-xl-80p wd-md-100p wd-xl-50p mx-auto" alt="logo">
                    </div>
                </div>
            </div>
            <!-- The content half -->
            <div class="col-md-6 col-lg-6 col-xl-5">
                <div class="login d-flex align-items-center py-2">
                    <!-- Demo content-->
                    <div class="container p-0">
                        <div class="row">
                            <div class="col-md-10 col-lg-10 col-xl-9 mx-auto">
                                <div class="card-sigin">
{{--                                    <div class="mb-5 d-flex">--}}
{{--                                        <h1 class="main-logo1 ml-1 mr-0 my-auto tx-28 text-danger">Sabo<span>ng</span> World</h1>--}}
{{--                                    </div>--}}
                                    <div class="card-sigin">
                                        <div class="main-signup-header">
                                            <h2 class="text-gold">Welcome to Sabong World Live!</h2>
                                            <h5 class="font-weight-semibold mb-4">Please sign in to continue.</h5>
                                            <form action="authenticate" method="POST">
                                                @csrf
                                                <div class="form-group">
                                                    <input type="number" name="contact_number" placeholder="Phone Number" class="form-control" style="border-radius: 1rem" />
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-md-12">
                                                        <input id="password-field" type="password" name="password" placeholder="Password" class="form-control" style="border-radius: 1rem" />
                                                        <span toggle="#password-field" class="bx bx-show field-icon toggle-password"></span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label>
                                                        <input type="checkbox" name="remember">
                                                        <span>
                                                            Keep me login
                                                        </span>
                                                    </label>
                                                </div>
                                                <button class="btn bg-gold btn-block">Sign In</button>
                                            </form>
                                            <div class="main-signin-footer mt-5">
                                                <p><a href="forgot-password-form">Forgot password?</a></p>
                                                <p>Don't have an account? <a href="register-form">Create an Account</a></p>
                                                <p>Can't see our Video? <a href="https://play.google.com/store/apps/details?id=com.cloudmosa.puffinFree&hl=en">Download Puffin Browser Here</a></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!-- End -->
                </div>
            </div><!-- End -->
        </div>
    </div>

</div>
<!-- End Page -->

<!-- JQuery min js -->
<script src="{{ asset('assets') }}/plugins/jquery/jquery.min.js"></script>

<!-- Bootstrap Bundle js -->
<script src="{{ asset('assets') }}/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Ionicons js -->
<script src="{{ asset('assets') }}/plugins/ionicons/ionicons.js"></script>

<!-- Moment js -->
<script src="{{ asset('assets') }}/plugins/moment/moment.js"></script>

<!-- eva-icons js -->
<script src="{{ asset('assets') }}/js/eva-icons.min.js"></script>

<!-- Rating js-->
<script src="{{ asset('assets') }}/plugins/rating/jquery.rating-stars.js"></script>
<script src="{{ asset('assets') }}/plugins/rating/jquery.barrating.js"></script>

<!-- custom js -->
<script src="{{ asset('assets') }}/js/custom.js"></script>

<script>
    $(".toggle-password").click(function() {


        var input = $($(this).attr("toggle"));
        if (input.attr("type") == "password") {
            input.attr("type", "text");
            $(this).removeClass("bx bx-show");
            $(this).toggleClass("bx bx-hide");
        } else {
            input.attr("type", "password");
            $(this).removeClass("bx bx-hide");
            $(this).toggleClass("bx bx-show");
        }
    });
</script>
<script>


    @if(Session::has('message'))

    let type = "{{ Session::get('type', 'info') }}";

    switch(type){

        case 'info':
            Swal.fire({

                type: 'info',
                title: "{{ Session::get('message') }}",
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 3000

            });

            break;

        case 'warning':

            Swal.fire({

                type: 'warning',
                title: "{{ Session::get('message') }}",
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 3000

            });

            break;

        case 'success':
            Swal.fire({

                type: 'success',
                title: "{{ Session::get('message') }}",
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 3000

            });
            break;

        case 'error':
            Swal.fire({

                type: 'error',
                title: "{{ Session::get('message') }}",
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 3000

            });
            break;
    }
    @endif

</script>
@if ($errors->any())
    @foreach ($errors->all() as $error)
        <script>
            toastr["warning"]("{{ $error }}");

            {{--new Noty({--}}
            {{--    type: 'warning',--}}
            {{--    layout: 'topRight',--}}
            {{--    text: "{{ $error }}"--}}
            {{--}).show();--}}
        </script>
    @endforeach
@endif
</body>
</html>
