<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WinningStreak extends Model
{
    //

    protected $fillable = [
        'left_streak', 'right_streak', 'game_session_id'
    ];
}
