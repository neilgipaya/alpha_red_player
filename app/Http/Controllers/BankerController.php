<<<<<<< HEAD
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BankAccounts;
class BankerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!auth()->user()->can('banker-module')){
            abort(403,'Unauthorized');
        }
        return view('Configuration.banker');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }
    public function details($id){
        $data = \DB::table('bets as a')
        ->select(\DB::raw('SUM(a.bet_payout) as pay'),'b.game_number','c.middle_win',\DB::raw('SUM(a.bet_middle) as claim'),'a.game_id','b.created_at',
        'c.left_win','c.middle_win','c.right_win')
        ->groupBy('a.game_id')
        ->join('games as b','b.id','=','a.game_id')
        ->join('matches as c','c.game_id','=','a.game_id')
        ->orderBy('b.game_number','DESC')
        ->where('banker_id',$id)->get();
        return response()->json($data);
    }
    public function game_details($game_id){
        $data = \DB::table('bets as a')
        ->select('b.username','b.wallet_balance','b.name','c.middle_win','a.bet_payout','a.bet_middle')
        ->join('users as b','a.user_id','=','b.id')
        ->join('matches as c','c.game_id','=','a.game_id')
        ->where('a.game_id',$game_id)
        ->get();
        return response()->json($data);
    }
    public function topup(Request $request){
        if(!auth()->user()->can('banker-topup')){
            abort(403,'Unauthorized');
        }
        $request->merge([
            'request_by' => auth()->user()->id
        ]);
        \DB::beginTransaction();
        try{
            $data = \App\BankerTopup::create($request->all());
            \App\UserLogs::create([
                'user_id' => auth()->user()->id,
                'type' => 'Banker Topup',
                'content' => 'Banker Top up requested for Banker ID:( '.$request->banker_id.' ) with Amount: ('.$request->amount.') || Track ID: '.$data->id
            ]);
            \DB::commit();
            $notification = array(
                'message' => 'Topup for banker has been submitted',
                'type' => 'success'
            );
            return response()->json($notification);
        }catch(\Exception $e){
            \DB::rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger'
            );
            return response()->json($notification);
        }
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        \DB::beginTransaction();
        try{
            $checkMatch = \DB::table('game_sessions as a')
            ->select('c.match_status')
            ->join('games as b','a.id','=','b.game_session_id')
            ->join('matches as c','c.game_id','=','b.id')
            ->where('a.status','ongoing')->first();
            if($checkMatch->match_status != 'Pending' && $checkMatch->match_status != 'Completed'){
                $notification = array(
                    'message' => 'Wait until the current game number to be completed before to update banker settings',
                    'type' => 'danger'
                );
                return response()->json($notification);
            }
            $data = BankAccounts::find($id);
            if($request->type == 'banker'){
                if(!$request->banker){
                    $data->update(['banker' => 0]);
                    $notification = array(
                        'message' => $data->account_name. ' has been removed as a banker !',
                        'type' => 'warning'
                    );
                }else{
                    $check = BankAccounts::where('category','Banker')->where('banker',1)->first();
                    if($check){
                        $notification = array(
                            'message' => 'You cannot activate 2 banker at the same time',
                            'type' => 'danger'
                        );
                        return response()->json($notification);
                    }
                    $data->update(['banker' => 1]);
                    $notification = array(
                        'message' => $data->account_name. ' has been set as a banker !',
                        'type' => 'success'
                    );
                }
            }else if($request->type == 'limit_bet'){
                if($request->limit_bet < $data->balance){
                    $data->update(['limit_bet' => $request->limit_bet]);
                    $notification = array(
                        'message' => $data->account_name. ' Limit bet has been set to '.$request->limit_bet,
                        'type' => 'success'
                    );
                }else{
                    $notification = array(
                        'message' => 'Limit bet cannot exceed the current balance of the bank',
                        'type' => 'danger'
                    );
                    return response()->json($notification);
                }
            }else if($request->type == 'total_limit_bet'){
                $data->update(['total_limit_bet' => $request->total_limit_bet]);
                $notification = array(
                    'message' => $data->account_name. ' Total Limit bet has been set to '.$request->limit_bet,
                    'type' => 'success'
                );
            }
            else if($request->type == 'rake'){
                if(!$data->rake){
                    $data->update(['rake' => 1]);
                    $notification = array(
                        'message' => $data->account_name. ' Rake has been activated !',
                        'type' => 'success'
                    );
                }else{
                    $data->update(['rake' => 0]);
                    $notification = array(
                        'message' => $data->account_name. ' Rake has been deactivated !',
                        'type' => 'success'
                    );
                }
            }
            \DB::commit();
            return response()->json($notification);
        }catch(\Exception $e){
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger'
            );
            return response()->json($notification);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
=======
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BankAccounts;
class BankerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!auth()->user()->can('banker-module')){
            abort(403,'Unauthorized');
        }
        return view('Configuration.banker');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }
    public function details($id){
        $data = \DB::table('bets as a')
        ->select(\DB::raw('SUM(a.bet_payout) as pay'),'b.game_number','c.middle_win',\DB::raw('SUM(a.bet_middle) as claim'),'a.game_id','b.created_at',
        'c.left_win','c.middle_win','c.right_win')
        ->groupBy('a.game_id')
        ->join('games as b','b.id','=','a.game_id')
        ->join('matches as c','c.game_id','=','a.game_id')
        ->orderBy('b.game_number','DESC')
        ->where('banker_id',$id)->get();
        return response()->json($data);
    }
    public function game_details($game_id){
        $data = \DB::table('bets as a')
        ->select('b.username','b.wallet_balance','b.name','c.middle_win','a.bet_payout','a.bet_middle')
        ->join('users as b','a.user_id','=','b.id')
        ->join('matches as c','c.game_id','=','a.game_id')
        ->where('a.game_id',$game_id)
        ->get();
        return response()->json($data);
    }
    public function topup(Request $request){
        if(!auth()->user()->can('banker-topup')){
            abort(403,'Unauthorized');
        }
        $request->merge([
            'request_by' => auth()->user()->id
        ]);
        \DB::beginTransaction();
        try{
            $data = \App\BankerTopup::create($request->all());
            \App\UserLogs::create([
                'user_id' => auth()->user()->id,
                'type' => 'Banker Topup',
                'content' => 'Banker Top up requested for Banker ID:( '.$request->banker_id.' ) with Amount: ('.$request->amount.') || Track ID: '.$data->id
            ]);
            \DB::commit();
            $notification = array(
                'message' => 'Topup for banker has been submitted',
                'type' => 'success'
            );
            return response()->json($notification);
        }catch(\Exception $e){
            \DB::rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger'
            );
            return response()->json($notification);
        }
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        \DB::beginTransaction();
        try{
            $checkMatch = \DB::table('game_sessions as a')
            ->select('c.match_status')
            ->join('games as b','a.id','=','b.game_session_id')
            ->join('matches as c','c.game_id','=','b.id')
            ->where('a.status','ongoing')->first();
            if($checkMatch){
                if($checkMatch->match_status != 'Pending' && $checkMatch->match_status != 'Completed'){
                    $notification = array(
                        'message' => 'Wait until the current game number to be completed before to update banker settings',
                        'type' => 'danger'
                    );
                    return response()->json($notification);
                }
            }
            $data = BankAccounts::find($id);
            if($request->type == 'banker'){
                if(!$request->banker){
                    $data->update(['banker' => 0]);
                    $notification = array(
                        'message' => $data->account_name. ' has been removed as a banker !',
                        'type' => 'warning'
                    );
                }else{
                    $check = BankAccounts::where('category','Banker')->where('banker',1)->first();
                    if($check){
                        $notification = array(
                            'message' => 'You cannot activate 2 banker at the same time',
                            'type' => 'danger'
                        );
                        return response()->json($notification);
                    }
                    $data->update(['banker' => 1]);
                    $notification = array(
                        'message' => $data->account_name. ' has been set as a banker !',
                        'type' => 'success'
                    );
                }
            }else if($request->type == 'limit_bet'){
                if($request->limit_bet < $data->balance){
                    $data->update(['limit_bet' => $request->limit_bet]);
                    $notification = array(
                        'message' => $data->account_name. ' Limit bet has been set to '.$request->limit_bet,
                        'type' => 'success'
                    );
                }else{
                    $notification = array(
                        'message' => 'Limit bet cannot exceed the current balance of the bank',
                        'type' => 'danger'
                    );
                    return response()->json($notification);
                }
            }else if($request->type == 'total_limit_bet'){
                $data->update(['total_limit_bet' => $request->total_limit_bet]);
                $notification = array(
                    'message' => $data->account_name. ' Total Limit bet has been set to '.$request->limit_bet,
                    'type' => 'success'
                );
            }
            else if($request->type == 'rake'){
                if(!$data->rake){
                    $data->update(['rake' => 1]);
                    $notification = array(
                        'message' => $data->account_name. ' Rake has been activated !',
                        'type' => 'success'
                    );
                }else{
                    $data->update(['rake' => 0]);
                    $notification = array(
                        'message' => $data->account_name. ' Rake has been deactivated !',
                        'type' => 'success'
                    );
                }
            }
            \DB::commit();
            return response()->json($notification);
        }catch(\Exception $e){
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger'
            );
            return response()->json($notification);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
>>>>>>> ae5b3d3ce322a6f5ff74d79fe1c0fb883afacf4c
