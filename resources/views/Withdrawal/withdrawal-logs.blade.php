@extends('layouts/contentLayoutMaster')

@section('breadcrumb')
    <div class="clearfix"></div>
@endsection
@section('content')
    @if(auth()->user()->type == 'Master Agent' || auth()->user()->type == 'Gold Agent' || auth()->user()->type == 'Silver Agent')
    <withdrawal-logs-agents></withdrawal-logs-agents>
    @else
    <withdrawal-logs></withdrawal-logs>
    @endif
@endsection

@section('vendor-scripts')

@endsection

