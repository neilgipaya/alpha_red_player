<<<<<<< HEAD
<?php

namespace App\Http\Controllers;

use App\Bets;
use App\Events\GameEvent;
use App\Events\NewGameEvent;
use App\Events\PlaceBetEvent;
use App\Events\RevertEvent;
use App\Events\ChatEvent;
use App\Events\DeclareWinnerEvent;
use App\BankAccounts;
use App\Events\WinnerEvent;
use App\Game;
use App\Events\AnnouncementEvent;
use App\GameHistory;
use App\GameScreenSaver;
use App\GameSession;
use App\Http\Services\BetService;
use App\Http\Services\PayoutService;
use App\Http\Services\RedeclarePayoutService;
use App\Matches;
use App\Partner;
use App\Topup;
use App\User;
use App\send_message;
use App\WinningStreak;
use App\Withdrawal;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Jenssegers\Agent\Agent;
use PHPUnit\Framework\MockObject\Builder\Match;

class GameController extends Controller
{
    /**
     * @var BetService
     */
    private $betService;
    /**
     * @var PayoutService
     */
    private $payoutService;

    private $redeclarePayoutService;

    /**
     * GameController constructor.
     * @param BetService $betService
     * @param PayoutService $payoutService
     */
    public function __construct(BetService $betService, PayoutService $payoutService, RedeclarePayoutService $redeclarePayoutService)
    {
        $this->betService = $betService;
        $this->redeclarePayoutService = $redeclarePayoutService;
        $this->payoutService = $payoutService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if(!auth()->user()->can('host-game-module')){
            abort(403, 'Unauthorized');
        }
        return view('Game.lobby');
    }

    public function getGameList()
    {
        // $game = GameSession::latest()->get();
        $game = \DB::table('game_sessions as a')
        ->select('a.*',\DB::raw('SUM(c.total_rake) as total_rake'))
        ->join('games as b','a.id','=','b.game_session_id')
        ->join('matches as c','c.game_id','=','b.id')
        ->groupBy('a.id')
        ->latest()->get();

        return response()->json($game);
    }
    // public function gamehistory($id){
    //     $game = GameSession::where('id',$id)->latest()->get();
    //     return response()->json($game);

    // }
    public function gameReports($id){
        if(!auth()->user()->can('host-game-module')){
            abort(403, 'Unauthorized');
        }
        return view('Game.reports',array('game_session_id' => $id));
    }
    public function viewBetting($game_id){
        if(!auth()->user()->can('host-game-module')){
            abort(403,'Unauthorized');
        }
        $data = Game::where('id',$game_id)->first();
        return view('Game.view_betting',array('data' => $data));
    }
    public function view_betting_data($game_id){
        $data = Game::with('matches')->with(['bets' => function($q){
            $q->with('user');
        }])->where('id',$game_id)->first();
        return response()->json($data);
    }
    public function getGameInfo($game_session_id){
        // $data = Game::where('game_session_id',$game_session_id)->get();
        $data['game'] = Game::with('matches')->with('bets')->with('bets.user')->latest()->where('game_session_id',$game_session_id)->get();
        $yesterday = date("Y-m-d", strtotime( '-1 days' ) );
        // $data['total_loading'] = \DB::table('topups')->whereDate('created_at', $yesterday)->where('status','Approved')->sum('amount');
        $data['total_loading'] = \DB::table('topups')->where('status','Approved')->sum('amount');
        $data['total_load_withdrawal'] = \DB::table('withdrawals')->where('status','Transferred')->sum('withdrawal_amount');
        $data['total_commission_cashout'] = \DB::table('user_logs')->where('type','Converted Commission')->sum('content');
        $data['current_agent_wallet'] = \DB::table('users')->where('type','!=','Official')->where('type','!=','Player')->sum('wallet_balance');
        $data['current_agent_commission'] = \DB::table('users')->where('type','!=','Official')->where('type','!=','Player')->sum('agent_commission_balance');
        $data['total_processed_commission'] = \DB::table('games as a')->join('matches as b','a.id','=','b.game_id')->where('a.game_session_id',$game_session_id)->sum('b.total_rake') * 0.2;
        $data['player_actual_points'] = \DB::table('users')->where('type','Player')->sum('wallet_balance');
        $data['game_info'] = \DB::table('game_sessions')->where('id',$game_session_id)->first();
        return response()->json($data);
    }

    public function get_message(){
        return response()->json(send_message::whereDate('created_at',Carbon::today())->get());
    }
    public function send_message(Request $request){
        $role = \DB::table('model_has_roles')->select('role_id')->where('model_id',auth()->user()->id)->get();
        DB::beginTransaction();
        try{
            $id = auth()->user()->id;
            $name = auth()->user()->name;
            $message = $request->message;
            $request->merge([
                'user_id' => $id,
                'type' => $role[0]->role_id,
                'name' => $name,
            ]);
            $get = send_message::create($request->all());
            DB::commit();
            event(new ChatEvent($get));
            $notification = array(
                'message' => 'Message sent',
                'type' => 'success',
            );
            return response()->json($notification);


        }catch (\Exception $e){
            DB::Rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger',
            );
            return response()->json($notification);

        }
    }

    public function getSessionHistory()
    {
        $game_session = GameSession::where('status','Ongoing')->latest()->first();
        if(empty($game_session)){
            $game_session = GameSession::where('status','Completed')->latest()->first();
        }
        $game = Game::with('matches')->with('bets')->with('bets.user')->latest()->where('game_session_id',$game_session->id)->get();

        return response()->json($game);
    }

    public function getSessionHistoryFiltered(Request $request)
    {
        $from = $request->from;
        $to = $request->to;
        $game = Game::whereBetween('created_at',[$from, $to])->with('matches')->with('bets')->with('bets.user')->latest()->get();

        return response()->json($game);
    }
    public function play()
    {

        $agent = new Agent();

        if ($agent->isMobile()){
            $show = true;
        } else {
            $show = false;
        }

        return view('Game.play-game', compact('show'));
    }

    public function logs()
    {
        if(!auth()->user()->can('manage-game-session')){
            abort(403, 'Unauthorized');
        }
        return view('Game.game-session-history');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        if(!auth()->user()->can('create-game-session')){
            abort(403, 'Unauthorized');
        }
        return view('Game.create-game');
    }

    public function getList()
    {
        $currentSession = GameSession::where('status', 'Ongoing')->latest()->first();
        $game = Bets::with('matches')->with('games')
            ->whereHas('games', function ($q) use($currentSession){
                $q->where('game_session_id', $currentSession->id);
            })
            ->where('user_id', auth()->user()->id)
            ->orderBy('created_at','desc')
            ->get();

        return response()->json($game);
    }
    public function openArena(Request $request){
        DB::beginTransaction();
        try{
            $validation = GameSession::where('status', 'Ongoing')->latest()->first();
            if($validation){
                $notification = array(
                    'message' => 'There is a current arena opened, you cannot open multiple arena.',
                    'type' => 'danger',
                );
                return response()->json($notification);
            }
            GameSession::where('id',$request->id)->update(['status' => 'Ongoing']);
            \App\UserLogs::create([
                'user_id' => auth()->user()->id,
                'type' => 'Arena',
                'content' => 'Open new arena - Game ID: '.$request->id
            ]);
            $game = Game::where('game_session_id',$request->id)->first();
            $games = Game::with('matches')
            ->with('session')
            ->whereHas('session', function ($q){
                $q->where('status', 'Ongoing');
            })
            ->with('session.screensaver')
            ->where('id',$game->id)->first();
            event(new PlaceBetEvent($games,$game->game_session_id));
            $notification = array(
                'message' => 'New arena has been opened.',
                'type' => 'success',
            );
            DB::commit();
            return response()->json($notification);
        }catch(\Exception $e){
            DB::rollback();
            $notification = array(
                'message' => 'Interval error '.$e->getMessage(),
                'type' => 'danger',
            );
            return response()->json($notification);
        }
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        DB::beginTransaction();

        try {
            //

            $request->merge([
                'status' => 'Pending',
                'created_by' => auth()->user()->id,
                'session_live' => 1,
            ]);



            $game_session = GameSession::create($request->all());

            $request->merge(['game_session_id' => $game_session->id]);

            $game = Game::create($request->all());
            event(new GameEvent());


            Matches::create([
                'game_id' => $game->id,
                'match_status' => 'Pending'
            ]);

            DB::commit();
            $notification = array(
                'message' => 'Game Created successfully. Redirecting you to Game Controller, Please wait',
                'id' => $game->id,
                'type' => 'success',
            );

            return response()->json($notification);

        } catch (\Exception $e) {

            DB::rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger',
            );

            return response()->json($notification);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function newGame(Request $request)
    {
        DB::beginTransaction();

        try {
            //

            $game = Game::create([
                'game_number' => $request->post('game_number') + 1,
                'game_title' => $request->post('game_title'),
                'game_session_id' => $request->post('game_session_id'),
                'created_by' => auth()->user()->id,
                'status' => 'Pending'
            ]);

            Matches::create([
                'game_id' => $game->id,
                'match_status' => 'Pending'
            ]);
            $newgame = Game::with('matches')
            ->with('session')
            ->whereHas('session', function ($q){
                $q->where('status', 'Ongoing');
            })
            ->with('session.screensaver')
            ->latest()->first();
            event(new NewGameEvent($newgame));
            // event(new GameEvent());

            DB::commit();
            $notification = array(
                'message' => 'New Game Created successfully',
                'type' => 'success',
            );

            return response()->json($notification);
        } catch (\Exception $e) {

            DB::rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger',
            );

            return response()->json($notification);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Game  $game
     * @return \Illuminate\Http\Response
     */
    public function show(Game $game)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Game  $game
     * @return \Illuminate\Http\Response
     */
    public function edit(Game $game)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            // Game::find($id)->update($request->all());
            $game = Game::find($id);
            $game->update($request->all());
            $games = Game::with('matches')
            ->with('session')
            ->whereHas('session', function ($q){
                $q->where('status', 'Ongoing');
            })
            ->with('session.screensaver')
            ->where('id',$id)->first();
            event(new PlaceBetEvent($games,$game->game_session_id));
            // event(new GameEvent());
            DB::commit();
            $notification = array(
                'message' => 'Game Updated successfully',
                'type' => 'success',
            );
            return response()->json($notification);
        } catch (\Exception $e) {
            DB::rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger',
            );

            return response()->json($notification);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Game  $game
     * @return \Illuminate\Http\Response
     */
    public function destroy(Game $game)
    {
        //
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function count()
    {
        $count = Game::where('created_at', Carbon::today()->format('Y-m-d'))->get()->count();

        return response()->json($count + 1);
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function currentSession()
    {
        if(!auth()->user()->can('manage-game-session')){
            abort(403, 'Unauthorized');
        }
        return view('Game.current-session');
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function currentGameSession()
    {
        $game = Game::with('matches')
            // ->with('bets')
            ->with('session')
            ->whereHas('session', function ($q){
                $q->where('status', 'Ongoing');
            })
            ->with('session.screensaver')
            ->latest()->first();

        return response()->json($game);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function matchWinner(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $this->payoutService->processWinner($request, $id);
            $game = Game::with('matches')
            // ->with('bets')
            ->with('session')
            ->whereHas('session', function ($q){
                $q->where('status', 'Ongoing');
            })
            ->with('session.screensaver')
            ->latest()->first();
            event(new DeclareWinnerEvent($game));
            $trends = Game::with('matches')->where('game_session_id', $game->game_session_id)->latest()->first();
            event(new \App\Events\TrendsEvent($trends));
            // event(new NewGameEvent($game));
            // event(new GameEvent());
            // event(new WinnerEvent());
            DB::commit();
            $notification = array(
                'message' => 'Marked as Winner Successfully',
                'type' => 'success',
            );
            return response()->json($notification);
        } catch (\Exception $e) {
            DB::rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger',
            );
            return response()->json($notification);
        }
    }
    public function RedeclareMatchWinner(Request $request,$id){
        DB::beginTransaction();
        try {
            $this->redeclarePayoutService->processWinner($request, $id);
            $declare = '';
            if($request->winner == 'left'){
                $declare = 'Meron';
            }else if($request->winner == 'right'){
                $declare = 'Wala';
            }else if($request->winner == 'middle'){
                $declare = 'Draw';
            }else if($request->winner == 'cancel'){
                $declare = 'Cancel';
            }
            $message = 'Game Number: "'.$request->game_number. '" Has been Redeclared as '.
            $declare.' All Bets has been updated';
            event(new AnnouncementEvent($message));
            DB::commit();
            $notification = array(
                'message' => $message,
                'type' => 'success',
            );
            return response()->json($notification);
        } catch (\Exception $e) {
            DB::rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger',
            );
            return response()->json($notification);
        }
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function cancelGame(Request $request, $id)
    {
        DB::beginTransaction();

        try {
            //
            Game::find($id)->update($request->all());
            if($request->status == 'Draw'){
                $this->payoutService->drawGame($request, $id);
            }else if($request->status == 'Cancelled'){
                $this->payoutService->cancelGame($request, $id);
            }
            event(new GameEvent());
            event(new WinnerEvent());

            DB::commit();
            $notification = array(
                'message' => 'Game Updated successfully',
                'type' => 'success',
            );

            return response()->json($notification);
        } catch (\Exception $e) {

            DB::rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger',
            );

            return response()->json($notification);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPlayerBet($id)
    {
        $bets = Bets::with('games')
            ->with('matches')
            ->where('game_id', $id)
            ->where('user_id', auth()->user()->id)
            ->latest()
            ->first();

        return response()->json($bets);
    }

    public function placeBet(Request $request)
    {
        if(!auth()->user()->type == 'Player'){
            $notification = array(
                'message' => 'Only player is allowed to play the game',
                'type' => 'danger',
            );
            return response()->json($notification);
        }
        DB::beginTransaction();

        try {
            //
            $games = Game::orderBy('id', 'DESC')->first();
            if($games->status != 'Open' && $games->status != 'Last Call'){
                $notification = array(
                    'message' => 'Game Session is already Closed!',
                    'type' => 'danger',
                );
                return response()->json($notification);
            }

            if ($request->post('bet_amount') >= 100){
                if ($request->post('bet_amount') <= auth()->user()->wallet_balance) {
                    $count = Matches::find($request->matches_id);
                    if($request->bet_position == 'left'){
                        $this->betService->placeBet($request);
                        $count->increment('left_count');
                    }else if($request->bet_position == 'right'){
                        $this->betService->placeBet($request);
                        $count->increment('right_count');
                    }else if($request->bet_position == 'middle'){
                        $check_banker = BankAccounts::where('category','Banker')->where('banker',1)->first();
                        if($check_banker){
                            $check_availability = Bets::where('game_id',$request->post('game_id'))->sum('bet_middle');
                            $future_payout = $check_availability + $request->post('bet_amount');
                            if($check_banker->total_limit_bet > 0){
                                $current_total_bet = \App\Bets::where('game_id',$request->matches_id)->sum('bet_middle');
                                $future_total = $current_total_bet + $request->post('bet_amount');
                                if($future_total > $check_banker->total_limit_bet){
                                    $notification = array(
                                        'message' => 'Banker Total Limit Bet has been reached cannot placebet anymore',
                                        'type' => 'danger'
                                    );
                                    return response()->json($notification);
                                }
                            }
                            if($check_banker->limit_bet > 0){
                                if($request->post('bet_amount') > $check_banker->limit_bet){
                                    $notification = array(
                                        'message' => 'You cannot bet higher than '.$check_banker->limit_bet.' for draw',
                                        'type' => 'danger'
                                    );
                                    return response()->json($notification);
                                }
                            }
                        }else{
                            $notification = array(
                                'message' => 'There`s no banker activated cannot placebet for Draw',
                                'type' => 'danger'
                            );
                            return response()->json($notification);
                        }
                        $this->betService->drawBet($request);
                        $count->increment('middle_count');
                    }
                    // event(new GameEvent());
                    $game = Game::with('matches')
                    ->with('session')
                    ->whereHas('session', function ($q){
                        $q->where('status', 'Ongoing');
                    })
                    ->with('session.screensaver')
                    ->latest()->first();
                    event(new PlaceBetEvent($game));

                } else {
                    $notification = array(
                        'message' => 'Insufficient Wallet Balance',
                        'type' => 'danger',
                    );

                    return response()->json($notification);
                }

            } else {
                $notification = array(
                    'message' => 'Minimum of 100PHP BET',
                    'type' => 'danger',
                );

                return response()->json($notification);
            }




            DB::commit();
            $notification = array(
                'message' => 'Bet Placed!',
                'type' => 'success',
            );

            return response()->json($notification);
        } catch (\Exception $e) {

            DB::rollback();
            $notification = array(
                'message' => $e->getMessage() . " " . $e->getLine(),
                'type' => 'danger',
            );

            return response()->json($notification);
        }

    }

    public function getTrends()
    {
        $currentSession = GameSession::where('status', 'Ongoing')->latest()->first();
        if ($currentSession){
            $trends = Game::with('matches')
                ->where('game_session_id', $currentSession->id)->latest()->get();
        } else {
            $trends = [];
        }
        return response()->json($trends);
    }
    public function getWins()
    {

        $currentSession = GameSession::where('status', 'Ongoing')->latest()->first();

        if (isset($currentSession)){
            $data['left'] = Game::with('matches')
                ->whereHas('matches',
                    function ($query){
                        $query->where('left_win', 1);
                    })
                ->where('status', 'Completed')
                ->where('game_session_id', $currentSession->id)
                ->latest()
                ->count();

            $data['right'] = Game::with('matches')
                ->whereHas('matches',
                    function ($query){
                        $query->where('right_win', 1);
                    })
                ->where('status', 'Completed')
                ->where('game_session_id', $currentSession->id)
                ->latest()
                ->count();
        } else {
            $data['left'] = 0;
            $data['right'] = 0;
        }



        return response()->json($data);
    }

    public function getStreak()
    {
        $currentSession = GameSession::where('status', 'Ongoing')->latest()->first();

        if (isset($currentSession)){
            $streak = WinningStreak::latest()->where('game_session_id', $currentSession->id)->first();
        } else {
            $streak = 0;
        }

        return response()->json($streak);
    }

    public function getWalletBalance()
    {
        $user = User::select('wallet_balance')->find(auth()->user()->id);

        return response()->json($user);
    }

    public function endDay(Request $request)
    {
        $session = GameSession::where('id',$request->id)->latest()->first();

        DB::beginTransaction();

        try {
            //

            GameSession::where('status', 'Ongoing')->update([
                'status' => 'Completed',
                'date_closed' => date('Y-m-d H:i:s')
            ]);

            event(new GameEvent());
            \App\UserLogs::create([
                'user_id' => auth()->user()->id,
                'type' => 'Arena',
                'content' => 'Closed Arena - Game ID: '.$request->id
            ]);
            DB::commit();
            $notification = array(
                'message' => 'Game Session Ended successfully',
                'type' => 'success',
            );

            return response()->json($notification);
        } catch (\Exception $e) {

            DB::rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger',
            );

            return response()->json($notification);
        }
    }

    public function userCashin()
    {
        $topups = Topup::with('decline_template')->where('user_id', auth()->user()->id)->latest()->get();

        return view('Game.user-cash-in', compact('topups'));
    }

    public function userCashout()
    {
        $cashout = Withdrawal::with('decline_template')->where('user_id', auth()->user()->id)->latest()->get();
       
        return view('Game.user-cash-out', compact('cashout'));
    }

    public function getScreenSaver($game_session_id)
    {
        $screensaver = GameScreenSaver::where('game_session_id', $game_session_id)->latest()->get();
        return response()->json($screensaver);
    }

    public function uploadScreenSaver(Request $request)
    {

        DB::beginTransaction();

        try {
            //

            GameScreenSaver::create($request->all());
            event(new GameEvent());

            DB::commit();
            $notification = array(
                'message' => 'Game Screen saver Uploaded successfully',
                'type' => 'success',
            );

            return response()->json($notification);
        } catch (\Exception $e) {

            DB::rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger',
            );

            return response()->json($notification);
        }
    }

    public function checkSession()
    {
        $currentSession = GameSession::where('status', 'Ongoing')->latest()->first();

        if (isset($currentSession)){
            return response()->json(true);
        }

        return response()->json(false);

    }

    public function getTotalRake()
    {
        $rake = Partner::all()->sum('rake_percent');

        $rake_percentage = $rake;

        return response()->json($rake_percentage);
    }

    public function getBets()
    {

        $data['total_partner_rake'] = Partner::all()->sum('rake_percent');

        $bets = Bets::whereDate('created_at', Carbon::now())->sum('bet_amount') * $data['total_partner_rake'];

        return response()->json($bets);
    }

    public function getRakeFiltered(Request $request)
    {

        $data['total_partner_rake'] = Partner::all()->sum('rake_percent');

        $bets = Bets::whereBetween('created_at',[$request->post('from'), $request->post('to')])->sum('bet_amount') * $data['total_partner_rake'];

        return response()->json($bets);
    }

    public function bets()
    {
        if(!auth()->user()->can('manage-game-session')){
            abort(403, 'Unauthorized');
        }
        return view('Game.bets');
    }

    public function getPlayerBets()
    {
        $game = Game::latest()->first();

        $bets = Bets::with('user')->where('game_id', $game->id)->get();

        return response()->json($bets);
    }

    public function getBetsByPlayer($id)
    {
        $game = Bets::with('games')->where('user_id', $id)->latest()->get();

        return response()->json($game);
    }
    public function GetPreviousMatch($game_session_id){
        $Game = Game::with('matches')
        ->with('bets')
        ->where('game_session_id',$game_session_id)
        ->where('status','!=','Pending')
        ->where('status','!=','Open')
        ->where('status','!=','Closed')
        ->where('status','!=','Last Call')
        ->latest()->take(3)->get();
        return response()->json($Game);
    }
    public function processRevertDeclaration(Request $request){
        DB::beginTransaction();
        try {
            /** Revert Bet payout from the winner */
           $this->RevertWin($request);
           /** Revert Bet payout from the Loser */
           $this->RevertLoss($request);
           $message = 'Sorry for wrong declaration revert declaration has been processed';
           event(new RevertEvent($message));
           $notification = array(
                'message' => 'Revert for Game Number '.$request->game_number.' has been processed successfully!',
                'type' => 'success',
            );
            DB::commit();
            return response()->json($notification);
        } catch (\Exception $e) {
            DB::rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger',
            );
            return response()->json($notification);
        }
    }
    private function RevertWin($request){
        $game = Bets::where('game_id',$request->id)->where('bet_result','Win')->get();
        $success_revert = [];
        foreach($game as $game_value){
            $user = User::find($game_value->user_id);
            if(!$game_value->bet_left && $game_value->bet_right || $game_value->bet_left && !$game_value->bet_right){
                if($user->wallet_balance >= $game_value->bet_payout){
                    $user->decrement('wallet_balance',$game_value->bet_payout);
                    array_push($success_revert,$game_value->user_id);
                } 
            }                       
        }
        $update_bet = Bets::whereIn('user_id',$success_revert)
        ->where('game_id',$request->id)
        ->update(['bet_result' => 'RevertWin']);
    }
    private function RevertLoss($request){
        $game = Bets::where('game_id',$request->id)->where('bet_result','Loss')->get();
        $success_revert = [];
        foreach($game as $game_value){
            $user = User::find($game_value->user_id);
            if($user->wallet_balance >= $game_value->bet_payout){
                if($request->matches['right_win'] === 1){
                    $user->increment('wallet_balance',$game_value->bet_left * $game_value->bet_odds_less_rake);
                }else{
                    $user->increment('wallet_balance',$game_value->bet_right * $game_value->bet_odds_less_rake);
                }
                array_push($success_revert,$game_value->user_id);
            }            
        }
        $update_bet = Bets::whereIn('user_id',$success_revert)
        ->where('game_id',$request->id)
        ->update(['bet_result' => 'RevertLoss']);
    }
}
=======
<?php

namespace App\Http\Controllers;

use App\Bets;
use App\Events\GameEvent;
use App\Events\NewGameEvent;
use App\Events\PlaceBetEvent;
use App\Events\RevertEvent;
use App\Events\ChatEvent;
use App\Events\DeclareWinnerEvent;
use App\BankAccounts;
use App\Events\WinnerEvent;
use App\Game;
use App\Events\AnnouncementEvent;
use App\GameHistory;
use App\GameScreenSaver;
use App\GameSession;
use App\Http\Services\BetService;
use App\Http\Services\PayoutService;
use App\Http\Services\RedeclarePayoutService;
use App\Matches;
use App\Partner;
use App\Topup;
use App\User;
use App\send_message;
use App\WinningStreak;
use App\Withdrawal;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Jenssegers\Agent\Agent;
use PHPUnit\Framework\MockObject\Builder\Match;

class GameController extends Controller
{
    /**
     * @var BetService
     */
    private $betService;
    /**
     * @var PayoutService
     */
    private $payoutService;

    private $redeclarePayoutService;

    /**
     * GameController constructor.
     * @param BetService $betService
     * @param PayoutService $payoutService
     */
    public function __construct(BetService $betService, PayoutService $payoutService, RedeclarePayoutService $redeclarePayoutService)
    {
        $this->betService = $betService;
        $this->redeclarePayoutService = $redeclarePayoutService;
        $this->payoutService = $payoutService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if(!auth()->user()->can('host-game-module')){
            abort(403, 'Unauthorized');
        }
        return view('Game.lobby');
    }

    public function getGameList()
    {
        // $game = GameSession::latest()->get();
        $game = \DB::table('game_sessions as a')
        ->select('a.*',\DB::raw('SUM(c.total_rake) as total_rake'))
        ->join('games as b','a.id','=','b.game_session_id')
        ->join('matches as c','c.game_id','=','b.id')
        ->groupBy('a.id')
        ->latest()->get();

        return response()->json($game);
    }
    // public function gamehistory($id){
    //     $game = GameSession::where('id',$id)->latest()->get();
    //     return response()->json($game);

    // }
    public function gameReports($id){
        if(!auth()->user()->can('host-game-module')){
            abort(403, 'Unauthorized');
        }
        return view('Game.reports',array('game_session_id' => $id));
    }
    public function viewBetting($game_id){
        if(!auth()->user()->can('host-game-module')){
            abort(403,'Unauthorized');
        }
        $data = Game::where('id',$game_id)->first();
        return view('Game.view_betting',array('data' => $data));
    }
    public function view_betting_data($game_id){
        $data = Game::with('matches')->with(['bets' => function($q){
            $q->with('user');
        }])->where('id',$game_id)->first();
        return response()->json($data);
    }
    public function getGameInfo($game_session_id){
        // $data = Game::where('game_session_id',$game_session_id)->get();
        $data['game'] = Game::with('matches')->with('bets')->with('bets.user')->latest()->where('game_session_id',$game_session_id)->get();
        $yesterday = date("Y-m-d", strtotime( '-1 days' ) );
        // $data['total_loading'] = \DB::table('topups')->whereDate('created_at', $yesterday)->where('status','Approved')->sum('amount');
        $data['total_loading'] = \DB::table('topups')->where('status','Approved')->sum('amount');
        $data['total_load_withdrawal'] = \DB::table('withdrawals')->where('status','Transferred')->sum('withdrawal_amount');
        $data['total_commission_cashout'] = \DB::table('user_logs')->where('type','Converted Commission')->sum('content');
        $data['current_agent_wallet'] = \DB::table('users')->where('type','!=','Official')->where('type','!=','Player')->sum('wallet_balance');
        $data['current_agent_commission'] = \DB::table('users')->where('type','!=','Official')->where('type','!=','Player')->sum('agent_commission_balance');
        $data['total_processed_commission'] = \DB::table('games as a')->join('matches as b','a.id','=','b.game_id')->where('a.game_session_id',$game_session_id)->sum('b.total_rake') * 0.2;
        $data['player_actual_points'] = \DB::table('users')->where('type','Player')->sum('wallet_balance');
        $data['game_info'] = \DB::table('game_sessions')->where('id',$game_session_id)->first();
        return response()->json($data);
    }

    public function get_message(){
        return response()->json(send_message::whereDate('created_at',Carbon::today())->get());
    }
    public function send_message(Request $request){
        $role = \DB::table('model_has_roles')->select('role_id')->where('model_id',auth()->user()->id)->get();
        DB::beginTransaction();
        try{
            $id = auth()->user()->id;
            $name = auth()->user()->name;
            $message = $request->message;
            $request->merge([
                'user_id' => $id,
                'type' => $role[0]->role_id,
                'name' => $name,
            ]);
            $get = send_message::create($request->all());
            DB::commit();
            event(new ChatEvent($get));
            $notification = array(
                'message' => 'Message sent',
                'type' => 'success',
            );
            return response()->json($notification);


        }catch (\Exception $e){
            DB::Rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger',
            );
            return response()->json($notification);

        }
    }

    public function getSessionHistory()
    {
        $game_session = GameSession::where('status','Ongoing')->latest()->first();
        if(empty($game_session)){
            $game_session = GameSession::where('status','Completed')->latest()->first();
        }
        $game = Game::with('matches')->with('bets')->with('bets.user')->latest()->where('game_session_id',$game_session->id)->get();

        return response()->json($game);
    }

    public function getSessionHistoryFiltered(Request $request)
    {
        $from = $request->from;
        $to = $request->to;
        $game = Game::whereBetween('created_at',[$from, $to])->with('matches')->with('bets')->with('bets.user')->latest()->get();

        return response()->json($game);
    }
    public function play()
    {

        $agent = new Agent();

        if ($agent->isMobile()){
            $show = true;
        } else {
            $show = false;
        }
        if(!auth()->user()->can('play-game')){
            abort(403);
        }
        return view('Game.play-game', compact('show'));
    }

    public function logs()
    {
        if(!auth()->user()->can('manage-game-session')){
            abort(403, 'Unauthorized');
        }
        return view('Game.game-session-history');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        if(!auth()->user()->can('create-game-session')){
            abort(403, 'Unauthorized');
        }
        return view('Game.create-game');
    }

    public function getList()
    {
        $currentSession = GameSession::where('status', 'Ongoing')->latest()->first();
        $game = Bets::with('matches')->with('games')
            ->whereHas('games', function ($q) use($currentSession){
                $q->where('game_session_id', $currentSession->id);
            })
            ->where('user_id', auth()->user()->id)
            ->orderBy('created_at','desc')
            ->get();

        return response()->json($game);
    }
    public function openArena(Request $request){
        DB::beginTransaction();
        try{
            $validation = GameSession::where('status', 'Ongoing')->latest()->first();
            if($validation){
                $notification = array(
                    'message' => 'There is a current arena opened, you cannot open multiple arena.',
                    'type' => 'danger',
                );
                return response()->json($notification);
            }
            GameSession::where('id',$request->id)->update(['status' => 'Ongoing']);
            \App\UserLogs::create([
                'user_id' => auth()->user()->id,
                'type' => 'Arena',
                'content' => 'Open new arena - Game ID: '.$request->id
            ]);
            $game = Game::where('game_session_id',$request->id)->first();
            $games = Game::with('matches')
            ->with('session')
            ->whereHas('session', function ($q){
                $q->where('status', 'Ongoing');
            })
            ->with('session.screensaver')
            ->where('id',$game->id)->first();
            event(new PlaceBetEvent($games,$game->game_session_id));
            $notification = array(
                'message' => 'New arena has been opened.',
                'type' => 'success',
            );
            DB::commit();
            return response()->json($notification);
        }catch(\Exception $e){
            DB::rollback();
            $notification = array(
                'message' => 'Interval error '.$e->getMessage(),
                'type' => 'danger',
            );
            return response()->json($notification);
        }
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        DB::beginTransaction();

        try {
            //

            $request->merge([
                'status' => 'Pending',
                'created_by' => auth()->user()->id,
                'session_live' => 1,
            ]);



            $game_session = GameSession::create($request->all());

            $request->merge(['game_session_id' => $game_session->id]);

            $game = Game::create($request->all());
            event(new GameEvent());


            Matches::create([
                'game_id' => $game->id,
                'match_status' => 'Pending'
            ]);

            DB::commit();
            $notification = array(
                'message' => 'Game Created successfully. Redirecting you to Game Controller, Please wait',
                'id' => $game->id,
                'type' => 'success',
            );

            return response()->json($notification);

        } catch (\Exception $e) {

            DB::rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger',
            );

            return response()->json($notification);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function newGame(Request $request)
    {
        DB::beginTransaction();

        try {
            //

            $game = Game::create([
                'game_number' => $request->post('game_number') + 1,
                'game_title' => $request->post('game_title'),
                'game_session_id' => $request->post('game_session_id'),
                'created_by' => auth()->user()->id,
                'status' => 'Pending'
            ]);

            Matches::create([
                'game_id' => $game->id,
                'match_status' => 'Pending'
            ]);
            $newgame = Game::with('matches')
            ->with('session')
            ->whereHas('session', function ($q){
                $q->where('status', 'Ongoing');
            })
            ->with('session.screensaver')
            ->latest()->first();
            event(new NewGameEvent($newgame));
            // event(new GameEvent());

            DB::commit();
            $notification = array(
                'message' => 'New Game Created successfully',
                'type' => 'success',
            );

            return response()->json($notification);
        } catch (\Exception $e) {

            DB::rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger',
            );

            return response()->json($notification);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Game  $game
     * @return \Illuminate\Http\Response
     */
    public function show(Game $game)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Game  $game
     * @return \Illuminate\Http\Response
     */
    public function edit(Game $game)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            // Game::find($id)->update($request->all());
            $game = Game::find($id);
            $game->update($request->all());
            $games = Game::with('matches')
            ->with('session')
            ->whereHas('session', function ($q){
                $q->where('status', 'Ongoing');
            })
            ->with('session.screensaver')
            ->where('id',$id)->first();
            event(new PlaceBetEvent($games,$game->game_session_id));
            // event(new GameEvent());
            DB::commit();
            $notification = array(
                'message' => 'Game Updated successfully',
                'type' => 'success',
            );
            return response()->json($notification);
        } catch (\Exception $e) {
            DB::rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger',
            );

            return response()->json($notification);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Game  $game
     * @return \Illuminate\Http\Response
     */
    public function destroy(Game $game)
    {
        //
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function count()
    {
        $count = Game::where('created_at', Carbon::today()->format('Y-m-d'))->get()->count();

        return response()->json($count + 1);
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function currentSession()
    {
        if(!auth()->user()->can('manage-game-session')){
            abort(403, 'Unauthorized');
        }
        return view('Game.current-session');
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function currentGameSession()
    {
        $game = Game::with('matches')
            // ->with('bets')
            ->with('session')
            ->whereHas('session', function ($q){
                $q->where('status', 'Ongoing');
            })
            ->with('session.screensaver')
            ->latest()->first();

        return response()->json($game);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function matchWinner(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $this->payoutService->processWinner($request, $id);
            $game = Game::with('matches')
            // ->with('bets')
            ->with('session')
            ->whereHas('session', function ($q){
                $q->where('status', 'Ongoing');
            })
            ->with('session.screensaver')
            ->latest()->first();
            $trends = Game::with('matches')->where('game_session_id', $game->game_session_id)->latest()->first();
            DB::commit();
            event(new DeclareWinnerEvent($game));
            event(new \App\Events\TrendsEvent($trends));
            $notification = array(
                'message' => 'Marked as Winner Successfully',
                'type' => 'success',
            );
            return response()->json($notification);
        } catch (\Exception $e) {
            DB::rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger',
            );
            return response()->json($notification);
        }
    }
    public function RedeclareMatchWinner(Request $request,$id){
        DB::beginTransaction();
        try {
            $this->redeclarePayoutService->processWinner($request, $id);
            $declare = '';
            if($request->winner == 'left'){
                $declare = 'Meron';
            }else if($request->winner == 'right'){
                $declare = 'Wala';
            }else if($request->winner == 'middle'){
                $declare = 'Draw';
            }else if($request->winner == 'cancel'){
                $declare = 'Cancel';
            }
            $message = 'Game Number: "'.$request->game_number. '" Has been Redeclared as '.
            $declare.' All Bets has been updated';
            DB::commit();
            event(new AnnouncementEvent($message));
            $notification = array(
                'message' => $message,
                'type' => 'success',
            );
            return response()->json($notification);
        } catch (\Exception $e) {
            DB::rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger',
            );
            return response()->json($notification);
        }
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function cancelGame(Request $request, $id)
    {
        DB::beginTransaction();

        try {
            //
            Game::find($id)->update($request->all());
            if($request->status == 'Draw'){
                $this->payoutService->drawGame($request, $id);
            }else if($request->status == 'Cancelled'){
                $this->payoutService->cancelGame($request, $id);
            }
            DB::commit();
            event(new GameEvent());
            event(new WinnerEvent());
            $notification = array(
                'message' => 'Game Updated successfully',
                'type' => 'success',
            );

            return response()->json($notification);
        } catch (\Exception $e) {

            DB::rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger',
            );

            return response()->json($notification);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPlayerBet($id)
    {
        $bets = Bets::with('games')
            ->with('matches')
            ->where('game_id', $id)
            ->where('user_id', auth()->user()->id)
            ->latest()
            ->first();

        return response()->json($bets);
    }

    public function placeBet(Request $request)
    {
        if(!auth()->user()->type == 'Player'){
            $notification = array(
                'message' => 'Only player is allowed to play the game',
                'type' => 'danger',
            );
            return response()->json($notification);
        }
        DB::beginTransaction();

        try {
            //
            $games = Game::orderBy('id', 'DESC')->first();
            if($games->status != 'Open' && $games->status != 'Last Call'){
                $notification = array(
                    'message' => 'Game Session is already Closed!',
                    'type' => 'danger',
                );
                return response()->json($notification);
            }

            if ($request->post('bet_amount') >= 100){
                if ($request->post('bet_amount') <= auth()->user()->wallet_balance) {
                    $count = Matches::find($request->matches_id);
                    if($request->bet_position == 'left'){
                        $this->betService->placeBet($request);
                        $count->increment('left_count');
                    }else if($request->bet_position == 'right'){
                        $this->betService->placeBet($request);
                        $count->increment('right_count');
                    }else if($request->bet_position == 'middle'){
                        $check_banker = BankAccounts::where('category','Banker')->where('banker',1)->first();
                        if($check_banker){
                            $check_availability = Bets::where('game_id',$request->post('game_id'))->sum('bet_middle');
                            $future_payout = $check_availability + $request->post('bet_amount');
                            if($check_banker->total_limit_bet > 0){
                                $current_total_bet = \App\Bets::where('game_id',$request->matches_id)->sum('bet_middle');
                                $future_total = $current_total_bet + $request->post('bet_amount');
                                if($future_total > $check_banker->total_limit_bet){
                                    $notification = array(
                                        'message' => 'Banker Total Limit Bet has been reached cannot placebet anymore',
                                        'type' => 'danger'
                                    );
                                    return response()->json($notification);
                                }
                            }
                            if($check_banker->limit_bet > 0){
                                if($request->post('bet_amount') > $check_banker->limit_bet){
                                    $notification = array(
                                        'message' => 'You cannot bet higher than '.$check_banker->limit_bet.' for draw',
                                        'type' => 'danger'
                                    );
                                    return response()->json($notification);
                                }
                            }
                        }else{
                            $notification = array(
                                'message' => 'There`s no banker activated cannot placebet for Draw',
                                'type' => 'danger'
                            );
                            return response()->json($notification);
                        }
                        $this->betService->drawBet($request);
                        $count->increment('middle_count');
                    }
                    // event(new GameEvent());
                    $game = Game::with('matches')
                    ->with('session')
                    ->whereHas('session', function ($q){
                        $q->where('status', 'Ongoing');
                    })
                    ->with('session.screensaver')
                    ->latest()->first();
                    event(new PlaceBetEvent($game));

                } else {
                    $notification = array(
                        'message' => 'Insufficient Wallet Balance',
                        'type' => 'danger',
                    );

                    return response()->json($notification);
                }

            } else {
                $notification = array(
                    'message' => 'Minimum of 100PHP BET',
                    'type' => 'danger',
                );

                return response()->json($notification);
            }




            DB::commit();
            $notification = array(
                'message' => 'Bet Placed!',
                'type' => 'success',
            );

            return response()->json($notification);
        } catch (\Exception $e) {

            DB::rollback();
            $notification = array(
                'message' => $e->getMessage() . " " . $e->getLine(),
                'type' => 'danger',
            );

            return response()->json($notification);
        }

    }

    public function getTrends()
    {
        $currentSession = GameSession::where('status', 'Ongoing')->latest()->first();
        if ($currentSession){
            $trends = Game::with('matches')
                ->where('game_session_id', $currentSession->id)->latest()->get();
        } else {
            $trends = [];
        }
        return response()->json($trends);
    }
    public function getWins()
    {

        $currentSession = GameSession::where('status', 'Ongoing')->latest()->first();

        if (isset($currentSession)){
            $data['left'] = Game::with('matches')
                ->whereHas('matches',
                    function ($query){
                        $query->where('left_win', 1);
                    })
                ->where('status', 'Completed')
                ->where('game_session_id', $currentSession->id)
                ->latest()
                ->count();

            $data['right'] = Game::with('matches')
                ->whereHas('matches',
                    function ($query){
                        $query->where('right_win', 1);
                    })
                ->where('status', 'Completed')
                ->where('game_session_id', $currentSession->id)
                ->latest()
                ->count();
        } else {
            $data['left'] = 0;
            $data['right'] = 0;
        }



        return response()->json($data);
    }

    public function getStreak()
    {
        $currentSession = GameSession::where('status', 'Ongoing')->latest()->first();

        if (isset($currentSession)){
            $streak = WinningStreak::latest()->where('game_session_id', $currentSession->id)->first();
        } else {
            $streak = 0;
        }

        return response()->json($streak);
    }

    public function getWalletBalance()
    {
        $user = User::select('wallet_balance')->find(auth()->user()->id);

        return response()->json($user);
    }

    public function endDay(Request $request)
    {
        $session = GameSession::where('id',$request->id)->latest()->first();

        DB::beginTransaction();

        try {
            //

            GameSession::where('status', 'Ongoing')->update([
                'status' => 'Completed',
                'date_closed' => date('Y-m-d H:i:s')
            ]);

            event(new GameEvent());
            \App\UserLogs::create([
                'user_id' => auth()->user()->id,
                'type' => 'Arena',
                'content' => 'Closed Arena - Game ID: '.$request->id
            ]);
            DB::commit();
            $notification = array(
                'message' => 'Game Session Ended successfully',
                'type' => 'success',
            );

            return response()->json($notification);
        } catch (\Exception $e) {

            DB::rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger',
            );

            return response()->json($notification);
        }
    }

    public function userCashin()
    {
        $topups = Topup::with('decline_template')->where('user_id', auth()->user()->id)->latest()->get();

        return view('Game.user-cash-in', compact('topups'));
    }

    public function userCashout()
    {
        $cashout = Withdrawal::with('decline_template')->where('user_id', auth()->user()->id)->latest()->get();
       
        return view('Game.user-cash-out', compact('cashout'));
    }

    public function getScreenSaver($game_session_id)
    {
        $screensaver = GameScreenSaver::where('game_session_id', $game_session_id)->latest()->get();
        return response()->json($screensaver);
    }

    public function uploadScreenSaver(Request $request)
    {

        DB::beginTransaction();

        try {
            //

            GameScreenSaver::create($request->all());
            event(new GameEvent());

            DB::commit();
            $notification = array(
                'message' => 'Game Screen saver Uploaded successfully',
                'type' => 'success',
            );

            return response()->json($notification);
        } catch (\Exception $e) {

            DB::rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger',
            );

            return response()->json($notification);
        }
    }

    public function checkSession()
    {
        $currentSession = GameSession::where('status', 'Ongoing')->latest()->first();

        if (isset($currentSession)){
            return response()->json(true);
        }

        return response()->json(false);

    }

    public function getTotalRake()
    {
        $rake = Partner::all()->sum('rake_percent');

        $rake_percentage = $rake;

        return response()->json($rake_percentage);
    }

    public function getBets()
    {

        $data['total_partner_rake'] = Partner::all()->sum('rake_percent');

        $bets = Bets::whereDate('created_at', Carbon::now())->sum('bet_amount') * $data['total_partner_rake'];

        return response()->json($bets);
    }

    public function getRakeFiltered(Request $request)
    {

        $data['total_partner_rake'] = Partner::all()->sum('rake_percent');

        $bets = Bets::whereBetween('created_at',[$request->post('from'), $request->post('to')])->sum('bet_amount') * $data['total_partner_rake'];

        return response()->json($bets);
    }

    public function bets()
    {
        if(!auth()->user()->can('manage-game-session')){
            abort(403, 'Unauthorized');
        }
        return view('Game.bets');
    }

    public function getPlayerBets()
    {
        $game = Game::latest()->first();

        $bets = Bets::with('user')->where('game_id', $game->id)->get();

        return response()->json($bets);
    }

    public function getBetsByPlayer($id)
    {
        $game = Bets::with('games')->where('user_id', $id)->latest()->get();

        return response()->json($game);
    }
    public function GetPreviousMatch($game_session_id){
        $Game = Game::with('matches')
        ->with('bets')
        ->where('game_session_id',$game_session_id)
        ->where('status','!=','Pending')
        ->where('status','!=','Open')
        ->where('status','!=','Closed')
        ->where('status','!=','Last Call')
        ->latest()->take(3)->get();
        return response()->json($Game);
    }
    public function processRevertDeclaration(Request $request){
        DB::beginTransaction();
        try {
            /** Revert Bet payout from the winner */
           $this->RevertWin($request);
           /** Revert Bet payout from the Loser */
           $this->RevertLoss($request);
           $message = 'Sorry for wrong declaration revert declaration has been processed';
           event(new RevertEvent($message));
           $notification = array(
                'message' => 'Revert for Game Number '.$request->game_number.' has been processed successfully!',
                'type' => 'success',
            );
            DB::commit();
            return response()->json($notification);
        } catch (\Exception $e) {
            DB::rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger',
            );
            return response()->json($notification);
        }
    }
    private function RevertWin($request){
        $game = Bets::where('game_id',$request->id)->where('bet_result','Win')->get();
        $success_revert = [];
        foreach($game as $game_value){
            $user = User::find($game_value->user_id);
            if(!$game_value->bet_left && $game_value->bet_right || $game_value->bet_left && !$game_value->bet_right){
                if($user->wallet_balance >= $game_value->bet_payout){
                    $user->decrement('wallet_balance',$game_value->bet_payout);
                    array_push($success_revert,$game_value->user_id);
                } 
            }                       
        }
        $update_bet = Bets::whereIn('user_id',$success_revert)
        ->where('game_id',$request->id)
        ->update(['bet_result' => 'RevertWin']);
    }
    private function RevertLoss($request){
        $game = Bets::where('game_id',$request->id)->where('bet_result','Loss')->get();
        $success_revert = [];
        foreach($game as $game_value){
            $user = User::find($game_value->user_id);
            if($user->wallet_balance >= $game_value->bet_payout){
                if($request->matches['right_win'] === 1){
                    $user->increment('wallet_balance',$game_value->bet_left * $game_value->bet_odds_less_rake);
                }else{
                    $user->increment('wallet_balance',$game_value->bet_right * $game_value->bet_odds_less_rake);
                }
                array_push($success_revert,$game_value->user_id);
            }            
        }
        $update_bet = Bets::whereIn('user_id',$success_revert)
        ->where('game_id',$request->id)
        ->update(['bet_result' => 'RevertLoss']);
    }
}
>>>>>>> ae5b3d3ce322a6f5ff74d79fe1c0fb883afacf4c
