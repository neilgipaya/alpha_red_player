<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GameScreenSaver extends Model
{
    //

    protected $fillable = [
        'game_session_id', 'game_id', 'img_base'
    ];

    public function game_session()
    {
        return $this->belongsTo('App\GameSession', 'game_session_id');
    }

    public function game()
    {
        return $this->belongsTo('App\Game', 'game_id');
    }

}
