@extends('layouts/contentLayoutMaster')

@section('page-styles')

@endsection
@section('breadcrumb')
    <div class="clearfix"></div>
@endsection
@section('content')
    @if(auth()->user()->type == 'Gold Agent' || auth()->user()->type == 'Silver Agent' || auth()->user()->type == 'Player')
    <my-cashout-agent></my-cashout-agent>
    @else
    <my-cashout></my-cashout>
    @endif
@endsection

@section('vendor-scripts')

@endsection
