<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GameSession extends Model
{
    //

    protected $fillable = [
        'game_title', 'game_start', 'expected_game_number', 'source_1', 'source_2',
        'max_betting_time', 'last_call', 'close_bet_delay', 'max_game_time', 'status',
        'session_live', 'screensaver_id', 'player_type', 'left_label', 'right_label', 'camera_url', 'location'
    ];

    protected $casts = [
        'max_betting_time' => 'array',
        'last_call' => 'array',
        'close_bet_delay' => 'array',
        'max_game_time' => 'array'
    ];

    public function screensaver()
    {
        return $this->belongsTo('App\GameScreenSaver', 'screensaver_id');
    }
    public function games(){
        return $this->hasMany('App\Game','game_session_id');
    }
}
