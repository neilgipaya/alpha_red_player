@extends('layouts/contentLayoutMaster')


@section('breadcrumb')
    <div class="clearfix"></div>
@endsection
@section('content')
    @if(auth()->user()->type == 'Master Agent' || auth()->user()->type == 'Gold Agent' || auth()->user()->type == 'Silver Agent')
    <top-up-recent-agent></top-up-recent-agent>
    @elseif(auth()->user()->type == 'Official')
    <top-up-recent></top-up-recent>
    @endif
@endsection

@section('vendor-scripts')

@endsection
