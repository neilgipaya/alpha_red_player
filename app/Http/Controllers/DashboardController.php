<?php

namespace App\Http\Controllers;

use App\BankAccounts;
use App\Bets;
use App\FundTransfer;
use App\Partner;
use App\Topup;
use App\User;
use App\Withdrawal;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    //

    public function index(Request $request)
    {

        if (auth()->user()->type === 'Player'){
            return redirect('/game/play-game');
        } else if (auth()->user()->type === 'Silver Agent'){
            return redirect('/agentDashboard');
        } else if (!auth()->user()->can('dashboard-admin')){
            return redirect('/profile');
        }

        $data['total_wallet_cash'] = User::all()->sum('wallet_balance');
        $data['total_players_online'] = DB::table('sessions')->where('user_id' , '!=', '')->count();
        $data['average_bets_per_game'] = Bets::whereDate('created_at', Carbon::now())->get()->average('bet_amount');
        $data['total_players_registered'] = User::all()->count();
        $data['inactive_players'] = 0;
        $data['new_players_today'] = User::whereDate('created_at', Carbon::now())->get()->count();

        $data['summary_pending_cashin'] = Topup::all()->where('status', 'Pending')->count();
        $data['summary_pending_withdrawal'] = Withdrawal::all()->where('status', 'Pending')->count();
        $data['summary_pending_fund_transfer'] = FundTransfer::all()->where('status', 'Pending')->count();

        $data['total_load_daily'] = Topup::whereDate('created_at', Carbon::now())->where('status', 'Approved')->sum('amount');
        $data['total_load_monthly'] = Topup::whereMonth('created_at', Carbon::now())->where('status', 'Approved')->sum('amount');

        $data['gcash_load_daily'] = Topup::with('payments')->where('status', 'Approved')->whereHas('payments', function ($q){
            $q->where('account_type', 'GCash');
        })->whereDate('created_at', Carbon::now())->sum('amount');
        $data['gcash_load_monthly'] = Topup::with('payments')->where('status', 'Approved')->whereHas('payments', function ($q){
            $q->where('account_type', 'GCash');
        })->whereMonth('created_at', Carbon::now())->sum('amount');

        $data['paymaya_load_daily'] = Topup::with('payments')->where('status', 'Approved')->whereHas('payments', function ($q){
            $q->where('account_type', 'PayMaya');
        })->whereDate('created_at', Carbon::now())->sum('amount');

        $data['paymaya_load_monthly'] = Topup::with('payments')->where('status', 'Approved')->whereHas('payments', function ($q){
            $q->where('account_type', 'PayMaya');
        })->whereMonth('created_at', Carbon::now())->sum('amount');


        $data['ml_load_daily'] = Topup::with('payments')->where('status', 'Approved')->whereHas('payments', function ($q){
            $q->where('account_type', 'ML');
        })->whereDate('created_at', Carbon::now())->sum('amount');

        $data['ml_load_monthly'] = Topup::with('payments')->where('status', 'Approved')->whereHas('payments', function ($q){
            $q->where('account_type', 'ML');
        })->whereMonth('created_at', Carbon::now())->sum('amount');


        $data['cebuana_load_daily'] = Topup::with('payments')->where('status', 'Approved')->whereHas('payments', function ($q){
            $q->where('account_type', 'Cebuana');
        })->whereDate('created_at', Carbon::now())->sum('amount');

        $data['cebuana_load_monthly'] = Topup::with('payments')->where('status', 'Approved')->whereHas('payments', function ($q){
            $q->where('account_type', 'Cebuana');
        })->whereMonth('created_at', Carbon::now())->sum('amount');

        $data['bets_daily_amount'] = Bets::whereDate('created_at', Carbon::now())->sum('bet_amount');
        $data['bets_daily_number'] = Bets::whereDate('created_at', Carbon::now())->count();



        $data['bets_monthly_amount'] = Bets::whereMonth('created_at', Carbon::now())->sum('bet_amount');
        $data['bets_monthly_number'] = Bets::whereMonth('created_at', Carbon::now())->count();

        $data['player_withdrawal_daily'] = Withdrawal::whereDate('created_at', Carbon::now())->sum('withdrawal_amount');
        $data['player_withdrawal_monthly'] = Withdrawal::whereMonth('created_at', Carbon::now())->sum('withdrawal_amount');

        $data['funds_principal_daily'] = BankAccounts::where('category', 'Principal')->whereDate('created_at', Carbon::now())->sum('balance');
        $data['funds_principal_monthly'] = BankAccounts::where('category', 'Principal')->whereMonth('created_at', Carbon::now())->sum('balance');

        $data['funds_service_provider_daily'] = BankAccounts::where('category', 'Service Provider')->whereDate('created_at', Carbon::now())->sum('balance');
        $data['funds_service_provider_monthly'] = BankAccounts::where('category', 'Service Provider')->whereMonth('created_at', Carbon::now())->sum('balance');

        $data['funds_player_withdrawal_daily'] = BankAccounts::where('category', 'Player')->whereDate('created_at', Carbon::now())->sum('balance');
        $data['funds_player_withdrawal_monthly'] = BankAccounts::where('category', 'Player')->whereMonth('created_at', Carbon::now())->sum('balance');


        $data['top_player_load'] = User::with('topups')
            ->whereHas('topups', function ($q){
                $q->whereDate('created_at', '>', Carbon::now()->subDays(30));
            })->addSelect(['topups' => Topup::selectRaw('sum(amount) as total')
                 ->whereColumn('user_id', 'users.id')
                ->groupBy('user_id')
            ])
            ->orderBy('topups', 'DESC')
            ->get()->take(6);

        $data['top_player_bet'] = User::with('bets')
            ->whereHas('bets', function ($q){
                $q->whereDate('created_at', '>', Carbon::now()->subDays(30));
            })->addSelect(['bets' => Bets::selectRaw('sum(bet_amount) as total')
                ->whereColumn('user_id', 'users.id')
                ->groupBy('user_id')
            ])
            ->orderBy('bets', 'DESC')
            ->get()->take(6);

        $data['top_player_winner'] = User::with('bets')
            ->whereHas('bets', function ($q){
                $q->whereDate('created_at', '>', Carbon::now()->subDays(30));
            })
            ->addSelect(['win' => Bets::selectRaw('sum(bet_payout) as total')
                ->whereColumn('user_id', 'users.id')
                ->groupBy('user_id')
            ])
            ->orderBy('win', 'DESC')
            ->get()->take(6);

        $data['total_partner_rake'] = Partner::all()->sum('rake_percent');

        $data['total_rake_daily'] = Bets::whereDate('created_at', Carbon::now())->sum('bet_amount') * $data['total_partner_rake'];
        $data['total_rake_monthly'] = Bets::whereMonth('created_at', Carbon::now())->sum('bet_amount') * $data['total_partner_rake'];

        $data['principal'] = Partner::all();

        //weeekly
        $data['total_load_weekly'] = Topup::whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->where('status', 'Approved')->sum('amount');
        $data['gcash_load_weekly'] = Topup::with('payments')->where('status', 'Approved')->whereHas('payments', function ($q){
            $q->where('account_type', 'GCash');
        })->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])
            ->sum('amount');
        $data['paymaya_load_weekly'] = Topup::with('payments')->where('status', 'Approved')->whereHas('payments', function ($q){
            $q->where('account_type', 'PayMaya');
        })->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->sum('amount');
        $data['ml_load_weekly'] = Topup::with('payments')->where('status', 'Approved')->whereHas('payments', function ($q){
            $q->where('account_type', 'ML');
        })->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->sum('amount');
        $data['cebuana_load_weekly'] = Topup::with('payments')->where('status', 'Approved')->whereHas('payments', function ($q){
            $q->where('account_type', 'Cebuana');
        })->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->sum('amount');
        $data['player_withdrawal_weekly'] = Withdrawal::whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->sum('withdrawal_amount');
        $data['bets_weekly_amount'] = Bets::whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->sum('bet_amount');
        $data['bets_weekly_number'] = Bets::whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->count();
        $data['funds_principal_weekly'] = BankAccounts::where('category', 'Principal')->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->sum('balance');
        $data['funds_service_provider_weekly'] = BankAccounts::where('category', 'Service Provider')->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->sum('balance');
        $data['funds_player_withdrawal_weekly'] = BankAccounts::where('category', 'Player')->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->sum('balance');
        $data['total_rake_weekly'] = Bets::whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->sum('bet_amount') * $data['total_partner_rake'];

        $pageConfigs = [
            'pageHeader' => false
        ];

        return view('Dashboard.dashboard', $data, [
            'pageConfigs' => $pageConfigs
        ]);
    }

    public function agentDashboard()
    {

//        $data['dashboards'] = Bets::with('games')->with('user')->whereHas('user', function ($q){
//            $q->where('agent_id', auth()->user()->id);
//        })
//            ->whereDate('created_at', '>', Carbon::now()->subDays(30))
//            ->latest()->get();


        // $data['agent_funds'] = BankAccounts::where('user_id', auth()->user()->id)->latest()->first();

        // $data['downline'] = User::where('agent_id', auth()->user()->id)
        //     ->where('type', '!=', 'Player')
        //     ->where('type', '!=', 'Official')
        //     ->addSelect(['bets' => Bets::selectRaw('sum(bet_amount) as total_bet')
        //         ->whereColumn('user_id', 'users.id')
        //     ])
        //     ->get();
        if(auth()->user()->type == 'Official' || auth()->user()->type == 'Player'){
            abort(403, 'Unauthorized');
        }
        return view('Dashboard.dashboard-agent');
    }
    public function agentDownline(){
        $data = User::where('agent_id', auth()->user()->id)
            ->where('type', '!=', 'Player')
            ->where('type', '!=', 'Official')
            ->addSelect(['bets' => Bets::selectRaw('sum(bet_amount) as total_bet')
                ->whereColumn('user_id', 'users.id')
            ])
            ->orderBy('agent_commission_balance','DESC')
            ->get();
        return response()->json($data);
    }
    public function fetchCardDashboard(){
        $data['recent_activities'] = Topup::with(['users'=>function($q){
            $q->select('name','id');
        }])
        ->where('status','Pending')->get()->take(10);
        $data['total_wallet_cash'] = User::select('wallet_balance')->sum('wallet_balance');
        $data['total_players_online'] = DB::table('sessions')->where('user_id' , '!=', '')->count();
        $data['average_bets_per_game'] = Bets::whereMonth('created_at', Carbon::now())->get()->average('bet_amount');
        $data['total_players_registered'] = User::all()->count();
        $data['inactive_players'] = 0;
        $data['new_players_today'] = User::whereDate('created_at', Carbon::now())->get()->count();
        $data['cashin'] = Topup::select('amount','created_at')->where('status','Approved')->whereMonth('created_at',Carbon::now())->get();
        $data['cashout'] = Withdrawal::select('withdrawal_amount','created_at')->where('status','Transferred')->whereMonth('created_at',Carbon::now())->get();
        $data['total_partner_rake'] = Partner::all()->sum('rake_percent');
        $data['rake_data'] = DB::table('bets')->select('bet_amount','created_at')->whereMonth('created_at', Carbon::now())->get();
        $data['top_player_load'] = \DB::table('users as a')
        ->leftjoin('topups as b','b.user_id','a.id')
        ->select('a.name',\DB::raw('sum(b.amount) as total_amount'))
        ->groupBy('a.id')
        ->whereDate('b.created_at', '>', Carbon::now()->subDays(30))
        ->orderBy('total_amount','DESC')
        ->where('b.status','Approved')->get()->take(5);
        $data['top_player_bet'] = \DB::table('users as a')
        ->select('a.name',\DB::raw('sum(b.bet_amount) as bet_total'))
        ->join('bets as b','a.id','=','b.user_id')
        ->groupBy('a.id')
        ->whereDate('b.created_at', '>', Carbon::now()->subDays(30))
        ->orderBy('bet_total','DESC')
        ->get()->take(5);
        $data['top_player_winner'] = \DB::table('users as a')
        ->leftjoin('bets as b','a.id','=','b.id')
        ->select('a.name',\DB::raw('sum(b.bet_payout) as win_total'))
        ->groupBy('a.id')
        ->orderBy('win_total','DESC')
        ->whereDate('b.created_at', '>', Carbon::now()->subDays(30))
        ->get()->take(5);
        $data['bank_funds'] = BankAccounts::select('account_type','balance')->get();
        return response()->json($data);
    }
    public function fetchplaystats(){
        $bets = Bets::where('user_id',auth()->user()->id)->whereMonth('created_at', Carbon::now())->get();
        return response()->json($bets);
    }
}
