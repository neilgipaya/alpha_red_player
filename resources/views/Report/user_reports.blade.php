@extends('layouts/contentLayoutMaster')


@section('breadcrumb')
    <div class="clearfix"></div>
@endsection
@section('content')
    <user-reports :id="{{$id}}"></user-reports>
@endsection

@section('vendor-scripts')

@endsection
