<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BankAccounts extends Model
{
    //

    public $timestamps = true;

    protected $guarded = [];

    public function bank_transactions()
    {
        return $this->hasMany('App\BankTransactions', 'bank_id');
    }
    public function topups(){

        return $this->hasMany('App\Topup', 'payment_id');
    }
    public function withdrawals(){
        return $this->hasMany('App\Withdrawal', 'source_fund_id');
    }
    public function funds(){
        return $this->hasMany('App\FundTransfer', 'to_id');
    }


}
