<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> Alpha PH</title>
    <!-- favicon -->
    <link rel="shortcut icon" href="favicon.html" type="image/x-icon">
    <!-- bootstrap -->
    <link rel="stylesheet" href="{{ asset('frontend') }}/css/bootstrap.min.css">
    <!-- fontawesome icon  -->
    <link rel="stylesheet" href="{{ asset('frontend') }}/css/fontawesome.min.css">
    <!-- flaticon css -->
    <link rel="stylesheet" href="{{ asset('frontend') }}/fonts/flaticon.css">
    <!-- animate.css -->
    <link rel="stylesheet" href="{{ asset('frontend') }}/css/animate.css">
    <!-- Owl Carousel -->
    <link rel="stylesheet" href="{{ asset('frontend') }}/css/owl.carousel.css">
<!-- <link rel="stylesheet" href="{{ asset('frontend') }}/css/owl.carousel.min.css"> -->
    <link rel="stylesheet" href="{{ asset('frontend') }}/css/owl.carousel.css">
    <!-- magnific popup -->
    <link rel="stylesheet" href="{{ asset('frontend') }}/css/modal-video.min.css">
    <!-- stylesheet -->
    <link rel="stylesheet" href="{{ asset('frontend') }}/css/style.css">
    <!-- responsive -->
    <link rel="stylesheet" href="{{ asset('frontend') }}/css/responsive.css">

    <style>
        .img-same-size {
            float: left;
            width:  550px;
            height: 550px;
            object-fit: cover;
        }
    </style>

    {{-- Page Css files --}}
    <link rel="stylesheet" href="{{ asset(mix('css/pages/authentication.css')) }}">
    <link rel="stylesheet" href="intl/build/css/intlTelInput.css">
    <style>
        .iti__country-name,.iti__dial-code{
            color:black !important;
        }
        .iti{
            width:100%;
            color:black !important;
        }
    </style>

</head>