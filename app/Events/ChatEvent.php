<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class ChatEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    public $chat_message;

    /**
     * @return array|Channel|Channel[]
     */
    public function __construct($chat_message)
    {
        $this->chat_message = $chat_message;
    }
    public function broadcastOn()
    {
        return ['my-alpha'];
    }

    /**
     * @return string
     */
    public function broadcastAs()
    {
        return 'chat-event';
    }
}
