<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bets extends Model
{
    //

    protected $guarded = [];

    public function matches()
    {
        return $this->belongsTo('App\Matches', 'matches_id');
    }

    public function games()
    {
        return $this->belongsTo('App\Game', 'game_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
