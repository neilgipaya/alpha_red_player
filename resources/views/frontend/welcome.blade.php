@extends('welcome')

@section('content')
<!-- banner section begin -->
<div class="banner" id="home">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-8 col-lg-8 col-sm-8">
                <div class="banner-txt">

                    <h1>WELCOME TO Alpha PH</h1>
                    <a href="login" class="def-btn">play now</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- banner section end -->

<!-- game begin -->
<div class="games" id="games">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-6 col-lg-7 col-sm-10">
                <div class="section-title style-2">
                    <h2>Games Offered</h2>
                    <p>Checkout our games ready for you</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12">
                <div class="all-games">
                    <div class="row">
                        <div class="col-xl-4 col-lg-4 col-sm-6">
                            <div class="single-game">
                                <div class="part-img">
                                    <img src="{{ asset('frontend') }}/img/manok.png" alt="" class="img-same-size">
                                    <img class="icon-img" src="{{ asset('frontend') }}/img/dw2.png" alt="">
                                </div>
                                <div class="part-text">
                                    <h4 class="game-title">
                                        Sabong
                                    </h4>
                                    <a href="login" class="def-btn def-small">Play Now</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-sm-6">
                            <div class="single-game">
                                <div class="part-img">
                                    <img src="{{ asset('frontend') }}/img/baccarat.jpeg" alt="" class="img-same-size">
                                    <img class="icon-img" src="{{ asset('frontend') }}/img/dw2.png" alt="">
                                </div>
                                <div class="part-text">
                                    <h4 class="game-title">
                                        Baccarat
                                    </h4>
                                    <a href="login" class="def-btn def-small">Play Now</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-sm-6">
                            <div class="single-game">
                                <div class="part-img">
                                    <img src="{{ asset('frontend') }}/img/casino.jpeg" alt="" class="img-same-size">
                                    <img class="icon-img" src="{{ asset('frontend') }}/img/dw2.png" alt="">
                                </div>
                                <div class="part-text">
                                    <h4 class="game-title">
                                        Casino Games
                                    </h4>
                                    <a href="login" class="def-btn def-small">Play Now</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-sm-6">
                            <div class="single-game">
                                <div class="part-img">
                                    <img src="{{ asset('frontend') }}/img/rb.jpg" alt="" class="img-same-size">
                                    <img class="icon-img" src="{{ asset('frontend') }}/img/dw2.png" alt="">
                                </div>
                                <div class="part-text">
                                    <h4 class="game-title">
                                        Roy's Basement
                                    </h4>
                                    <a href="login" class="def-btn def-small">Play Now</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- game end -->

<!-- partner begin -->
<div class="partner">
    <div class="container">
        <div class="row justify-content-xl-between justify-content-lg-between justify-content-sm-center justify-content-md-start">
            <div class="col-xl-5 col-lg-6 col-md-9 col-sm-10 d-xl-flex d-lg-flex d-block align-items-center">
                <div class="part-text">
                    <h2 class="title">Our CSR Team</h2>
                    <p>Our CSR Team is Ready to help you with what you need all the time</p>
                </div>
            </div>
            <div class="col-xl-6 col-lg-6 col-md-12 col-sm-10">
                <div class="part-partner-logo">
                    <div class="row no-gutters">
                        <div class="col-xl-4 col-lg-4 col-sm-6 col-md-4">
                            <div class="single-logo">
                                <a href="#0">
                                    <img src="{{ asset('frontend') }}/img/manok.png" alt="">
                                </a>
                            </div>
                        </div>

                        <div class="col-xl-4 col-lg-4 col-sm-6 col-md-4">
                            <div class="single-logo">
                                <a href="#0">
                                    <img src="{{ asset('frontend') }}/img/manok.png" alt="">
                                </a>
                            </div>
                        </div>

                        <div class="col-xl-4 col-lg-4 col-sm-6 col-md-4">
                            <div class="single-logo">
                                <a href="#0">
                                    <img src="{{ asset('frontend') }}/img/partner/sponsor-3.png" alt="">
                                </a>
                            </div>
                        </div>

                        <div class="col-xl-4 col-lg-4 col-sm-6 col-md-4">
                            <div class="single-logo">
                                <a href="#0">
                                    <img src="{{ asset('frontend') }}/img/manok.png" alt="">
                                </a>
                            </div>
                        </div>

                        <div class="col-xl-4 col-lg-4 col-sm-6 col-md-4">
                            <div class="single-logo">
                                <a href="#0">
                                    <img src="{{ asset('frontend') }}/img/manok.png" alt="">
                                </a>
                            </div>
                        </div>

                        <div class="col-xl-4 col-lg-4 col-sm-6 col-md-4">
                            <div class="single-logo">
                                <a href="#0">
                                    <img src="{{ asset('frontend') }}/img/manok.png" alt="">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- partner end -->

<!-- blog begin -->
<div class="news" id="blog">
    <div class="container news-container">
        <div class="row justify-content-center">
            <div class="col-xl-6 col-lg-7 col-sm-10">
                <div class="section-title style-2">
                    <h2>Alpha Latest News & Event</h2>
                </div>
            </div>
        </div>
        <div class="row justify-content-sm-center justify-content-md-start">
            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-10">
                <div class="single-news">
                    <div class="part-img">
                        <img src="{{ asset('frontend') }}/img/manok.png" alt="">
                    </div>
                    <div class="part-text">
                        <h3 class="title">News 1</h3>
                        <p>
                            News 1
                        </p>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- blog end -->
@endsection