<?php

namespace App\Http\Controllers;

use App\Events\GameEvent;
use App\Game;
use App\GameSession;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GameSessionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\GameSession  $gameSession
     * @return \Illuminate\Http\Response
     */
    public function show(GameSession $gameSession)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\GameSession  $gameSession
     * @return \Illuminate\Http\Response
     */
    public function edit(GameSession $gameSession)
    {
        //
    }

    public function updateRecord(Request $request, $id)
    {
        DB::beginTransaction();

        try {
            //

            GameSession::find($id)->update($request->all());

//            Game::where('game_session_id', $id)->update($request->all());

            DB::commit();
            $notification = array(
                'message' => 'Game Session Updated successfully',
                'type' => 'success',
            );

            return response()->json($notification);
        } catch (\Exception $e) {

            DB::rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger',
            );

            return response()->json($notification);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\GameSession  $gameSession
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        DB::beginTransaction();

        try {
            //

            GameSession::find($id)->update($request->only('screensaver_id'));
            event(new GameEvent());

            DB::commit();
            $notification = array(
                'message' => 'Game Session Updated successfully',
                'type' => 'success',
            );

            return response()->json($notification);
        } catch (\Exception $e) {

            DB::rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger',
            );

            return response()->json($notification);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\GameSession  $gameSession
     * @return \Illuminate\Http\Response
     */
    public function destroy(GameSession $gameSession)
    {
        //
    }

    public function updateBroadcast(Request $request, $id)
    {
        DB::beginTransaction();

        try {
            //

            //broadcast 1 = true, 0 = false
            $request->merge([
                'session_live' => $request->post('broadcast')
            ]);

            GameSession::find($id)->update($request->only('session_live'));
            event(new GameEvent());

            DB::commit();
            $notification = array(
                'message' => ' successfully',
                'type' => 'success',
            );

            return response()->json($notification);
        } catch (\Exception $e) {

            DB::rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger',
            );

            return response()->json($notification);
        }
    }
}
