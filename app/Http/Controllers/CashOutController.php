<?php

namespace App\Http\Controllers;

use App\Topup;
use App\BankAccounts;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CashOutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }
    public function getListFiltered(Request $request){
        $from = $request->from;
        $to = $request->to;
        $topup = \DB::table('bank_accounts as a')
        ->select('a.account_type','c.status','c.withdrawal_amount','d.name','d.contact_number','c.created_at')
        ->leftjoin('withdrawals as c','c.source_fund_id','=','a.id')
        ->leftjoin('users as d','d.id','=','c.user_id')
        ->whereBetween('c.created_at',[$from, $to])
        ->get();
        $data = [];
        $collection = collect($topup);
        $count = 0;
        foreach($topup as $top_val){
            $check = false;
            foreach($data as $data_val){
                if($data_val['account_type'] == $top_val->account_type){
                    $check = true;
                }
            }
            if($check == false){
                $data[] = [
                    'account_type' => $top_val->account_type,
                    'total' => 0,
                    'total_no' => 0,
                    'total_declined' => 0,
                    'total_approved' => 0,
                    'ave_approval' => 0
                ];
                $par = $top_val->account_type;
                $par1 = 'Transferred';
                $filtered = $collection->filter(function ($value) use($par,$par1) {
                    return $value->account_type == $par && $value->status == $par1;
                });
                $data[$count]['users'] = $filtered;
                $count++;
            }
            if($top_val->status == 'Transferred'){
                for($i = 0 ; $i < count($data); $i++){
                    if($data[$i]['account_type'] == $top_val->account_type){
                        $data[$i]['total_approved'] +=1;
                        $data[$i]['total'] +=intval($top_val->withdrawal_amount);
                        $data[$i]['total_no'] += 1;
                        $data[$i]['ave_approval'] = $data[$i]['total_approved'] / ($data[$i]['total_approved'] + $data[$i]['total_declined']);
                    }
                }
            }else if($top_val->status == 'Declined'){
                for($i = 0 ; $i < count($data); $i++){
                    if($data[$i]['account_type'] == $top_val->account_type){
                        $data[$i]['total_declined'] +=1;
                        $data[$i]['total_no'] += 1;
                        if($data[$i]['total_approved'] <= 0){
                            $data[$i]['ave_approval'] = 0;
                        }else{
                            $data[$i]['ave_approval'] = $data[$i]['total_approved'] / ($data[$i]['total_approved'] + $data[$i]['total_declined']);
                        }
                    }
                }

            }
        }
        return response()->json($data);

    }

    public function getList()
    {
        $topup = \DB::table('bank_accounts as a')
        ->select('a.account_type','c.status','c.withdrawal_amount','d.name','d.contact_number','c.created_at')
        ->leftjoin('withdrawals as c','c.source_fund_id','=','a.id')
        ->leftjoin('users as d','d.id','=','c.user_id')
        ->whereMonth('c.created_at',Carbon::today())
        ->get();
        $data = [];
        $collection = collect($topup);
        $count = 0;
        foreach($topup as $top_val){
            $check = false;
            foreach($data as $data_val){
                if($data_val['account_type'] == $top_val->account_type){
                    $check = true;
                }
            }
            if($check == false){
                $data[] = [
                    'account_type' => $top_val->account_type,
                    'total' => 0,
                    'total_no' => 0,
                    'total_declined' => 0,
                    'total_approved' => 0,
                    'ave_approval' => 0
                ];
                $par = $top_val->account_type;
                $par1 = 'Transferred';
                $filtered = $collection->filter(function ($value) use($par,$par1) {
                    return $value->account_type == $par && $value->status == $par1;
                });
                $data[$count]['users'] = $filtered;
                $count++;
            }
            if($top_val->status == 'Transferred'){
                for($i = 0 ; $i < count($data); $i++){
                    if($data[$i]['account_type'] == $top_val->account_type){
                        $data[$i]['total_approved'] +=1;
                        $data[$i]['total'] +=intval($top_val->withdrawal_amount);
                        $data[$i]['total_no'] += 1;
                        $data[$i]['ave_approval'] = $data[$i]['total_approved'] / ($data[$i]['total_approved'] + $data[$i]['total_declined']);
                    }
                }
            }else if($top_val->status == 'Declined'){
                for($i = 0 ; $i < count($data); $i++){
                    if($data[$i]['account_type'] == $top_val->account_type){
                        $data[$i]['total_declined'] +=1;
                        $data[$i]['total_no'] += 1;
                        if($data[$i]['total_approved'] <= 0){
                            $data[$i]['ave_approval'] = 0;
                        }else{
                            $data[$i]['ave_approval'] = $data[$i]['total_approved'] / ($data[$i]['total_approved'] + $data[$i]['total_declined']);
                        }
                    }
                }

            }
        }
        return response()->json($data);
    }
    public function getCashoutChartReports(Request $request){
        $get = DB::table('bank_accounts')
        ->select('bank_accounts.*','withdrawals.withdrawal_amount','withdrawals.created_at','withdrawals.reference_number')
        ->join('withdrawals','withdrawals.source_fund_id','=','bank_accounts.id')
        ->where('withdrawals.status','Transferred')
        ->where('withdrawals.reference_number','!=','')
        ->whereMonth('withdrawals.created_at',Carbon::today())
        ->where('bank_accounts.account_type',$request->type)->get();
        return response()->json($get);
    }
    public function getCashoutChartReportsPOST(Request $request){
        $from = $request->from;
        $to = $request->to;
        $get = DB::table('bank_accounts')
        ->select('bank_accounts.*','withdrawals.withdrawal_amount','withdrawals.created_at','withdrawals.reference_number')
        ->join('withdrawals','withdrawals.source_fund_id','=','bank_accounts.id')
        ->where('withdrawals.status','Transferred')
        ->where('withdrawals.reference_number','!=','')
        ->whereBetween('withdrawals.created_at',[$from, $to])
        ->where('bank_accounts.account_type',$request->type)->get();
        return response()->json($get);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Partner  $partner
     * @return \Illuminate\Http\Response
     */
    public function show(Partner $partner)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Partner  $partner
     * @return \Illuminate\Http\Response
     */
    public function edit(Partner $partner)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Partner  $partner
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Partner  $partner
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
