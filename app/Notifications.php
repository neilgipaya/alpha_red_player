<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notifications extends Model
{
    //

    protected $fillable = [
        'notification_content', 'triggered_content_from', 'status', 'module_url', 'triggered_user_to'
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_triggered_from');
    }
}
