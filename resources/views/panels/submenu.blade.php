{{-- For submenu --}}
<ul class="menu-content">
    @foreach($menu as $submenu)
        <?php
            $submenuTranslation = "";
            if(isset($menu->i18n)){
                $submenuTranslation = $menu->i18n;
            }
        ?>
        @if($submenu->permission !== '')
            @can($submenu->permission)
                    @if($submenu->userType !== '')
                        @if($submenu->userType === auth()->user()->type || $submenu->userType2 === auth()->user()->type)
                            <li class="{{ (request()->is($submenu->url)) ? 'active' : '' }}">
                                <a href="/{{ $submenu->url }}">
                                    <i class="{{ isset($submenu->icon) ? $submenu->icon : "" }}"></i>
                                    <span class="menu-title" data-i18n="{{ $submenuTranslation }}">{{ $submenu->name }}</span>
                                </a>
                                @if (isset($submenu->submenu))
                                    @include('panels/submenu', ['menu' => $submenu->submenu])
                                @endif
                            </li>
                        @endif
                    @else
                <li class="{{ (request()->is($submenu->url)) ? 'active' : '' }}">
                    <a href="/{{ $submenu->url }}">
                        <i class="{{ isset($submenu->icon) ? $submenu->icon : "" }}"></i>
                        <span class="menu-title" data-i18n="{{ $submenuTranslation }}">{{ $submenu->name }}</span>
                    </a>
                    @if (isset($submenu->submenu))
                        @include('panels/submenu', ['menu' => $submenu->submenu])
                    @endif
                </li>
                    @endif
                @endcan
            @else
                @if($menu->userType2 === auth()->user()->type)
                <li class="{{ (request()->is($submenu->url)) ? 'active' : '' }}">
                    <a href="/{{ $submenu->url }}">
                        <i class="{{ isset($submenu->icon) ? $submenu->icon : "" }}"></i>
                        <span class="menu-title" data-i18n="{{ $submenuTranslation }}">{{ $submenu->name }}</span>
                    </a>
                    @if (isset($submenu->submenu))
                        @include('panels/submenu', ['menu' => $submenu->submenu])
                    @endif
                </li>
                @endif
                @if($menu->userType === auth()->user()->type)
                    <li class="{{ (request()->is($submenu->url)) ? 'active' : '' }}">
                        <a href="/{{ $submenu->url }}">
                            <i class="{{ isset($submenu->icon) ? $submenu->icon : "" }}"></i>
                            <span class="menu-title" data-i18n="{{ $submenuTranslation }}">{{ $submenu->name }}</span>
                        </a>
                        @if (isset($submenu->submenu))
                            @include('panels/submenu', ['menu' => $submenu->submenu])
                        @endif
                    </li>
                @endif
            @endif
    @endforeach
</ul>