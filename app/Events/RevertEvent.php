<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class RevertEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    public $message;
    /**
     * @return array|Channel|Channel[]
     */
    public function __construct($message)
    {
        $this->message = $message;
    }
    public function broadcastOn()
    {
        return ['my-alpha'];
    }

    /**
     * @return string
     */
    public function broadcastAs()
    {
        return 'revert-event';
    }
}
