<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMatchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('matches', function (Blueprint $table) {
            $table->id();
            $table->string('game_id');
            $table->boolean('left_win')->default(false);
            $table->boolean('right_win')->default(false);
            $table->decimal('left_bet')->default(0);
            $table->decimal('right_bet')->default(0);
            $table->decimal('game_dds')->default(0);
            $table->decimal('net_payout')->default(0);
            $table->string('match_status');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('matches');
    }
}
