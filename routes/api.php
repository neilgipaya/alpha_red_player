<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => '/', 'middleware' => 'auth:api'], function () {
    Route::patch('/topupplayer/approvedRequest','TopupPlayerController@approvedRequest');
    Route::patch('/topupplayer/declineTopup','TopupPlayerController@declineTopup');
    Route::get('/topupplayer/topuphistory','TopupPlayerController@topuphistory');
    Route::get('/topupplayer/myplayer','TopupPlayerController@myplayer');
    Route::resource('/topupplayer','TopupPlayerController');
    Route::post('/advancequery','AdvanceController@processQuery');
    Route::resource('/archive','ArchiveReportController');
    Route::post('/advancequeryEdited','AdvanceController@processQueryEdited');
    Route::post('/savetemplate','AdvanceController@savetemplate');
    Route::resource('/ReportTemplate','ReportTemplateController');

    Route::get('/getUsers', 'UsersController@getUsers');
    Route::get('/getPlayers', 'UsersController@getPlayers');
    Route::get('/getAgents', 'UsersController@getAgents');
    Route::post('/createUsers', 'UsersController@store');
    Route::patch('/updateUsers/{id}', 'UsersController@update');
    Route::delete('/deleteUsers/{id}', 'UsersController@destroy');
    Route::get('/getDeactivatePlayers','UsersController@getDeactivatePlayers');
    Route::post('/restoreAccount','UsersController@restoreAccount');
    Route::get('/getCommission','UsersController@getCommission');

    Route::get('/getPaymentMethods', 'PaymentDetailsController@getList');
    Route::post('/createPaymentMethods', 'PaymentDetailsController@store');
    Route::patch('/updatePaymentMethods/{id}', 'PaymentDetailsController@update');
    Route::delete('/deletePaymentMethods/{id}', 'PaymentDetailsController@destroy');

    Route::get('/getPaymentMethodType/{type}', 'PaymentDetailsController@getByType');

    Route::get('/getBankAccounts', 'BankAccountsController@getList');
    Route::get('/getBankerAccounts', 'BankAccountsController@getBankerList');
    Route::get('/getBankTransactions/{id}', 'BankAccountsController@getBankTransactions');
    Route::post('/createBankAccount', 'BankAccountsController@store');
    Route::patch('/updateBankAccount/{id}', 'BankAccountsController@update');
    Route::delete('/deleteBankAccount/{id}', 'BankAccountsController@destroy');

    Route::post('/createTopup', 'TopupController@deposit');
    Route::post('/createTopupOther', 'TopupController@store');
    Route::get('/getRecentTopup', 'TopupController@getRecentTopup');
    Route::get('/getTopupList', 'TopupController@getTopupList');
    Route::get('/getPersonalTopup', 'TopupController@getPersonalTopup');
    Route::patch('/updateTopup/{id}', 'TopupController@update');
    Route::patch('/declineTopup/{id}', 'TopupController@decline');
    Route::patch('/approveBankerTopup/{id}','TopupController@approvedBanker');
    Route::get('/BankerTopupRequest','TopupController@getBankerTopupList');
    Route::get('/BankerTopupRequestHistory','TopupController@getBankerTopupHistoryList');
    Route::post('/declineBankerTopup','TopupController@declineBankerTopup');


    Route::get('/getDeclineTemplate', 'DeclineTemplateController@getList');
    Route::post('/createDeclineTemplate', 'DeclineTemplateController@store');
    Route::patch('/updateDeclineTemplate/{id}', 'DeclineTemplateController@update');
    Route::delete('/deleteDeclineTemplate/{id}', 'DeclineTemplateController@destroy');

    Route::get('/getRoles', 'RolesController@getList');
    Route::get('/getRolesUser', 'RolesController@getRolesUser');
    Route::post('/createRoles', 'RolesController@store');
    Route::patch('/updateRoles/{id}', 'RolesController@update');
    Route::delete('/deleteRoles/{id}', 'RolesController@destroy');

    Route::post('/createFundTransfer', 'FundTransferController@store');
    Route::patch('/updateFundTransfer/{id}', 'FundTransferController@update');
    Route::patch('/declineFundTransfer/{id}', 'FundTransferController@decline');
    Route::get('/getPendingFunds', 'FundTransferController@getPending');
    Route::get('/getFunds', 'FundTransferController@getList');

    Route::patch('/openArena','GameController@openArena');
    Route::get('/getGameHistory', 'GameController@getList');
    // Route::get('/gamehistory/{id}', 'GameController@gamehistory');
    Route::get('/checkSession', 'GameController@checkSession');
    Route::get('/getWalletBalance', 'GameController@getWalletBalance');
    Route::post('/createGame', 'GameController@store');
    Route::post('/newGame', 'GameController@newGame');
    Route::patch('/updateGame/{id}', 'GameController@update');
    Route::patch('/updateGameSession/{id}', 'GameSessionController@update');
    Route::get('/gameCount', 'GameController@count');
    Route::get('/currentGameSession', 'GameController@currentGameSession');
    Route::post('/send_message', 'GameController@send_message');
    Route::get('/get_message', 'GameController@get_message');
    Route::patch('/matchWinner/{id}', 'GameController@matchWinner');
    Route::patch('/RedeclareMatchWinner/{id}','GameController@RedeclareMatchWinner');
    Route::get('/GetPreviousMatch/{game_session_id}', 'GameController@GetPreviousMatch');
    Route::patch('/processRevertDeclaration','GameController@processRevertDeclaration');
    Route::get('/getPlayerBet/{id}', 'GameController@getPlayerBet');
    Route::post('/placeBet', 'GameController@placeBet');
    Route::get('/getTrends', 'GameController@getTrends');
    Route::get('/getWins', 'GameController@getWins');
    Route::get('/getSessionHistory', 'GameController@getSessionHistory');
    Route::post('/getSessionHistoryFiltered', 'GameController@getSessionHistoryFiltered');
    Route::get('/getStreak', 'GameController@getStreak');
    Route::patch('/cancelGame/{game_id}', 'GameController@cancelGame');
    Route::get('/getmyagents','UsersController@getmyagents');
    Route::post('/myAgentsByType','UsersController@myAgentsByType');


    Route::get('/getWithdrawalTemplate', 'WithdrawalController@getWithdrawalTemplate');
    Route::post('/createWithdrawalTemplate', 'WithdrawalController@createWithdrawalTemplate');
    Route::post('/createWithdrawal', 'WithdrawalController@store');
    Route::post('/playerWithdrawal','WithdrawalController@playerWithdrawal');
    Route::post('/createWithdrawalAgent', 'WithdrawalController@storeAgent');
    Route::get('/getRecentWithdrawal', 'WithdrawalController@getRecent');
    Route::get('/getRecentWithdrawalAgents', 'WithdrawalController@getRecentAgents');
    Route::get('/getWithdrawalLogs', 'WithdrawalController@getLogs');
    Route::get('/getWithdrawalLogsAgents', 'WithdrawalController@getLogsAgents');
    Route::patch('/markpaid/{id}','WithdrawalController@markpaid');
    Route::patch('updateWithdrawal/{id}', 'WithdrawalController@update');
    Route::patch('updateWithdrawalAgents/{id}', 'WithdrawalController@updateWithdrawalAgents');
    Route::patch('declineWithdrawal/{id}', 'WithdrawalController@decline');
    Route::patch('declineWithdrawalAgent/{id}', 'WithdrawalController@declineWithdrawalAgent');
    Route::patch('updateWithTemplate/{id}','WithdrawalController@updateWithTemplate');
    Route::delete('deleteWithTemplate/{id}','WithdrawalController@deleteWithTemplate');

    Route::patch('/activateBanker/{id}','BankerController@update');
    Route::get('/banker_details/{id}','BankerController@details');
    Route::post('/BankerTopup','BankerController@topup');
    Route::get('/banker_game_details/{game_id}','BankerController@game_details');

    Route::post('/createPartner', 'PartnerController@store');
    Route::get('/getPartnerList', 'PartnerController@getList');
    Route::get('/getPartnerTotal', 'PartnerController@getPartnerTotal');
    Route::post('/getPartnerListFiltered', 'PartnerController@getPartnerListFiltered');
    Route::get('/getCashinReports','CashInController@getList');
    Route::post('/getCashinChartPOST/{type}','CashInController@getCashinChartReportsPOST');
    Route::get('/getUserReports/{id}','UsersController@getUserReports');
    Route::post('/getUserReportsFiltered','UsersController@getUserReportsFiltered');
    Route::patch('/DemoteToPlayer','UsersController@DemoteToPlayer');
    Route::post('/getListFiltered','CashInController@getListFiltered');
    Route::get('/getCashOutReports','CashOutController@getList');
    Route::post('/getCashOutChartPOST/{type}','CashOutController@getCashoutChartReportsPOST');
    Route::post('/getCashOutReportsFiltered','CashOutController@getListFiltered');
    Route::get('/getFundTransferReports','FundTransferController@getReport');
    Route::post('/getFundTransferReportsFiltered','FundTransferController@getReportFiltered');
    Route::get('/getFundsAccountsReports','FundsAccountController@getReport');
    Route::post('/getFundsAccountsReportsFiltered','FundsAccountController@getFundsAccountsReportsFiltered');
    Route::get('/getBetting','BetsController@getReport');
    Route::get('/getCashOutChart/{type}','CashOutController@getCashoutChartReports');
    Route::get('/getFundTransferChart/{type}','FundTransferController@getFundTransferChartReports');
    Route::post('/getFundTransferChartPOST/{type}','FundTransferController@getFundTransferChartReportsPOST');
    Route::get('/getCashinChart/{type}','CashInController@getCashinChartReports');
    Route::patch('/updatePartner/{id}', 'PartnerController@update');
    Route::delete('/deletePartner/{id}', 'PartnerController@destroy');
    Route::get('/getPartnerHistory','PartnerController@getPartnerHistory');

    Route::get('/getNotification', 'NotificationsController@getList');
    Route::get('/getNotificationUser', 'NotificationsController@getListUser');
    Route::post('/markAllRead', 'NotificationsController@markAllRead');
    Route::get('/getGameInfo/{game_id}','GameController@getGameInfo');
    Route::patch('/endDay', 'GameController@endDay');
    Route::get('/getMyWin', 'GameController@myWin');
    Route::get('/getScreenSaver/{game_session_id}', 'GameController@getScreenSaver');
    Route::post('/uploadScreenSaver', 'GameController@uploadScreenSaver');
    Route::patch('/updateBroadcast/{id}', 'GameSessionController@updateBroadcast');
    Route::get('/getAllPaymentMethods', 'PaymentDetailsController@getAllList');
    Route::get('/getPaymentMethodByType/{type}', 'PaymentDetailsController@getPaymentMethodByType');
    Route::post('/transferCommission','UsersController@transferCommission');

    Route::get('/view-betting/{game_id}','GameController@view_betting_data');
    Route::post('/makeAnnouncement', 'AnnouncementsController@store');
    Route::post('/removeAnnouncement','AnnouncementsController@remove');
    Route::get('/getAnnouncement', 'AnnouncementsController@getAnnouncement');
    Route::get('/activateBankAccount/{id}', 'BankAccountsController@activateBankAccount');
    Route::get('/getAgentPlayers/{id}', 'UsersController@getAgentPlayers');
    Route::get('/getMyPlayers', 'UsersController@getMyPlayers');
    Route::get('/getMyInactivePlayers', 'UsersController@getMyInactivePlayers');
    Route::get('/getDownlineAgents', 'UsersController@getDownlineAgents');
    Route::get('/getOtherUsers', 'UsersController@getOtherUsers');
    Route::patch('/promoteUser/{id}', 'UsersController@promoteUser');
    Route::get('getTotalRake', 'GameController@getTotalRake');
    Route::get('getLogs', 'UserLogsController@getLogs');
    Route::get('/getBets', 'GameController@getBets');
    Route::get('/getPlayerBets', 'GameController@getPlayerBets');
    Route::get('/getOnlinePlayers', 'UsersController@getOnlinePlayers');
    Route::get('/getBetsByPlayer/{id}', 'GameController@getBetsByPlayer');

    Route::post('/filterBets', 'GameController@getRakeFiltered');
    Route::get('/agentBypassLimit/{id}', 'UsersController@agentBypassLimit');
    Route::get('/getWalletHistory/{id}', 'UsersController@getWalletHistory');
    Route::get('/getAgentCommission', 'UsersController@getAgentCommission');

    Route::post('/getPlayerByType', 'UsersController@getAgentByType');
    Route::post('/getSelectedAgent', 'UsersController@getSelectedAgent');
    Route::patch('/transferUser/{id}', 'UsersController@transferUser');

    Route::get('/getAllPlayers', 'UsersController@getAllPlayers');
    Route::post('/transferWallet', 'UsersController@transferWallet');

    Route::get('/getGameList', 'GameController@getGameList');
    Route::patch('/updateGameSession/{id}', 'GameSessionController@updateRecord');

    //dashboard chart
    Route::get('/fetchCardDashboard','DashboardController@fetchCardDashboard');
    Route::get('/fetchplaystats','DashboardController@fetchplaystats');
    Route::get('/agentDownline','DashboardController@agentDownline');
    Route::get('/getmywithdrawal','WithdrawalController@getmywithdrawal');
    Route::get('/getmywithdrawalAgent','WithdrawalController@getmywithdrawalAgent');
    Route::post('/cancel_mycashout','WithdrawalController@cancel_mycashout');
});
