<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Game extends Model
{
    //

    protected $table = "games";

    protected $fillable = [
        'game_number', 'game_title', 'camera_configuration_id', 'status', 'created_by', 'game_session_id','redeclared'
    ];

    public function session()
    {
        return $this->belongsTo('App\GameSession', 'game_session_id');
    }

    public function matches()
    {
        return $this->hasOne('App\Matches', 'game_id');
    }

    public function bets()
    {
        return $this->hasMany('App\Bets', 'game_id');
    }


}
