@extends('layouts/contentLayoutMaster')


@section('breadcrumb')
    <div class="clearfix"></div>
@endsection
@section('content')
    <view-agent-downline :data="{{$data}}"></view-agent-downline>
@endsection

@section('vendor-scripts')

@endsection
