<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

class OTPController extends Controller
{

    /*
    * var client
    */
    private $client;

    /**
     * OTPController constructor.
     * @param $message
     * @param $contact_number
     */
    public function __construct()
    {


        $this->client = new Client([
            'headers'=>[
                'Content-Type' => 'application/json'
            ]
        ]);

    }

    //

    public function sendOTP($message, $address)
    {

        try {

            $url = "https://devapi.globelabs.com.ph/smsmessaging/v1/outbound/21586776/requests";

            $request = $this->client->post(
                $url,
                [
                    'verify' => false,
                    'json' => [
                        'app_id' => env('GLOBE_APP_ID'),
                        'app_secret' => env('GLOBE_APP_SECRET'),
                        'passphrase' => env('GLOBE_PASSPRASE'),
                        'message' => $message,
                        'address' => $address
                    ]
                ]
            );

            return $request;

        } catch (\Exception $e){
            return $e;
        }
    }
}
