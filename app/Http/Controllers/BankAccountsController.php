<?php

namespace App\Http\Controllers;

use App\BankAccounts;
use App\BankTransactions;
use App\UserLogs;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BankAccountsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!auth()->user()->can('fund-accounts')){
            abort(403, 'Unauthorized');
        }
        return view('Configuration.Banks.bank-accounts');
    }


    public function getBankTransactions($id)
    {
        $transactions = BankTransactions::where('bank_id', $id)->latest()->get();

        return response()->json($transactions);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        DB::beginTransaction();

        try {
            //

            $bank = BankAccounts::create($request->all());
            BankTransactions::create([
               'bank_id' =>  $bank->id,
                'debit' => 0,
                'credit' => $bank->balance ?? 0,
                'transaction_code' => 'BT-'.$this->generateRandomString(10),
                'transaction_details' => auth()->user()->name.'('.auth()->user()->id.') Created Fund accounts with amount of '.$bank->balance ?? 0
            ]);
            UserLogs::create([
                'user_id' => \auth()->user()->id,
                'type' => 'Funds',
                'content' => "Fund Account Created Successfully"
            ]);
            DB::commit();
            $notification = array(
                'message' => 'Fund Account Created Successfully',
                'type' => 'success',
            );

            return response()->json($notification);
        } catch (\Exception $e) {

            DB::rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger',
            );

            return response()->json($notification);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BankAccounts  $bankAccounts
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return view('Configuration.Banks.bank-transactions');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BankAccounts  $bankAccounts
     * @return \Illuminate\Http\Response
     */
    public function edit(BankAccounts $bankAccounts)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BankAccounts  $bankAccounts
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        //

        DB::beginTransaction();

        try {
            //

            BankAccounts::find($id)->update($request->all());
            UserLogs::create([
                'user_id' => \auth()->user()->id,
                'type' => 'Funds',
                'content' => "Fund Account Updated Successfully"
            ]);
            DB::commit();
            $notification = array(
                'message' => 'Fund Accounts Updated Successfully',
                'type' => 'success',
            );

            return response()->json($notification);
        } catch (\Exception $e) {

            DB::rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger',
            );

            return response()->json($notification);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BankAccounts  $bankAccounts
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        DB::beginTransaction();

        try {
            //

            BankAccounts::destroy($id);
            UserLogs::create([
                'user_id' => \auth()->user()->id,
                'type' => 'Funds',
                'content' => "Fund Account Deleted Successfully"
            ]);
            DB::commit();
            $notification = array(
                'message' => 'Fund Accounts Deleted Successfully',
                'type' => 'warning',
            );

            return response()->json($notification);
        } catch (\Exception $e) {

            DB::rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger',
            );

            return response()->json($notification);
        }
    }

    public function getList()
    {
        $banks = BankAccounts::where('category','!=','Banker')->latest()->get();

        return response()->json($banks, 200);
    }
    public function getBankerList(){
        $banks = BankAccounts::where('category','Banker')->latest()->get();
        return response()->json($banks, 200);
    }

    public function activateBankAccount($id)
    {
        $bank = BankAccounts::find($id);

        if ($bank->status === 1){
            $bank->update([
                'status' => 0
            ]);
        } else {
            $bank->update([
                'status' => 1
            ]);
        }
    }

    private function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}
