@extends('layouts/contentLayoutMaster')


@section('breadcrumb')
    <div class="clearfix"></div>
@endsection
@section('content')
    @if(auth()->user()->type == 'Master Agent' || auth()->user()->type == 'Gold Agent' || auth()->user()->type == 'Silver Agent')
    <top-up-list-agent></top-up-list-agent>
    @elseif(auth()->user()->type == 'Official')
    <top-up-list></top-up-list>
    @endif
@endsection

@section('vendor-scripts')

@endsection
