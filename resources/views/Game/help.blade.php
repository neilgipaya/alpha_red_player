@extends('layouts/contentLayoutMaster')


@section('breadcrumb')
    <div class="clearfix"></div>
@endsection
@section('content')
   <div class="row">
       <div class="col-md-12">
           <div class="card">
               <div class="card-header">
                   <h4 class="card-title">Game Instructions</h4>
               </div>
               <div class="card-body">
                   <div class="card-content">
                       <ol>
                           <li>
                               <p>Game credit is required to get access to the live stream.</p>
                           </li>
                           <li>
                               <p>When credit falls  to 0, player is given 2 minutes to replenish credits, otherwise access to live stream is disabled.</p>
                           </li>
                           <li>
                               <p>Minimum bet amount is 50.</p>
                           </li>
                           <li>
                               <p>Open Betting is 01 Minute and 30 Seconds.</p>
                           </li>
                           <li>
                               <p>Last Call is 30 Seconds.</p>
                           </li>
                           <li>
                               <p>The Game employs sports betting payout method.</p>
                           </li>
                           <li>
                               <p>Odds are computed based on each sides total bet amount.</p>
                           </li>
                           <li>
                               <p>Game commission is 5.5%.</p>
                           </li>
                           <li>
                               <p>If game is cancelled, current bets are returned to player's wallet.</p>
                           </li>
                       </ol>
                   </div>
               </div>
           </div>
       </div>
   </div>

{{--   <div class="row">--}}
{{--       <div class="col-md-12">--}}
{{--           <div class="card">--}}
{{--               <div class="card-header">--}}
{{--                   <h4 class="card-title">Cash-in Request Policy</h4>--}}
{{--               </div>--}}
{{--               <div class="card-body">--}}
{{--                   <div class="card-content">--}}
{{--                       <ol>--}}
{{--                           <li>--}}
{{--                               <p>Receiver accounts are posted depending on the chosen payment methods. some payment option will take upto 1 day before it will be posted to your account.</p>--}}
{{--                           </li>--}}
{{--                           <li>--}}
{{--                               <p>If the chosen receiver account has disappeared from current list, the account will still be available in the drop down list of the form for another 5 minutes.</p>--}}

{{--                           </li>--}}
{{--                       </ol>--}}
{{--                   </div>--}}
{{--               </div>--}}
{{--           </div>--}}
{{--       </div>--}}
{{--   </div>--}}

   <div class="row">
       <div class="col-md-12">
           <div class="card">
               <div class="card-header">
                   <h4 class="card-title">Cash-in Request Guide</h4>
               </div>
               <div class="card-body">
                   <div class="card-content">
                       <ol>
                           <li>
                               <p>Select Cash-in Method.</p>
                           </li>
                           <li>
                               <p>Select Game Account as Receiver.</p>
                           </li>
                           <li>
                               <p>Fill in transaction details, selected Game Account and insert attachment. Add comment as needed.</p>
                           </li>
                           <li>
                               <p>Submit Request.</p>
                           </li>
                       </ol>

                       <h4>Important Note</h4>
                       <ol>
                           <li>Money remittance Cash-In method will require a minimum 1 day processing.</li>
                       </ol>
                   </div>
               </div>
           </div>
       </div>
   </div>

   <div class="row">
       <div class="col-md-12">
           <div class="card">
               <div class="card-header">
                   <h4 class="card-title">Cash-out Request Policy</h4>
               </div>
               <div class="card-body">
                   <div class="card-content">
                       <ol>
                           <li>
                               <p>Fund withdrawal requests are processed at a minimum of 1 banking day.</p>
                           </li>
                           <li>
                               <p>Transaction and bank fees will be shouldered by user.</p>

                           </li>
                       </ol>
                   </div>
               </div>
           </div>
       </div>
   </div>
@endsection

@section('vendor-scripts')

@endsection
