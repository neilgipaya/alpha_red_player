<?php

namespace App\Http\Controllers;

use App\BankAccounts;
use App\PaymentDetails;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PaymentDetailsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        //
        return view('Configuration.PaymentMethods.index');
    }

    public function getList()
    {

        $bankAccounts = BankAccounts::with('bank_transactions')
            ->where('account_type', '!=', 'Bank')
            ->where('status', '1')
            ->latest()->get();

        return response()->json($bankAccounts, 200);
    }

    public function getPaymentMethodByType($type)
    {
        $bankAccounts = BankAccounts::with('bank_transactions')
            ->where('account_type', '=', $type)
            ->where('status', '1')
            ->latest()->get();

        return response()->json($bankAccounts, 200);
    }

    public function getAllList()
    {
        $bankAccounts = BankAccounts::with('bank_transactions')
            ->where('status', '1')
            ->latest()->get();

        return response()->json($bankAccounts, 200);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        DB::beginTransaction();

        try {
            //

            PaymentDetails::create($request->all());

            DB::commit();
            $notification = array(
                'message' => 'Payment Details Created successfully',
                'type' => 'success',
            );

            return response()->json($notification);
        } catch (\Exception $e) {

            DB::rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger',
            );

            return response()->json($notification);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PaymentDetails  $paymentDetails
     * @return \Illuminate\Http\Response
     */
    public function show(PaymentDetails $paymentDetails)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PaymentDetails  $paymentDetails
     * @return \Illuminate\Http\Response
     */
    public function edit(PaymentDetails $paymentDetails)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PaymentDetails  $paymentDetails
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        DB::beginTransaction();

        try {
            //

            PaymentDetails::find($id)->update($request->all());

            DB::commit();
            $notification = array(
                'message' => 'Payment Details Updated successfully',
                'type' => 'success',
            );

            return response()->json($notification);
        } catch (\Exception $e) {

            DB::rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger',
            );

            return response()->json($notification);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PaymentDetails  $paymentDetails
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        DB::beginTransaction();

        try {
            //

            PaymentDetails::destroy($id);

            DB::commit();
            $notification = array(
                'message' => 'Payment Details Deleted Successfully',
                'type' => 'success',
            );

            return response()->json($notification);
        } catch (\Exception $e) {

            DB::rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger',
            );

            return response()->json($notification);
        }
    }

    public function getByType($type)
    {

        if ($type === 'Silver Agent'){
            $category = "Agent Account";
        } else {
            $category = "Game Account";
        }

        if (auth()->user()->type == 'Silver Agent'){

            if ($type === 'Silver Agent'){
                $paymentMethods = BankAccounts::where('category', $category)
                    ->where('account_type', 'Bank')
                    ->where('status', '1')
                    ->where('user_id', auth()->user()->id)
                    ->inRandomOrder()->get();
            } else{
                $paymentMethods = BankAccounts::where('category', $category)
                    ->where('account_type', $type)
                    ->where('status', '1')
                    ->inRandomOrder()->get();
            }


        } else {
            if ($type === 'Silver Agent'){
                $type = 'Bank';
            }
            $paymentMethods = BankAccounts::where('account_type', $type)
                ->where('status', '1')
                ->where('category', $category)
                ->inRandomOrder()->get();
        }

        return response()->json($paymentMethods, 200);
    }
}
