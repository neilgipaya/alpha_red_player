<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Http\Requests\UserRequest;
use App\User;
use App\UserLogs;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AuthenticationController extends Controller
{
    //
    /**
     * @var OTPController
     */
    private $OTPController;

    /**
     * AuthenticationController constructor.
     * @param OTPController $OTPController
     */
    public function __construct(OTPController $OTPController)
    {
        $this->OTPController = $OTPController;
    }

    public function authenticate(Request $request)
    {
        $credentials = $request->only('contact_number', 'password');

        if (Auth::attempt($credentials, ($request->post('remember') == 'on') ? true : false) || Auth::viaRemember()){

//            if (\auth()->user()->status === 'Inactive'){
//            		  Auth::logout();
//            		$notification = [
//             		'message' => 'We are currently under going maintenance',
//             		'type' => 'info'
//         			];
//            		  return redirect('/')->with($notification);
//                $otp = rand (10000 , 99999);

//                User::find(\auth()->user()->id)->update([
//                   'otp' => $otp
//                ]);

//                $message = "Your SWL OTP is: " . $otp;

//                $this->OTPController->sendOTP($message, \auth()->user()->contact_number);

//                return redirect('otp');
//            }

            UserLogs::create([
               'user_id' => \auth()->user()->id,
               'type' => 'auth',
               'content' => 'User Successfully Login'
            ]);

            if (\auth()->user()->type !== 'Official'){
                return redirect('/game/play-game');
            }

            return redirect('dashboard');
        }

        $notification = [
            'message' => 'Invalid Contact Number / Password',
            'type' => 'danger'
        ];


        return redirect()->back()->with($notification);
    }

    public function register(Request $request)
    {

        DB::beginTransaction();

        try {


            $request->validate([
                'contact_number' => 'required|unique:users',
            ]);

            /*
             * @method Generate new OTP
             */

            $otp = rand (10000 , 99999);

            $request->merge([
                'otp' => $otp
            ]);

//            $message = "Your SWL OTP is: " . $otp;
//
//            $this->OTPController->sendOTP($message, $request->post('contact_number'));

            $user = User::create($request->all());

            $user->syncRoles(1);


            DB::commit();
            $notification = array(
                'message' => 'Register successfully',
                'type' => 'success',
            );

            Auth::loginUsingId($user->id);

            return redirect('dashboard');

        } catch (\Exception $e) {

            DB::rollback();
            $notification = array(
                'message' => "Mobile Number Already Exists",
                'type' => 'danger',
            );

            return redirect()->back()->with($notification);
        }
    }

    public function otp()
    {
        return view('otp');
    }

    public function verify(Request $request)
    {
        if ($request->post('otp') === \auth()->user()->otp){
            User::find(\auth()->user()->id)->update([
                'status' => 'Active'
            ]);

            return redirect('dashboard');
        } else {
            $notification = [
                'message' => 'Invalid OTP',
                'type' => 'danger'
            ];

            return redirect()->back()->with($notification);
        }
    }
}
