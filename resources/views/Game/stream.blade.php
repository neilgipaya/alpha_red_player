<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <script src="https://kit.fontawesome.com/93084fae9a.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
 
    <title>Hello, world!</title>
    <style>
    body { margin: 0;}
        .muted_btn{
            position:absolute;
            left:15px;
            top:15px;
        }
        .show{
            display:block;
        }
        .none{
            display:none;
        }
    </style>
</head>
<body>
<script
    src="https://code.jquery.com/jquery-3.5.1.min.js"
    integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
    crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/hls.js@latest"></script>
<script src="https://cdn.jsdelivr.net/npm/hls.js@alpha"></script>
<!-- <iframe height="300px" src="//www.streamers.ninja/client/source/72?auth=auto&secret=d4wmnan0e6vdmh50dqq50vi38vwq6k8h&secureToken=i3krc8ambkhep79scdo06ahi5bqp13yi&player=html5&theme=six&volume=1&aspectRatio=169&controls=true" width="100%" allowfullscreen="true" id="wgjc2" name="ninjastreamers_player" border="0" scrolling="no" frameborder="0" marginheight="0" marginwidth="0" style="position:absolute !important;top: 0 !important; left: 0 !important;width: 100% !important;height: 100% !important;"></iframe> -->
<video class="video-js vjs-default-skin" id="video" autoplay playsinline muted width="100%"  height="100%">
    <source id="vidsource" src="/stream/cam.m3u8" type="application/x-mpegURL">
</video>
 <button id="mute" class="muted_btn show" type="button"><i class="fas fa-volume-mute"></i></button>
<script>
    
$(document).ready(function(){
    var muted = true;
    var video = document.getElementById('video');
    var videoSrc = '/stream/cam.m3u8';
    if (Hls.isSupported()) {
        var hls = new Hls();
        hls.loadSource(videoSrc);
        hls.attachMedia(video);
        hls.on(Hls.Events.MANIFEST_PARSED, function() {
            //   video.play();
        });
    }
    else if (video.canPlayType('application/vnd.apple.mpegurl')) {
        video.src = videoSrc;
        video.addEventListener('loadedmetadata', function() {
            //   video.play();
        });
    }
    $('video').click(function(){
        $('.muted_btn').removeClass('none');
        $('.muted_btn').addClass('show');
        transit();
    })
    transit();
    function transit(){
        setTimeout(function(){
            $('.muted_btn').removeClass('show');
            $('.muted_btn').addClass('none');
        },3000);
    }
    document.getElementById('video').addEventListener('loadedmetadata', function(e) {
        this.currentTime = video.duration - 1;

    }, false);
    function iOSversion() {
      if (/iP(hone|od|ad)/.test(navigator.platform)) {
        // supports iOS 2.0 and later: <http://bit.ly/TJjs1V>
        var v = (navigator.appVersion).match(/OS (\d+)_(\d+)_?(\d+)?/);
        return [parseInt(v[1], 10), parseInt(v[2], 10), parseInt(v[3] || 0, 10)];
      }
    }
    
    ver = iOSversion();
    if(ver == undefined){
        video.ontimeupdate = function(){
            var get = video.duration - video.currentTime;
            console.log(get)
            
            if(get > 2){
                var back = video.playbackRate = 1.1;
                console.log(back)
                
                // video.currentTime = video.duration - 1;
            }else{
                var next = video.playbackRate = 1;
                console.log(next)
            }
        }
    }


    $('#mute').on('click',function(){
        if(video.muted){
            video.muted = false;
            $('#mute').html('<i class="fas fa-volume-up"></i>');
        }else{
            video.muted = true;
            $('#mute').html('<i class="fas fa-volume-mute"></i>');
        }

    })

})
</script>
</body>
</html>

