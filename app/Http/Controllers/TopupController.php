<<<<<<< HEAD
<?php

namespace App\Http\Controllers;

use App\BankAccounts;
use App\BankTransactions;
use App\Events\NotificationEvent;
use App\Topup;
use App\User;
use App\UserLogs;
use App\WalletHistory;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;

class TopupController extends Controller
{
    /**

     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if(!auth()->user()->can('top-up-recent')){
            abort(403, 'Unauthorized');
        }
        return view('Topup.topup-recent');
    }

    public function logs()
    {
        if(!auth()->user()->can('top-up-history')){
            abort(403, 'Unauthorized');
        }
        return view('Topup.topup-list');
    }
    public function banker(){
        if(!auth()->user()->can('banker-topup-approval')){
            abort(403,'Unauthorized');
        }
        return view('Topup.banker-topup-approval');
    }

    public function myTopup()
    {
        return view('Topup.topup-personal');
    }

    public function getPersonalTopup()
    {
        $topup = Topup::with('users')->with('payments')->where('user_id', auth()->user()->id)->latest()->get();

        return response()->json($topup);
    }
    public function request()
    {
        return view('Topup.topup-request');
    }
    public function getBankerTopupList(){
        $data = \App\BankerTopup::with('banker')->with('users')->where('status','Pending')->get();
        return response()->json($data);
    }
    public function getBankerTopupHistoryList(){
        $data = \App\BankerTopup::with('banker')->with('processed_by')->get();
        return response()->json($data);
    }
    public function approvedBanker($id){
        if(!auth()->user()->can('banker-topup-approval')){
            abort(403);
        }
        \DB::beginTransaction();
        try{
            $data = \App\BankerTopup::where('id',$id)->first();
            if($data->status != 'Pending'){
                $m = array(
                    'message' => 'This request has been processed already',
                    'type' => 'danger'
                );
                return response()->json($m);
            }
            $data->update(['status' => 'Approved','processed_by' => auth()->user()->id]);
            $update = \App\BankAccounts::find($data->banker_id);
            $update->increment('balance',$data->amount);
            if(!$update){
                $m = array(
                    'message' => 'Update has not been executed missing Banker ID',
                    'type' => 'warning'
                );
                return response()->json($m);
            }
            \App\UserLogs::create([
                'user_id' => auth()->user()->id,
                'type' => 'Banker Topup',
                'content' => 'Banker Topup has been approved Track Banker Top Up ID:('.$data->id.') with Amount:('.$data->amount.')'
            ]);
            \DB::commit();
            $m = array(
                'message' => 'Request has been approved successfully',
                'type' => 'success'
            );
            return response()->json($m);

        }catch(\Exception $e){
            \DB::rollback();
            $m = array(
                'message' => $e->getMessage(),
                'type' => 'danger'
            );
            return response()->json($m);
        }
    }
    public function declineBankerTopup(Request $request){
        \DB::beginTransaction();
        try{
            $data = \App\BankerTopup::where('status','Pending')->where('id',$request->id)->first();
            if(!$data){
                $m = array('message' => 'This request doesn`t exist or been processed already','type' => 'danger');
                return response()->json($m);
            }
            $decline_reason = ($request->description == 'Other')?$request->decline_reason:$request->description;
            $data->update(['status' => 'Declined','decline_reason' => $decline_reason]);
            \App\UserLogs::create([
                'user_id' => auth()->user()->id,
                'type' => 'banker Topup',
                'content' => 'Banker Topup ID:('.$request->id.') has been declined by CSR with Reason: '.$decline_reason
            ]);
            \DB::commit();
            $m = array(
                'message' => 'Banker Request has been declined',
                'type' => 'success'
            );
            return response()->json($m);

        }catch(\Exception $e){
            \DB::rollback();
            $m = array(
                'message' => $e->getMessage(),
                'type' => 'danger'
            );
            return response()->json($m);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        define('UPLOAD_DIR', 'images/cashin/');
        DB::beginTransaction();

        try {



//            if ($request->post('type') === 'Silver Agent'){
//                $request->merge([
//                    'status' => 'Approved',
//                    'transaction_code' => "CI-".$this->generateRandomString(20)
//                ]);
//                $request->merge([
//                    'wallet_balance' => $request->post('amount')
//                ]);
//
//                $user = User::where('id', $request->post('user_id'));
//                $user->increment('wallet_balance', $request->post('amount'));
//
//                UserLogs::create([
//                    'user_id' => \auth()->user()->id,
//                    'type' => 'auth',
//                    'content' => "Fund Transfer by $user->name has been approved automatically"
//                ]);
//
//                BankAccounts::find($request->post('payment_id'))->increment('balance', $request->post('amount'));
//
//                BankTransactions::create(
//                    [
//                        'bank_id' => $request->post('payment_id'),
//                        'credit' => $request->post('amount'),
//                        'debit' => 0,
//                        'transaction_code' => $request->post('transaction_code')
//                    ]
//                );
//
//                $message = "Your Cash-in has been approved";
//
//                event(new NotificationEvent($message, auth()->user()->id, '/topup', $request->post('user_id')));
//
//                Topup::create($request->except('type', 'wallet_balance','filename','file'));
//            }
//            else {
                if(isset($_FILES['file']['type'])){
                    if($_FILES['file']['type'] != 'image/jpeg' && $_FILES['file']['type'] != 'image/png'){
                        $notification = array(
                            'message' => 'Invalid Upload File type',
                            'type' => 'danger',
                        );
                        return response()->json($notification);
                    }
                }

                UserLogs::create([
                    'user_id' => \auth()->user()->id,
                    'type' => 'Topup',
                    'content' => "Top-up for other player has been requested"
                ]);

                Topup::create($request->except('filename', 'file','type'));

//            }

            DB::commit();
            if(isset($_FILES['file']['type'])){
                $upload_path = UPLOAD_DIR . $request->filename;
                if(move_uploaded_file($_FILES['file']['tmp_name'], $upload_path)){
                    $notification = array(
                        'message' => 'Cash-in Submitted Successfully',
                        'type' => 'success',
                    );
                }else{
                    $notification = array(
                        'message' => 'your file did not save successfully',
                        'type' => 'danger',
                    );
                    DB::rollback();
                }
                return response()->json($notification);

            }
            $notification = array(
                'message' => 'Cash-in Submitted Successfully',
                'type' => 'success',
            );


            return response()->json($notification);
        } catch (\Exception $e) {

            DB::rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger',
            );

            return response()->json($notification);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        DB::beginTransaction();

        try {
            //
            $request->merge([
                'status' => 'Approved',
                'approved_by' => auth()->user()->id
            ]);


            $request->merge([
                'wallet_balance' => $request->post('amount')
            ]);


            $topup = Topup::find($id);
            if($topup->status != 'Pending'){
                $notification = array(
                    'message' => 'This request is has been processed already',
                    'type' => 'danger',
                );
                return response()->json($notification);
            }
            $user = User::find($request->post('user_id'));
            $prev = $user->wallet_balance;
            $user->increment('wallet_balance', $request->post('wallet_balance'));

            BankAccounts::where('id', $request->post('payment_id'))->increment('balance', $request->post('amount'));

            BankTransactions::create(
                [
                    'bank_id' => $request->post('payment_id'),
                    'credit' => $request->post('amount'),
                    'debit' => 0,
                    'transaction_code' => $topup->transaction_code,
                    'transaction_details' => 'Request cash in of '.$request->sender_name.'('.$request->id.') 
                    has been approved by'.auth()->user()->name 
                ]
            );

            UserLogs::create([
                'user_id' => \auth()->user()->id,
                'type' => 'Topup',
                'content' => "Top-up $topup->transaction_code has been approved with amount of ".$request->post('amount')." Bef: ".$prev." Aft: ".$user->wallet_balance
            ]);
            WalletHistory::create([
                'user_id' => $request->post('user_id'),
                'amount' => $request->post('amount'),
                'type' => 'Topup request approved by '.auth()->user()->name.'('.auth()->user()->id.')'
            ]);

            $topup->update($request->only('amount', 'status', 'approved_by'));

            $message = "Your Cash-in has been approved";

            event(new NotificationEvent($message, auth()->user()->id, '/topup', $request->post('user_id')));


            DB::commit();
            $notification = array(
                'message' => 'Cash-in Processed successfully',
                'type' => 'success',
            );

            return response()->json($notification);
        } catch (\Exception $e) {

            DB::rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger',
            );

            return response()->json($notification);
        }
    }

    public function decline(Request $request, $id)
    {
        //
        DB::beginTransaction();

        try {
            //
            $request->merge([
                'status' => 'Declined',
                'approved_by' => auth()->user()->id
            ]);

            $topup = Topup::find($id);
            $topup->update($request->only('amount', 'status', 'approved_by', 'decline_reason', 'decline_template_id'));

            UserLogs::create([
                'user_id' => \auth()->user()->id,
                'type' => 'Topup',
                'content' => "Top-up $topup->transaction_code has been declined"
            ]);
            WalletHistory::create([
                'user_id' => $topup->user_id,
                'amount' => $topup->amount,
                'type' => 'Topup request declined by '.auth()->user()->name.'('.auth()->user()->id.') :transaction '.$topup->transaction_code
            ]);
            DB::commit();
            $notification = array(
                'message' => 'Topup Declined Successfully',
                'type' => 'warning',
            );

            return response()->json($notification);
        } catch (\Exception $e) {

            DB::rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger',
            );

            return response()->json($notification);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function deposit(Request $request)
    {
        define('UPLOAD_DIR', 'images/cashin/');
        DB::beginTransaction();
        
        try {
            $validator = Topup::where('reference_number',$request->post('reference_number'))->get();
            if(count($validator) > 0){
                $notification = array(
                    'message' => 'Reference number is already exist in database. please wait until further notice.',
                    'type' => 'danger',
                );
                return response()->json($notification);
            }

            if($_FILES['file']['type'] != 'image/jpeg' && $_FILES['file']['type'] != 'image/png'){
                $notification = array(
                    'message' => 'Invalid Upload File type',
                    'type' => 'danger',
                );
                return response()->json($notification);
            }
        	if($_FILES['file']['size'] > 2000000){
                $notification = array(
                    'message' => 'Your Image is too large',
                    'type' => 'danger',
                );
                return response()->json($notification);

            }
            $upload_path = UPLOAD_DIR . $request->filename;
            if(move_uploaded_file($_FILES['file']['tmp_name'], $upload_path)){
                $notification = array(
                    'message' => 'Cash-in Submitted Successfully',
                    'type' => 'success',
                );
                $request->merge([
                    'user_id' => auth()->user()->id,
                    'transaction_code' => "CI-".$this->generateRandomString(20)
                ]);
    
                $message = "You have a New Cash-in Request";
                event(new NotificationEvent($message, auth()->user()->id, '/topup', ''));
                WalletHistory::create([
                    'user_id' => \auth()->user()->id,
                    'amount' => $request->post('amount'),
                    'type' => 'Request for Cash In '.$_FILES['file']['size']
                ]);
                UserLogs::create([
                    'user_id' => \auth()->user()->id,
                    'type' => 'Topup',
                    'content' => "Top-up has been requested"
                ]);
    
    
                Topup::create($request->except('filename', 'file'));
    
                DB::commit();
            }else{
                $notification = array(
                    'message' => 'your file did not save successfully',
                    'type' => 'daner',
                );
                DB::rollback();
            }
            return response()->json($notification);


        } catch (\Exception $e) {

            DB::rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger',
            );

            return response()->json($notification);
        }
    }

    public function getRecentTopup()
    {
        $topup = Topup::with('users')->with('payments')->where('status', 'Pending')->latest()->get();

        return response()->json($topup);
    }

    public function getTopupList()
    {
        $topup = Topup::with('users')->with('approved_by')->with('payments')->with('decline_template')->latest()->get();

        return response()->json($topup);
    }

    public function other()
    {
        return view('Topup.topup-others');
    }

    private function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}
=======
<?php

namespace App\Http\Controllers;

use App\BankAccounts;
use App\BankTransactions;
use App\Events\NotificationEvent;
use App\Topup;
use App\User;
use App\UserLogs;
use App\WalletHistory;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;

class TopupController extends Controller
{
    /**

     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if(!auth()->user()->can('top-up-recent')){
            abort(403, 'Unauthorized');
        }
        return view('Topup.topup-recent');
    }

    public function logs()
    {
        if(!auth()->user()->can('top-up-history')){
            abort(403, 'Unauthorized');
        }
        return view('Topup.topup-list');
    }
    public function banker(){
        if(!auth()->user()->can('banker-topup-approval')){
            abort(403,'Unauthorized');
        }
        return view('Topup.banker-topup-approval');
    }

    public function myTopup()
    {
        return view('Topup.topup-personal');
    }

    public function getPersonalTopup()
    {
        $topup = Topup::with('users')->with('payments')->where('user_id', auth()->user()->id)->latest()->get();

        return response()->json($topup);
    }
    public function request()
    {
        if(!auth()->user()->can('top-up-request')){
            abort(403,'Unauthorized');
        }
        return view('Topup.topup-request');
    }
    public function getBankerTopupList(){
        $data = \App\BankerTopup::with('banker')->with('users')->where('status','Pending')->get();
        return response()->json($data);
    }
    public function getBankerTopupHistoryList(){
        $data = \App\BankerTopup::with('banker')->with('processed_by')->get();
        return response()->json($data);
    }
    public function approvedBanker($id){
        if(!auth()->user()->can('banker-topup-approval')){
            abort(403);
        }
        \DB::beginTransaction();
        try{
            $data = \App\BankerTopup::where('id',$id)->first();
            if($data->status != 'Pending'){
                $m = array(
                    'message' => 'This request has been processed already',
                    'type' => 'danger'
                );
                return response()->json($m);
            }
            $data->update(['status' => 'Approved','processed_by' => auth()->user()->id]);
            $update = \App\BankAccounts::find($data->banker_id);
            $update->increment('balance',$data->amount);
            if(!$update){
                $m = array(
                    'message' => 'Update has not been executed missing Banker ID',
                    'type' => 'warning'
                );
                return response()->json($m);
            }
            \App\UserLogs::create([
                'user_id' => auth()->user()->id,
                'type' => 'Banker Topup',
                'content' => 'Banker Topup has been approved Track Banker Top Up ID:('.$data->id.') with Amount:('.$data->amount.')'
            ]);
            \DB::commit();
            $m = array(
                'message' => 'Request has been approved successfully',
                'type' => 'success'
            );
            return response()->json($m);

        }catch(\Exception $e){
            \DB::rollback();
            $m = array(
                'message' => $e->getMessage(),
                'type' => 'danger'
            );
            return response()->json($m);
        }
    }
    public function declineBankerTopup(Request $request){
        \DB::beginTransaction();
        try{
            $data = \App\BankerTopup::where('status','Pending')->where('id',$request->id)->first();
            if(!$data){
                $m = array('message' => 'This request doesn`t exist or been processed already','type' => 'danger');
                return response()->json($m);
            }
            $decline_reason = ($request->description == 'Other')?$request->decline_reason:$request->description;
            $data->update(['status' => 'Declined','decline_reason' => $decline_reason]);
            \App\UserLogs::create([
                'user_id' => auth()->user()->id,
                'type' => 'banker Topup',
                'content' => 'Banker Topup ID:('.$request->id.') has been declined by CSR with Reason: '.$decline_reason
            ]);
            \DB::commit();
            $m = array(
                'message' => 'Banker Request has been declined',
                'type' => 'success'
            );
            return response()->json($m);

        }catch(\Exception $e){
            \DB::rollback();
            $m = array(
                'message' => $e->getMessage(),
                'type' => 'danger'
            );
            return response()->json($m);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        define('UPLOAD_DIR', 'images/cashin/');
        DB::beginTransaction();

        try {



//            if ($request->post('type') === 'Silver Agent'){
//                $request->merge([
//                    'status' => 'Approved',
//                    'transaction_code' => "CI-".$this->generateRandomString(20)
//                ]);
//                $request->merge([
//                    'wallet_balance' => $request->post('amount')
//                ]);
//
//                $user = User::where('id', $request->post('user_id'));
//                $user->increment('wallet_balance', $request->post('amount'));
//
//                UserLogs::create([
//                    'user_id' => \auth()->user()->id,
//                    'type' => 'auth',
//                    'content' => "Fund Transfer by $user->name has been approved automatically"
//                ]);
//
//                BankAccounts::find($request->post('payment_id'))->increment('balance', $request->post('amount'));
//
//                BankTransactions::create(
//                    [
//                        'bank_id' => $request->post('payment_id'),
//                        'credit' => $request->post('amount'),
//                        'debit' => 0,
//                        'transaction_code' => $request->post('transaction_code')
//                    ]
//                );
//
//                $message = "Your Cash-in has been approved";
//
//                event(new NotificationEvent($message, auth()->user()->id, '/topup', $request->post('user_id')));
//
//                Topup::create($request->except('type', 'wallet_balance','filename','file'));
//            }
//            else {
                if(isset($_FILES['file']['type'])){
                    if($_FILES['file']['type'] != 'image/jpeg' && $_FILES['file']['type'] != 'image/png'){
                        $notification = array(
                            'message' => 'Invalid Upload File type',
                            'type' => 'danger',
                        );
                        return response()->json($notification);
                    }
                }

                UserLogs::create([
                    'user_id' => \auth()->user()->id,
                    'type' => 'Topup',
                    'content' => "Top-up for other player has been requested"
                ]);

                Topup::create($request->except('filename', 'file','type'));

//            }

            DB::commit();
            if(isset($_FILES['file']['type'])){
                $upload_path = UPLOAD_DIR . $request->filename;
                if(move_uploaded_file($_FILES['file']['tmp_name'], $upload_path)){
                    $notification = array(
                        'message' => 'Cash-in Submitted Successfully',
                        'type' => 'success',
                    );
                }else{
                    $notification = array(
                        'message' => 'your file did not save successfully',
                        'type' => 'danger',
                    );
                    DB::rollback();
                }
                return response()->json($notification);

            }
            $notification = array(
                'message' => 'Cash-in Submitted Successfully',
                'type' => 'success',
            );


            return response()->json($notification);
        } catch (\Exception $e) {

            DB::rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger',
            );

            return response()->json($notification);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        DB::beginTransaction();

        try {
            //
            $request->merge([
                'status' => 'Approved',
                'approved_by' => auth()->user()->id
            ]);


            $request->merge([
                'wallet_balance' => $request->post('amount')
            ]);


            $topup = Topup::find($id);
            if($topup->status != 'Pending'){
                $notification = array(
                    'message' => 'This request is has been processed already',
                    'type' => 'danger',
                );
                return response()->json($notification);
            }
            $user = User::find($request->post('user_id'));
            $prev = $user->wallet_balance;
            $user->increment('wallet_balance', $request->post('wallet_balance'));

            BankAccounts::where('id', $request->post('payment_id'))->increment('balance', $request->post('amount'));

            BankTransactions::create(
                [
                    'bank_id' => $request->post('payment_id'),
                    'credit' => $request->post('amount'),
                    'debit' => 0,
                    'transaction_code' => $topup->transaction_code,
                    'transaction_details' => 'Request cash in of '.$request->sender_name.'('.$request->id.') 
                    has been approved by'.auth()->user()->name 
                ]
            );

            UserLogs::create([
                'user_id' => \auth()->user()->id,
                'type' => 'Topup',
                'content' => "Top-up $topup->transaction_code has been approved with amount of ".$request->post('amount')." Bef: ".$prev." Aft: ".$user->wallet_balance
            ]);
            WalletHistory::create([
                'user_id' => $request->post('user_id'),
                'amount' => $request->post('amount'),
                'type' => 'Topup request approved by '.auth()->user()->name.'('.auth()->user()->id.')'
            ]);

            $topup->update($request->only('amount', 'status', 'approved_by'));

            $message = "Your Cash-in has been approved";

            event(new NotificationEvent($message, auth()->user()->id, '/topup', $request->post('user_id')));


            DB::commit();
            $notification = array(
                'message' => 'Cash-in Processed successfully',
                'type' => 'success',
            );

            return response()->json($notification);
        } catch (\Exception $e) {

            DB::rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger',
            );

            return response()->json($notification);
        }
    }

    public function decline(Request $request, $id)
    {
        //
        DB::beginTransaction();

        try {
            //
            $request->merge([
                'status' => 'Declined',
                'approved_by' => auth()->user()->id
            ]);

            $topup = Topup::find($id);
            $topup->update($request->only('amount', 'status', 'approved_by', 'decline_reason', 'decline_template_id'));

            UserLogs::create([
                'user_id' => \auth()->user()->id,
                'type' => 'Topup',
                'content' => "Top-up $topup->transaction_code has been declined"
            ]);
            WalletHistory::create([
                'user_id' => $topup->user_id,
                'amount' => $topup->amount,
                'type' => 'Topup request declined by '.auth()->user()->name.'('.auth()->user()->id.') :transaction '.$topup->transaction_code
            ]);
            DB::commit();
            $notification = array(
                'message' => 'Topup Declined Successfully',
                'type' => 'warning',
            );

            return response()->json($notification);
        } catch (\Exception $e) {

            DB::rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger',
            );

            return response()->json($notification);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function deposit(Request $request)
    {
        define('UPLOAD_DIR', 'images/cashin/');
        DB::beginTransaction();
        
        try {
            $validator = Topup::where('reference_number',$request->post('reference_number'))->get();
            if(count($validator) > 0){
                $notification = array(
                    'message' => 'Reference number is already exist in database. please wait until further notice.',
                    'type' => 'danger',
                );
                return response()->json($notification);
            }

            if($_FILES['file']['type'] != 'image/jpeg' && $_FILES['file']['type'] != 'image/png'){
                $notification = array(
                    'message' => 'Invalid Upload File type',
                    'type' => 'danger',
                );
                return response()->json($notification);
            }
        	if($_FILES['file']['size'] > 2000000){
                $notification = array(
                    'message' => 'Your Image is too large',
                    'type' => 'danger',
                );
                return response()->json($notification);

            }
            $upload_path = UPLOAD_DIR . $request->filename;
            if(move_uploaded_file($_FILES['file']['tmp_name'], $upload_path)){
                $notification = array(
                    'message' => 'Cash-in Submitted Successfully',
                    'type' => 'success',
                );
                $request->merge([
                    'user_id' => auth()->user()->id,
                    'transaction_code' => "CI-".$this->generateRandomString(20)
                ]);
    
                $message = "You have a New Cash-in Request";
                event(new NotificationEvent($message, auth()->user()->id, '/topup', ''));
                WalletHistory::create([
                    'user_id' => \auth()->user()->id,
                    'amount' => $request->post('amount'),
                    'type' => 'Request for Cash In '.$_FILES['file']['size']
                ]);
                UserLogs::create([
                    'user_id' => \auth()->user()->id,
                    'type' => 'Topup',
                    'content' => "Top-up has been requested"
                ]);
    
    
                Topup::create($request->except('filename', 'file'));
    
                DB::commit();
            }else{
                $notification = array(
                    'message' => 'your file did not save successfully',
                    'type' => 'daner',
                );
                DB::rollback();
            }
            return response()->json($notification);


        } catch (\Exception $e) {

            DB::rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger',
            );

            return response()->json($notification);
        }
    }

    public function getRecentTopup()
    {
        $topup = Topup::with('users')->with('payments')->where('status', 'Pending')->latest()->get();

        return response()->json($topup);
    }

    public function getTopupList()
    {
        $topup = Topup::with('users')->with('approved_by')->with('payments')->with('decline_template')->latest()->get();

        return response()->json($topup);
    }

    public function other()
    {
        return view('Topup.topup-others');
    }

    private function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}
>>>>>>> ae5b3d3ce322a6f5ff74d79fe1c0fb883afacf4c
