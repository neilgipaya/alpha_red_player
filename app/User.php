<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable, HasRoles, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'username', 'password', 'otp', 'contact_number', 'wallet_balance', 'type', 'role_id', 'agent_id', 'agent_rate','fb_url','email',
        'agent_commission_balance', 'status', 'agent_limit','verified_at','reset_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = bcrypt($password);
    }

    public function topups()
    {
        return $this->hasMany('App\Topup', 'user_id', 'id');
    }

    public function bets()
    {
        return $this->hasMany('App\Bets', 'user_id', 'id');
    }
    public function comm(){
        return $this->hasMany('App\User', 'agent_id', 'id');
    }

    public function agent()
    {
        return $this->belongsTo('App\User', 'agent_id');
    }

    public function session()
    {
        return $this->hasOne('App\UserSession', 'user_id');
    }
    public function logs()
    {
        return $this->hasMany('App\UserLogs','user_id');
    }
    public static function whereInMultiple(array $columns, $values)
    {
        $values = array_map(function (array $value) {
            return "('".implode($value, "', '")."')"; 
        }, $values);

        return static::query()->whereRaw(
            '('.implode($columns, ', ').') in ('.implode($values, ', ').')'
        );
    }
}
