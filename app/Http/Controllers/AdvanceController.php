<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\ReportTemplate;
use App\UserLogs;
use Carbon\Carbon;
use App\Whitelist;
class AdvanceController extends Controller
{
    public function processQuery(Request $request){
        $result = \DB::table($request->primary_module['table']);
        $calculation = \DB::table($request->primary_module['table']);
        $select = null;
        $show = false;
        foreach($request->selected_column as $column){
            $result->addSelect($column);
        }
        foreach($request->related_module as $related_table){
            $result->leftjoin($related_table['table'],$related_table['related_key'],'=',$related_table['primary_key']);
        }
        foreach($request->calculations['sum'] as $val){
            $calculation->addSelect(\DB::raw("SUM({$val['column']}) as 'Sum_$val[text]'"));
            $show = true;
        }
        foreach($request->calculations['ave'] as $val){
            $calculation->addSelect(\DB::raw("AVG({$val['column']}) as 'Avg_$val[text]'"));
            $show = true;
        }
        foreach($request->calculations['min'] as $val){
            $calculation->addSelect(\DB::raw("MIN({$val['column']}) as 'Min_$val[text]'"));
            $show = true;
        }
        foreach($request->calculations['max'] as $val){
            $calculation->addSelect(\DB::raw("MAX({$val['column']}) as 'Max_$val[text]'"));
            $show = true;
        }
        foreach($request->group_by as $val){
            if($val['column']){
                $string = $val['column'];
                $split = explode('.',$string);
                if($split[1] == 'created_at'){
                    $result->addSelect(\DB::raw('DATE_FORMAT('.$val['column'].',"%Y-%m-%d") as Date'));
                    $result->groupBy('Date');
                }else{
                    $result->groupBy($val['column']);
                }
                if($val['sort_order']){
                    $result->orderBy($val['column'],substr($val['sort_order'], 1));
                }
            }
        }
        foreach($request->condition as $val){
            if($val['column']){
                if($val['operator']){
                    if($val['operator'] == 'Equals') $result->where($val['column'],'=',$val['value']);
                    if($val['operator'] == 'Not Equal To') $result->where($val['column'],'!=',$val['value']);
                    if($val['operator'] == 'Starts With') $result->where($val['column'],'LIKE',$val['value'].'%');
                    if($val['operator'] == 'Ends With') $result->where($val['column'],'LIKE','%'.$val['value']);
                    if($val['operator'] == 'Contains') $result->where($val['column'],'LIKE','%'.$val['value'].'%');
                    if($val['operator'] == 'Does Not Contains') $result->where($val['column'],'NOT LIKE','%'.$val['value'].'%');
                    if($val['operator'] == 'Is Empty') $result->whereNull($val['column']);
                    if($val['operator'] == 'Between') $result->whereBetween($val['column'],array($val['between1'],$val['between2']));
                    if($val['operator'] == 'Not Between') $result->whereNotBetween($val['column'],array($val['between1'],$val['between2']));
                    if($val['operator'] == 'Is Not Empty') $result->whereNotNull($val['column']);
                    if($val['operator'] == 'Greater Than') $result->where($val['column'],'>',$val['value']);
                    if($val['operator'] == 'Less Than') $result->where($val['column'],'<',$val['value']);
                    if($val['operator'] == 'Greater Than Equal To') $result->where($val['column'],'>=',$val['value']);
                    if($val['operator'] == 'Less Than Equal To') $result->where($val['column'],'<=',$val['value']);
                }
            }
        }
        $data['result'] = $result->get();
        $data['calculations'] = ($show)?$calculation->get():[];
        return response()->json($data);
    }
    public function processQueryEdited(Request $request){
        $result = \DB::table($request->new_primary_module['table']);
        $calculation = \DB::table($request->new_primary_module['table']);
        $select = null;
        $show = false;
        foreach($request->selected_column as $column){
            $result->addSelect($column);
        }
        foreach($request->related_column_reform as $related_table){
            $result->leftjoin($related_table['table'],$related_table['related_key'],'=',$related_table['primary_key']);
        }
        foreach($request->calculations['sum'] as $val){
            $calculation->addSelect(\DB::raw("SUM({$val['column']}) as 'Sum_$val[text]'"));
            $show = true;
        }
        foreach($request->calculations['ave'] as $val){
            $calculation->addSelect(\DB::raw("AVG({$val['column']}) as 'Avg_$val[text]'"));
            $show = true;
        }
        foreach($request->calculations['min'] as $val){
            $calculation->addSelect(\DB::raw("MIN({$val['column']}) as 'Min_$val[text]'"));
            $show = true;
        }
        foreach($request->calculations['max'] as $val){
            $calculation->addSelect(\DB::raw("MAX({$val['column']}) as 'Max_$val[text]'"));
            $show = true;
        }
        foreach($request->group_by as $val){
            if($val['column']){
                $string = $val['column'];
                $split = explode('.',$string);
                if($split[1] == 'created_at'){
                    $result->addSelect(\DB::raw('DATE_FORMAT('.$val['column'].',"%Y-%m-%d") as Date'));
                    $result->groupBy('Date');
                }else{
                    $result->groupBy($val['column']);
                }
                if($val['sort_order']){
                    $result->orderBy($val['column'],substr($val['sort_order'], 1));
                }
            }
        }
        foreach($request->condition as $val){
            if($val['column']){
                if($val['operator']){
                    if($val['operator'] == 'Equals') $result->where($val['column'],'=',$val['value']);
                    if($val['operator'] == 'Not Equal To') $result->where($val['column'],'!=',$val['value']);
                    if($val['operator'] == 'Starts With') $result->where($val['column'],'LIKE',$val['value'].'%');
                    if($val['operator'] == 'Ends With') $result->where($val['column'],'LIKE','%'.$val['value']);
                    if($val['operator'] == 'Contains') $result->where($val['column'],'LIKE','%'.$val['value'].'%');
                    if($val['operator'] == 'Does Not Contains') $result->where($val['column'],'NOT LIKE','%'.$val['value'].'%');
                    if($val['operator'] == 'Is Empty') $result->whereNull($val['column']);
                    if($val['operator'] == 'Between') $result->whereBetween($val['column'],array($val['between1'],$val['between2']));
                    if($val['operator'] == 'Not Between') $result->whereNotBetween($val['column'],array($val['between1'],$val['between2']));
                    if($val['operator'] == 'Is Not Empty') $result->whereNotNull($val['column']);
                    if($val['operator'] == 'Greater Than') $result->where($val['column'],'>',$val['value']);
                    if($val['operator'] == 'Less Than') $result->where($val['column'],'<',$val['value']);
                    if($val['operator'] == 'Greater Than Equal To') $result->where($val['column'],'>=',$val['value']);
                    if($val['operator'] == 'Less Than Equal To') $result->where($val['column'],'<=',$val['value']);
                }
            }
        }
        $data['result'] = $result->get();
        $data['calculations'] = ($show)?$calculation->get():[];
        return response()->json($data);

    }
    public function savetemplate(Request $request){
        \DB::begintransaction();
        try{
            ReportTemplate::create([
                'report_name' => $request->report_name,
                'report_description' => $request->report_description,
                'report_data' => json_encode($request->all())
            ]);
            \DB::commit();
            $notification = array(
                'message' => 'Report Template has been saved successfully',
                'type' => 'success',
            );
            return response()->json($notification);

        }catch(\Exception $e){
            \DB::rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger',
            );
            return response()->json($notification);
        }
    }
}
