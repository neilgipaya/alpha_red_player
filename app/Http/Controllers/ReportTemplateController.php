<?php

namespace App\Http\Controllers;

use App\ReportTemplate;
use Illuminate\Http\Request;
class ReportTemplateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = ReportTemplate::all();
        return response()->json($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return response()->json(['data' => 'create']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return response()->json(['data' => 'store']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ReportTemplate  $reportTemplate
     * @return \Illuminate\Http\Response
     */
    public function show(ReportTemplate $reportTemplate,$id)
    {
        return $reportTemplate::where('id',$id)->first();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ReportTemplate  $reportTemplate
     * @return \Illuminate\Http\Response
     */
    public function edit(ReportTemplate $reportTemplate)
    {
        return response()->json(['data' => 'edit']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ReportTemplate  $reportTemplate
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ReportTemplate $reportTemplate)
    {
        \DB::beginTransaction();
        try{
            $data = $reportTemplate->first();
            $data->update($request->all());
            \DB::commit();
            $notification = array(
                'message' => 'Changes has been saved successfully !',
                'type' => 'success'
            );
            return response()->json($notification);
        }catch(\Exception $e){
            // \DB::rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger'
            );
            return response()->json($notification);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ReportTemplate  $reportTemplate
     * @return \Illuminate\Http\Response
     */
    public function destroy(ReportTemplate $reportTemplate,$id)
    {
        \DB::beginTransaction();
        try{
            $report = ReportTemplate::find($id);
            \App\UserLogs::create([
                'user_id' => auth()->user()->id,
                'type' => 'Report',
                'content' => 'Deleted Report Template Report Name: '.$report->report_name
            ]);
            $reportTemplate::where('id',$id)->delete();
            \DB::commit();
            $notification = array(
                'message' => 'Deleting Template successfully',
                'type' => 'success'
            );
            return response()->json($notification);
        }catch(\Exception $e){
            \DB::rollback();
            $notification = array(
                'message' => $e->getMessage(),
                'type' => 'danger'
            );
            return response()->json($notification);
        }
    }
}
